﻿namespace Binaree.Game.Utility
{
    using System.Collections.Generic;
    using System;
    using UnityEngine.UI;
    using UnityEngine;
    using Core;

    /// <summary>
    /// Handles unity functionality for handling images and sprites
    /// Used for swapping resource art
    /// </summary>
    public class Spritey
    {
        public static void ReplaceIcon(Image sourceIcon, Image copyIcon)
        {
            sourceIcon.sprite = copyIcon.GetComponent<SpriteRenderer>().sprite;
            sourceIcon.color = copyIcon.GetComponent<SpriteRenderer>().color;
        }

        public static void ReplaceIcon(Image sourceIcon, string path)
        {
            GameObject iconObj = Resources.Load(path) as GameObject;
            if (iconObj == null) { Say.Warn("Unable to find resource " + path); return; }
            sourceIcon.sprite = iconObj.GetComponent<SpriteRenderer>().sprite;
            sourceIcon.color = iconObj.GetComponent<SpriteRenderer>().color;
        }

        public static void ReplaceIcon(Image sourceIcon, string dir, string resourceId)
        {
            ReplaceIcon(sourceIcon, dir + resourceId);
        }

        public static void ReplaceSprite(SpriteRenderer sourceSprite, string dir, string resourceId)
        {
            GameObject iconObj = Resources.Load(dir + resourceId) as GameObject;
            if (iconObj == null) { Say.Warn("Unable to find resource " + dir + resourceId); return; }
            sourceSprite.sprite = iconObj.GetComponent<SpriteRenderer>().sprite;
            sourceSprite.color = iconObj.GetComponent<SpriteRenderer>().color;
        }
    }
}
