namespace Binaree.Game.Utility
{
    using Core;
    using UnityEngine;

    /// <summary>
    /// Collection of helper functions for better handling input
    /// </summary>
    public class Inputy
    {
        public static Vector3 InputPosition()
        {
            //Say.Log("Mouse position is " + Input.mousePosition.x + " , " + Input.mousePosition.y);
            return Input.mousePosition;
        }

        public static Vector3 WorldPosition()
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(InputPosition());
            pos.y = 0.0f;
            return pos;
        }

        public static float DistToPoint(Vector3 pos)
        {
            Vector3 inPos = InputPosition();
            // first, let's remove an z component to ensure a "2D" calculation
            inPos.z = pos.z = 0.0f;

            return Vector3.Distance(inPos, pos);
        }
    }
}