﻿namespace Binaree.Game.Utility
{
    using System;
    
    public class SplitTrig
    {
        public string Value { get; private set; }
        public float Chance { get; private set; }

        public SplitTrig(string val)
        {
            string[] split = val.Split(':');
            if (split.Length == 1)
                Value = split[0];
            else
            {
                Value = split[0];
                Chance = Convert.ToSingle(split[1]);
            }
        }

        public bool CheckHigh()
        {
            return Randomize.Value() >= Chance;
        }

        public bool CheckLow()
        {
            return Randomize.Value() <= Chance;
        }
    }
}
