namespace Binaree.Game.Core
{
    using System.Collections;
    using System.Xml;
    using Component;
    using Table;
    using Ui;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using User;
    using Utility;
    using System.Collections.Generic;

    /// <summary>
    /// Functions as the project root
    /// Responsible for loading all game, factory, and user data
    /// Handles the movement between game scenes
    /// </summary>
    public class TheCreator : MonoBehaviour
    {
        public const int MAIN_SCENE_NUM = 1;

        public string UserId = "User"; // the id to use when referencing the user
        public string UserDir = ""; // the directory to output user data to; if empty, defaults to the ApplicationPath/UserId
        public string UserStartDir = ""; // the directory to input user data from; if empty, defaults to the UserDir

        public List<GameFactory> Factory; // Stores a list of factories used to create game objects

        // Xml text assets which store all the data used for the game
        public TextAsset AttractantXml;
        public TextAsset CollectionXml;
        public TextAsset CreatureXml;
        public TextAsset CrystalXml;
        public TextAsset GateXml;
        public TextAsset GuideXml;
        public TextAsset HitXml;
        public TextAsset LeagueXml;
        public TextAsset LineXml;
        public TextAsset LootBoxXml;
        public TextAsset MoveXml;
        public TextAsset PetXml;
        public TextAsset PortalXml;
        public TextAsset ProbeXml;
        public TextAsset PromotionXml;
        public TextAsset PurchaseXml;
        public TextAsset RemoteXml;
        public TextAsset RewardXml;
        public TextAsset RocketXml;
        public TextAsset SatelliteXml;
        public TextAsset ScriptXml;
        public TextAsset StatXml;
        public TextAsset SurfaceXml;
        public TextAsset TableXml;
        public TextAsset TankXml;

        private AsyncOperation asyncLoading = null;

        private static TheCreator _instance = null;

        public static TheCreator Man { get { return _instance; } }

        void Awake()
        {
            _instance = this;
            DontDestroyOnLoad(this);
            Say.Log("The Creator is Awake");

            // Required for loading sub-data
            Creature.Man.Load(AssetToXml(CreatureXml));
            Surface.Man.Load(AssetToXml(SurfaceXml));
            Table.Man.Load(AssetToXml(TableXml));

            // Standard loading
            Attractant.Man.Load(AssetToXml(AttractantXml));
            Collection.Man.Load(AssetToXml(CollectionXml));
            Crystal.Man.Load(AssetToXml(CrystalXml));
            Gate.Man.Load(AssetToXml(GateXml));
            Guide.Man.Load(AssetToXml(GuideXml));
            Hit.Man.Load(AssetToXml(HitXml));
            League.Man.Load(AssetToXml(LeagueXml));
            Line.Man.Load(AssetToXml(LineXml));
            Loot.Man.Load(AssetToXml(LootBoxXml));
            Move.Man.Load(AssetToXml(MoveXml));
            Pet.Man.Load(AssetToXml(PetXml));
            Portal.Man.Load(AssetToXml(PortalXml));
            Probe.Man.Load(AssetToXml(ProbeXml));
            Promotion.Man.Load(AssetToXml(PromotionXml));
            Purchase.Man.Load(AssetToXml(PurchaseXml));
            Remote.Man.Load(AssetToXml(RemoteXml));
            Reward.Man.Load(AssetToXml(RewardXml));
            Rocket.Man.Load(AssetToXml(RocketXml));
            Satellite.Man.Load(AssetToXml(SatelliteXml));
            Script.Man.Load(AssetToXml(ScriptXml));
            Stat.Man.Load(AssetToXml(StatXml));
            Tank.Man.Load(AssetToXml(TankXml));
        }

        void Start()
        {
            // Hopefully all required data is loaded before this executes?
            User.Graph.Generate(UserId, UserDir, UserStartDir);
            UserLoaded();
        }

        void Update()
        {
            //if (asyncLoading != null)
            //    LoadingScreen.Inst.Progress(asyncLoading.progress);
        }

        private void UserLoaded()
        {
            Say.Log("User has been loaded, proceeding to generate all Factories");
            for (int i = 0; i < Factory.Count; ++i)
                Factory[i].Generate();

            Say.Log("All loading should now be complete. Launching into game...");
            LaunchMainMenu();
        }

        IEnumerator UpdatedLoad(int lvlNum)
        {
            //LoadingScreen.Inst.ShowLoad(LoadingScreen.Style.Progress);
            asyncLoading = SceneManager.LoadSceneAsync(lvlNum);

            while (!asyncLoading.isDone)
            {
                //LoadingScreen.Inst.Progress(asyncLoading.progress);
                yield return null;
            }

            asyncLoading = null;
        }

        public void LaunchMainMenu()
        {
            Say.Log("Launching into Main Menu. Standby.");
            StartCoroutine(UpdatedLoad(MAIN_SCENE_NUM));
        }

        public static XmlDocument AssetToXml(TextAsset ta)
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(ta.ToString());
            return xml;
        }
    }
}
