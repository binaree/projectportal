namespace Binaree.Game.Core
{
    using System;
    using User;

    /// <summary>
    /// Handles the controlling, adjusting, and other dynamic aspects of a game component
    /// refNum must be unique for the type of blob being tracked
    /// </summary>
    public class GameBlob
    {
        public int RefNum { get; private set; }

        virtual public void LoadFromUser(UserData userData)
        {
            RefNum = RefFromCompoundId(userData.UserId());
        }

        // Only do this if you know what you are doing!
        public void SetDirectRef(int refVal) { RefNum = refVal; }

        virtual public void GenerateNew(GameData gameData)
        {
            RefNum = Unique.Ref.Value();
        }

        public static string IdFromCompoundId(string compoundId)
        {
            return compoundId.Split(':')[0];
        }

        public static int RefFromCompoundId(string compoundId)
        {
            return Convert.ToInt32(compoundId.Split(':')[1]);
        }

        public static string BaseFromId(string dataId, string dataHeader)
        {
            return dataId.Replace(dataHeader, "");
        }

        public static string CompoundId(string dataId, int refNum)
        {
            return dataId + ":" + refNum;
        }
    }
}