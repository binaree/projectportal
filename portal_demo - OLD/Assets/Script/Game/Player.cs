namespace Binaree.Game.Component
{
    using System;
    using Core;
    using Table;
    using User;
    using Ui;

    /// <summary>
    /// Tracks information relating to the player properties
    /// </summary>
    public class Player
    {
        public string Name { get; private set; }
        public int Level { get { return (int)level; } }

        private UserPlayer userData = null;
        private float level = 0.0f;

        private static Player _instance = new Player();

        public static Player Man { get { return _instance; } }

        public Player()
        {
            Name = "Default";
        }

        public void Sync(UserPlayer theUserData)
        {
            Name = theUserData.Name;
            level = theUserData.Level;
            userData = theUserData;
        }

        public int MaxLevel() { return Table.Man.Player.MaxLevel; }
    }
}
