namespace Binaree.Game.User
{
    using Component;
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;
    using Utility;

    public class UserCurrency : UserData
    {
        public const string TAG = "currency";

        [XmlIgnore]
        public List<int> CurrencyValue { get; private set; }

        [XmlAttribute("value")]
        public string CurrencyValueStr
        {
            get { return Stringy.FormatList<int>(CurrencyValue); }
            private set { Stringy.ParseList<int>(value, CurrencyValue); }
        }

        public UserCurrency() : base()
        {
            CurrencyValue = new List<int>();
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }
        
        public void SetCurrencies(List<int> value)
        {
            CurrencyValue = value;
            Dirty();
        }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            if (CurrencyValue.Count != Currency.CURRENCY_COUNT)
                Say.Warn("Currency does not have " + Currency.CURRENCY_COUNT + " values being tracked");

            Currency.Man.Sync(this);

            Dirty();
        }
    }
}
