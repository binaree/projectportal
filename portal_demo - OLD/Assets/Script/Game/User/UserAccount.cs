namespace Binaree.Game.User
{
    using System.Xml;
    using System.Xml.Serialization;
    using Component;

    /// <summary>
    /// Tracks all data regarding a user's account and settings
    /// </summary>
    [XmlRoot(UserAccount.TAG)]
    public class UserAccount : UserGraph
    {
        public const string TAG = "account";

        [XmlAttribute("id")]
        public string Id { get; private set; }

        [XmlElement(UserPlayer.TAG)]
        public UserPlayer Player { get; private set; }

        [XmlElement(UserCurrency.TAG)]
        public UserCurrency Currency { get; private set; }

        public UserAccount() : base()
        {
        }

        override public string UserId() { return Id; }
        override public string Tag() { return TAG; }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);
            Player.Load(this);
            Currency.Load(this);

            Dirty();
        }
    }
}
