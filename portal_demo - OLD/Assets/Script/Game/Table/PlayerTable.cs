namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;

    /// <summary>
    /// Provides designer functionaly for player leveling
    /// </summary>
    public class PlayerTable : TableData
    {
        public int MaxLevel { get; private set; }
        public float LevelPerBanner { get; private set; }

        private List<int> expChart = new List<int>();

        public PlayerTable(XmlNode xData) : base(xData)
        {
            MaxLevel = Convert.ToInt32(xData.Attributes.GetNamedItem("max").Value);
            LevelPerBanner = Convert.ToSingle(xData.Attributes.GetNamedItem("ban").Value);
        }

        public float ExperienceForBanner(int bannerCount)
        {
            return (bannerCount * LevelPerBanner);
        }
    }
}