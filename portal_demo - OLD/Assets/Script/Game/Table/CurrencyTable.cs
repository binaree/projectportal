namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    using Factory;
    using Fx;

    /// <summary>
    /// Provides designer functionaly for currency draws
    /// </summary>
    public class CurrencyTable : TableData
    {
        private float scBaseScale = 1.0f;
        /*private List<float> scStandardChance = new List<float>();
        private List<float> scEliteChance = new List<float>();

        private List<SplitF> scBaseDrop = new List<SplitF>();
        private List<SplitF> scLevelDrop = new List<SplitF>();
        private List<float> eliteScale = new List<float>();
        private List<int> scDropProp = new List<int>();

        private List<float> hcEliteChance = new List<float>();
        private List<float> hcBannerChance = new List<float>();
        private List<int> hcDrop = new List<int>();*/

        public CurrencyTable(XmlNode xData) : base(xData)
        {
            scBaseScale = Convert.ToSingle(xData.Attributes.GetNamedItem("scScale").Value);
            /*Stringy.ParseList(xData.Attributes.GetNamedItem("scStd").Value, scStandardChance);
            Stringy.ParseList(xData.Attributes.GetNamedItem("scEl").Value, scEliteChance);

            Stringy.ParseSplitFList(xData.Attributes.GetNamedItem("scBase").Value, scBaseDrop);
            Stringy.ParseSplitFList(xData.Attributes.GetNamedItem("scLvl").Value, scLevelDrop);
            Stringy.ParseList(xData.Attributes.GetNamedItem("elSc").Value, eliteScale);
            Stringy.ParseList(xData.Attributes.GetNamedItem("scProp").Value, scDropProp);

            Stringy.ParseList(xData.Attributes.GetNamedItem("hcEl").Value, hcEliteChance);
            Stringy.ParseList(xData.Attributes.GetNamedItem("hcBan").Value, hcBannerChance);
            Stringy.ParseList(xData.Attributes.GetNamedItem("hcDrop").Value, hcDrop);*/
        }

        /*public void EnemyDrop(EnemyBlob en)
        {
            DropSC(en);
            DropHC(en);
        }

        private void DropSC(EnemyBlob en)
        {
            int rarity = (int)en.Data.Rarity;
            List<float> chance = (en.IsElite()) ? scEliteChance : scStandardChance;
            if (Randomize.Value() > chance[rarity]) return;

            float sc = scBaseDrop[rarity].Rand();
            SplitF lvl = scLevelDrop[rarity];
            int loopLvl = Player.Man.Level - 1;
            for (int i = 0; i < loopLvl; ++i)
                sc += lvl.Rand();

            if (en.IsElite()) sc *= eliteScale[rarity];
            sc *= scBaseScale;

            if (sc <= 0) return;
            int prop = scDropProp[rarity];
            int amt = (int)sc;
            int remain = amt % prop;
            amt = amt / prop;
            for(int i = 0; i < prop; ++i)
            {
                FxCurrencyDrop drop = FxFactory.Inst.TakeDrop(FxFactory.SC_DROP);
                drop.Activate((i == 0) ? (amt + remain) : amt, en.EObject.gameObject.transform.position);
            }
        }

        private void DropHC(EnemyBlob en)
        {
            float chance = 0.0f;
            if (en.IsBanner()) chance = hcBannerChance[(int)en.Data.Rarity];
            else if (en.IsElite()) chance = hcEliteChance[(int)en.Data.Rarity];
            if (Randomize.Value() > chance) return;

            int amt = hcDrop[(int)en.Data.Rarity];
            for (int i = 0; i < amt; ++i)
            {
                FxCurrencyDrop drop = FxFactory.Inst.TakeDrop(FxFactory.HC_DROP);
                drop.Activate(1, en.EObject.gameObject.transform.position);
            }
        }*/
    }
}