namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public class UiPromoteOverlay : UiOverlay
    {
        public const string PAGE_ID = "promote";

        public GameObject ContratsPanel;
        public UiLeagueBadge LeagueBadge;
        public Text LeagueName;
        public GameObject NewLeague;

        private LeagueBlob league = null;
        private RewardBlob reward = null;
        private Action completeAction = null;

        override public string RefId() { return PAGE_ID; }

        override public void ActivateOverlay()
        {
            base.ActivateOverlay();
        }

        public void AssignLeague(LeagueBlob old, LeagueBlob next, RewardBlob wonReward, Action overlayCompleteAction)
        {
            league = next;
            reward = wonReward;
            completeAction = overlayCompleteAction;
            LeagueBadge.AssignLeague(league.Data, league.IsMax());
            NewLeague.SetActive(old.Data.Group != next.Data.Group);
            LeagueName.text = next.Name();
            LeagueBadge.AssignLeague(next.Data, next.IsMax());
        }

        override public void CloseOverlay()
        {
            base.CloseOverlay();
            if (completeAction != null)
                completeAction();
        }

        public void SelectConfirm()
        {
            if (reward == null)
                CloseOverlay();
            else
            {
                UiRewardOverlay over = UiMain.Man.SelectOverlay(UiRewardOverlay.PAGE_ID) as UiRewardOverlay;
                over.AssignReward(reward, SelectConfirm);
                reward = null;
            }
        }
    }
}
