namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiCollectTile : UiTileObj
    {
        public const string PREFAB = "tile_collect";

        public Image Icon;
        public UiStarRank Rank;
        public Text Name;
        public Text Quantity;

        public GameObject CanCollectPanel;

        private CollectCrystal crystal = null;

        public void AssignCrystal(CollectCrystal cry)
        {
            crystal = cry;
            Spritey.ReplaceIcon(Icon, UiCrystalTile.CRYSTAL_DIR, Creature.DATA_HEADER + cry.CrystalRefId);
            Rank.AssignRank(cry.CrystalRank, Table.Man.Creature.MaxRank);
            Name.text = Script.Man.Name(Creature.DATA_HEADER + cry.CrystalRefId);
            Refresh();
        }

        public override void Refresh()
        {
            if (crystal == null) return;
            CrystalBlob b = Crystal.Man.BlobRef(crystal.CrystalRefId);
            Quantity.text = b.RankQty(crystal.CrystalRank) + " / " + crystal.Quantity;
            CanCollectPanel.SetActive(b.RankQty(crystal.CrystalRank) >= crystal.Quantity);
        }
    }
}
