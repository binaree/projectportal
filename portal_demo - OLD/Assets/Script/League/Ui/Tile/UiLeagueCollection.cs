namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiLeagueCollection : UiCollectionTile
    {
        public const string LEAGUE_PREFAB = "tile_league_collect";

        public GameObject CollectNowPanel;
        public GameObject RewardPanel;
        public UiRewardInfo RewardInfo;

        private UiLeaguePanel targetPanel = null;

        override public string IconPrefab() { return LEAGUE_PREFAB; }

        override public void AssignCollection(CollectionBlob col)
        {
            base.AssignCollection(col);
            CollectNowPanel.SetActive(collection.CanComplete());

            RewardBlob rew = collection.GetReward();
            if (rew != null)
            {
                RewardPanel.SetActive(true);
                RewardInfo.AssignReward(rew);
            }
            else
                RewardPanel.SetActive(false);
        }

        public void AssignTarget(UiLeaguePanel pan)
        {
            targetPanel = pan;
        }

        override protected bool CompleteCollection()
        {
            if (collection.CompleteLeagueCollection(League.Man.Standard, collection.Slot, true))
            {
                RewardBlob b = collection.GetReward();
                if (b != null)
                {
                    UiRewardOverlay over = UiMain.Man.SelectOverlay(UiRewardOverlay.PAGE_ID) as UiRewardOverlay;
                    over.AssignReward(b, CollectRewardComplete);
                }
                else
                    CollectRewardComplete();
                return true;
            }
            return false;
        }

        public void SelectCollect()
        {
            CompleteCollection();
        }

        public override void Refresh()
        {
            Tiles.Refresh();
        }

        private void CollectRewardComplete()
        {
            CollectedPanel.SetActive(true);
            CollectNowPanel.SetActive(false);
            if (targetPanel != null) targetPanel.CollectionComplete();
        }
    }
}
