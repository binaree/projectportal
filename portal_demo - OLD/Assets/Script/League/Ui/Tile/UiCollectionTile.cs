namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiCollectionTile : UiTileObj
    {
        public const string BASE_PREFAB = "tile_collection";

        public GameObject CollectedPanel;
        public GameObject CanCollectPanel;

        public UiTileSetObj Tiles;

        protected CollectionBlob collection = null;

        virtual public string IconPrefab() { return BASE_PREFAB; }

        virtual public void AssignCollection(CollectionBlob col)
        {
            collection = col;
            CollectedPanel.SetActive(collection.Complete);
            CanCollectPanel.SetActive(true);// collection.CanComplete());

            List<CollectCrystal> cry = collection.Data.Crystal;
            for(int i = 0; i < cry.Count; ++i)
            {
                UiCollectTile tile = Tiles.AddNewItem(UiCollectTile.PREFAB, null) as UiCollectTile;
                tile.AssignCrystal(cry[i]);
            }
        }

        virtual protected bool CompleteCollection()
        {
            if( collection.CompleteCollection())
            {
                CollectedPanel.SetActive(true);
                return true;
            }
            return false;
        }
    }
}
