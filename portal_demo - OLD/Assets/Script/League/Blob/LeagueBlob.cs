namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.Ui;
    using Binaree.Game.User;
    using Core;
    using Factory;
    using System;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;

    public class LeagueBlob : GameBlob
    {
        public LeagueData Data { get; private set; }
        public UserLeague User { get; private set; }
        public PromotionBlob Promote { get; private set; }
        public GateData PortalGate { get; private set; }

        public int MaxRank { get; private set; }
        public int CurrentRank { get { return Data.Rank; } }
        public float RankLevel { get { return (CurrentRank) / (MaxRank); } }
        public bool IsMax() { return Data.Rank == MaxRank; }
        public string Name() { return Script.Man.Name(League.DATA_HEADER + Data.Group); }

        private RewardBlob reward = null;

        public LeagueBlob(LeagueData d)
        {
            Data = d;
            SetDirectRef(Data.Rank);
            //Say.Log("Creating league: " + Data.Id + " with ref #" + RefNum);
            Promote = Promotion.Man.GenerateLeaguePromotion(this);
            PortalGate = Gate.Man.DataLeague(Data.GateRefId);

            if (Data.Type == LeagueType.Standard)
                MaxRank = Table.Man.League.StandardMax(Data.Group);
            else
                MaxRank = 1;
        }

        public void AssignUser(UserLeague user, bool resetUser = true)
        {
            User = user;
            if (resetUser)
            {
                User.AssignOwner(this);
                List<int> col = new List<int>();
                for (int i = 0; i < Promote.Collect.Count; ++i)
                    col.Add(0);
                User.SetCollections(col);
            }
            else
            {
                for (int i = 0; i < Promote.Collect.Count; ++i)
                {
                    if (i >= user.CollectionComplete.Count) break;
                    if (user.CollectionComplete[i] <= 0) break;
                    Promote.Collect[i].CompleteLeagueCollection(this);
                }
            }

            Activate();
        }

        private void Activate()
        {
            PortalGate.AddKeys(RankLevel);
        }

        public void Deactivate()
        {
            PortalGate.RemoveKeys();
        }

        public bool DoPromote(Action completeAction)
        {
            if (!Promote.CanPromote()) return false;
            if (!Promote.HasCost()) return false;

            RewardBlob rew = GetReward();
            LeagueBlob next = League.Man.Promote(this);
            if (next == this) return false;

            Promote.Execute();
            if (rew != null) rew.Give();

            UiPromoteOverlay over = UiMain.Man.SelectOverlay(UiPromoteOverlay.PAGE_ID) as UiPromoteOverlay;
            over.AssignLeague(this, next, rew, completeAction);
            return true;
        }

        public LeagueData NextLeague()
        {
            LeagueData d = null;
            if (Promote.Data.IsRankPromo() && !IsMax())
                d = League.Man.DataL(Data.Group + "_" + (CurrentRank + 1));
            else
                d = League.Man.DataL(Promote.Data.PromoteTo);
            return d;
        }

        public RewardBlob GetReward()
        {
            if (reward != null) return reward;
            if (Promote.Data.RewardRefId == string.Empty) return null;
            reward = Reward.Man.GenerateReward(Promote.Data.RewardRefId);
            return reward;
        }
    }
}