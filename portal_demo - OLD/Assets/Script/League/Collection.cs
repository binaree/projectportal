namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Collection : DataManager
    {
        public const string DATA_HEADER = "col_";
        
        private static Collection _instance = new Collection();

        public static Collection Man { get { return _instance; } }

        public CollectionData Data(string id)
        {
            return GetData(id) as CollectionData;
        }

        public CollectionData DataC(string id)
        {
            return GetData(DATA_HEADER + id) as CollectionData;
        }

        public CollectionBlob GenerateCollection(string refId, int slotNum)
        {
            CollectionData d = DataC(refId);
            CollectionBlob b = GenerateBlob(d) as CollectionBlob;
            b.SetSlot(slotNum);
            return b;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "collect/c");
        }

        protected override GameData NewData(XmlNode node)
        {
            CollectionData d = new CollectionData(node);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new CollectionBlob(d as CollectionData);
        }
    }
}
