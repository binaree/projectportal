namespace Binaree.Game.User
{
    using Component;
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;
    using Utility;
    
    public class UserLeague : UserData
    {
        public const string TAG = "l";

        [XmlAttribute("type")]
        public int Type { get; private set; }

        [XmlAttribute("id")]
        public string RefId { get; private set; }

        [XmlIgnore]
        public List<int> CollectionComplete { get; private set; }

        [XmlAttribute("col")]
        public string CollectionCompleteStr
        {
            get { return Stringy.FormatList<int>(CollectionComplete); }
            private set { Stringy.ParseList<int>(value, CollectionComplete); }
        }

        public UserLeague() : base()
        {
            CollectionComplete = new List<int>();
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }

        public void AssignOwner(LeagueBlob league)
        {
            RefId = league.Data.RefIdFromId(League.DATA_HEADER);
            Dirty();
        }
        
        public void SetCollections(List<int> value)
        {
            CollectionComplete = value;
            Dirty();
        }

        public void SetComplete(int slotNum, bool complete)
        {
            CollectionComplete[slotNum] = (complete) ? 1 : 0;
            Dirty();
        }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            League.Man.GenerateFromUser(this);

            Dirty();
        }
    }
}
