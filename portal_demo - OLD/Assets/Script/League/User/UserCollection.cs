namespace Binaree.Game.User
{
    using Component;
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;
    using Utility;

    public class UserCollection : UserData
    {
        public const string TAG = "c";

        [XmlAttribute("id")]
        public string RefId { get; private set; }

        public UserCollection() : base()
        {
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            //Crystal.Man.GenerateFromUser(this);

            Dirty();
        }
    }
}
