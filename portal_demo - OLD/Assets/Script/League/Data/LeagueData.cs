namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public enum LeagueType
    {
        Standard = 0,
        Event = 1,
        Tournament = 2,
    }

    public class LeagueData : GameData
    {
        public static readonly string[] TYPE = { "std", "ev", "tour" };

        public LeagueType Type { get; private set; }
        public string Group { get; private set; }
        public int Rank { get; private set; }

        public string RemoteRefId { get; private set; }
        public string GateRefId { get; private set; }

        public LeagueData(XmlNode xData) : base(xData)
        {
            Type = StrToType(xData.Attributes.GetNamedItem("type").Value);
            Group = xData.Attributes.GetNamedItem("group").Value;
            Rank = Convert.ToInt32(xData.Attributes.GetNamedItem("rank").Value);

            RemoteRefId = xData.Attributes.GetNamedItem("rem").Value;
            GateRefId = xData.Attributes.GetNamedItem("gate").Value;
        }

        public static LeagueType StrToType(string val)
        {
            for (int i = 0; i < TYPE.Length; ++i)
                if (val == TYPE[i]) return (LeagueType)i;
            return LeagueType.Standard;
        }

        public static string TypeToStr(LeagueType val)
        {
            return TYPE[(int)val];
        }
    }
}