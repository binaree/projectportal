namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public enum PromotionType
    {
        League = 0,
    }

    public class PromotionData : GameData
    {
        private static readonly string[] TYPE = { "lea" };
        private const string RANK_PROMOTE = "rank";
        private const string END_PROMOTE = "end";

        public PromotionType Type { get; private set; }
        public string PromoteTo { get; private set; }
        public string RewardRefId { get; private set; }

        public List<int> Cost { get; private set; }
        public List<string> Collection { get; private set; }

        public PromotionData(XmlNode xData) : base(xData)
        {
            Type = StrToType(xData.Attributes.GetNamedItem("type").Value);
            PromoteTo = xData.Attributes.GetNamedItem("to").Value;
            RewardRefId = xData.Attributes.GetNamedItem("rew").Value;

            Cost = new List<int>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("cur").Value, Cost);

            Collection = new List<string>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("col").Value, Collection);
        }

        public bool IsRankPromo() { return PromoteTo == string.Empty || PromoteTo == RANK_PROMOTE; }
        public bool IsLastPromo() { return PromoteTo == END_PROMOTE; }

        public static PromotionType StrToType(string val)
        {
            for (int i = 0; i < TYPE.Length; ++i)
                if (val == TYPE[i]) return (PromotionType)i;
            return PromotionType.League;
        }

        public static string TypeToStr(PromotionType val)
        {
            return TYPE[(int)val];
        }
    }
}