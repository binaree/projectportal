namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class CollectCrystal
    {
        public string CrystalRefId;
        public int CrystalRank;
        public int Quantity;

        public CollectCrystal(string val)
        {
            string[] split = val.Split('|');
            if(split.Length < 3)
            {
                Say.Warn("Collected crystal must have 3 '|' elements: " + val);
                return;
            }
            CrystalRefId = split[0];
            CrystalRank = Convert.ToInt32(split[1]);
            Quantity = Convert.ToInt32(split[2]);
        }

        public bool CanComplete()
        {
            CrystalBlob b = Crystal.Man.BlobRef(CrystalRefId);
            if (b == null) return false;
            return b.RankQty(CrystalRank) >= Quantity;
        }
    }

    public class CollectionData : GameData
    {
        public string RewardRefId { get; private set; }
        public List<CollectCrystal> Crystal { get; private set; }

        public CollectionData(XmlNode xData) : base(xData)
        {
            RewardRefId = xData.Attributes.GetNamedItem("rew").Value;

            Crystal = new List<CollectCrystal>();
            string[] split = xData.Attributes.GetNamedItem("cry").Value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < split.Length; ++i)
                Crystal.Add(new CollectCrystal(split[i]));
        }
    }
}