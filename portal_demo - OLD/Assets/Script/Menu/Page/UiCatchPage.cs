namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class UiCatchPage : UiPage
    {
        public enum State
        {
            Catch = 0,
            Info = 1,
            Photo = 2,
        }

        public const string PAGE_ID = "catch";
        private const string CREATURE_DIR = "creatureui/";

        public GameObject CatchUi;
        public GameObject PhotoUi;

        public Image CreatureImage;
        public float ShowTime = 2.0f;

        public GameObject CreaturePanel;
        public Text CreatureName;
        public GameObject TrophyIcon;
        public GameObject StarRankPanel;
        public Text StarRankValue;
        public Text SizeValue;
        public Text SCValue;
        public Text MarkValue;

        private State state = State.Catch;
        private float curTime = 0.0f;

        override public string RefId() { return PAGE_ID; }

        override public void Activate()
        {
            CatchUi.SetActive(false);
            PhotoUi.SetActive(false);
            SetCreatureImage();
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        private void Update()
        {
            if (state == State.Catch)
            {
                curTime -= TimeLord.Inst.ArenaDelta;
                if (curTime <= 0.0f)
                    SetInfo();
            }
        }

        public void StartPhoto()
        {
            SetPhoto();
        }

        public void CancelPhoto()
        {
            SetInfo();
        }

        private void SetCreatureImage()
        {
            CreatureBlob c = Creature.Man.Active;
            CreatureName.text = Script.Man.Name(c.Data.Id);
            Spritey.ReplaceIcon(CreatureImage, CREATURE_DIR, c.Data.Id);
            CreatureImage.gameObject.SetActive(true);
            SizeValue.text = Stringy.FormatSize(c.Size);
            SCValue.text = Table.Man.Reward.DetermineCurrencyReward(Currency.Type.Soft).ToString();
            MarkValue.text = Table.Man.Reward.DetermineCurrencyReward(Currency.Type.Mark).ToString();
            if( c.IsTrophy())
            {
                TrophyIcon.gameObject.SetActive(true);
                StarRankPanel.SetActive(false);
            }
            else
            {
                TrophyIcon.gameObject.SetActive(false);
                StarRankPanel.SetActive(true);
                StarRankValue.text = ((int)(c.Rank + 1)).ToString();
            }

            curTime = ShowTime;
            state = State.Catch;
        }

        private void SetInfo()
        {
            CatchUi.SetActive(true);
            PhotoUi.SetActive(false);
            state = State.Info;
            UiMain.Man.Footer.SelectNav(UiNavCatch.NAV_ID);
        }

        private void SetPhoto()
        {
            CatchUi.SetActive(false);
            PhotoUi.SetActive(true);
            state = State.Photo;
            UiMain.Man.Footer.SelectNav(UiNavPhoto.NAV_ID);
        }
    }
}
