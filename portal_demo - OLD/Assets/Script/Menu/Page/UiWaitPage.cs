namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Core;
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class UiWaitPage : UiPage
    {
        public const string PAGE_ID = "wait";

        public UiDepthWidget DepthWidget;
        public UiTapWidget TapWidget;
        public Text InstructionLabel;

        override public string RefId() { return PAGE_ID; }

        override public void Activate()
        {
            UiMain.Man.Footer.SelectNav(UiNavWait.NAV_ID);
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }
    }
}
