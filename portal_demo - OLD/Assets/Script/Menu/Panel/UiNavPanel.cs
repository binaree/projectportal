namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using System;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class UiNavPanel : UiObj
    {
        
        virtual public string RefId() { return "NONE"; }

        virtual public void Activate()
        {

        }

        virtual public void Refresh()
        {

        }

        virtual public void Close()
        {

        }
    }
}
