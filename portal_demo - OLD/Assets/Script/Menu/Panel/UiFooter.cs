namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using System;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class UiFooter : UiObj
    {
        public enum Widget
        {
            None = 0,
            Pet = 1,
        }

        public List<UiNavPanel> NavPanel;

        public UiCreatureWidget RemotePet;

        public UiNavPanel SelectedNav { get; private set; }

        private Dictionary<string, UiNavPanel> nav = new Dictionary<string, UiNavPanel>();

        private void Awake()
        {
            SelectedNav = null;
            for (int i = 0; i < NavPanel.Count; ++i)
                nav.Add(NavPanel[i].RefId(), NavPanel[i]);
            CloseAllPanels();
        }

        public UiNavPanel SelectNav(string navRefId)
        {
            CloseAllPanels();
            SelectedNav = nav[navRefId];
            if (SelectedNav == null)
            {
                Say.Warn("Ui could not find a page with a reference name of " + navRefId);
                return null;
            }
            SelectedNav.gameObject.SetActive(true);
            SelectedNav.Activate();
            return SelectedNav;
        }

        public void SelectWidget(Widget type)
        {
            CloseAllWidgets();
            if(type == Widget.Pet)
            {
                RemotePet.Activate();
            }
        }

        public void Refresh()
        {
            if (SelectedNav != null) SelectedNav.Refresh();
        }

        private void CloseAllPanels()
        {
            if(SelectedNav != null)
            {
                SelectedNav.Close();
                SelectedNav = null;
            }

            for (int i = 0; i < NavPanel.Count; ++i)
                NavPanel[i].gameObject.SetActive(false);
        }

        private void CloseAllWidgets()
        {
            RemotePet.gameObject.SetActive(false);
        }
    }
}
