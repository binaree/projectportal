namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiRewardWidget : UiObj
    {
        public const string DIR = "uiprefab/";
        public const string PREFAB = "reward_widget";

        public Image RewardIcon;
        public GameObject InfoPanel;
        public GameObject NamePanel;
        public Text Name;
        public GameObject NewPanel;
        public Text New;
        public GameObject QuantityPanel;
        public Text Quantity;
        public GameObject LuckyPanel;
        public Text Lucky;

        public GameObject GeneralPanel;
        public List<GameObject> RarityPanel;
        public UiStarRank Rank;

        private RewardBlob reward = null;

        public void AssignReward(RewardBlob rew)
        {
            CloseAll();
            reward = rew;
            Spritey.ReplaceIcon(RewardIcon, reward.IconDir(), reward.IconPrefab());
            InfoPanel.SetActive(true);
            Name.text = reward.RewardName();
            Quantity.text = "+ " + reward.Amount;
            if (reward.RewardWon == FinalReward.Currency) SetCurrency();
            else if (reward.RewardWon == FinalReward.Crystal) SetCrystal();
            else if (reward.RewardWon == FinalReward.Card) SetCard();
            else SetSupply();
            Deactivate();
        }

        public void Activate()
        {
            gameObject.SetActive(true);
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);
        }

        private void CloseAll()
        {
            InfoPanel.SetActive(false);
            NamePanel.SetActive(false);
            NewPanel.SetActive(false);
            QuantityPanel.SetActive(false);
            LuckyPanel.SetActive(false);
            GeneralPanel.SetActive(false);
            for (int i = 0; i < RarityPanel.Count; ++i)
                RarityPanel[i].SetActive(false);
            Rank.gameObject.SetActive(false);
        }

        private void SetCurrency()
        {
            GeneralPanel.SetActive(true);
            NamePanel.SetActive(true);
            QuantityPanel.SetActive(true);
            LuckyPanel.SetActive(reward.Bonus);
        }

        private void SetCrystal()
        {
            Rank.gameObject.SetActive(true);
            Rank.AssignRank(reward.RewardSubNum, Table.Man.Creature.MaxRank);
            NamePanel.SetActive(true);
            QuantityPanel.SetActive(true);
            LuckyPanel.SetActive(reward.Bonus);
            CreatureData cd = Creature.Man.Data((reward.RewardData as CrystalData).CreatureId);
            RarityPanel[(int)cd.Rarity].SetActive(true);
        }

        private void SetCard()
        {

        }

        private void SetSupply()
        {
            RarityPanel[reward.RewardSubNum].SetActive(true);
            NamePanel.SetActive(true);
            QuantityPanel.SetActive(true);
            LuckyPanel.SetActive(reward.Bonus);
        }
    }
}
