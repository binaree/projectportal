namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiLeagueBadge : UiObj
    {
        private const string LEAGUE_DIR = "leagueui/";

        public Image LeagueIcon;
        public UiStarRank Rank;

        private void Awake()
        {
        }

        public void AssignLeague(LeagueData league, bool isMax)
        {
            Spritey.ReplaceIcon(LeagueIcon, LEAGUE_DIR, League.DATA_HEADER + league.Group);
            Rank.AssignRank(league.Rank, (isMax) ? league.Rank : league.Rank + 1);
        }
    }
}
