namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public class UiReleaseOverlay : UiOverlay
    {

        public const string PAGE_ID = "release";

        public Text ConfirmText;
        public Text CloseText;

        override public string RefId() { return PAGE_ID; }

        override public void ActivateOverlay()
        {
            base.ActivateOverlay();
        }

        override public void CloseOverlay()
        {
            base.CloseOverlay();
        }

        public void SelectConfirm()
        {
            Step.Man.Catch.ReleaseCatch(this);
        }

        public void SelectClose()
        {
            CloseOverlay();
        }
    }
}
