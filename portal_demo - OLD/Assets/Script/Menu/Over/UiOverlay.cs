namespace Binaree.Game.Ui
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    /// Handles game screen designed to hover over the ui, blocking input until removed
    /// </summary>
    public class UiOverlay : UiObj
    {

        virtual public string RefId() { return "NONE"; }

        virtual public void ActivateOverlay()
        {
            gameObject.SetActive(true);
        }

        virtual public void CloseOverlay()
        {
            gameObject.SetActive(false);
            //#UiMain.Man.SelectOverlay(-1);
        }
    }
}
