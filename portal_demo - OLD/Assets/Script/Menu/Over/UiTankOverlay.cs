namespace Binaree.Game.Ui
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public class UiTankOverlay : UiOverlay
    {
        public const string PAGE_ID = "tank";

        public UiTankPanel TankPanel;

        private UiNavPanel lastNav = null;

        override public string RefId() { return PAGE_ID; }

        override public void ActivateOverlay()
        {
            base.ActivateOverlay();
            lastNav = UiMain.Man.Footer.SelectedNav;
            UiNavTank nav = UiMain.Man.Footer.SelectNav(UiNavTank.NAV_ID) as UiNavTank;
            nav.AssignOverlay(this);
        }

        override public void CloseOverlay()
        {
            base.CloseOverlay();
            UiMain.Man.Footer.SelectNav(lastNav.RefId());
            lastNav = null;
        }
    }
}
