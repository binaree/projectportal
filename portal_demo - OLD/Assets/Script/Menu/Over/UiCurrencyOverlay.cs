namespace Binaree.Game.Ui
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public class UiCurrencyOverlay : UiOverlay
    {

        public const string PAGE_ID = "currency";

        public UiPurchasePanel Purchase;

        override public string RefId() { return PAGE_ID; }

        override public void ActivateOverlay()
        {
            base.ActivateOverlay();
            Purchase.Activate();
        }

        override public void CloseOverlay()
        {
            base.CloseOverlay();
        }

        public void SelectClose()
        {
            CloseOverlay();
        }
    }
}
