namespace Binaree.Game.Ui
{
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    
    public class UiTileSetObj : UiObj
    {
        private const string UI_DIR = "uiprefab/";
    
        public enum Style
        {
            LeftToRight = 0,
            RightToLeft = 1,
            UpToDown = 2,
            DownToUp = 4,
        }

        public Style SortStyle = Style.LeftToRight;
        public int HorizontalMax = 1;
        public int VerticalMax = 1;
        public float HorizontalSpace = 1.0f;
        public float VerticalSpace = 1.0f;

        public List<UiTileObj> Items { get; private set; }
        public UiTileObj Active { get; private set; }

        private int currentX = 0;
        private int currentY = 0;
        private bool isInitialized = false;
        private bool isFull = false;

        private void Awake()
        {
        }

        public UiTileObj AddNewItem(string prefabName, GameBlob initBlob)
        {
            if (isFull) return null;
            if (!isInitialized) Initialize();

            GameObject go = UiMain.GeneratePrefab(UI_DIR, prefabName);
            if (go == null) return null;
            UiTileObj ui = go.GetComponent<UiTileObj>();
            if (ui == null) return null;

            ui.DeselectTile();
            go.transform.SetParent(gameObject.transform, false);
            float flipX = (SortStyle == Style.RightToLeft) ? -1.0f : 1.0f;
            float flipY = (SortStyle == Style.UpToDown) ? -1.0f : 1.0f;
            go.transform.Translate(HorizontalSpace * currentX * flipX, VerticalSpace * currentY * flipY, 0.0f);
            if (initBlob != null) ui.AssignBlob(initBlob);
            UpdateIndex();
            Items.Add(ui);
            return ui;
        }

        public void DeselectAll()
        {
            for (int i = 0; i < Items.Count; ++i)
                Items[i].DeselectTile();
        }

        public UiTileObj Select(string tileRef)
        {
            UiTileObj tile = Find(tileRef);
            if (tile == null) return null;
            DeselectAll();
            tile.SelectTile();
            return tile;
        }

        public UiTileObj Find(string tileRef)
        {
            for (int i = 0; i < Items.Count; ++i)
                if (Items[i].RefId() == tileRef)
                    return Items[i];
            return null;
        }

        public void Initialize()
        {
            currentX = currentY = 0;
            if (SortStyle == Style.RightToLeft)
                currentX = HorizontalMax - 1;
            else if (SortStyle == Style.DownToUp)
                currentY = VerticalMax - 1;
            Active = null;
            Clear();
            isFull = false;
            isInitialized = true;
        }

        public void Refresh()
        {
            for (int i = 0; i < Items.Count; ++i)
                Items[i].Refresh();
        }

        private void UpdateIndex()
        {
            if(SortStyle == Style.LeftToRight)
            {
                if (++currentX >= HorizontalMax)
                {
                    if(++currentY >= VerticalMax) { isFull = true; return; }
                    currentX = 0;
                }
            }
            else if (SortStyle == Style.RightToLeft)
            {
                if (--currentX < 0)
                {
                    if (++currentY >= VerticalMax) { isFull = true; return; }
                    currentX = HorizontalMax - 1;
                }
            }
            else if (SortStyle == Style.UpToDown)
            {
                if (++currentY >= VerticalMax)
                {
                    if (++currentX >= HorizontalMax) { isFull = true; return; }
                    currentY = 0;
                }
            }
            else if (SortStyle == Style.DownToUp)
            {
                if (--currentY >= VerticalMax)
                {
                    if (++currentX >= HorizontalMax) { isFull = true; return; }
                    currentY = VerticalMax - 1;
                }
            }
        }

        private void Clear()
        {
            if(Items != null)
            {
                for (int i = Items.Count - 1; i >= 0; --i)
                    Destroy(Items[i].gameObject);
            }
            Items = new List<UiTileObj>();
            isInitialized = false;
        }
    }
}
