namespace Binaree.Game.Ui
{
    using Core;
    using UnityEngine;

    /// <summary>
    /// Base object for banner objects which are intended to provide information and 
    /// be listed in a scrolling list
    /// </summary>
    public class UiBannerObj : UiObj
    {

        virtual public void SelectBanner()
        {
            Say.Log("Banner " + name + " has been selected");
        }
    }
}
