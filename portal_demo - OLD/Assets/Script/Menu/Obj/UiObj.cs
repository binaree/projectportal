namespace Binaree.Game.Ui
{
    using Core;
    using UnityEngine;

    /// <summary>
    /// Base method to handle functionality common to all ui components in the game
    /// </summary>
    public class UiObj : GameObj
    {
        virtual public bool AutoRemoveOnLevelLoad() { return false; }
    }
}
