namespace Binaree.Game.Ui
{
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    
    public class UiTileObj : UiObj
    {
        virtual public string RefId() { return "NONE"; }

        virtual public void AssignBlob(GameBlob b)
        {
        }

        virtual public void SelectTile()
        {
        }

        virtual public void DeselectTile()
        {
        
        }

        virtual public void Refresh()
        {

        }
    }
}
