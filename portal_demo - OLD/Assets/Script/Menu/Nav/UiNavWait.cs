namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using System;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiNavWait : UiNavPanel
    {
        public const string NAV_ID = "wait";

        public Text CancelLabel;
        public Text SetLabel;
        public Text DrawLabel;

        override public string RefId() { return NAV_ID; }

        override public void Activate()
        {
            UiMain.Man.Footer.SelectWidget(UiFooter.Widget.None);
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public void SelectCancel()
        {
            Step.Man.Wait.DrawIn();
        }

        public void SelectSet()
        {
            Step.Man.Wait.AttemptSet();
        }

        public void SelectDraw()
        {
            if (!(UiMain.Man.SelectedPage is UiWaitPage)) return;
            UiWaitPage pg = UiMain.Man.SelectedPage as UiWaitPage;
            pg.TapWidget.ScreenTap();
        }
    }
}
