namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using System;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiNavTank : UiNavPanel
    {
        public const string NAV_ID = "tank";

        public Text CancelLabel;

        private UiTankOverlay overlay = null;

        override public string RefId() { return NAV_ID; }

        override public void Activate()
        {
            UiMain.Man.Footer.SelectWidget(UiFooter.Widget.None);
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public void SelectCancel()
        {
            if (overlay != null)
                overlay.CloseOverlay();
        }

        public void AssignOverlay(UiTankOverlay over)
        {
            overlay = over;
        }
    }
}
