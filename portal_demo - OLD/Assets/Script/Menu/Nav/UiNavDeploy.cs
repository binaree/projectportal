namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using System;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiNavDeploy : UiNavPanel
    {
        public const string NAV_ID = "deploy";

        public Text CancelLabel;
        public Text DeployLabel;

        override public string RefId() { return NAV_ID; }

        override public void Activate()
        {
            UiMain.Man.Footer.SelectWidget(UiFooter.Widget.None);
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public void SelectCancel()
        {
            Step.Man.Begin(StepSelect.STEP_ID);
        }

        public void SelectDeploy()
        {
            if (!(UiMain.Man.SelectedPage is UiDeployPage)) return;
            UiDeployPage pg = UiMain.Man.SelectedPage as UiDeployPage;
            Step.Man.Deploy.GoDeploy(pg.DeployPosition());
        }
    }
}
