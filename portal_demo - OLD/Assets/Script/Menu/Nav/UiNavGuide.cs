namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using System;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiNavGuide : UiNavPanel
    {
        public const string NAV_ID = "guide";

        public enum Option
        {
            News = 0,
            Creature = 1,
            Portal = 2,
            Tips = 3,
        }

        public Option StartingOption;
        public List<GameObject> SelectedImage;
        public List<Text> ButtonLabel;
        public Text CancelLabel;

        private UiGuideOverlay overlay = null;
        private Option selectedOption = Option.Tips;

        override public string RefId() { return NAV_ID; }

        override public void Activate()
        {
            UiMain.Man.Footer.SelectWidget(UiFooter.Widget.None);
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public void SelectCancel()
        {
            if (overlay != null)
                overlay.CloseOverlay();
        }

        public void SelectNews()
        {
            NavigateTo(Option.News);
        }

        public void SelectCreature()
        {
            NavigateTo(Option.Creature);
        }

        public void SelectPortal()
        {
            NavigateTo(Option.Portal);
        }

        public void SelectTip()
        {
            NavigateTo(Option.Tips);
        }

        public void AssignOverlay(UiGuideOverlay over)
        {
            overlay = over;
            NavigateTo(StartingOption);
        }

        private void NavigateTo(Option to)
        {
            if (overlay == null) return;
            DeselectAll();
            SelectedImage[(int)to].SetActive(true);
            overlay.ShowNavOption(to);
            selectedOption = to;
        }

        private void DeselectAll()
        {
            for (int i = 0; i < SelectedImage.Count; ++i)
                SelectedImage[i].SetActive(false);
        }
    }
}
