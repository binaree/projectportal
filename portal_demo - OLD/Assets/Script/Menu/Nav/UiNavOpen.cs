namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using System;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiNavOpen : UiNavPanel
    {
        public const string NAV_ID = "open";

        public Text CancelLabel;
        public Text OpenLabel;
        public Text ScanLabel;

        override public string RefId() { return NAV_ID; }

        override public void Activate()
        {
            UiMain.Man.Footer.SelectWidget(UiFooter.Widget.Pet);
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public void SelectCancel()
        {
            Step.Man.Init.CancelGenerate();
        }

        public void SelectOpen()
        {
            Step.Man.Init.CaptureSurface();
        }

        public void SelectScan()
        {
            UiOkOverlay ok = UiMain.Man.SelectOverlay(UiOkOverlay.PAGE_ID) as UiOkOverlay;
            ok.SetData("na", "na_scan", "ok");
        }
    }
}
