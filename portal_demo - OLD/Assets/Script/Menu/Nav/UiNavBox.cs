namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using System;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiNavBox : UiNavPanel
    {
        public const string NAV_ID = "box";

        public Text CancelLabel;

        private UiBoxOverlay overlay = null;

        override public string RefId() { return NAV_ID; }

        override public void Activate()
        {
            UiMain.Man.Footer.SelectWidget(UiFooter.Widget.None);
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public void SelectCancel()
        {
            if (overlay != null)
                overlay.CloseOverlay();
        }

        public void AssignOverlay(UiBoxOverlay over)
        {
            overlay = over;
        }
    }
}
