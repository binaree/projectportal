namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using System;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiNavCatch : UiNavPanel
    {
        public const string NAV_ID = "catch";

        public Text PhotoLabel;
        public Text ReleaseLabel;
        public Text KeepLabel;
        public Text ConvertLabel;
        public Text GuideLabel;

        override public string RefId() { return NAV_ID; }

        override public void Activate()
        {
            UiMain.Man.Footer.SelectWidget(UiFooter.Widget.None);
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public void SelectPhoto()
        {
            if (!(UiMain.Man.SelectedPage is UiCatchPage)) return;
            (UiMain.Man.SelectedPage as UiCatchPage).StartPhoto();
        }

        public void SelectRelease()
        {
            UiMain.Man.SelectOverlay(UiReleaseOverlay.PAGE_ID);
        }

        public void SelectKeep()
        {
            UiMain.Man.SelectOverlay(UiKeepOverlay.PAGE_ID);
        }

        public void SelectConvert()
        {
            UiMain.Man.SelectOverlay(UiConvertOverlay.PAGE_ID);
        }

        public void SelectGuide()
        {
            UiMain.Man.SelectOverlay(UiGuideOverlay.PAGE_ID);
        }
    }
}
