namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using System;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiNavRig : UiNavPanel
    {
        public const string NAV_ID = "rig";

        public enum Option
        {
            Satellite = 0,
            Probe = 1,
            Line = 2,
            Rocket = 3,
            Attratant = 4,
        }

        public Option StartingOption;
        public List<GameObject> SelectedImage;
        public List<Text> ButtonLabel;
        public Text CancelLabel;

        private UiRigOverlay overlay = null;
        private Option selectedOption = Option.Attratant;

        override public string RefId() { return NAV_ID; }

        override public void Activate()
        {
            UiMain.Man.Footer.SelectWidget(UiFooter.Widget.None);
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public void SelectCancel()
        {
            if (overlay != null)
                overlay.CloseOverlay();
        }

        public void SelectSatellite()
        {
            NavigateTo(Option.Satellite);
        }

        public void SelectProbe()
        {
            NavigateTo(Option.Probe);
        }

        public void SelectLine()
        {
            NavigateTo(Option.Line);
        }

        public void SelectRocket()
        {
            NavigateTo(Option.Rocket);
        }

        public void SelectAttract()
        {
            NavigateTo(Option.Attratant);
        }

        public void AssignOverlay(UiRigOverlay over)
        {
            overlay = over;
            NavigateTo(StartingOption);
        }

        private void NavigateTo(Option to)
        {
            if (overlay == null) return;
            DeselectAll();
            SelectedImage[(int)to].SetActive(true);
            overlay.ShowNavOption(to);
            selectedOption = to;
        }

        private void DeselectAll()
        {
            for (int i = 0; i < SelectedImage.Count; ++i)
                SelectedImage[i].SetActive(false);
        }
    }
}
