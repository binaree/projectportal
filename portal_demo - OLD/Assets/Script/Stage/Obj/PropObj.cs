namespace Binaree.Game.Base
{
    using Component;
    using Core;
    using UnityEngine;

    /// <summary>
    /// Any object that functions as part of the prop
    /// </summary>
    public class PropObj : GameObj
    {
        virtual public GameObject GetSpawn(int spawnIndex) { return gameObject; }
    }
}
