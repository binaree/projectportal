namespace Binaree.Game.Object
{
    using Base;
    using Component;
    using Core;
    using Factory;
    using Object;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class SurfaceCamObj : PropObj
    {
        public int ShortSize = 480;
        public int TallSize = 640;
        public float ShortScale = 8000;
        public float TallScale = 14000;

        private WebCamTexture camTexture = null;

        override public GameObject GetSpawn(int spawnIndex) { return gameObject; }

        private void Start()
        {
            if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                camTexture = new WebCamTexture(ShortSize, TallSize);
                transform.localScale = new Vector3(TallScale, 1.0f, ShortScale);
                transform.Rotate(0.0f, 270.0f, 0.0f);
            }
            else
            {
                camTexture = new WebCamTexture(TallSize, ShortSize);
                transform.localScale = new Vector3(ShortScale, 1.0f, TallScale);
                transform.Rotate(0.0f, 180.0f, 0.0f);
            }

            gameObject.GetComponent<Renderer>().material.mainTexture = camTexture;
            camTexture.Play();
        }

        public Color SampleRegionByPercent(float x, float y, float sizeWidth, float sizeHeight)
        {
            //Say.Log("Surface width = " + camTexture.width + ", Surface height = " + camTexture.height);

            int startX = (int)((camTexture.width * x) - (sizeWidth / 2.0f));
            int startY = (int)((camTexture.height * y) - (sizeHeight / 2.0f));
            int width = (int)(sizeWidth);
            int height = (int)(sizeHeight);

            Color[] pixels = camTexture.GetPixels(startX, startY, width, height);
            if (pixels.Length == 0) return Color.black;

            float[] col = { 0.0f, 0.0f, 0.0f };
            for(int i = 0; i < pixels.Length; ++i)
            {
                col[0] += pixels[i].r;
                col[1] += pixels[i].g;
                col[2] += pixels[i].b;
            }

            Color final = new Color(col[0] / pixels.Length, col[1] / pixels.Length, col[2] / pixels.Length);
            //Say.Log("Color average is " + final.ToString());
            return final;
        }
    }
}
