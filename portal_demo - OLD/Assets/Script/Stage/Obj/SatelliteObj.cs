namespace Binaree.Game.Object
{
    using Base;
    using Component;
    using Core;
    using Factory;
    using Object;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class SatelliteObj : PropObj
    {
        public enum Status
        {
            Wait = 0,
            Nibble = 1,
            Bite = 2,
            Hit = 3,
        }

        public SpriteRenderer SatImage;
        public SpriteRenderer SplashImage;
        public SpriteRenderer RippleImage;

        public float SplashTime = 2.0f;
        public float NibbleArea = 500.0f;

        public float NibbleMaxAlpha = 0.85f;
        public float BiteMinAlpha = 0.1f;

        private Status state = Status.Wait;
        private float curTime = 0.0f;

        override public GameObject GetSpawn(int spawnIndex) { return gameObject; }

        public void AssignToPortal(PortalBlob port)
        {
            Use();
            gameObject.transform.position = port.Obj.GetSpawn(0).transform.position;
            SetWait();
        }

        override public void Use()
        {
            base.Use();
            Step.Man.Layer.AssignObjToWorld(this, LayerControl.World.Ground);
        }

        public void SetWait()
        {
            SatImage.color = new Color(SatImage.color.r, SatImage.color.b, SatImage.color.b, 1.0f);
            SatImage.gameObject.SetActive(true);
            SplashImage.gameObject.SetActive(false);
            RippleImage.gameObject.SetActive(false);
        }

        public void SetNibble()
        {
            SatImage.gameObject.SetActive(true);
            SatImage.color = new Color(SatImage.color.r, SatImage.color.b, SatImage.color.b, NibbleMaxAlpha);
            SplashImage.gameObject.SetActive(false);
            RippleImage.gameObject.SetActive(true);
        }

        public void SetBite()
        {
            SatImage.gameObject.SetActive(true);
            SatImage.color = new Color(SatImage.color.r, SatImage.color.b, SatImage.color.b, BiteMinAlpha);
            SplashImage.gameObject.SetActive(true);
            RippleImage.gameObject.SetActive(false);
        }

        public void SetHit()
        {
            SatImage.gameObject.SetActive(false);
            SplashImage.gameObject.SetActive(false);
            RippleImage.gameObject.SetActive(true);
        }
    }
}
