namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Portal : DataManager
    {
        public const string DATA_HEADER = "port_";

        public PortalBlob Active { get; private set; }
        
        private List<PortalBlob> allPortal = new List<PortalBlob>();
        private Dictionary<string, PortalBlob> portal = new Dictionary<string, PortalBlob>();
        
        private static Portal _instance = new Portal();

        public static Portal Man { get { return _instance; } }

        public PortalData Data(string id)
        {
            return GetData(id) as PortalData;
        }

        public PortalData DataP(string id)
        {
            return GetData(DATA_HEADER + id) as PortalData;
        }

        public PortalBlob Blob(string id)
        {
            if(!portal.ContainsKey(id))
            {
                Say.Warn("Could not find portal of id = " + id);
                return null;
            }
            return portal[id];
        }

        public PortalBlob BlobP(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public PortalBlob OpenPortal()
        {
            CloseActivePortal();
            Active = Table.Man.Portal.PickPortal(allPortal);
            Active.Open();
            return Active;
        }

        public void CloseActivePortal()
        {
            if (Active == null) return;
            Active.Close();
            Active = null;
        }

        public void KillPortal()
        {
            CloseActivePortal();
            Step.Man.Begin(StepInit.STEP_ID);
        }

        public void Load(XmlDocument xml)
        {
            Active = null;
            LoadData(xml, "portal/p");
        }

        protected override GameData NewData(XmlNode node)
        {
            PortalData d = new PortalData(node);
            PortalBlob b = GenerateRefBlob(d, d.RefNum) as PortalBlob;
            allPortal.Add(b);
            portal.Add(d.Id, b);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new PortalBlob(d as PortalData);
        }
    }
}
