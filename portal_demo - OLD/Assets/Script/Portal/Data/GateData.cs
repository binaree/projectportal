namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public enum GateType
    {
        League = 0,
        Pet = 1,
    }

    public class GateData : GameData
    {
        private static readonly string[] TYPE = { "lea", "pet" };

        public GateType Type { get; private set; }
        public List<PortalKeyComp> Key { get; private set; }

        public GateData(XmlNode xData) : base(xData)
        {
            Type = StrToType(xData.Attributes.GetNamedItem("type").Value);

            Key = new List<PortalKeyComp>();
            string[] split = xData.Attributes.GetNamedItem("key").Value.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries); ;
            for(int i = 0; i < split.Length; ++i)
            {
                Key.Add(new PortalKeyComp(split[i]));
            }
        }

        public void AddKeys(float level)
        {
            Portal portMan = Portal.Man;
            for(int i = 0; i < Key.Count; ++i)
            {
                PortalBlob port = portMan.BlobP(Key[i].RefId);
                Key[i].AddLevel = level;
                port.PortalGate.AddKey(Type, Key[i]);
            }
        }

        public void RemoveKeys()
        {
            Portal portMan = Portal.Man;
            for (int i = 0; i < Key.Count; ++i)
            {
                PortalBlob port = portMan.BlobP(Key[i].RefId);
                port.PortalGate.RemoveKey(Type, Key[i]);
            }
        }

        public static GateType StrToType(string val)
        {
            for (int i = 0; i < TYPE.Length; ++i)
                if (val == TYPE[i]) return (GateType)i;
            return GateType.Pet;
        }

        public static string TypeToStr(GateType val)
        {
            return TYPE[(int)val];
        }
    }
}