namespace Binaree.Game.Object
{
    using Base;
    using Component;
    using Core;
    using Factory;
    using Object;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class PortalObj : PropObj
    {
        public SpriteRenderer PortalImage;
        public GameObject SatellitePos;

        override public GameObject GetSpawn(int spawnIndex) { return SatellitePos; }

        override public void Use()
        {
            base.Use();
            Step.Man.Layer.AssignObjToWorld(this, LayerControl.World.Ground);
        }
    }
}
