namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.Object;
    using Binaree.Game.Ui;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;

    public class GateComp
    {
        public float Power = 0.0f;
        public float RareBonus = 1.0f;
    }
    
    public class GateBlob : GameBlob
    {
        public PortalBlob Owner { get; private set; }

        private GateComp leagueInfo = new GateComp();
        private GateComp petInfo = new GateComp();

        private Dictionary<int, PortalKeyComp> leagueKey = new Dictionary<int, PortalKeyComp>();
        private Dictionary<int, PortalKeyComp> petKey = new Dictionary<int, PortalKeyComp>();

        public GateBlob(PortalBlob port)
        {
            Owner = port;
        }

        public bool CanOpen()
        {
            if (leagueKey.Count == 0 || leagueInfo.Power < 0.0f) return false;
            return true;
        }

        public float LeaguePower()
        {
            if (leagueInfo.Power > 1.0f) return 1.0f;
            return leagueInfo.Power;
        }

        public float PetPower()
        {
            float pow = Mathf.Abs(petInfo.Power);
            if (petInfo.Power > 1.0f) return 1.0f;
            return pow;
        }

        public bool PetPowerNeg() { return petInfo.Power < 0.0f; }

        public float LeagueRareBonus()
        {
            return leagueInfo.RareBonus;
        }

        public float PetRareBonus()
        {
            return petInfo.RareBonus;
        }

        public void AddKey(GateType type, PortalKeyComp key)
        {
            if (type == GateType.League) AddKeyTo(leagueKey, key, leagueInfo);
            else if (type == GateType.Pet) AddKeyTo(petKey, key, petInfo);
        }

        public void RemoveKey(GateType type, PortalKeyComp key)
        {
            if (type == GateType.League) RemoveKeyFrom(leagueKey, key, leagueInfo);
            else if (type == GateType.Pet) RemoveKeyFrom(petKey, key, petInfo);
        }

        private void AddKeyTo(Dictionary<int, PortalKeyComp> dict, PortalKeyComp key, GateComp info)
        {
            if(dict.ContainsKey(key.RefNum))
            {
                Say.Warn("Cannot add key to gate that already contains key with ref #" + key.RefNum);
                return;
            }
            dict.Add(key.RefNum, key);
            info.Power += key.Power.Value(key.AddLevel);
            info.RareBonus += key.RareBonus.Value(key.AddLevel);
        }

        private void RemoveKeyFrom(Dictionary<int, PortalKeyComp> dict, PortalKeyComp key, GateComp info)
        {
            if (!dict.ContainsKey(key.RefNum))
            {
                Say.Warn("Cannot remove key from gate that does not contain key with ref #" + key.RefNum);
                return;
            }
            dict.Remove(key.RefNum);
            info.Power -= key.Power.Value(key.AddLevel);
            info.RareBonus -= key.RareBonus.Value(key.AddLevel);
        }
    }
}