namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.User;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class TankSlot : GameBlob
    {
        public UserTankSlot User { get; private set; }
        public TankBlob TankCreature { get; private set; }

        public TankSlot(UserTankSlot user, int slotNum)
        {
            SetDirectRef(slotNum);
            User = user;
            if(User.SlotState == (int)UserRemoteSlot.State.Fill)
            {
                CreatureData cd = Creature.Man.DataC(User.CreatureRefId);
                TankData con = Tank.Man.DataRef(User.CreatureRefId);
                TankCreature = new TankBlob(cd, con, this);
                TankCreature.AssignRank(User.PetRank, User.PetLevel);
            }
            else
            {
                TankCreature = null;
            }
        }

        public bool IsUnlocked() { return User.SlotState != (int)UserTankSlot.State.Locked; }
        public bool IsOpen() { return User.SlotState == (int)UserTankSlot.State.Open; }
        public bool IsFull() { return User.SlotState == (int)UserTankSlot.State.Fill; }

        public void Activate()
        {
        }

        public void Deactivate()
        {
        }

        public TankBlob AssignCreature(CreatureBlob cr)
        {
            if(TankCreature != null)
            {
                Say.Warn("Cannot assign a creature to a full tank!");
                return null;
            }
            TankData t = Tank.Man.DataRef(cr.Data.RefIdFromId(Creature.DATA_HEADER));
            TankCreature = new TankBlob(cr.Data, t, this);
            TankCreature.AssignRank(cr.Rank, cr.SizeLevel);
            User.AssignCreature(cr);
            return TankCreature;
        }

        public void AssignWait(float waitTime)
        {
            User.AssignTime((int)waitTime);
        }

        public void EmptyTank()
        {
            TankCreature = null;
            User.RemoveCreature();
        }
    }
}