namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Xml;
    using Utility;

    public enum CreatureHit
    {
        Nibbler = 0,
        Striker = 1,
        Joker = 2,
        Gulper = 3,
    }

    public class HitData : GameData
    {
        private static readonly string[] HIT = { "n", "s", "j", "g" };

        public CreatureHit Hit { get; private set; }
        public float StillAttract { get; private set; }
        public float MoveAttract { get; private set; }
        public SplitInt NibCount { get; private set; }
        public SplitF NibTime { get; private set; }
        public SplitF NibHitChance { get; private set; }
        public SplitF NibLoseChance { get; private set; }
        public SplitF BiteTime { get; private set; }
        public SplitF ActionTime { get; private set; }

        public HitData(XmlNode xData) : base(xData)
        {
            Hit = StrToHit(xData.Attributes.GetNamedItem("hit").Value);
            StillAttract = Convert.ToSingle(xData.Attributes.GetNamedItem("still").Value);
            MoveAttract = Convert.ToSingle(xData.Attributes.GetNamedItem("move").Value);
            NibCount = new SplitInt(xData.Attributes.GetNamedItem("nib").Value);
            NibTime = new SplitF(xData.Attributes.GetNamedItem("nibT").Value);
            NibHitChance = new SplitF(xData.Attributes.GetNamedItem("nibH").Value);
            NibLoseChance = new SplitF(xData.Attributes.GetNamedItem("nibL").Value);
            BiteTime = new SplitF(xData.Attributes.GetNamedItem("bite").Value);
            ActionTime = new SplitF(xData.Attributes.GetNamedItem("step").Value);
        }

        public static CreatureHit StrToHit(string val)
        {
            for (int i = 0; i < HIT.Length; ++i)
                if (val == HIT[i]) return (CreatureHit)i;
            return CreatureHit.Nibbler;
        }

        public static string HitToStr(CreatureRarity val)
        {
            return HIT[(int)val];
        }
    }
}