namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public enum MoveType
    {
        Standard = 0,
        Running = 1,
        Thrashing = 2,
        Exhausted = 3,
        Warning = 4,
    }

    public enum PatternType
    {
        Loop = 0,
        Single = 1,
        Reverse = 2,
    }

    public class MoveData : GameData
    {
        private static readonly string[] MOVE_TYPE = { "s", "r", "t", "e", "w" };
        private static readonly string[] PATTERN_TYPE = { "l", "s", "r" };

        public List<float> Power { get; private set; }
        public List<float> Recover { get; private set; }
        public float StandardPollTime { get; private set; }
        public float TensionPollTime { get; private set; }
        public List<SplitF> MoveTime { get; private set; }
        public float StaminaActMin { get; private set; }
        public SplitF BaseLoss { get; private set; }

        public PatternType PatternType { get; private set; }
        public List<MoveType> Pattern { get; private set; }

        public MoveData(XmlNode xData) : base(xData)
        {
            StandardPollTime = Convert.ToSingle(xData.Attributes.GetNamedItem("stPoll").Value);
            TensionPollTime = Convert.ToSingle(xData.Attributes.GetNamedItem("tenPoll").Value);
            StaminaActMin = Convert.ToSingle(xData.Attributes.GetNamedItem("sm").Value);
            BaseLoss = new SplitF(xData.Attributes.GetNamedItem("loss").Value);

            Power = new List<float>();
            Stringy.ParseList<float>(xData.Attributes.GetNamedItem("pow").Value, Power);
            while (Power.Count < MOVE_TYPE.Length)
                Power.Add(0.0f);

            Recover = new List<float>();
            Stringy.ParseList<float>(xData.Attributes.GetNamedItem("rec").Value, Recover);
            while (Recover.Count < MOVE_TYPE.Length)
                Recover.Add(0.0f);

            MoveTime = new List<SplitF>();
            MoveTime.Add(new SplitF(xData.Attributes.GetNamedItem("st").Value));
            MoveTime.Add(new SplitF(xData.Attributes.GetNamedItem("rt").Value));
            MoveTime.Add(new SplitF(xData.Attributes.GetNamedItem("tt").Value));
            MoveTime.Add(new SplitF(xData.Attributes.GetNamedItem("et").Value));
            MoveTime.Add(new SplitF(xData.Attributes.GetNamedItem("wt").Value));

            PatternType = StrToPatternType(xData.Attributes.GetNamedItem("pType").Value);
            Pattern = new List<MoveType>();
            string[] split = xData.Attributes.GetNamedItem("pat").Value.Split(',');
            for (int i = 0; i < split.Length; ++i)
                Pattern.Add(StrToMoveType(split[i]));
        }

        public static MoveType StrToMoveType(string val)
        {
            for (int i = 0; i < MOVE_TYPE.Length; ++i)
                if (val == MOVE_TYPE[i]) return (MoveType)i;
            return MoveType.Standard;
        }

        public static string MoveTypeToStr(MoveType val)
        {
            return MOVE_TYPE[(int)val];
        }

        public static PatternType StrToPatternType(string val)
        {
            for (int i = 0; i < PATTERN_TYPE.Length; ++i)
                if (val == PATTERN_TYPE[i]) return (PatternType)i;
            return PatternType.Loop;
        }

        public static string PatternTypeToStr(PatternType val)
        {
            return PATTERN_TYPE[(int)val];
        }
    }
}