namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public enum CreatureRarity
    {
        Common = 0,
        Rare = 1,
        Legend = 2,
    }

    public enum CreatureSize
    {
        Tiny = 0,
        ExtraSmall = 1,
        Small = 2,
        Medium = 3,
        Large = 4,
        ExtraLarge = 5,
        Giant = 6,
        Colossus = 7,
    }

    public class CreatureData : GameData
    {
        private static readonly string[] RARITY = { "c", "r", "l" };
        private static readonly string[] SIZE = { "t", "xS", "s", "m", "l", "xl", "xxl", "xxxl" };

        public int RefNum { get; private set; }
        public CreatureRarity Rarity { get; private set; }
        public List<float> RankRate { get; private set; }
        public List<float> SizeChart { get; private set; }
        public float ScScalar { get; private set; }
        public float MarkScalar { get; private set; }

        public CreatureData(XmlNode xData) : base(xData)
        {
            RefNum = Convert.ToInt32(xData.Attributes.GetNamedItem("num").Value);
            Rarity = StrToRarity(xData.Attributes.GetNamedItem("rare").Value);

            RankRate = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("rank").Value, RankRate);

            SizeChart = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("size").Value, SizeChart);

            ScScalar = Convert.ToSingle(xData.Attributes.GetNamedItem("sc").Value);
            MarkScalar = Convert.ToSingle(xData.Attributes.GetNamedItem("mark").Value);
        }

        public static CreatureRarity StrToRarity(string val)
        {
            for (int i = 0; i < RARITY.Length; ++i)
                if (val == RARITY[i]) return (CreatureRarity)i;
            return CreatureRarity.Common;
        }

        public static string RarityToStr(CreatureRarity val)
        {
            return RARITY[(int)val];
        }

        public static CreatureSize StrToHookSize(string val)
        {
            for (int i = 0; i < SIZE.Length; ++i)
                if (val == SIZE[i]) return (CreatureSize)i;
            return CreatureSize.Medium;
        }

        public static string HookSizeToStr(CreatureSize val)
        {
            return SIZE[(int)val];
        }
    }
}