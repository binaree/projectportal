namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiSlotTile : UiTileObj
    {
        public enum State
        {
            Locked = 0,
            Empty = 1,
            Ready = 2,
            Wait = 3,
        }

        public Image Selected;

        public GameObject UsedPanel;
        public Image Icon;
        public UiStarRank Rank;
        public Text Level;
        public GameObject ReadyPanel;
        public Text ReadyLabel;
        public GameObject WaitPanel;
        public Text WaitLabel;

        public GameObject EmptyPanel;
        public Text EmptyLabel;

        public GameObject LockPanel;
        public Text LockLabel;

        protected State state = State.Locked;
        protected UiSlotPanel targetPanel = null;

        public State GetState() { return state; }
        public bool IsUsed() { return state == State.Ready || state == State.Wait; }

        virtual public string IconDir() { return string.Empty; }
        virtual public GameBlob GetSlot() { return null; }

        virtual public void AssignTarget(UiSlotPanel target)
        {
            targetPanel = target;
        }

        public void AssignUnused(int userState)
        {
            CloseAll();
            if (userState == (int)State.Locked)
                SetLocked();
            else if (userState == (int)State.Empty)
                SetEmpty();
        }

        virtual public void AssignCreature(CreatureData cr, int rank, float size, float wait)
        {
            CloseAll();
            SetFull(cr, rank, size, wait);
        }

        virtual public void AssignSelect(bool turnOn)
        {
            if (turnOn && targetPanel != null)
                targetPanel.ShowSlot(GetSlot());
            Selected.gameObject.SetActive(turnOn);
        }

        protected void CloseAll()
        {
            UsedPanel.SetActive(false);
            EmptyPanel.SetActive(false);
            LockPanel.SetActive(false);
        }

        protected void SetLocked()
        {
            state = State.Locked;
            LockPanel.SetActive(true);
        }

        protected void SetEmpty()
        {
            state = State.Empty;
            EmptyPanel.SetActive(true);
        }

        protected void SetFull(CreatureData cr, int rank, float size, float wait)
        {
            if (wait <= 0.0f) SetReady(cr);
            else SetWait(cr, wait);
            
            Spritey.ReplaceIcon(Icon, IconDir(), cr.Id);
            Rank.AssignRank(rank, Table.Man.Creature.MaxRank);
            Level.text = Stringy.FormatSize(size);
            UsedPanel.SetActive(true);
        }

        protected void SetReady(CreatureData cr)
        {
            state = State.Ready;
            ReadyPanel.SetActive(true);
            WaitPanel.SetActive(false);
            ReadyLabel.text = Script.Man.Name(cr.Id);
        }

        protected void SetWait(CreatureData cr, float wait)
        {
            state = State.Wait;
            ReadyPanel.SetActive(false);
            WaitPanel.SetActive(true);
            WaitLabel.text = Stringy.FormatTime(wait);
        }
    }
}
