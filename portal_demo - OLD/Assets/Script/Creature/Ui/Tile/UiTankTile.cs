namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.User;
    using Core;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiTankTile : UiSlotTile
    {
        public const string PREFAB = "tile_tank";

        public TankSlot slot = null;

        override public string RefId() { return (slot.TankCreature == null) ? string.Empty : slot.TankCreature.DataC.Id; }
        override public string IconDir() { return "creatureui/"; }
        override public GameBlob GetSlot() { return slot; }

        override public void AssignBlob(GameBlob b)
        {
            slot = b as TankSlot;
            if (slot.User.SlotState == (int)UserRemoteSlot.State.Fill)
            {
                AssignCreature(slot.TankCreature.DataC, slot.TankCreature.Rank, slot.TankCreature.Size, slot.TankCreature.TimeToUse());
            }
            else
                AssignUnused(slot.User.SlotState);
            ReadyLabel.text = "Extract";
            DeselectTile();
        }

        override public void SelectTile()
        {
            AssignSelect(true);
        }

        override public void DeselectTile()
        {
            AssignSelect(false);
        }
    }
}
