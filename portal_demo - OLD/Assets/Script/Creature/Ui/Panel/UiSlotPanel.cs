namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiSlotPanel : UiObj
    {
        public GameObject ThePanel;
        public GameObject UsedPanel;
        public Text Name;
        public Image Icon;
        public UiStarRank Rank;
        public Text Level;
        public GameObject ReadyPanel;
        public Text ReadyLabel;
        public GameObject WaitPanel;
        public Text WaitLabel;

        public GameObject EmptyPanel;

        public GameObject LockPanel;

        public UiTileSetObj Tile;

        private UiSlotTile.State state = UiSlotTile.State.Locked;

        virtual public string IconDir() { return string.Empty; }
        virtual public string PrefabName() { return string.Empty; }
        virtual public bool IsEquipped() { return false; }

        public UiSlotTile.State GetState() { return state; }
        public bool IsUsed() { return state == UiSlotTile.State.Ready || state == UiSlotTile.State.Wait; }

        virtual public void ShowSlot(GameBlob b)
        {
            
        }

        virtual public void Deselect()
        {
            ThePanel.SetActive(false);
        }

        virtual public UiSlotTile AddTile(GameBlob b)
        {
            UiSlotTile t = Tile.AddNewItem(PrefabName(), b) as UiSlotTile;
            if(t != null) t.AssignTarget(this);
            return t;
        }

        virtual public void AssignUnused(int userState)
        {
            Tile.DeselectAll();
            CloseAll();
            ThePanel.SetActive(true);
            if (userState == (int)UiSlotTile.State.Locked)
                SetLocked();
            else if (userState == (int)UiSlotTile.State.Empty)
                SetEmpty();
        }

        virtual public void AssignCreature(CreatureData cr, int rank, float level, float wait)
        {
            Tile.DeselectAll();
            CloseAll();
            ThePanel.SetActive(true);
            SetFull(cr, rank, level, wait);
        }

        virtual public void SelectApply()
        {
            Say.Log("Equip or extract will go here");
        }

        virtual public void SelectSkip()
        {
            Say.Log("Speed up slot time will go here");
        }

        virtual public void SelectUnlock()
        {
            Say.Log("Unlock the slot will go here");
        }

        protected void CloseAll()
        {
            UsedPanel.SetActive(false);
            EmptyPanel.SetActive(false);
            LockPanel.SetActive(false);
        }

        virtual protected void SetLocked()
        {
            state = UiSlotTile.State.Locked;
            LockPanel.SetActive(true);
        }

        virtual protected void SetEmpty()
        {
            state = UiSlotTile.State.Empty;
            EmptyPanel.SetActive(true);
        }

        virtual protected void SetFull(CreatureData cr, int rank, float level, float wait)
        {
            UsedPanel.SetActive(true);
            if (wait <= 0.0f) SetReady(cr);
            else SetWait(cr, wait);
            
            Spritey.ReplaceIcon(Icon, IconDir(), cr.Id);
            Rank.AssignRank(rank, Table.Man.Creature.MaxRank);
            Level.text = Stringy.FormatSize(Table.Man.Creature.CreatureSize(cr, rank, level));
            Name.text = Script.Man.Name(cr.Id);
        }

        virtual protected void SetReady(CreatureData cr)
        {
            state = UiSlotTile.State.Ready;
            ReadyPanel.SetActive(true);
            WaitPanel.SetActive(false);
        }

        virtual protected void SetWait(CreatureData cr, float wait)
        {
            state = UiSlotTile.State.Wait;
            ReadyPanel.SetActive(false);
            WaitPanel.SetActive(true);
            WaitLabel.text = Stringy.FormatTime(wait);
        }
    }
}
