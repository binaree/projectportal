namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class StatBlob : GameBlob
    {
        public StatData Data { get; private set; }
        public float Power { get; private set; }
        public float Speed { get; private set; }
        public float Recover { get; private set; }
        public float Consume { get; private set; }
        public float MaxStamina { get; private set; }
        public float Stamina { get; private set; }

        public StatBlob(StatData d)
        {
            Data = d;
            Power = Speed = Recover = Consume = MaxStamina = Stamina = 0.0f;
        }

        public void AssignCreature(CreatureBlob fish)
        {
            float lvl = fish.SizeLevel;
            Power = Data.Power.Value(lvl);
            Speed = Data.Speed.Value(lvl);
            Recover = Data.Recover.Value(lvl);
            Consume = Data.Consume.Value(lvl);
            MaxStamina = Stamina = Data.Stamina.Value(lvl);
        }

        public bool RestoreStamina(float amtScale)
        {
            Stamina += Recover * amtScale * TimeLord.Inst.ArenaDelta;
            if (Stamina >= MaxStamina)
            {
                Stamina = MaxStamina;
                return true;
            }
            return false;
        }

        public bool TakeStamina(float amtScale)
        {
            Stamina -= Consume * amtScale * TimeLord.Inst.ArenaDelta;
            if (Stamina <= 0.0f)
            {
                Stamina = 0.0f;
                return true;
            }
            return false;
        }
    }
}