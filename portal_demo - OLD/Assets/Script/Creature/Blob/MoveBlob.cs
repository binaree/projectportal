namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class MoveBlob : GameBlob
    {
        public MoveData Data { get; private set; }
        public MoveType Style { get; private set; }
        public float BaseLoss { get; private set; }

        private TensionTable tension = null;
        private BattleTable battle = null;
        private CreatureBlob cr = null;
        private int curPattern = 0;
        private int patDir = 1;
        private float moveTime = 0.0f;
        private float testTime = 0.0f;
        private float limitTime = 0.0f;

        public MoveBlob(MoveData d)
        {
            Data = d;
        }

        public void AssignCreature(CreatureBlob f)
        {
            tension = Table.Man.Tension;
            battle = Table.Man.Battle;
            cr = f;
            BaseLoss = Data.BaseLoss.Value(cr.SizeLevel);
        }

        public void Begin()
        {
            SetCurrentPattern();
            SetMoveTime();
            SetLimitTime();
        }

        public void UpdateMove()
        {
            if (UpdatePollTime())
                return;

            moveTime -= TimeLord.Inst.ArenaDelta;
            if (moveTime <= 0.0f)
                SetCurrentPattern();
            ApplyStyle();
        }

        public void ExhaustCreature()
        {
            SetMoveStyle(MoveType.Exhausted);
        }

        private void SetCurrentPattern()
        {
            if (curPattern >= Data.Pattern.Count) return;

            SetMoveStyle( Data.Pattern[curPattern] );
            if (patDir == 1)
            {
                curPattern++;
                if (curPattern >= Data.Pattern.Count)
                {
                    LoopPattern();
                }
            }
            else
            {
                curPattern--;
                if (curPattern < 0)
                {
                    LoopPattern();
                }
            }
        }

        private void SetMoveStyle(MoveType mt)
        {
            if (mt == MoveType.Running || mt == MoveType.Thrashing || mt == MoveType.Warning)
            {
                if(cr.Stats.Stamina < Data.StaminaActMin)
                {
                    SetMoveStyle(MoveType.Standard);
                    return;
                }
            }

            Style = mt;
            moveTime = Data.MoveTime[(int)Style].Rand();
            if (Style == MoveType.Warning)
                moveTime = battle.ActionAdjust(moveTime);
            else if (Style == MoveType.Running || Style == MoveType.Thrashing)
                moveTime = battle.ControlAdjust(moveTime);
        }

        private void LoopPattern()
        {
            if (Data.PatternType == PatternType.Loop)
            {
                curPattern = 0;
            }
            else if (Data.PatternType == PatternType.Reverse)
            {
                if( patDir == 1)
                {
                    patDir = -1;
                    curPattern = Data.Pattern.Count - 1;
                }
                else
                {
                    patDir = 1;
                    curPattern = 0;
                }
            }
        }

        private void ApplyStyle()
        {
            tension.ApplyCreatureMove(cr, Data.Power[(int)Style], !(Style == MoveType.Thrashing || Style == MoveType.Exhausted));
            cr.Stats.RestoreStamina(Data.Recover[(int)Style]);
        }

        private void SetMoveTime()
        {
            testTime = battle.StandardPollAdjust(Data.StandardPollTime);
        }

        private void SetLimitTime()
        {
            limitTime = battle.LimitPollAdjust(Data.TensionPollTime);
        }

        private bool UpdatePollTime()
        {
            testTime -= TimeLord.Inst.ArenaDelta;
            if (testTime <= 0.0f)
            {
                if (TestTension(false))
                    return true;
                SetMoveTime();
            }

            limitTime -= TimeLord.Inst.ArenaDelta;
            if (tension.IsLimit && limitTime <= 0.0f)
            {
                if (TestTension(true))
                    return true;
                SetLimitTime();
            }
            return false;
        }

        private bool TestTension(bool isLimit)
        {
            return tension.CheckTension(isLimit);
        }
    }
}