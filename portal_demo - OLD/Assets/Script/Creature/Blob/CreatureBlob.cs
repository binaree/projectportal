namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class CreatureBlob : GameBlob
    {
        public CreatureData Data { get; private set; }
        public StatBlob Stats { get; private set; }
        public HitBlob HitAi { get; private set; }
        public MoveBlob MoveAi { get; private set; }

        public int Rank { get; private set; }
        public float Size { get; private set; }
        public CreatureSize SizeType { get; private set; }
        public float SizeLevel { get; private set; }

        public CreatureBlob(CreatureData d)
        {
            Data = d;
        }

        public void Activate(int rank, float size)
        {
            Rank = rank;
            Size = size;
            SizeType = Table.Man.Probe.SizeByCreature(Size);
            SizeLevel = (Size - Data.SizeChart[rank]) / (Data.SizeChart[rank + 1] - Data.SizeChart[rank]);

            Stats = Stat.Man.CreateForCreature(this);
            HitAi = Hit.Man.CreateForCreature(this);
            MoveAi = Move.Man.CreateForCreature(this);
        }

        public void Clear()
        {

        }

        public bool IsTrophy() { return Rank == Table.Man.Creature.MaxRank; }
    }
}