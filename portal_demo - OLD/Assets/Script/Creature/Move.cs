namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Move : DataManager
    {
        public const string DATA_HEADER = "move_";
        
        private static Move _instance = new Move();

        public static Move Man { get { return _instance; } }

        public MoveData Data(string id)
        {
            return GetData(id) as MoveData;
        }

        public MoveData DataM(string id)
        {
            return GetData(DATA_HEADER + id) as MoveData;
        }

        public MoveBlob Blob(int refNum)
        {
            return GetBlob(refNum) as MoveBlob;
        }

        public MoveBlob CreateForCreature(CreatureBlob cr)
        {
            MoveData d = DataM(cr.Data.Id);
            MoveBlob b = GenerateBlob(d) as MoveBlob;
            b.AssignCreature(cr);
            return b;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "move/m");
        }

        protected override GameData NewData(XmlNode node)
        {
            MoveData d = new MoveData(node);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new MoveBlob(d as MoveData);
        }
    }
}
