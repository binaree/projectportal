namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    using Binaree.Game.Ui;

    public class CreatureTable : TableData
    {
        public int MaxRank { get; private set; }
        public List<string> LaunchAction { get; private set; }
        public List<string> ReactionAction { get; private set; }

        public CreatureTable(XmlNode xData) : base(xData)
        {
            MaxRank = Convert.ToInt32(xData.Attributes.GetNamedItem("maxRank").Value);

            LaunchAction = new List<string>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("launch").Value, LaunchAction);

            ReactionAction = new List<string>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("react").Value, ReactionAction);
        }

        public float RandomCreatureSize(CreatureData cr, int rank, float sizeMod)
        {
            float sizeMin = cr.SizeChart[rank];
            float sizeMax = cr.SizeChart[rank + 1];
            float size = Randomize.Range(sizeMin, sizeMax) * sizeMod;
            if (size < sizeMin) size = sizeMin;
            if (size > sizeMax) size = sizeMax;
            return size;
        }

        public float CreatureSize(CreatureData cr, int rank, float level)
        {
            return cr.SizeChart[rank] + (level * (cr.SizeChart[rank + 1] - cr.SizeChart[rank]));
        }

        public void HandleRelease(CreatureData cr, int rank, float level, Action completeAction)
        {
            List<RewardBlob> rew = Table.Man.Reward.FormReleaseReward(cr, rank, level);
            Table.Man.Reward.GiveReward();

            UiRewardOverlay over = UiMain.Man.SelectOverlay(UiRewardOverlay.PAGE_ID) as UiRewardOverlay;
            over.AssignRewards(rew, completeAction);
        }
    }
}