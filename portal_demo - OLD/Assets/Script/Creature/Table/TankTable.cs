namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;

    public class TankTable : TableData
    {
        private float tankTimeScale = 1.0f;
        private List<SplitF> tankTime = new List<SplitF>();

        private float rewardTriggerScale = 1.0f;
        private float bonusTriggerScale = 1.0f;
        private float bonusAmountScale = 1.0f;

        public TankTable(XmlNode xData) : base(xData)
        {
            tankTimeScale = Convert.ToSingle(xData.Attributes.GetNamedItem("ts").Value);
            Stringy.ParseSplitFList(xData.Attributes.GetNamedItem("time").Value, tankTime);

            rewardTriggerScale = Convert.ToSingle(xData.Attributes.GetNamedItem("rt").Value);
            bonusTriggerScale = Convert.ToSingle(xData.Attributes.GetNamedItem("bt").Value);
            bonusAmountScale = Convert.ToSingle(xData.Attributes.GetNamedItem("ba").Value);
        }

        public void AddToTank(CreatureBlob cr)
        {
            TankSlot slot = Tank.Man.FindOpenSlot();
            if(slot == null) { Say.Warn("No empty tanks to add creature to!"); return; }
                        
            slot.AssignCreature(cr);
            StartConversion(slot);
        }

        public void StartConversion(TankSlot slot)
        {
            TankBlob tb = slot.TankCreature;
            float tt = tankTime[tb.Rank].Value(tb.SizeLevel);
            tt *= tb.DataT.TankTimeScale;
            tt *= tankTimeScale;
            slot.AssignWait(tt);
        }

        public void PerformConversion(TankSlot tank)
        {
            if (!tank.IsFull()) { Say.Warn("There is no creature in the tank to convert!"); return; }

            tank.TankCreature.DetermineRewards(rewardTriggerScale, bonusTriggerScale, bonusAmountScale);
            tank.EmptyTank();
        }
    }
}