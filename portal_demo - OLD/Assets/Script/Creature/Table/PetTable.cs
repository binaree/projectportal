namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;

    public class PetTable : TableData
    {
        private float restTimeScale = 1.0f;
        private List<SplitF> restTime = new List<SplitF>();

        public PetTable(XmlNode xData) : base(xData)
        {
            restTimeScale = Convert.ToSingle(xData.Attributes.GetNamedItem("ts").Value);
            Stringy.ParseSplitFList(xData.Attributes.GetNamedItem("time").Value, restTime);
        }

        public bool AddToRemote(CreatureBlob cr)
        {
            PetSlot slot = Pet.Man.FindOpenSlot();
            if(slot == null) { Say.Warn("No empty remote slots to add creature to!"); return false; }
                        
            slot.AssignPet(cr);
            return true;
        }

        public void PortalOpened()
        {
            PetBlob pet = Rig.Man.TheRemote.Pet;
            float tt = restTime[pet.Rank].Value(pet.SizeLevel);
            tt *= pet.DataP.RestTimeScale;
            tt *= restTimeScale;
            pet.Slot.AssignRest(tt);
        }
    }
}