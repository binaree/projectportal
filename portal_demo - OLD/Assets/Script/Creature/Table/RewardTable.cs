namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    
    public class RewardTable : TableData
    {
        public List<float> CatchRarityScale { get; private set; }
        public List<float> ReleaseRarityScale { get; private set; }

        public List<RewardBlob> CurrentReward { get; private set; }

        private Dictionary<int, List<LootBoxComp>> catchReward = new Dictionary<int, List<LootBoxComp>>();
        private Dictionary<int, List<LootBoxComp>> releaseReward = new Dictionary<int, List<LootBoxComp>>();
        private bool hasAwarded = false;

        public RewardTable(XmlNode xData) : base(xData)
        {
            string[] split = xData.Attributes.GetNamedItem("cat").Value.Split(';');
            for(int i = 0; i < split.Length; ++i)
            {
                catchReward.Add(i, new List<LootBoxComp>());
                string[] sub = split[i].Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                for (int s = 0; s < sub.Length; ++s)
                    catchReward[i].Add(new LootBoxComp(sub[s]));
            }

            split = xData.Attributes.GetNamedItem("rel").Value.Split(';');
            for (int i = 0; i < split.Length; ++i)
            {
                releaseReward.Add(i, new List<LootBoxComp>());
                string[] sub = split[i].Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                for (int s = 0; s < sub.Length; ++s)
                    releaseReward[i].Add(new LootBoxComp(sub[s]));
            }

            CatchRarityScale = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("scCat").Value, CatchRarityScale);

            ReleaseRarityScale = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("scRel").Value, ReleaseRarityScale);
        }

        public List<RewardBlob> FormCatchReward(CreatureBlob c)
        {
            CurrentReward = new List<RewardBlob>();
            List<LootBoxComp> loot = catchReward[c.Rank];
            CurrentReward = Reward.Man.FormFromBox(loot, CatchRarityScale[(int)c.Data.Rarity], c.SizeLevel);
            hasAwarded = false;
            return CurrentReward;
        }

        public List<RewardBlob> FormReleaseReward(CreatureData c, int rank, float level)
        {
            CurrentReward = new List<RewardBlob>();
            List<LootBoxComp> loot = releaseReward[rank];
            CurrentReward = Reward.Man.FormFromBox(loot, CatchRarityScale[(int)c.Rarity], level);
            hasAwarded = false;
            return CurrentReward;
        }

        public void GiveReward()
        {
            if (CurrentReward == null) return;
            if (hasAwarded) return;
            for (int i = 0; i < CurrentReward.Count; ++i)
                CurrentReward[i].Give();
            hasAwarded = true;
        }

        public int DetermineCurrencyReward(Currency.Type curType)
        {
            if (CurrentReward == null || CurrentReward.Count == 0) return -1;
            for (int i = 0; i < CurrentReward.Count; ++i)
                if (CurrentReward[i].Data.Type == RewardType.Currency && CurrentReward[i].RewardSubNum == (int)curType)
                    return CurrentReward[i].Amount;
            return -1;
        }
    }

}