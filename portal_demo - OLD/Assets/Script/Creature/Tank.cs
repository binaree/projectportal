namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Tank : DataManager
    {
        public const string DATA_HEADER = "tank_";
        public const string FULL_HEADER = DATA_HEADER + "cr_";

        public TankSlot Active { get; private set; }
        public List<TankSlot> Slot { get; private set; }
        
        private static Tank _instance = new Tank();

        public static Tank Man { get { return _instance; } }

        public TankData Data(string id)
        {
            return GetData(id) as TankData;
        }

        public TankData DataT(string id)
        {
            return GetData(DATA_HEADER + id) as TankData;
        }

        public TankData DataRef(string fullRefId)
        {
            return GetData(FULL_HEADER + fullRefId) as TankData;
        }

        public TankSlot SetActiveSlot(int slotNum)
        {
            if (slotNum >= Slot.Count) return null;
            if (Active != null)
                Active.Deactivate();
            Active = Slot[slotNum];
            Active.Activate();
            return Active;
        }

        public TankSlot FindOpenSlot()
        {
            for (int i = 0; i < Slot.Count; ++i)
                if (Slot[i].IsOpen())
                    return Slot[i];
            return null;
        }

        public TankSlot GenerateSlotFromUser(UserTankSlot user)
        {
            TankSlot ps = new TankSlot(user, Slot.Count);
            Slot.Add(ps);
            return ps;
        }

        public void Load(XmlDocument xml)
        {
            Active = null;
            Slot = new List<TankSlot>();
            LoadData(xml, "tank/t");
        }

        protected override GameData NewData(XmlNode node)
        {
            TankData d = new TankData(node);
            return d;
        }
    }
}
