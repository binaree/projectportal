namespace Binaree.Game.Factory
{
    using Component;
    using Core;
    using Base;
    using System.Collections.Generic;
    using Object;
    
    public class PetFactory : GameFactory
    {
        public const string PET_HEADER = "pet_";

        public string PetDirectory = "pet";
        public int PreloadAmount = 1;

        private static PetFactory _instance = null;

        public static PetFactory Inst { get { return _instance; } }

        override public void Generate()
        {
            _instance = this;
            List<GameData> allPet = Pet.Man.AllGameData();
            for(int i = 0; i < allPet.Count; ++i)
            {
                if (store.ContainsKey(allPet[i].Id)) continue;
                TrackPrefab(allPet[i].Id, PreloadAmount);
            }
            hasGenerated = true;
        }

        override protected string FormatTypeToPrefab(string objType) { return /*HERO_HEADER +*/ objType; }
        override protected string PrefabDirectory() { return PetDirectory + "/"; }

        public PetObj TakePet(string objType) { return Take(objType) as PetObj; }
    }
}
