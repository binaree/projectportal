namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;

    /// <summary>
    /// Base class for a table that allows design to pass data and alter algorithms which effect blobs
    /// </summary>
    public class TableData : GameData
    {
        public TableData(XmlNode xData) : base(xData)
        {
        }
    }
}