namespace Binaree.Game.Component
{
    using Binaree.Game.Core;
    using System;
    using Table;
    using Ui;

    public class StepBattle : StepBase
    {
        public enum Lost
        {
            Distance = 0,
            LinePower = 1,
            Tension = 2,
        }

        public const string STEP_ID = "battle";

        override public void Begin()
        {
            Table.Man.Battle.StartBattle();
            UiMain.Man.SelectPage(UiBattlePage.PAGE_ID);
            StartBattle();
        }

        override public void End()
        {

        }

        public void StartBattle()
        {
            Table.Man.Tension.StartTension();
        }

        public void Catch()
        {
            Rig.Man.TheSatellite.ReleaseSatellite();
            Step.Man.Begin(StepCatch.STEP_ID);
        }

        public void LostCreature(Lost method)
        {
            UiOkOverlay over = UiMain.Man.SelectOverlay(UiOkOverlay.PAGE_ID) as UiOkOverlay;
            if (method == Lost.Distance)
                over.SetData("lost_cr", "lost_depth", "ok");
            else if (method == Lost.LinePower)
                over.SetData("lost_cr", "lost_power", "ok");
            else if (method == Lost.Tension)
                over.SetData("lost_cr", "lost_tension", "ok");

            Creature.Man.ClearActive();
            Rig.Man.TheSatellite.ReleaseSatellite();
            Step.Man.Begin(StepSelect.STEP_ID);
        }
    }
}