namespace Binaree.Game.Component
{
    using Binaree.Game.Core;
    using System;
    using Table;
    using Ui;

    public class StepCatch : StepBase
    {
        public const string STEP_ID = "catch";

        private UiOverlay startOverlay = null;

        override public void Begin()
        {
            Table.Man.Guide.HandleCatch(Creature.Man.Active);
            Table.Man.Reward.FormCatchReward(Creature.Man.Active);
            Table.Man.Reward.GiveReward();

            startOverlay = null;
            UiMain.Man.Header.SetVisibility(false);
            UiMain.Man.SelectPage(UiCatchPage.PAGE_ID);
        }

        override public void End()
        {
            UiMain.Man.Header.SetVisibility(true);
        }

        public void ReleaseCatch(UiOverlay overlay)
        {
            startOverlay = overlay;
            CreatureBlob cr = Creature.Man.Active;
            Table.Man.Creature.HandleRelease(cr.Data, cr.Rank, cr.SizeLevel, Resume);
        }

        public void KeepCatch()
        {
            PetSlot ps = Pet.Man.FindOpenSlot();
            if(ps == null)
            {
                // TODO: Add message and go to remote overlay
                return;
            }
            ps.AssignPet(Creature.Man.Active);
            Resume();
        }

        public void ConvertCatch()
        {
            TankSlot ts = Tank.Man.FindOpenSlot();
            if(ts == null)
            {
                // TODO: Add message and go to tank overlay
                return;
            }
            ts.AssignCreature(Creature.Man.Active);
            Resume();
        }

        private void Resume()
        {
            Creature.Man.ClearActive();
            if (startOverlay != null)
                startOverlay.CloseOverlay();
            Step.Man.Begin(StepSelect.STEP_ID);
        }
    }
}