namespace Binaree.Game.Component
{
    using Binaree.Game.Core;
    using System;
    using Table;
    using Ui;

    public class StepWait : StepBase
    {
        public enum Lost
        {
            BadProbe = 0,
            BadDepth = 1,
            TookProbe = 2,
            FailSet = 3,
        }
        public const string STEP_ID = "wait";

        override public void Begin()
        {
            Table.Man.Wait.StartWait();
            UiMain.Man.Header.EnableCurrency(false);
            UiMain.Man.Header.EnablePortal(Portal.Man.Active, false);
            UiMain.Man.SelectPage(UiWaitPage.PAGE_ID);
            RestartWait();
        }

        override public void End()
        {

        }

        public void RestartWait()
        {
            Table.Man.Attract.EstablishDepth(true);
        }

        public void AttemptSet()
        {
            if (Table.Man.Set.AttemptSet())
                Step.Man.Begin(StepBattle.STEP_ID);
        }

        public void LostCreature(Lost method)
        {
            UiOkOverlay over = UiMain.Man.SelectOverlay(UiOkOverlay.PAGE_ID) as UiOkOverlay;
            if (method == Lost.BadProbe)
                over.SetData("no_cr", "bad_probe", "ok");
            else if (method == Lost.BadDepth)
                over.SetData("no_cr", "bad_depth", "ok");
            else if (method == Lost.TookProbe)
                over.SetData("lost_probe", "took_probe", "ok");
            else if (method == Lost.FailSet)
                over.SetData("lost_probe", "fail_set", "ok");

            DrawIn();
        }

        public void DrawIn()
        {
            Creature.Man.ClearActive();
            Rig.Man.TheSatellite.ReleaseSatellite();
            Step.Man.Begin(StepSelect.STEP_ID);
        }
    }
}