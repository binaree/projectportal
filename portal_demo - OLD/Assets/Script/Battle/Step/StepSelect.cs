namespace Binaree.Game.Component
{
    using Binaree.Game.Core;
    using System;
    using Ui;

    public class StepSelect : StepBase
    {
        public const string STEP_ID = "select";

        override public void Begin()
        {
            UiMain.Man.Header.SetVisibility(true);
            UiMain.Man.Header.EnablePortal(Portal.Man.Active, true);
            UiMain.Man.SelectPage(UiSelectPage.PAGE_ID);
        }

        override public void End()
        {

        }

        public void StartDeploy()
        {
            Step.Man.Begin(StepDeploy.STEP_ID);
        }
    }
}