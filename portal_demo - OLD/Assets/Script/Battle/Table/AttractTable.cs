namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;

    public class AttractTable : TableData
    {
        public List<float> BaseOccurence { get; private set; }
        public List<float> BaseAttract { get; private set; }
        public List<float> BaseRankRate { get; private set; }
        public List<float> BaseSizeRate { get; private set; }

        private List<RegionBlob> region = null;

        public AttractTable(XmlNode xData) : base(xData)
        {
            BaseOccurence = new List<float>();
            Stringy.ParseList<float>(xData.Attributes.GetNamedItem("occ").Value, BaseOccurence);

            BaseAttract = new List<float>();
            Stringy.ParseList<float>(xData.Attributes.GetNamedItem("att").Value, BaseAttract);

            BaseRankRate = new List<float>();
            Stringy.ParseList<float>(xData.Attributes.GetNamedItem("rank").Value, BaseRankRate);

            BaseSizeRate = new List<float>();
            Stringy.ParseList<float>(xData.Attributes.GetNamedItem("size").Value, BaseSizeRate);
        }

        public void EstablishDepth(bool init = false)
        {
            float depth = Table.Man.Depth.CurrentDepth;
            region = new List<RegionBlob>();
            List<RegionBlob> all = Portal.Man.Active.Region;
            for(int i = 0; i < all.Count; ++i)
            {
                if(all[i].IsValid(depth))
                {
                    all[i].Activate(init);
                    region.Add(all[i]);
                }
            }
        }

        public void MoveDepth()
        {
            float depth = Table.Man.Depth.CurrentDepth;
            for(int i = region.Count - 1; i >= 0; --i)
            {
                if(!region[i].IsValid(depth))
                {
                    region[i].Close();
                    region.RemoveAt(i);
                }
            }
        }

        public bool UpdateAttract()
        {
            if (Creature.Man.Active != null) return false;
            
            for (int i = region.Count - 1; i >= 0; --i)
            {
                if (region[i].UpdateRegion())
                {
                    Say.Log("Checking for creature in region = " + region[i].Data.Id);
                    CreatureBlob cr = AttractCreature(region[i]);
                    if (cr != null)
                    {
                        Table.Man.Set.AssignAttractedCreature(cr, region[i]);
                        region = null;
                        return true;
                    }
                }
            }
            return false;
        }

        private CreatureBlob AttractCreature(RegionBlob reg)
        {
            List<RegionCreature> present = FindOccuringCreature(reg);
            if (present.Count < 1) return null;
            //Say.Log("Number of occuring creature is " + present.Count);

            ProbeCreature attract = AttractedCreature(reg, present);
            if (attract == null) return null;
            //Say.Log("Bait has attrated creature " + attract.RefId);

            return CreateAttractedCreature(attract);
        }

        private List<RegionCreature> FindOccuringCreature(RegionBlob reg)
        {
            List<RegionCreature> occ = new List<RegionCreature>();
            List<RegionCreature> all = reg.Data.Creature;
            for(int i = 0; i < all.Count; ++i)
            {
                CreatureData d = Creature.Man.DataC(all[i].RefId);
                float check = all[i].Occurence * reg.Data.BaseOccurence * BaseOccurence[(int)d.Rarity];
                if (Randomize.Low(check))
                    occ.Add(all[i]);
            }
            return occ;
        }

        private ProbeCreature AttractedCreature(RegionBlob reg, List<RegionCreature> occ)
        {
            List<ProbeCreature> pc = Rig.Man.TheProbe.Data.Creature;
            float lineDetect = Rig.Man.TheLine.Data.Detection;
            for( int i = occ.Count - 1; i >= 0; --i)
            {
                for( int j = pc.Count - 1; j >= 0; --j)
                {
                    if (occ[i].RefId != pc[j].RefId) continue;
                    CreatureData d = Creature.Man.DataC(occ[i].RefId);
                    float att = pc[j].Attraction * lineDetect * reg.Data.BaseAttract * BaseAttract[(int)d.Rarity];
                    if (Randomize.Low(att))
                    {
                        return pc[j];
                    }
                }
            }
            return null;
        }

        private CreatureBlob CreateAttractedCreature(ProbeCreature pc)
        {
            CreatureData d = Creature.Man.DataC(pc.RefId);
            CreatureBlob b = Creature.Man.GenerateCreature(d);
            int rank = PickRank(d, pc);
            float size = PickSize(d, pc, rank);
            b.Activate(rank, size);
            Say.Log("You attracted a " + d.Id + " of rank = " + rank + " & size = " + size);
            return b;
        }

        private int PickRank(CreatureData d, ProbeCreature pc)
        {
            float checkMod = pc.RankMod * BaseRankRate[(int)d.Rarity];
            checkMod *= Rig.Man.TheSatellite.Data.LvlRankMod(Rig.Man.TheSatellite.Level);

            for (int i = d.RankRate.Count -1; i >= 0; --i)
            {
                if (Randomize.Low(d.RankRate[i] * checkMod))
                    return i + 1;
            }

            return 1;
        }

        private float PickSize(CreatureData d, ProbeCreature pc, int rank)
        {
            float sizeMod = pc.SizeMod * BaseSizeRate[(int)d.Rarity];
            sizeMod *= Rig.Man.TheSatellite.Data.LvlSizeMod(Rig.Man.TheSatellite.Level);

            return Table.Man.Creature.RandomCreatureSize(d, rank, sizeMod);
        }
    }
}