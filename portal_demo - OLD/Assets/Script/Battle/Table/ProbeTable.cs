namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;

    public class ProbeTable : TableData
    {
        public List<float> SizeChart { get; private set; }
        public float DecayBase { get; private set; }
        public float TensionBase { get; private set; }
        public float LimitBase { get; private set; }

        public ProbeTable(XmlNode xData) : base(xData)
        {
            SizeChart = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("size").Value, SizeChart);
            DecayBase = Convert.ToSingle(xData.Attributes.GetNamedItem("decay").Value);
            TensionBase = Convert.ToSingle(xData.Attributes.GetNamedItem("ten").Value);
            LimitBase = Convert.ToSingle(xData.Attributes.GetNamedItem("lim").Value);
        }

        public CreatureSize SizeByCreature(float creatureSize)
        {
            for(int i = 0; i < SizeChart.Count; ++i)
            {
                if (creatureSize < SizeChart[i])
                    return (CreatureSize)i;
            }
            return CreatureSize.Colossus;
        }

        public float SetChance(CreatureBlob c)
        {
            ProbeData probe = Rig.Man.TheProbe.Data;
            float set = probe.SetRarity(c.Data.Rarity);
            if (c.SizeType == probe.Size)
                return set;
            int dist = Math.Abs((int)c.SizeType - (int)probe.Size);
            set -= probe.SizeDecay * DecayBase * dist;
            if (set < 0.0f)
                set = 0.0f;
            return set;
        }

        public float TensionChance(CreatureBlob c, bool isLimit)
        {
            ProbeData probe = Rig.Man.TheProbe.Data;
            float set = probe.SetRarity(c.Data.Rarity) * ((isLimit) ? LimitBase : TensionBase);
            if (c.SizeType == probe.Size)
                return set;
            int dist = Math.Abs((int)c.SizeType - (int)probe.Size);
            set -= probe.SizeDecay * DecayBase * dist;
            if (set < 0.0f)
                set = 0.0f;
            return set;
        }
    }
}