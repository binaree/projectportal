namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;

    public class SetTable : TableData
    {
        public SplitF MissDelay { get; private set; }
        public List<float> BaseSet { get; private set; }
        public List<float> HitSet { get; private set; }
        public List<float> ControlSet { get; private set; }

        private DepthTable depth = null;
        private CreatureBlob cr = null;
        private RegionBlob region = null;

        public SetTable(XmlNode xData) : base(xData)
        {
            MissDelay = new SplitF(xData.Attributes.GetNamedItem("delay").Value);

            BaseSet = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("base").Value, BaseSet);

            HitSet = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("set").Value, HitSet);

            ControlSet = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("con").Value, ControlSet);
        }

        public void AssignAttractedCreature(CreatureBlob attract, RegionBlob crRegion)
        {
            depth = Table.Man.Depth;
            cr = attract;
            region = crRegion;
        }

        public bool UpdateHit(bool isMoving)
        {
            if (cr == null) return false;
            if (depth.CurrentDepth < region.Data.Depth.Min())
            {
                ReleaseCreature();
                return false;
            }

            cr.HitAi.UpdateHit(isMoving);
            return false;
        }

        public bool AttemptSet()
        {
            if (cr == null) return false;
            float baseHit = cr.HitAi.BaseHitChance() * BaseSet[(int)cr.Data.Rarity];
            float set = Rig.Man.TheProbe.DetermineSet(cr) * HitSet[(int)cr.Data.Rarity];
            float control = Rig.Man.TheSatellite.Data.LvlControl(Rig.Man.TheSatellite.Level) * ControlSet[cr.Rank];
            float hitChance = baseHit * set * control;
            Say.Log("Set chance = " + hitChance);
            
            if( Randomize.Low(hitChance))
            {
                cr.HitAi.AttachProbe();
                return true;
            }
            else
            {
                if( cr.HitAi.MissHit(MissDelay.Rand()) )
                {
                    Step.Man.Wait.LostCreature(StepWait.Lost.FailSet);
                }
            }
            return false;
        }

        private void ReleaseCreature()
        {
            cr = null;
            region = null;
            Table.Man.Attract.EstablishDepth();
        }
    }
}