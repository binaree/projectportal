namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    using Binaree.Game.Ui;

    public class TensionTable : TableData
    {
        public float Tension { get; private set; }
        public float MaxTension { get; private set; }
        public float TensionLevel { get { return Tension / MaxTension; } }
        public bool IsLimit { get; private set; }
        public int LimitCount { get; private set; }

        public float LimitTension { get; private set; }
        public SplitF RodStrength { get; private set; }
        public float RemotePower { get; private set; }
        public float CreaturePower { get; private set; }
        public float LineMax { get; private set; }
        public float RemoteDrag { get; private set; }
        public float RemoteTension { get; private set; }
        public float CreatureTension { get; private set; }
        public float StaminaRestore { get; private set; }
        public float StaminaConsume { get; private set; }

        public List<float> StandardLoss { get; private set; }
        public List<float> LimitLoss { get; private set; }
        public float LimitScale { get; private set; }

        private DepthTable depth = null;
        private CreatureBlob cr = null;
        private float remotePow = 0.0f;
        private float crPow = 0.0f;
        private bool crMoving = false;
        private float crMaxPower = 0.0f;
        private float dragTension = 0.0f;

        public TensionTable(XmlNode xData) : base(xData)
        {
            LimitTension = Convert.ToSingle(xData.Attributes.GetNamedItem("limit").Value);
            RodStrength = new SplitF(xData.Attributes.GetNamedItem("str").Value);

            RemotePower = Convert.ToSingle(xData.Attributes.GetNamedItem("rem").Value);
            CreaturePower = Convert.ToSingle(xData.Attributes.GetNamedItem("cr").Value);
            LineMax = Convert.ToSingle(xData.Attributes.GetNamedItem("lMax").Value);
            RemoteDrag = Convert.ToSingle(xData.Attributes.GetNamedItem("rDrag").Value);
            RemoteTension = Convert.ToSingle(xData.Attributes.GetNamedItem("rt").Value);
            CreatureTension = Convert.ToSingle(xData.Attributes.GetNamedItem("ct").Value);
            StaminaConsume = Convert.ToSingle(xData.Attributes.GetNamedItem("con").Value);
            StaminaRestore = Convert.ToSingle(xData.Attributes.GetNamedItem("rest").Value);

            StandardLoss = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("sLoss").Value, StandardLoss);

            LimitLoss = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("lLoss").Value, LimitLoss);
            LimitScale = Convert.ToSingle(xData.Attributes.GetNamedItem("lSc").Value);
        }

        public void StartTension()
        {
            depth = Table.Man.Depth;
            cr = Creature.Man.Active;
            Tension = remotePow = crPow = 0.0f;
            MaxTension = RodStrength.Value(Rig.Man.TheSatellite.Data.LvlStrength(Rig.Man.TheSatellite.Level));
            IsLimit = crMoving = false;
            LimitCount = 0;
            crMaxPower = Rig.Man.TheLine.Data.MaxPower * LineMax;
            dragTension = Rig.Man.TheRemote.Data.LvlDragTension(Rig.Man.TheRemote.Level) * RemoteDrag;
        }

        public void ApplyRemotePower(float pow, float powLvl)
        {
            remotePow = pow * RemotePower;
        }

        public void ApplyCreatureMove(CreatureBlob creat, float pow, bool isMoving)
        {
            crMoving = isMoving;
            crPow = pow * creat.Stats.Power * CreaturePower;
            if (crPow >= crMaxPower)
            {                
                Step.Man.Battle.LostCreature(StepBattle.Lost.LinePower);
            }
        }

        public void UpdateTension()
        {
            if (Creature.Man.Active == null) return;

            float pow = (remotePow - crPow) * dragTension;
            Tension = (remotePow * RemoteTension) + (crPow * CreatureTension);
            if(pow > 0.0f)
            {
                depth.ApplyDraw(pow);
            }
            else
            {
                //Say.Log("Style is " + cr.MoveAi.Style);
                if (cr.MoveAi.Style != MoveType.Thrashing)
                    depth.ApplyDrag(-pow);
                //else
                //    Say.Log("Should not be moving because of thrash");
            }
            DetermineTensionLimit();

            cr.Stats.RestoreStamina(StaminaRestore);
            if (cr.Stats.TakeStamina(Math.Abs(pow) * StaminaConsume))
                cr.MoveAi.ExhaustCreature();
            //Say.Log("Creature stamina is " + cr.Stats.Stamina);
        }

        public bool CheckTension(bool isLimitCheck)
        {
            //if (isLimitCheck && !IsLimit) return false;

            float baseLose = cr.MoveAi.BaseLoss;
            baseLose *= (isLimitCheck) ? LimitLoss[(int)cr.Rank] : StandardLoss[(int)cr.Rank];
            if (isLimitCheck)
                baseLose += LimitScale * LimitCount++;
            float hook = Table.Man.Probe.TensionChance(cr, isLimitCheck);
            //float control = Rig.Man.TheRod.Data.LvlControl(Rig.Man.TheRod.Level) * ControlHit[fish.IndexRank];
            float loseChance = baseLose * hook;
            //Say.Log("Lose chance = " + loseChance + " with limit check: " + isLimitCheck + ", " + baseLose + ", " + hook);

            if (Randomize.Low(loseChance))
            {
                Step.Man.Battle.LostCreature(StepBattle.Lost.Tension);
                return true;
            }
            return false;
        }

        private bool DetermineTensionLimit()
        {
            if (TensionLevel >= LimitTension)
            {
                if(IsLimit == false)
                {
                    IsLimit = true;
                    LimitCount = 0;
                }
            }
            else
            {
                if(IsLimit == true)
                {
                    IsLimit = false;
                }
            }
            return IsLimit;
        }
    }
}