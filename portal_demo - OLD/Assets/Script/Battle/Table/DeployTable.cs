namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    
    public class DeployTable : TableData
    {
        public float MarkerSpeed { get; private set; }

        public DeployTable(XmlNode xData) : base(xData)
        {
            MarkerSpeed = Convert.ToSingle(xData.Attributes.GetNamedItem("speed").Value);
        }
        
        public float MarkerMoveSpeed()
        {
            return MarkerSpeed * TimeLord.Inst.ArenaDelta;
        }

        public float MinDeployDistance()
        {
            float maxReel = Table.Man.Depth.MaxDistance();
            float sinkerScale = Rig.Man.TheRocket.Data.Depth.Min();
            return maxReel * sinkerScale;
        }

        public float MaxDeployDistance()
        {
            float maxReel = Table.Man.Depth.MaxDistance();
            float sinkerScale = Rig.Man.TheRocket.Data.Depth.Max();
            return maxReel * sinkerScale;
        }

        public float DeployDistance(float sinkPercent)
        {
            float maxReel = Table.Man.Depth.MaxDistance();
            float sinkerScale = Rig.Man.TheRocket.Data.Depth.Value(sinkPercent);
            return maxReel * sinkerScale;
        }
    }
}