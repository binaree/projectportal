namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Reward : DataManager
    {
        public const string DATA_HEADER = "rew_";
        public const string COLLECTION_HEADER = DATA_HEADER + "col_";
        public const string CRYSTAL_HEADER = DATA_HEADER + "cry_";
        public const string ATTRACTANT_HEADER = DATA_HEADER + "sup_attr_";
        public const string LINE_HEADER = DATA_HEADER + "sup_line_";
        public const string ROCKET_HEADER = DATA_HEADER + "sup_rock_";
        public const string CARD_HEADER = DATA_HEADER + "card_";

        private static Reward _instance = new Reward();

        public static Reward Man { get { return _instance; } }

        public RewardData Data(string id)
        {
            return GetData(id) as RewardData;
        }

        public RewardData DataR(string id)
        {
            return GetData(DATA_HEADER + id) as RewardData;
        }

        public RewardData DataCollection(string refId)
        {
            return GetData(COLLECTION_HEADER + refId) as RewardData;
        }

        public RewardData DataCrystal(string refId)
        {
            return GetData(CRYSTAL_HEADER + refId) as RewardData;
        }

        public RewardData DataAttractant(string refId)
        {
            return GetData(ATTRACTANT_HEADER + refId) as RewardData;
        }

        public RewardData DataLine(string refId)
        {
            return GetData(LINE_HEADER + refId) as RewardData;
        }

        public RewardData DataRocket(string refId)
        {
            return GetData(ROCKET_HEADER + refId) as RewardData;
        }

        public RewardData DataCard(string refId)
        {
            return GetData(ROCKET_HEADER + refId) as RewardData;
        }

        public RewardBlob GenerateReward(string rewardRefId, bool isBonus = false, float bonusAmount = 1.0f, float level = -1.0f)
        {
            RewardData d = DataR(rewardRefId);
            return new RewardBlob(d, isBonus, bonusAmount, level);
        }

        public List<RewardBlob> FormFromBox(List<LootBoxComp> box, float scaleBonus = 1.0f, float level = -1.0f)
        {
            List<RewardBlob> list = new List<RewardBlob>();
            for (int i = 0; i < box.Count; ++i)
            {
                if (!box[i].IsTrigger()) continue;
                RewardBlob rew = Reward.Man.GenerateReward(box[i].RewardId, box[i].IsBonus(), box[i].BonusAmount * scaleBonus, level);
                list.Add(rew);
            }
            return list;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "reward/r");
        }

        protected override GameData NewData(XmlNode node)
        {
            RewardData d = new RewardData(node);
            return d;
        }
    }
}
