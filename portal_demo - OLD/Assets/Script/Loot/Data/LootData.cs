namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public enum LootType
    {
        Standard = 0,
        Premium = 1,
        Reward = 2,
        Table = 3,
    }

    public class LootData : GameData
    {
        public static readonly string[] TYPE = { "std", "pr", "rew", "tab" };

        public LootType Type { get; private set; }

        public LootData(XmlNode xData) : base(xData)
        {
            Type = StrToType(xData.Attributes.GetNamedItem("type").Value);
        }

        public static LootType StrToType(string val)
        {
            for (int i = 0; i < TYPE.Length; ++i)
                if (val == TYPE[i]) return (LootType)i;
            return LootType.Standard;
        }

        public static string TypeToStr(LootType val)
        {
            return TYPE[(int)val];
        }
    }
}