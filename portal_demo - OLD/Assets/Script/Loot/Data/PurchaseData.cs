namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class PurchaseData : GameData
    {
        public string Cost { get; private set; }
        public CurrencyComp Give { get; private set; }

        public PurchaseData(XmlNode xData) : base(xData)
        {
            Cost = xData.Attributes.GetNamedItem("cost").Value;
            Give = new CurrencyComp(xData.Attributes.GetNamedItem("give").Value);
        }
    }
}