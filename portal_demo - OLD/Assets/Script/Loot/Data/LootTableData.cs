namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public enum LootTableStyle
    {
        LookUp = 0,
        Pick = 1,
        Attractant = 2,
        Line = 3,
        Rocket = 4,
        Crystal = 5,
        Card = 6,
    }

    public class LootTableData : LootData
    {
        public static readonly string[] STYLE = { "look", "pick", "attr", "line", "rock", "cry", "card" };

        public LootTableStyle Style { get; private set; }
        public List<string> Table { get; private set; }

        public LootTableData(XmlNode xData) : base(xData)
        {
            Style = StrToStyle(xData.Attributes.GetNamedItem("style").Value);

            Table = new List<string>();
            Stringy.ParseEmpty(xData.Attributes.GetNamedItem("tab").Value, Table);
        }

        public RewardData FindReward()
        {
            if (Style == LootTableStyle.Crystal) return Reward.Man.DataCrystal(RandEntry());
            else if (Style == LootTableStyle.Attractant) return Reward.Man.DataAttractant(RandEntry());
            else if (Style == LootTableStyle.Line) return Reward.Man.DataLine(RandEntry());
            else if (Style == LootTableStyle.Rocket) return Reward.Man.DataRocket(RandEntry());
            else if (Style == LootTableStyle.Card) return Reward.Man.DataCard(RandEntry());

            string entry = (Style == LootTableStyle.Pick) ? RandEntry() : WeightEntry();
            if (entry == string.Empty) { Say.Log("Could not find sub loot table in " + Table.ToString()); return null; }
            LootTableData tab = Loot.Man.DataTab(entry);
            if (tab == null) return null;
            return tab.FindReward();
        }

        public string RandEntry()
        {
            return Table[Randomize.Index(Table.Count)];
        }

        public string WeightEntry()
        {
            for(int i = Table.Count - 1; i >= 0; --i)
            {
                SplitTrig trig = new SplitTrig(Table[i]);
                if (trig.CheckLow())
                    return trig.Value;
            }
            return string.Empty;
        }

        public static LootTableStyle StrToStyle(string val)
        {
            for (int i = 0; i < STYLE.Length; ++i)
                if (val == STYLE[i]) return (LootTableStyle)i;
            return LootTableStyle.Pick;
        }

        public static string StyleToStr(LootTableStyle val)
        {
            return STYLE[(int)val];
        }
    }
}