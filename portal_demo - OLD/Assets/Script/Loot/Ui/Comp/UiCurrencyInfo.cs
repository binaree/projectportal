namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiCurrencyInfo : UiObj
    {
        public const string DIR = "uiicon/";
        public static readonly string[] PREFAB = { "icon_sc", "icon_hc", "icon_mark" };

        public Image Icon;
        public Text Quantity;

        private void Awake()
        {
        }

        public void AssignCurrency(Currency.Type curType, int amount)
        {
            Spritey.ReplaceIcon(Icon, DIR, PREFAB[(int)curType]);
            Quantity.text = amount.ToString();
        }
    }
}
