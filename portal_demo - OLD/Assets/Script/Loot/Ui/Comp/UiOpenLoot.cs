namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiOpenLoot : UiObj
    {
        public Image BoxIcon;
        public GameObject CloseBackground;
        public GameObject OpenBackground;

        private Action complete = null;

        private void Awake()
        {
            gameObject.SetActive(false);
        }

        public void AssignLoot(LootBlob loot)
        {
            gameObject.SetActive(true);
            Spritey.ReplaceIcon(BoxIcon, UiLootTile.DIR, loot.Data.Prefab);
            CloseBackground.SetActive(true);
            OpenBackground.SetActive(false);
        }

        public void Open(Action completeAction)
        {
            complete = completeAction;

            // TODO: Do any box opening animation here
            CloseBackground.SetActive(false);
            OpenBackground.SetActive(true);

            Finish();
        }

        private void Finish()
        {
            if (complete != null)
                complete();
            gameObject.SetActive(false);
        }
    }
}
