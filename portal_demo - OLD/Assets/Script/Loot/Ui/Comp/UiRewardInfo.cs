namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiRewardInfo : UiObj
    {
        public Image Icon;
        public Text Name;
        public UiStarRank Rank;
        public Text Quantity;

        private void Awake()
        {
        }

        public void AssignReward(RewardBlob rew)
        {
            Spritey.ReplaceIcon(Icon, rew.IconDir(), rew.IconPrefab());
            Name.text = rew.RewardName();
            Quantity.text = "+ " + rew.Amount.ToString();
            if (rew.RewardWon == FinalReward.Crystal)
                Rank.AssignRank(rew.RewardSubNum, Table.Man.Creature.MaxRank);
            else
                Rank.gameObject.SetActive(false);
        }
    }
}
