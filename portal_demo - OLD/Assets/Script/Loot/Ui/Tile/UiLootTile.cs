namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiLootTile : UiTileObj
    {
        public const string DIR = "lootui/";
        public const string PREFAB = "tile_loot";

        public Image BoxIcon;
        public Text BoxName;
        public Text BoxInfo;

        public UiCurrencyInfo Cost;

        private LootBlob loot = null;

        override public void AssignBlob(GameBlob theLoot)
        {
            loot = theLoot as LootBlob;

            Spritey.ReplaceIcon(BoxIcon, DIR, loot.Data.Prefab);
            BoxName.text = Script.Man.Name(loot.Data.Id);
            BoxInfo.text = Script.Man.Text(loot.Data.Id);

            Cost.AssignCurrency(Currency.Type.Hard, loot.HardCost());
        }

        public override void SelectTile()
        {
            if(!loot.Acquire())
            {
                UiConfirmOverlay over = UiMain.Man.SelectOverlay(UiConfirmOverlay.PAGE_ID) as UiConfirmOverlay;
                over.SetData("no_hc", "no_hc", "cancel", "buy", GoToBuy);
                return;
            }

        }

        private void GoToBuy()
        {
            UiMain.Man.SelectOverlay(UiCurrencyOverlay.PAGE_ID);
        }
    }
}
