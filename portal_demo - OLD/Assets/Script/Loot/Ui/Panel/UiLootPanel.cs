namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiLootPanel : UiObj
    {
        public UiTileSetObj Tile;

        public void Activate()
        {
            Tile.Initialize();

            List<LootBlob> loot = Loot.Man.PremiumLoot();
            for (int i = 0; i < loot.Count; ++i)
                AddTile(loot[i]);
        }

        public UiLootTile AddTile(LootBlob b)
        {
            UiLootTile t = Tile.AddNewItem(UiLootTile.PREFAB, b) as UiLootTile;
            return t;
        }
    }
}
