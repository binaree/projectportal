namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiPurchasePanel : UiObj
    {
        public UiTileSetObj Tile;

        public void Activate()
        {
            Tile.Initialize();

            List<PurchaseData> purchase = Purchase.Man.AllPurchases();
            for (int i = 0; i < purchase.Count; ++i)
                AddTile(purchase[i]);
        }

        public UiPurchaseTile AddTile(PurchaseData d)
        {
            UiPurchaseTile t = Tile.AddNewItem(UiPurchaseTile.PREFAB, null) as UiPurchaseTile;
            if (t == null) return t;
            t.AssignPurchase(d);
            return t;
        }
    }
}
