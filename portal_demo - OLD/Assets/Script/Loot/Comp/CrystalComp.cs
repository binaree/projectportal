namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class CrystalComp
    {
        public string RefId;
        public int Rank;
        public SplitF Amount;

        public CrystalComp(string val)
        {
            string[] split = val.Split('|');
            if (split.Length > 0) RefId = split[0];
            if (split.Length > 1) Rank = Convert.ToInt32(split[1]);
            if (split.Length > 2) Amount = new SplitF(split[2]);
        }
    }
}