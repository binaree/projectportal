namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class LootBoxComp
    {
        public string RewardId;
        public float TriggerChance;
        public float BonusChance;
        public float BonusAmount;

        public LootBoxComp(string val)
        {
            string[] split = val.Split('|');
            if (split.Length > 0) RewardId = split[0];
            if (split.Length > 1) TriggerChance = Convert.ToSingle(split[1]); else TriggerChance = 0.0f;
            if (split.Length > 2) BonusChance = Convert.ToSingle(split[2]); else BonusChance = 0.0f;
            if (split.Length > 3) BonusAmount = Convert.ToSingle(split[3]); else BonusAmount = 1.0f;
        }

        public bool IsTrigger() { return Randomize.Low(TriggerChance); }
        public bool IsBonus() { return Randomize.Low(BonusChance); }
    }
}