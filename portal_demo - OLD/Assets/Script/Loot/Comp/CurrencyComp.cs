namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class CurrencyComp
    {
        public Currency.Type Type;
        public SplitF Amount;

        public CurrencyComp(string val)
        {
            string[] split = val.Split('|');
            if (split.Length > 0) Type= (Currency.Type)Convert.ToInt32(split[0]);
            if (split.Length > 1) Amount = new SplitF(split[1]);
        }
    }
}