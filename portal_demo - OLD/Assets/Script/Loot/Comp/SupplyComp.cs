namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class SupplyComp
    {
        public LootTableStyle Style;
        public string RefId;
        public SplitF Amount;

        public SupplyComp(string val)
        {
            string[] split = val.Split('|');
            if (split.Length > 0) Style = LootTableData.StrToStyle( split[0] );
            if (split.Length > 1) RefId = split[1];
            if (split.Length > 2) Amount = new SplitF(split[2]);
        }
    }
}