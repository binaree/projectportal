namespace Binaree.Game.Factory
{
    using Core;
    using Fx;
    using Object;

    /// <summary>
    /// Handles and tracks the instantiation of backdrops
    /// </summary>
    public class FxFactory : GameFactory
    {
        public const string FLOAT_DAMAGE = "float_damage";
        public const string FLOAT_RESTORE = "float_restore";

        public const string FX_HEADER = "fx_";

        public string FxDirectory = "fx";
        public int PreloadAmount = 1;

        private static FxFactory _instance = null;

        public static FxFactory Inst { get { return _instance; } }

        override public void Generate()
        {
            _instance = this;
            TrackPrefab(FLOAT_DAMAGE, PreloadAmount);
            TrackPrefab(FLOAT_RESTORE, PreloadAmount);
            hasGenerated = true;
        }

        override protected string FormatTypeToPrefab(string objType) { return FX_HEADER + objType; }
        override protected string PrefabDirectory() { return FxDirectory + "/"; }

        public FxFloatText TakeFloatText(string objType) { return Take(objType) as FxFloatText; }
    }
}
