namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;

    public class GuideTable : TableData
    {
        private string newReward = "";
        private string rankReward = "";
        private string sizeReward = "";

        private string newPortal = "";
        private string sessionReward = "";

        public GuideTable(XmlNode xData) : base(xData)
        {
            newReward = xData.Attributes.GetNamedItem("new").Value;
            rankReward = xData.Attributes.GetNamedItem("rank").Value;
            sizeReward = xData.Attributes.GetNamedItem("size").Value;

            newPortal = xData.Attributes.GetNamedItem("pNew").Value;
            sessionReward = xData.Attributes.GetNamedItem("pSes").Value;
        }

        public void HandleCatch(CreatureBlob cr)
        {
            GuideCreatureBlob entry = Guide.Man.CreatureEntry(cr.Data.Id);
            if (entry == null) return;

            string rewId = "";
            bool isNew = false;
            if (entry.IsNewCreature()) { rewId = newReward + (int)cr.Rank; isNew = true; }
            else if (entry.IsNewRank(cr)) rewId = rankReward + (int)cr.Rank;
            else if (entry.IsSizeRecord(cr)) rewId = sizeReward + (int)cr.Rank;

            if (rewId != "")
            {
                RewardBlob rew = Reward.Man.GenerateReward(rewId, true, entry.Data.RewardScale);
                entry.SetReward(rew, isNew);
            }
            else
                entry.SetReward(null, isNew);

            entry.UpdateGuide(cr);
        }

        public void PortalOpened(PortalBlob port)
        {
            GuidePortalBlob entry = Guide.Man.PortalEntry(port.Data.Id);
            if (entry == null) return;

            string rewId = "";
            bool isNew = false;
            if (entry.IsNewPortal()) { rewId = newPortal; isNew = true; }
            else if (entry.IsLongestSession(port)) rewId = sessionReward;

            if (rewId != "")
            {
                RewardBlob rew = Reward.Man.GenerateReward(rewId, true, entry.Data.RewardScale);
                entry.SetReward(rew, isNew);
            }
            else
                entry.SetReward(null, isNew);

            entry.UpdateGuide(port);
        }
    }
}