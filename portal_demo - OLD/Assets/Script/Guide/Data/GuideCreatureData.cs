namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class GuideCreatureData : GuideData
    {
        public string GuideCompleteReward { get; private set; }
        public List<string> RankCollectReward { get; private set; }

        public GuideCreatureData(XmlNode xData) : base(xData)
        {
            Type = GuideType.Creature;
            GuideCompleteReward = xData.Attributes.GetNamedItem("rew").Value;

            RankCollectReward = new List<string>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("rank").Value, RankCollectReward);
        }
    }
}