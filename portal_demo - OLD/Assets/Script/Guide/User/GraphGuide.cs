namespace Binaree.Game.User
{
    using System.Xml;
    using System.Xml.Serialization;
    using Component;
    using System.Collections.Generic;
    
    [XmlRoot(GraphGuide.TAG)]
    public class GraphGuide : UserGraph
    {
        public const string TAG = "guide";

        [XmlElement(UserGuideCreature.TAG)]
        public List<UserGuideCreature> Creatures { get; private set; }

        [XmlElement(UserGuidePortal.TAG)]
        public List<UserGuidePortal> Portals { get; private set; }

        public GraphGuide() : base()
        {
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            for (int i = 0; i < Creatures.Count; ++i)
                Creatures[i].Load(this);

            for (int i = 0; i < Portals.Count; ++i)
                Portals[i].Load(this);

            Dirty();
        }
    }
}
