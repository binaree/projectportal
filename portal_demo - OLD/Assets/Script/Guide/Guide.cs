namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Guide : DataManager
    {
        public const string DATA_HEADER = "gui_";

        private Dictionary<string, GuideCreatureBlob> creature = new Dictionary<string, GuideCreatureBlob>();
        private Dictionary<string, GuidePortalBlob> portal = new Dictionary<string, GuidePortalBlob>();

        private static Guide _instance = new Guide();

        public static Guide Man { get { return _instance; } }

        public GuideData Data(string id)
        {
            return GetData(id) as GuideData;
        }

        public GuideData DataG(string id)
        {
            return GetData(DATA_HEADER + id) as GuideData;
        }

        public GuideBlob BlobCr(string id)
        {
            if (!creature.ContainsKey(id))
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return creature[id];
        }

        public GuideCreatureBlob CreatureEntry(string refId)
        {
            return BlobCr(DATA_HEADER + refId) as GuideCreatureBlob;
        }

        public GuideCreatureBlob GenerateCreatureFromUser(UserGuideCreature user)
        {
            GuideData d = DataG(user.RefId);
            GuideCreatureBlob b = new GuideCreatureBlob(d);
            b.LoadFromUser(user);
            creature.Add(d.Id, b);
            return b;
        }

        public GuidePortalBlob BlobPort(string id)
        {
            if (!portal.ContainsKey(id))
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return portal[id];
        }

        public GuidePortalBlob PortalEntry(string refId)
        {
            return BlobPort(DATA_HEADER + refId);
        }

        public GuidePortalBlob GeneratePortalFromUser(UserGuidePortal user)
        {
            GuideData d = DataG(user.RefId);
            GuidePortalBlob b = new GuidePortalBlob(d);
            b.LoadFromUser(user);
            portal.Add(d.Id, b);
            return b;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "guide/g");
        }

        protected override GameData NewData(XmlNode node)
        {
            GuideType type = GuideData.StrToType(node.Attributes.GetNamedItem("type").Value);
            if (type == GuideType.Creature) return new GuideCreatureData(node);
            else if (type == GuideType.Portal) return new GuidePortalData(node);
            return new GuideData(node);
        }
    }
}
