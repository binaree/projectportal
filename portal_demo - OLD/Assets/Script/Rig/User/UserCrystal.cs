namespace Binaree.Game.User
{
    using Component;
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;
    using Utility;

    public class UserCrystal : UserData
    {
        public const string TAG = "c";

        [XmlAttribute("id")]
        public string RefId { get; private set; }

        [XmlIgnore]
        public List<int> Quantity { get; private set; }

        [XmlAttribute("own")]
        public string QuantityStr
        {
            get { return Stringy.FormatList<int>(Quantity); }
            private set { Stringy.ParseList<int>(value, Quantity); }
        }

        public UserCrystal() : base()
        {
            Quantity = new List<int>();
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }
        
        public void SetQuantities(List<int> value)
        {
            Quantity = value;
            Dirty();
        }

        public void SetQuantity(int amt, int rank)
        {
            Quantity[rank] = amt;
            Dirty();
        }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            Crystal.Man.GenerateFromUser(this);

            Dirty();
        }
    }
}
