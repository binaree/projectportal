namespace Binaree.Game.User
{
    using System.Xml;
    using System.Xml.Serialization;
    using Component;
    using System.Collections.Generic;
    
    [XmlRoot(GraphSupply.TAG)]
    public class GraphSupply : UserGraph
    {
        public const string TAG = "supply";

        [XmlElement(UserSupply.TAG)]
        public List<UserSupply> Supply { get; private set; }

        public GraphSupply() : base()
        {
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            for (int i = 0; i < Supply.Count; ++i)
                Supply[i].Load(this);

            Dirty();
        }
    }
}
