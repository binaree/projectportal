namespace Binaree.Game.User
{
    using System.Xml;
    using System.Xml.Serialization;
    using Component;
    using System.Collections.Generic;
    using System;

    [XmlRoot(GraphRig.TAG)]
    public class GraphRig : UserGraph
    {
        public const string TAG = "rig";

        [XmlIgnore]
        public string RemoteRefId { get; private set; }

        [XmlIgnore]
        public int RemoteSlot { get; private set; }

        [XmlAttribute("rem")]
        public string RemoteStr
        {
            get { return ProbeRefId + ":" + RemoteSlot; }
            private set
            {
                string[] split = value.Split(':');
                RemoteRefId = split[0];
                RemoteSlot = Convert.ToInt32(split[1]);
            }
        }

        [XmlAttribute("sat")]
        public string SatelliteRefId { get; private set; }

        [XmlIgnore]
        public string ProbeRefId { get; private set; }

        [XmlIgnore]
        public string CrystalRefId { get; private set; }

        [XmlIgnore]
        public int CrystalRank { get; private set; }

        [XmlAttribute("pro")]
        public string ProbeStr
        {
            get { return ProbeRefId + ((CrystalRefId != string.Empty) ? (":" + CrystalRefId + ":" + CrystalRank) : string.Empty); }
            private set
            {
                string[] split = value.Split(':');
                ProbeRefId = split[0];
                if (split.Length > 2)
                {
                    CrystalRefId = split[1];
                    CrystalRank = Convert.ToInt32(split[2]);
                }
            }
        }

        [XmlAttribute("att")]
        public string AttractantRefId { get; private set; }

        [XmlAttribute("line")]
        public string LineRefId { get; private set; }

        [XmlAttribute("rock")]
        public string RocketRefId { get; private set; }

        public GraphRig() : base()
        {
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }

        public void SetRemote(RemoteBlob rem) { RemoteRefId = rem.Data.RefIdFromId(Remote.DATA_HEADER); Dirty(); }
        public void SetRemoteSlot(int slotNum) { RemoteSlot = slotNum; Dirty(); }
        public void SetSatellite(SatelliteBlob sat) { SatelliteRefId = sat.Data.RefIdFromId(Satellite.DATA_HEADER); Dirty(); }
        public void SetProbe(ProbeBlob pro) { ProbeRefId = pro.Data.RefIdFromId(Probe.DATA_HEADER); Dirty(); }
        public void SetAttractant(AttractantBlob att) { AttractantRefId = att.Data.RefIdFromId(Attractant.DATA_HEADER); Dirty(); }
        public void SetLine(LineBlob line) { LineRefId = line.Data.RefIdFromId(Line.DATA_HEADER); Dirty(); }
        public void SetRocket(RocketBlob rock) { RocketRefId = rock.Data.RefIdFromId(Rocket.DATA_HEADER); Dirty(); }

        public void SetProbeCrystal(string cryRef, int cryRank) { CrystalRefId = cryRef; CrystalRank = cryRank; Dirty(); }
        public void ClearProbeCrystal() { CrystalRefId = string.Empty; Dirty(); }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            Rig.Man.AssignRigGraph(this);

            Dirty();
        }
    }
}
