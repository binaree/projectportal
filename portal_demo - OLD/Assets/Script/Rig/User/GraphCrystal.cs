namespace Binaree.Game.User
{
    using System.Xml;
    using System.Xml.Serialization;
    using Component;
    using System.Collections.Generic;
    
    [XmlRoot(GraphCrystal.TAG)]
    public class GraphCrystal : UserGraph
    {
        public const string TAG = "crystal";

        [XmlElement(UserCrystal.TAG)]
        public List<UserCrystal> Crystal { get; private set; }

        public GraphCrystal() : base()
        {
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            for (int i = 0; i < Crystal.Count; ++i)
                Crystal[i].Load(this);

            Dirty();
        }
    }
}
