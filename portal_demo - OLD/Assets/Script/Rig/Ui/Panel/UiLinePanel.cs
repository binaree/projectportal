namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiLinePanel : UiRigPanel
    {
        public Text Quantity;

        public Text SellLabel;

        private LineBlob supply = null;

        override public string IconDir() { return UiSupplyTile.SUPPLY_DIR; }
        override public string PrefabName() { return UiSupplyTile.PREFAB; }
        override public bool IsEquipped() { return supply == Rig.Man.TheLine; }

        private void OnEnable()
        {
            Deselect();
            Tile.Initialize();
            List<LineBlob> list = Line.Man.AllOwned();
            LineBlob activeBlob = Rig.Man.TheLine;
            for (int i = 0; i < list.Count; ++i)
            {
                UiRigTile t = AddTile(list[i]);
                if (activeBlob == list[i])
                    t.SelectTile();
            }
        }

        override public void ShowBlob(RigBlob b)
        {
            supply = b as LineBlob;
            base.ShowBlob(b);
            Quantity.text = (supply.Owns()) ? "Qty: " + supply.Quantity.ToString() : "";
        }

        public override void Deselect()
        {
            base.Deselect();
            supply = null;
        }

        override public void SelectItem()
        {
            if (IsEquipped()) return;
            Rig.Man.AssignLine(supply);
            base.SelectItem();
        }

        public void SelectSell()
        {
            Say.Log("Sell has been selected");
        }
    }
}
