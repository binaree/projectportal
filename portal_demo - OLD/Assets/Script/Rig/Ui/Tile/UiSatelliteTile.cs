namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiSatelliteTile : UiRigTile
    {
        public const string PREFAB = "tile_satellite";

        public Text Level;

        private SatelliteBlob satellite = null;

        override public string RefId() { return satellite.Data.Id; }
        override public string IconDir() { return "satelliteui/"; }
        override public RigBlob GetBlob() { return satellite; }

        override public void AssignBlob(GameBlob b)
        {
            satellite = b as SatelliteBlob;
            LoadRig(RefId());
            Level.text = "Lvl: " + satellite.Level;
            DeselectTile();
        }

        override public void SelectTile()
        {
            AssignSelect(true);
        }

        override public void DeselectTile()
        {
            AssignSelect(false);
        }
    }
}
