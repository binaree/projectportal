namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiSupplyTile : UiRigTile
    {
        public const string PREFAB = "tile_supply";
        public const string SUPPLY_DIR = "supplyui/";

        public Text Quantity;

        public SupplyBlob supply = null;

        override public string RefId() { return supply.GetRigData().Id; }
        override public string IconDir() { return SUPPLY_DIR; }
        override public RigBlob GetBlob() { return supply; }

        override public void AssignBlob(GameBlob b)
        {
            supply = b as SupplyBlob;
            LoadRig(RefId());
            Quantity.text = (supply.Owns()) ? "Qty: " + supply.Quantity.ToString() : "";
            DeselectTile();
        }

        override public void SelectTile()
        {
            AssignSelect(true);
        }

        override public void DeselectTile()
        {
            AssignSelect(false);
        }
    }
}
