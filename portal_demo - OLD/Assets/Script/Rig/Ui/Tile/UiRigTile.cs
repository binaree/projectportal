namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiRigTile : UiTileObj
    {
        public Image Selected;
        public Image Icon;
        public Text Name;

        protected UiRigPanel targetPanel = null;

        virtual public string IconDir() { return string.Empty; }
        virtual public RigBlob GetBlob() { return null; }

        virtual public void AssignTarget(UiRigPanel target)
        {
            targetPanel = target;
        }

        virtual public void AssignSelect(bool turnOn)
        {
            if (turnOn && targetPanel != null)
                targetPanel.ShowBlob(GetBlob());
            Selected.gameObject.SetActive(turnOn);
        }

        virtual public void LoadRig(string iconName)
        {
            Name.text = Script.Man.Name(iconName);
            Spritey.ReplaceIcon(Icon, IconDir(), iconName);
        }
    }
}
