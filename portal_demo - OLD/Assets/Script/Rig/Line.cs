namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Line : DataManager
    {
        public const string DATA_HEADER = "line_";

        private List<LineBlob> allLine = new List<LineBlob>();
        private Dictionary<string, LineBlob> line = new Dictionary<string, LineBlob>();

        private static Line _instance = new Line();

        public static Line Man { get { return _instance; } }

        public LineData Data(string id)
        {
            return GetData(id) as LineData;
        }

        public LineData DataL(string id)
        {
            return GetData(DATA_HEADER + id) as LineData;
        }

        public LineBlob Blob(int refNum)
        {
            return GetBlob(refNum) as LineBlob;
        }

        public LineBlob Blob(string id)
        {
            if (!line.ContainsKey(id))
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return line[id];
        }

        public LineBlob BlobL(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public List<LineBlob> AllBlob() { return allLine; }

        public List<LineBlob> AllOwned()
        {
            List<LineBlob> own = new List<LineBlob>();
            for (int i = 0; i < allLine.Count; ++i)
                if (allLine[i].CanUse())
                    own.Add(allLine[i]);
            return own;
        }

        public LineBlob GenerateFromUser(UserSupply user)
        {
            LineData d = DataL(user.RefId);
            LineBlob b = GenerateRefBlob(d, d.RefNum) as LineBlob;
            b.LoadFromUser(user);
            line.Add(d.Id, b);
            allLine.Add(b);
            return b;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "line/l");
        }

        protected override GameData NewData(XmlNode node)
        {
            LineData d = new LineData(node);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new LineBlob(d as LineData);
        }
    }
}
