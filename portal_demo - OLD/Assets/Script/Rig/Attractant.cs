namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Attractant : DataManager
    {
        public const string DATA_HEADER = "attr_";

        private List<AttractantBlob> allAttract = new List<AttractantBlob>();
        private Dictionary<string, AttractantBlob> attract = new Dictionary<string, AttractantBlob>();

        private static Attractant _instance = new Attractant();

        public static Attractant Man { get { return _instance; } }

        public AttractantData Data(string id)
        {
            return GetData(id) as AttractantData;
        }

        public AttractantData DataA(string id)
        {
            return GetData(DATA_HEADER + id) as AttractantData;
        }

        public AttractantBlob Blob(int refNum)
        {
            return GetBlob(refNum) as AttractantBlob;
        }

        public AttractantBlob Blob(string id)
        {
            if (!attract.ContainsKey(id))
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return attract[id];
        }

        public AttractantBlob BlobA(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public List<AttractantBlob> AllAttractant() { return allAttract; }

        public List<AttractantBlob> AllOwned()
        {
            List<AttractantBlob> own = new List<AttractantBlob>();
            for (int i = 0; i < allAttract.Count; ++i)
                if (allAttract[i].CanUse())
                    own.Add(allAttract[i]);
            return own;
        }

        public AttractantBlob GenerateFromUser(UserSupply user)
        {
            AttractantData d = DataA(user.RefId);
            AttractantBlob b = GenerateRefBlob(d, d.RefNum) as AttractantBlob;
            b.LoadFromUser(user);
            attract.Add(d.Id, b);
            allAttract.Add(b);
            return b;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "attractant/a");
        }

        protected override GameData NewData(XmlNode node)
        {
            AttractantData d = new AttractantData(node);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new AttractantBlob(d as AttractantData);
        }
    }
}
