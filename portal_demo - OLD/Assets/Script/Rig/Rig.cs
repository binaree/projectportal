namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public enum GearRig
    {
        Probe = 0,
        Remote = 1,
        Satellite = 2,
    }

    public enum SupplyRig
    {
        Line = 0,
        Rocket = 1,
        Attractant = 2,
    }

    public class Rig
    {
        public const int GEAR_COUNT = 3;
        public const int SUPPLY_COUNT = 3;

        public ProbeBlob TheProbe { get; private set; }
        public RemoteBlob TheRemote { get; private set; }
        public SatelliteBlob TheSatellite { get; private set; }

        public LineBlob TheLine { get; private set; }
        public RocketBlob TheRocket { get; private set; }
        public AttractantBlob TheAttractant { get; private set; }

        public GraphRig Graph { get; private set; }

        private static Rig _instance = new Rig();

        public static Rig Man { get { return _instance; } }

        public Rig()
        {
        }

        public void AssignRigGraph(GraphRig userRig)
        {
            Graph = userRig;
            TheProbe = Probe.Man.BlobP(Graph.ProbeRefId);
            if(Graph.CrystalRefId != string.Empty)
            {
                TheProbe.AssignCrystal(Graph.CrystalRefId, Graph.CrystalRank);
            }
            TheRemote = Remote.Man.BlobR(Graph.RemoteRefId);
            Pet.Man.SetActiveSlot(Graph.RemoteSlot);
            TheSatellite = Satellite.Man.BlobS(Graph.SatelliteRefId);

            TheLine = Line.Man.BlobL(Graph.LineRefId);
            TheRocket = Rocket.Man.BlobR(Graph.RocketRefId);
            TheAttractant = Attractant.Man.BlobA(Graph.AttractantRefId);
        }

        public void AssignProbe(ProbeBlob probe)
        {
            TheProbe = probe;
            Graph.SetProbe(probe);
            if (TheProbe.Active != null)
                AssignProbeCrystal(TheProbe.Active, TheProbe.CrystalRank);
        }

        public void AssignProbeCrystal(CrystalBlob crystal, int rank)
        {
            if (crystal == null) Graph.ClearProbeCrystal();
            else Graph.SetProbeCrystal(crystal.Data.RefIdFromId(Crystal.FULL_HEADER), rank);
        }

        public void AssignRemote(RemoteBlob remote, int slot)
        {
            TheRemote = remote;
            Graph.SetRemote(remote);
            Graph.SetRemoteSlot(slot);
        }

        public void AssignSatellite(SatelliteBlob satellite)
        {
            TheSatellite = satellite;
            Graph.SetSatellite(satellite);
        }

        public void AssignLine(LineBlob line)
        {
            TheLine = line;
            Graph.SetLine(line);
        }

        public void AssignRocket(RocketBlob rocket)
        {
            TheRocket = rocket;
            Graph.SetRocket(rocket);
        }

        public void AssignAttractant(AttractantBlob attract)
        {
            TheAttractant = attract;
            Graph.SetAttractant(attract);
        }
    }
}
