namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class RigBlob : GameBlob
    {

        public RigBlob()
        {
        }

        virtual public RigData GetRigData() { return null; }
    }
}