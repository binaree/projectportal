namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.Ui;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class RemoteBlob : GearBlob
    {
        public RemoteData Data { get; private set; }
        public PetBlob Pet { get; private set; }

        public RemoteBlob(RemoteData d)
        {
            Data = d;
            Pet = null;
        }

        override public RigData GetRigData() { return Data; }

        public void AssignPet(PetBlob pet, bool updateUi = false)
        {
            if(pet == null && Pet != null)
            {
                Pet.PetGate.RemoveKeys();
            }
            Pet = pet;
            if(Pet != null)
            {
                Pet.PetGate.AddKeys(Pet.SizeLevel);
                if(updateUi) UiMain.Man.Footer.RemotePet.AssignCreature(Pet);
            }
        }

        public bool CanOpen()
        {
            if (Pet == null) return false;
            return Pet.CanUse();
        }

        public void OpenPortal()
        {
            Table.Man.Pet.PortalOpened();
        }
    }
}