namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.User;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class SupplyBlob : RigBlob
    {
        public int Quantity { get; private set; }
        public UserSupply User { get; private set; }

        public SupplyBlob(SupplyData d)
        {
            //Quantity = d.StartQuantity;
        }

        public bool CanUse() { return Quantity != 0; }
        public bool IsInfinite() { return Quantity < 0; }
        public bool Owns() { return Quantity > 0; }

        public override void LoadFromUser(UserData userData)
        {
            User = userData as UserSupply;
            Quantity = User.Quantity;
        }

        public int AdjustQuantity(int amount)
        {
            if (IsInfinite()) return -1;
            Quantity += amount;
            if (Quantity < 0) Quantity = 0;
            User.SetQuantity(Quantity);
            return Quantity;
        }
    }
}