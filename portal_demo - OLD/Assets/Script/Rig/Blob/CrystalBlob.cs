namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.User;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class CrystalBlob : RigBlob
    {
        public CrystalData Data { get; private set; }
        public UserCrystal User { get; private set; }
        public CreatureData TheCreature { get; private set; }

        public CrystalBlob(CrystalData d)
        {
            Data = d;
            TheCreature = Creature.Man.Data(Data.RefIdFromId(Crystal.DATA_HEADER));
        }

        public override void LoadFromUser(UserData userData)
        {
            User = userData as UserCrystal;
        }

        public bool Owns()
        {
            for (int i = 0; i < User.Quantity.Count; ++i)
                if (User.Quantity[i] > 0)
                    return true;
            return false;
        }

        public int RankQty(int rank)
        {
            if (rank >= User.Quantity.Count) return 0;
            return User.Quantity[rank];
        }

        public int AdjustQty(int amt, int rank)
        {
            if (rank >= User.Quantity.Count) return 0;
            int q = User.Quantity[rank] + amt;
            if (q < 0) q = 0;
            User.SetQuantity(q, rank);
            return q;
        }
    }
}