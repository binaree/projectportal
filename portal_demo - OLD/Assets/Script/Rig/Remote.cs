namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Remote : DataManager
    {
        public const string DATA_HEADER = "remote_";

        private Dictionary<string, RemoteBlob> reel = new Dictionary<string, RemoteBlob>();

        private static Remote _instance = new Remote();

        public static Remote Man { get { return _instance; } }

        public RemoteData Data(string id)
        {
            return GetData(id) as RemoteData;
        }

        public RemoteData DataR(string id)
        {
            return GetData(DATA_HEADER + id) as RemoteData;
        }

        public RemoteBlob Blob(int refNum)
        {
            return GetBlob(refNum) as RemoteBlob;
        }

        public RemoteBlob Blob(string id)
        {
            if (!reel.ContainsKey(id))
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return reel[id];
        }

        public RemoteBlob BlobR(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "remote/r");
        }

        protected override GameData NewData(XmlNode node)
        {
            RemoteData d = new RemoteData(node);
            RemoteBlob b = GenerateRefBlob(d, d.RefNum) as RemoteBlob;
            reel.Add(d.Id, b);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new RemoteBlob(d as RemoteData);
        }
    }
}
