namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Satellite : DataManager
    {
        public const string DATA_HEADER = "sat_";

        private List<SatelliteBlob> allBob = new List<SatelliteBlob>();
        private Dictionary<string, SatelliteBlob> bob = new Dictionary<string, SatelliteBlob>();

        private static Satellite _instance = new Satellite();

        public static Satellite Man { get { return _instance; } }

        public SatelliteData Data(string id)
        {
            return GetData(id) as SatelliteData;
        }

        public SatelliteData DataS(string id)
        {
            return GetData(DATA_HEADER + id) as SatelliteData;
        }

        public SatelliteBlob Blob(int refNum)
        {
            return GetBlob(refNum) as SatelliteBlob;
        }

        public SatelliteBlob Blob(string id)
        {
            if (!bob.ContainsKey(id))
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return bob[id];
        }

        public SatelliteBlob BlobS(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public List<SatelliteBlob> AllBlob() { return allBob; }

        public List<SatelliteBlob> AllOwned()
        {
            List<SatelliteBlob> own = new List<SatelliteBlob>();
            for (int i = 0; i < allBob.Count; ++i)
                if (allBob[i].Owns())
                    own.Add(allBob[i]);
            return own;
        }

        public SatelliteBlob GenerateFromUser(UserSatellite user)
        {
            SatelliteData d = DataS(user.RefId);
            SatelliteBlob b = GenerateRefBlob(d, d.RefNum) as SatelliteBlob;
            b.LoadFromUser(user);
            bob.Add(d.Id, b);
            allBob.Add(b);
            return b;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "satellite/s");
        }

        protected override GameData NewData(XmlNode node)
        {
            SatelliteData d = new SatelliteData(node);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new SatelliteBlob(d as SatelliteData);
        }
    }
}
