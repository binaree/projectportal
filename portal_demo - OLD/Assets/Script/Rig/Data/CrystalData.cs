namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Xml;
    using Utility;

    public class CrystalData : GameData
    {
        public string CreatureId { get; private set; }

        public CrystalData(XmlNode xData) : base(xData)
        {
            CreatureId = RefIdFromId(Crystal.DATA_HEADER);
        }
    }
}