namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Xml;
    using Utility;

    public enum LineType
    {
        Standard = 0,
        Invisible = 1,
        Visible = 2,
    }

    public class LineData : SupplyData
    {
        private static readonly string[] LINE_TYPE = { "std", "in", "v" };

        public LineType Type { get; private set; }
        public float Detection { get; private set; }
        public float Size { get; private set; }
        public float MaxPower { get; private set; }
        public float Resistance { get; private set; }

        public LineData(XmlNode xData) : base(xData)
        {
            Type = StrToLineType(xData.Attributes.GetNamedItem("type").Value);
            Detection = Convert.ToSingle(xData.Attributes.GetNamedItem("det").Value);
            Size = Convert.ToSingle(xData.Attributes.GetNamedItem("size").Value);
            MaxPower = Convert.ToSingle(xData.Attributes.GetNamedItem("max").Value);
            Resistance = Convert.ToSingle(xData.Attributes.GetNamedItem("res").Value);
        }

        public static LineType StrToLineType(string val)
        {
            for (int i = 0; i < LINE_TYPE.Length; ++i)
                if (val == LINE_TYPE[i]) return (LineType)i;
            return LineType.Standard;
        }

        public static string LineTypeToStr(LineType val)
        {
            return LINE_TYPE[(int)val];
        }

    }
}