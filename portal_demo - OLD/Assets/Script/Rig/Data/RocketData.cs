namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Xml;
    using Utility;

    public class RocketData : SupplyData
    {
        public SplitF Depth { get; private set; }

        public RocketData(XmlNode xData) : base(xData)
        {
            Depth = new SplitF(xData.Attributes.GetNamedItem("depth").Value);
        }
        
    }
}