namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class ProbeCreature
    {
        public string RefId;
        public float Attraction;
        public float RankMod;
        public float SizeMod;
    }

    public class ProbeData : GearData
    {
        private const char LIST_SEP = ',';
        private const char VAL_SEP = '|';
        private const int VAL_ID = 0;
        private const int VAL_ATT = 1;
        private const int VAL_RANK = 2;
        private const int VAL_SIZE = 3;

        public CreatureSize Size { get; private set; }
        public float SizeDecay { get; private set; }
        public List<float> Set { get; private set; }

        public List<ProbeCreature> Creature { get; private set; }

        public ProbeData(XmlNode xData) : base(xData)
        {
            Size = CreatureData.StrToHookSize(xData.Attributes.GetNamedItem("size").Value);
            SizeDecay = Convert.ToSingle(xData.Attributes.GetNamedItem("sizeD").Value);
            Set = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("set").Value, Set);

            Creature = new List<ProbeCreature>();
            string[] split = xData.Attributes.GetNamedItem("cr").Value.Split(LIST_SEP);
            for (int i = 0; i < split.Length; ++i)
                AddCreature(split[i]);
        }

        public float SetRarity(CreatureRarity r) { return Set[(int)r]; }

        private void AddCreature(string val)
        {
            string[] split = val.Split(VAL_SEP);
            ProbeCreature c = new ProbeCreature();
            if (split.Length > VAL_ID) c.RefId = split[VAL_ID]; else c.RefId = "";
            if (split.Length > VAL_ATT) c.Attraction = Convert.ToSingle(split[VAL_ATT]); else c.Attraction = 0.0f;
            if (split.Length > VAL_RANK) c.RankMod = Convert.ToSingle(split[VAL_RANK]); else c.RankMod = 1.0f;
            if (split.Length > VAL_SIZE) c.SizeMod = Convert.ToSingle(split[VAL_SIZE]); else c.SizeMod = 1.0f;
            Creature.Add(c);
        }
    }
}