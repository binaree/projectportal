namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Rocket : DataManager
    {
        public const string DATA_HEADER = "rock_";

        private List<RocketBlob> allRocket = new List<RocketBlob>();
        private Dictionary<string, RocketBlob> rock = new Dictionary<string, RocketBlob>();

        private static Rocket _instance = new Rocket();

        public static Rocket Man { get { return _instance; } }

        public RocketData Data(string id)
        {
            return GetData(id) as RocketData;
        }

        public RocketData DataR(string id)
        {
            return GetData(DATA_HEADER + id) as RocketData;
        }

        public RocketBlob Blob(int refNum)
        {
            return GetBlob(refNum) as RocketBlob;
        }

        public RocketBlob Blob(string id)
        {
            if (!rock.ContainsKey(id))
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return rock[id];
        }

        public RocketBlob BlobR(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public List<RocketBlob> AllBlob() { return allRocket; }

        public List<RocketBlob> AllOwned()
        {
            List<RocketBlob> own = new List<RocketBlob>();
            for (int i = 0; i < allRocket.Count; ++i)
                if (allRocket[i].CanUse())
                    own.Add(allRocket[i]);
            return own;
        }

        public RocketBlob GenerateFromUser(UserSupply user)
        {
            RocketData d = DataR(user.RefId);
            RocketBlob b = GenerateRefBlob(d, d.RefNum) as RocketBlob;
            b.LoadFromUser(user);
            rock.Add(d.Id, b);
            allRocket.Add(b);
            return b;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "rocket/r");
        }

        protected override GameData NewData(XmlNode node)
        {
            RocketData d = new RocketData(node);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new RocketBlob(d as RocketData);
        }
    }
}
