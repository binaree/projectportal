namespace Binaree.Game.Object
{
    using Base;
    using Component;
    using Core;
    using Factory;
    using Object;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class CreatureObj : PropObj
    {



        public void AssignToPortal(PortalBlob port)
        {
            Use();
            gameObject.transform.position = port.Obj.GetSpawn(0).transform.position;
            //SetWait();
        }

        override public void Use()
        {
            base.Use();
            Step.Man.Layer.AssignObjToWorld(this, LayerControl.World.Ground);
        }
    }
}
