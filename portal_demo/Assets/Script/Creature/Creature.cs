namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Creature : DataManager
    {
        public const string DATA_HEADER = "cr_";

        public CreatureBlob Active { get; private set; }
        
        private static Creature _instance = new Creature();

        public static Creature Man { get { return _instance; } }

        public CreatureData Data(string id)
        {
            return GetData(id) as CreatureData;
        }

        public CreatureData DataC(string id)
        {
            return GetData(DATA_HEADER + id) as CreatureData;
        }

        public CreatureBlob Blob(int refNum)
        {
            return GetBlob(refNum) as CreatureBlob;
        }

        public CreatureBlob GenerateCreature(CreatureData d)
        {
            ClearActive();
            Active = GenerateBlob(d) as CreatureBlob;
            return Active;
        }

        public void ClearActive()
        {
            if (Active == null) return;
            Active.Clear();
            Active = null;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "creature/c");
        }

        protected override GameData NewData(XmlNode node)
        {
            CreatureData d = new CreatureData(node);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new CreatureBlob(d as CreatureData);
        }
    }
}
