namespace Binaree.Game.User
{
    using System.Xml;
    using System.Xml.Serialization;
    using Component;
    using System.Collections.Generic;
    
    [XmlRoot(GraphTank.TAG)]
    public class GraphTank : UserGraph
    {
        public const string TAG = "tank";

        [XmlElement(UserTankSlot.TAG)]
        public List<UserTankSlot> Slot { get; private set; }

        public GraphTank() : base()
        {
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            for (int i = 0; i < Slot.Count; ++i)
                Slot[i].Load(this);

            Dirty();
        }
    }
}
