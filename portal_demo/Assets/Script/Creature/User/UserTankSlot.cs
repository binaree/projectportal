namespace Binaree.Game.User
{
    using Component;
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;
    using Utility;

    public class UserTankSlot : UserData
    {
        public const string TAG = "slot";
        public const int NO_REST = 0;

        public enum State
        {
            Locked = 0,
            Open = 1,
            Fill = 2,
        }

        [XmlAttribute("st")]
        public int SlotState { get; private set; }

        [XmlAttribute("cr")]
        public string CreatureRefId { get; private set; }

        [XmlAttribute("rank")]
        public int PetRank { get; private set; }

        [XmlAttribute("lvl")]
        public float PetLevel { get; private set; }

        [XmlAttribute("wait")]
        public int WaitTime { get; private set; }

        public UserTankSlot() : base()
        {
            SlotState = 0;
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }
        
        public void UnlockSlot()
        {
            if (SlotState != (int)State.Locked) return;
            SlotState = (int)State.Open;
            CreatureRefId = string.Empty;
            PetRank = 0;
            PetLevel = 0;
            WaitTime = NO_REST;
            Dirty();
        }

        public void AssignCreature(CreatureBlob cr)
        {
            if (SlotState != (int)State.Open) return;
            SlotState = (int)State.Fill;
            CreatureRefId = cr.Data.RefIdFromId(Creature.DATA_HEADER);
            PetRank = cr.Rank;
            PetLevel = cr.SizeLevel;
            WaitTime = NO_REST;
            Dirty();
        }

        public void RemoveCreature()
        {
            if (SlotState != (int)State.Fill) return;
            SlotState = (int)State.Open;
            CreatureRefId = string.Empty;
            PetRank = 0;
            PetLevel = 0;
            WaitTime = NO_REST;
            Dirty();
        }

        public void AssignTime(int amt)
        {
            if (SlotState != (int)State.Fill) return;
            WaitTime = amt;
            Dirty();
        }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            Tank.Man.GenerateSlotFromUser(this);

            Dirty();
        }
    }
}
