namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.User;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class PetSlot : GameBlob
    {
        public UserRemoteSlot User { get; private set; }
        public PetBlob ThePet { get; private set; }

        public PetSlot(UserRemoteSlot user, int slotNum)
        {
            SetDirectRef(slotNum);
            User = user;
            if(User.SlotState == (int)UserRemoteSlot.State.Fill)
            {
                CreatureData cd = Creature.Man.DataC(User.PetRefId);
                PetData pd = Pet.Man.DataRef(User.PetRefId);
                ThePet = new PetBlob(cd, pd, this);
                ThePet.AssignRank(User.PetRank, User.PetLevel);
            }
            else
            {
                ThePet = null;
            }
        }

        public bool IsUnlocked() { return User.SlotState != (int)UserRemoteSlot.State.Locked; }
        public bool IsOpen() { return User.SlotState == (int)UserTankSlot.State.Open; }
        public bool IsFull() { return User.SlotState == (int)UserTankSlot.State.Fill; }

        public void Activate()
        {
            Rig.Man.TheRemote.AssignPet(ThePet);
        }

        public void Deactivate()
        {
            Rig.Man.TheRemote.AssignPet(null);
        }

        public PetBlob AssignPet(CreatureBlob cr)
        {
            if (ThePet != null)
            {
                Say.Warn("Cannot assign a creature to a full remote slot!");
                return null;
            }
            PetData d = Pet.Man.DataRef(cr.Data.RefIdFromId(Creature.DATA_HEADER));
            ThePet = new PetBlob(cr.Data, d, this);
            ThePet.AssignRank(cr.Rank, cr.SizeLevel);
            User.AssignCreature(cr);
            return ThePet;
        }

        public void AssignRest(float restTime)
        {
            User.AssignRest((int)restTime);
        }

        public void EmptySlot()
        {
            ThePet = null;
            User.RemoveCreature();
        }
    }
}