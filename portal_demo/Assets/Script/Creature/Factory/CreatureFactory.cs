namespace Binaree.Game.Factory
{
    using Component;
    using Core;
    using Base;
    using System.Collections.Generic;
    using Object;
    
    public class CreatureFactory : GameFactory
    {
        public const string cr_HEADER = "cr_";

        public string CreatureDirectory = "creature";
        public int PreloadAmount = 1;

        private static CreatureFactory _instance = null;

        public static CreatureFactory Inst { get { return _instance; } }

        override public void Generate()
        {
            _instance = this;
            List<GameData> allCr = Creature.Man.AllGameData();
            for(int i = 0; i < allCr.Count; ++i)
            {
                if (store.ContainsKey(allCr[i].Id)) continue;
                TrackPrefab(allCr[i].Id, PreloadAmount);
            }
            hasGenerated = true;
        }

        override protected string FormatTypeToPrefab(string objType) { return /*HERO_HEADER +*/ objType; }
        override protected string PrefabDirectory() { return CreatureDirectory + "/"; }

        public CreatureObj TakeCreature(string objType) { return Take(objType) as CreatureObj; }
    }
}
