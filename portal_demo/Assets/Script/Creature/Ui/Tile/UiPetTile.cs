namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.User;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiPetTile : UiSlotTile
    {
        public const string PREFAB = "tile_pet";

        public PetSlot slot = null;

        override public string RefId() { return (slot.ThePet == null) ? string.Empty : slot.ThePet.DataC.Id; }
        override public string IconDir() { return "creatureui/"; }
        override public GameBlob GetSlot() { return slot; }

        override public void AssignBlob(GameBlob b)
        {
            slot = b as PetSlot;
            if (slot.User.SlotState == (int)UserRemoteSlot.State.Fill)
            {
                AssignCreature(slot.ThePet.DataC, slot.ThePet.Rank, slot.ThePet.Size, slot.ThePet.TimeToUse());
            }
            else
                AssignUnused(slot.User.SlotState);
            DeselectTile();
        }

        override public void SelectTile()
        {
            AssignSelect(true);
        }

        override public void DeselectTile()
        {
            AssignSelect(false);
        }
    }
}
