namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiTankPanel : UiSlotPanel
    {
        private TankSlot slot = null;

        override public string IconDir() { return "creatureui/"; }
        override public string PrefabName() { return UiTankTile.PREFAB; }
        override public bool IsEquipped() { return slot == Tank.Man.Active; }

        private void OnEnable()
        {
            Deselect();
            Tile.Initialize();
            List<TankSlot> list = Tank.Man.Slot;
            TankSlot activeSlot = Tank.Man.Active;
            for (int i = 0; i < list.Count; ++i)
            {
                UiSlotTile t = AddTile(list[i]);
                if (activeSlot == list[i])
                    t.SelectTile();
            }
        }

        override public void ShowSlot(GameBlob b)
        {
            slot = b as TankSlot;
            if (slot == null) { Deselect(); return; }
            base.ShowSlot(b);
            if (slot.IsFull())
                AssignCreature(slot.TankCreature.DataC, slot.TankCreature.Rank, slot.TankCreature.SizeLevel, slot.TankCreature.TimeToUse());
            else
                AssignUnused(slot.User.SlotState);
        }

        public override void Deselect()
        {
            base.Deselect();
            slot = null;
        }

        override public void SelectApply()
        {
            if (slot == null || slot.TankCreature == null) return;
            // TODO: Perform the conversion
        }

        public void SelectConvert()
        {
            Say.Log("Convert has been selected");
        }

        public void SelectRelease()
        {
            Say.Log("Release has been selected");
        }
    }
}
