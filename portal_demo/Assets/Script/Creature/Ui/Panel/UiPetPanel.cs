namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiPetPanel : UiSlotPanel
    {
        public Text EquipLabel;

        public GameObject FeedPanel;
        public Text SizeLabel;
        public Slider SizeMeter;

        public GameObject EvolvePanel;

        public GameObject MaxPanel;

        private PetSlot slot = null;

        override public string IconDir() { return "creatureui/"; }
        override public string PrefabName() { return UiPetTile.PREFAB; }
        override public bool IsEquipped() { return slot == Pet.Man.Active; }

        private void OnEnable()
        {
            Deselect();
            Tile.Initialize();
            List<PetSlot> list = Pet.Man.Slot;
            PetSlot activeSlot = Pet.Man.Active;
            for (int i = 0; i < list.Count; ++i)
            {
                UiSlotTile t = AddTile(list[i]);
                if (activeSlot == list[i])
                    t.SelectTile();
            }
        }

        override public void ShowSlot(GameBlob b)
        {
            slot = b as PetSlot;
            if (slot == null) { Deselect(); return; }
            base.ShowSlot(b);
            if (slot.IsFull())
                AssignCreature(slot.ThePet.DataC, slot.ThePet.Rank, slot.ThePet.SizeLevel, slot.ThePet.TimeToUse());
            else
                AssignUnused(slot.User.SlotState);
        }

        public override void Deselect()
        {
            base.Deselect();
            slot = null;
        }

        override public void SelectApply()
        {
            if (slot == null || slot.ThePet == null || slot == Pet.Man.Active) return;
            Pet.Man.SetActiveSlot(slot.RefNum);
        }

        public void SelectConvert()
        {
            Say.Log("Convert has been selected");
        }

        public void SelectRelease()
        {
            Say.Log("Release has been selected");
        }

        public void SelectFeed()
        {
            Say.Log("Feed has been selected");
        }

        public void SelectEvolve()
        {
            Say.Log("Evolve has been selected");
        }

        override protected void SetFull(CreatureData cr, int rank, float level, float wait)
        {
            base.SetFull(cr, rank, level, wait);
            if (slot.ThePet.CanFeed())
                SetFeed(cr, rank, level);
            else if (slot.ThePet.CanEvolve())
                SetEvolve(cr, rank, level);
            else
                SetMax(cr, rank, level);
            EquipLabel.text = (slot == Pet.Man.Active) ? "In Use" : "Equip";
        }

        private void SetFeed(CreatureData cr, int rank, float level)
        {
            CloseSubPanels();
            SizeLabel.text = Stringy.FormatSize(Table.Man.Creature.CreatureSize(cr, rank, level));
            SizeMeter.value = level;
            FeedPanel.SetActive(true);
        }

        private void SetEvolve(CreatureData cr, int rank, float level)
        {
            CloseSubPanels();
            EvolvePanel.SetActive(true);
        }

        private void SetMax(CreatureData cr, int rank, float level)
        {
            CloseSubPanels();
            MaxPanel.SetActive(true);
        }

        private void CloseSubPanels()
        {
            FeedPanel.SetActive(false);
            EvolvePanel.SetActive(false);
            MaxPanel.SetActive(false);
        }
    }
}
