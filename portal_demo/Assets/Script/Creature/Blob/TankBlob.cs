namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class TankBlob : GameBlob
    {
        public CreatureData DataC { get; private set; }
        public TankData DataT { get; private set; }
        public TankSlot Slot { get; private set; }

        public int Rank { get; private set; }
        public float Size { get; private set; }
        public float SizeLevel { get; private set; }

        public List<RewardBlob> Rewards { get; private set; }

        public TankBlob(CreatureData cd, TankData td, TankSlot slot)
        {
            DataC = cd;
            DataT = td;
            Slot = slot;
        }

        public void AssignRank(int rank, float level)
        {
            Rank = rank;
            SizeLevel = level;
            Size = Table.Man.Creature.CreatureSize(DataC, rank, level);
        }

        public bool IsTrophy() { return Rank == Table.Man.Creature.MaxRank; }
        public float TimeToUse() { return Slot.User.WaitTime; }

        public List<RewardBlob> DetermineRewards()
        {
            return DetermineRewards(1.0f, 1.0f, 1.0f);
        }

        public List<RewardBlob> DetermineRewards(float triggerChanceScale, float bonusChanceScale, float bonusAmountScale)
        {
            Rewards = new List<RewardBlob>();
            for (int i = 0; i < DataT.Reward.Count; ++i)
            {
                if (!DataT.Reward[i].IsTrigger(triggerChanceScale)) continue;
                RewardBlob rew = Reward.Man.GenerateReward(DataT.Reward[i].RewardId, 
                                                           DataT.Reward[i].IsBonus(bonusChanceScale), 
                                                           DataT.Reward[i].BonusAmount * bonusAmountScale);
                Rewards.Add(rew);
            }

            return Rewards;
        }
    }
}