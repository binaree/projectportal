namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class PetBlob : GameBlob
    {
        public CreatureData DataC { get; private set; }
        public PetData DataP { get; private set; }
        public GateData PetGate { get; private set; }
        public PetSlot Slot { get; private set; }

        public int Rank { get; private set; }
        public float Size { get; private set; }
        public CreatureSize SizeType { get; private set; }
        public float SizeLevel { get; private set; }

        public PetBlob(CreatureData cd, PetData pd, PetSlot slot)
        {
            DataC = cd;
            DataP = pd;
            Slot = slot;
            PetGate = Gate.Man.DataPet(DataP.GateRefId);
        }

        public void AssignRank(int rank, float level)
        {
            Rank = rank;
            Size = Table.Man.Creature.CreatureSize(DataC, rank, level);
            SizeType = Table.Man.Probe.SizeByCreature(Size);
            SizeLevel = level;
        }

        public void Clear()
        {

        }

        public bool IsTrophy() { return Rank == Table.Man.Creature.MaxRank; }
        public bool CanUse() { return Slot.User.RestTime <= 0.0f; }
        public float TimeToUse() { return Slot.User.RestTime; }
        public bool CanFeed() { return SizeLevel < 1.0f; }
        public bool CanEvolve() { return SizeLevel >= 1.0f; }
        public bool AtMax() { return Rank == Table.Man.Creature.MaxRank && CanEvolve(); }

        public void OpenPortal()
        {
            
        }
    }
}