namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.Object;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;

    public enum HitStatus
    {
        Wait = 0,
        Nibble = 1,
        Bite = 2,
        Attached = 3,
    }
    
    public class HitBlob : GameBlob
    {
        public HitData Data { get; private set; }
        public HitStatus Status { get; private set; }

        private CreatureBlob cr = null;
        private SatelliteObj sat = null;
        private float actTime = 0.0f;
        private int nibsBeforeBite = 0;
        private float nibHit = 0.0f;

        public HitBlob(HitData d)
        {
            Data = d;
        }

        public void AssignCreature(CreatureBlob theCr)
        {
            cr = theCr;
            sat = Rig.Man.TheSatellite.Obj;
            nibsBeforeBite = Data.NibCount.Rand();

            if (Data.Hit == CreatureHit.Striker)
                CheckAction(Table.Man.Wait.CurrentPower > 0.0f);
            else
                SetActTime();
        }

        public void UpdateHit(bool isMoving)
        {
            actTime -= TimeLord.Inst.ArenaDelta;
            if( actTime < 0.0f )
            {
                if (Status == HitStatus.Wait)
                    CheckAction(isMoving);
                else if (Status == HitStatus.Nibble)
                    EndNibble();
                else if (Status == HitStatus.Bite)
                {
                    EndBite();
                    Step.Man.Wait.LostCreature(StepWait.Lost.TookProbe);
                }
            }
        }

        public float BaseHitChance()
        {
            if (Status == HitStatus.Wait) return 0.0f;
            else if (Status == HitStatus.Bite) return 1.0f;
            return nibHit;// Data.NibHitChance.Rand();
        }

        public bool MissHit(float delay)
        {
            if (Status == HitStatus.Bite)
            {
                EndBite();
                return true;
            }
            else if (Status == HitStatus.Nibble)
            {
                EndNibble();
                if( Randomize.Low(Data.NibLoseChance.Rand()) )
                {
                    return true;
                }
            }
            actTime += delay;
            return false;
        }

        public void AttachProbe()
        {
            Say.Log("Attached!");
            Status = HitStatus.Attached;
            actTime = 9999999.0f;
            sat.SetHit();
        }

        private void CheckAction(bool isMoving)
        {
            if (Randomize.Low((isMoving) ? Data.MoveAttract : Data.StillAttract))
            {
                if (nibsBeforeBite <= 0)
                    SetBite();
                else
                    SetNibble();
            }
            else
                SetActTime();
        }

        private void SetActTime()
        {
            actTime = Data.ActionTime.Rand();
            Status = HitStatus.Wait;
            sat.SetWait();
        }

        private void SetNibble()
        {
            Say.Log("Nibble");
            actTime = Data.NibTime.Rand();
            Status = HitStatus.Nibble;
            nibHit = Data.NibHitChance.Rand();
            sat.SetNibble(1.0f - nibHit);
            --nibsBeforeBite;
        }

        private void EndNibble()
        {
            SetActTime();
        }

        private void SetBite()
        {
            Say.Log("Bite");
            actTime = Data.BiteTime.Rand();
            Status = HitStatus.Bite;
            sat.SetBite();
        }

        private void EndBite()
        {
            SetActTime();
            actTime = 9999999.0f;
        }
    }
}