namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class TankData : GameData
    {
        public float TankTimeScale { get; private set; }
        public List<TankRewardComp> Reward { get; private set; }

        public TankData(XmlNode xData) : base(xData)
        {
            TankTimeScale = Convert.ToSingle(xData.Attributes.GetNamedItem("ts").Value);

            Reward = new List<TankRewardComp>();
            string[] split = xData.Attributes.GetNamedItem("rew").Value.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < split.Length; ++i)
                Reward.Add(new TankRewardComp(split[i]));
        }
    }
}