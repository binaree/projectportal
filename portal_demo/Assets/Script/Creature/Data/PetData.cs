namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class PetData : GameData
    {
        public string GateRefId { get; private set; }
        public float RestTimeScale { get; private set; }

        public PetData(XmlNode xData) : base(xData)
        {
            GateRefId = xData.Attributes.GetNamedItem("gate").Value;
            RestTimeScale = Convert.ToSingle(xData.Attributes.GetNamedItem("rt").Value);
        }
    }
}