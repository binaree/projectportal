namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Pet : DataManager
    {
        public const string DATA_HEADER = "pet_";
        public const string FULL_HEADER = "pet_cr_";

        public PetSlot Active { get; private set; }
        public List<PetSlot> Slot { get; private set; }
        
        private static Pet _instance = new Pet();

        public static Pet Man { get { return _instance; } }

        public PetData Data(string id)
        {
            return GetData(id) as PetData;
        }

        public PetData DataP(string id)
        {
            return GetData(DATA_HEADER + id) as PetData;
        }

        public PetData DataRef(string fullRefId)
        {
            return GetData(FULL_HEADER + fullRefId) as PetData;
        }

        public PetSlot SetActiveSlot(int slotNum)
        {
            if (slotNum >= Slot.Count) return null;
            if (Active != null)
                Active.Deactivate();
            Active = Slot[slotNum];
            Active.Activate();
            return Active;
        }

        public PetSlot FindOpenSlot()
        {
            for (int i = 0; i < Slot.Count; ++i)
                if (Slot[i].IsOpen())
                    return Slot[i];
            return null;
        }

        public PetSlot GenerateSlotFromUser(UserRemoteSlot user)
        {
            PetSlot ps = new PetSlot(user, Slot.Count);
            Slot.Add(ps);
            return ps;
        }

        public void Load(XmlDocument xml)
        {
            Active = null;
            Slot = new List<PetSlot>();
            LoadData(xml, "pet/p");
        }

        protected override GameData NewData(XmlNode node)
        {
            PetData d = new PetData(node);
            return d;
        }
    }
}
