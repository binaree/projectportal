namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class LineBlob : SupplyBlob
    {
        public LineData Data { get; private set; }

        public LineBlob(LineData d) : base(d)
        {
            Data = d;
        }

        override public RigData GetRigData() { return Data; }
    }
}