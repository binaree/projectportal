namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class RocketBlob : SupplyBlob
    {
        public RocketData Data { get; private set; }

        public RocketBlob(RocketData d) : base(d)
        {
            Data = d;
        }

        override public RigData GetRigData() { return Data; }
    }
}