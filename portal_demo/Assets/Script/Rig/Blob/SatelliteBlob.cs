namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.Object;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    using Binaree.Game.User;

    public class SatelliteBlob : GearBlob
    {
        public SatelliteData Data { get; private set; }
        public SatelliteObj Obj { get; private set; }
        public UserSatellite User { get; private set; }

        public SatelliteBlob(SatelliteData d)
        {
            Data = d;
            Obj = null;
        }

        override public RigData GetRigData() { return Data; }

        public override void LoadFromUser(UserData userData)
        {
            User = userData as UserSatellite;
        }

        public bool Owns()
        {
            return User.Level >= 0;
        }

        public void CreateSatellite()
        {
            ReleaseSatellite();
            Obj = SatelliteFactory.Inst.TakeSatellite(Data.Id);
            Obj.AssignToPortal(Portal.Man.Active);
        }

        public void ReleaseSatellite()
        {
            if (Obj == null) return;
            SatelliteFactory.Inst.Release(Obj);
            Obj = null;
        }
    }
}