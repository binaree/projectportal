namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class GearBlob : RigBlob
    {
        public int Level { get; private set; }

        public GearBlob()
        {
            Level = 1;
        }
    }
}