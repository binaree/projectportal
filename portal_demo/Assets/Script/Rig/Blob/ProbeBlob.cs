namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class ProbeBlob : GearBlob
    {
        public ProbeData Data { get; private set; }
        public float Set { get; private set; }

        public CrystalBlob Active { get; private set; }
        public int CrystalRank { get; private set; }

        public ProbeBlob(ProbeData d)
        {
            Data = d;
            Set = 0.0f;
            Active = null;
            CrystalRank = 0;
        }

        override public RigData GetRigData() { return Data; }

        public float DetermineSet(CreatureBlob c)
        {
            Set = Table.Man.Probe.SetChance(c);
            return Set;
        }

        public void AssignCrystal(string refId, int crystalRank)
        {
            AssignCrystal(Crystal.Man.BlobRef(refId), crystalRank);
        }

        public void AssignCrystal(CrystalBlob crystal, int crystalRank)
        {
            // TODO: Do something here to remove the old crystal if needed
            CrystalRank = crystalRank;
            Active = crystal;
            Rig.Man.AssignProbeCrystal(Active, CrystalRank);
        }
    }
}