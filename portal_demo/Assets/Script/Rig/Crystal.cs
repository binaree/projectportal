namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Crystal : DataManager
    {
        public const string DATA_HEADER = "cry_";
        public const string FULL_HEADER = "cry_cr_";
        
        private List<CrystalBlob> allCrystal = new List<CrystalBlob>();
        private Dictionary<string, CrystalBlob> crystal = new Dictionary<string, CrystalBlob>();

        private static Crystal _instance = new Crystal();

        public static Crystal Man { get { return _instance; } }

        public CrystalData Data(string id)
        {
            return GetData(id) as CrystalData;
        }

        public CrystalData DataC(string id)
        {
            return GetData(DATA_HEADER + id) as CrystalData;
        }

        public CrystalData DataRef(string refId)
        {
            return GetData(FULL_HEADER + refId) as CrystalData;
        }

        public CrystalBlob Blob(string id)
        {
            if (!crystal.ContainsKey(id))
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return crystal[id];
        }

        public CrystalBlob BlobC(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public CrystalBlob BlobRef(string refId)
        {
            return Blob(FULL_HEADER + refId);
        }

        public List<CrystalBlob> AllBlob() { return allCrystal; }

        public List<CrystalBlob> AllOwned()
        {
            List<CrystalBlob> own = new List<CrystalBlob>();
            for (int i = 0; i < allCrystal.Count; ++i)
                if (allCrystal[i].Owns())
                    own.Add(allCrystal[i]);
            return own;
        }

        public CrystalBlob GenerateFromUser(UserCrystal user)
        {
            CrystalData d = DataRef(user.RefId);
            CreatureData crDat = Creature.Man.DataC(user.RefId);
            CrystalBlob b = GenerateRefBlob(d, crDat.RefNum) as CrystalBlob;
            b.LoadFromUser(user);
            crystal.Add(d.Id, b);
            allCrystal.Add(b);
            return b;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "crystal/c");
        }

        protected override GameData NewData(XmlNode node)
        {
            CrystalData d = new CrystalData(node);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new CrystalBlob(d as CrystalData);
        }
    }
}
