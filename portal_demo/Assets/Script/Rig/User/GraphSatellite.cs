namespace Binaree.Game.User
{
    using System.Xml;
    using System.Xml.Serialization;
    using Component;
    using System.Collections.Generic;
    
    [XmlRoot(GraphSatellite.TAG)]
    public class GraphSatellite : UserGraph
    {
        public const string TAG = "satellite";

        [XmlElement(UserSatellite.TAG)]
        public List<UserSatellite> Satellite { get; private set; }

        public GraphSatellite() : base()
        {
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            for (int i = 0; i < Satellite.Count; ++i)
                Satellite[i].Load(this);

            Dirty();
        }
    }
}
