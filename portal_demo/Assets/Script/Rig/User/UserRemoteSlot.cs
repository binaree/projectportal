namespace Binaree.Game.User
{
    using Component;
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;
    using Utility;

    public class UserRemoteSlot : UserData
    {
        public const string TAG = "slot";
        public const int NO_REST = 0;

        public enum State
        {
            Locked = 0,
            Open = 1,
            Fill = 2,
        }

        [XmlAttribute("st")]
        public int SlotState { get; private set; }

        [XmlAttribute("pet")]
        public string PetRefId { get; private set; }

        [XmlAttribute("rank")]
        public int PetRank { get; private set; }

        [XmlAttribute("lvl")]
        public float PetLevel { get; private set; }

        [XmlAttribute("rest")]
        public int RestTime { get; private set; }

        public UserRemoteSlot() : base()
        {
            SlotState = 0;
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }
        
        public void UnlockSlot()
        {
            if (SlotState != (int)State.Locked) return;
            SlotState = (int)State.Open;
            PetRefId = string.Empty;
            PetRank = 0;
            PetLevel = 0;
            RestTime = NO_REST;
            Dirty();
        }

        public void AssignCreature(CreatureBlob cr)
        {
            if (SlotState != (int)State.Open) return;
            SlotState = (int)State.Fill;
            PetRefId = cr.Data.RefIdFromId(Creature.DATA_HEADER);
            PetRank = cr.Rank;
            PetLevel = cr.SizeLevel;
            RestTime = NO_REST;
            Dirty();
        }

        public void EvolveCreature(int rank, float lvl)
        {
            if (SlotState != (int)State.Fill) return;
            PetRank = rank;
            PetLevel = lvl;
            Dirty();
        }

        public void AssignRest(int amt)
        {
            if (SlotState != (int)State.Fill) return;
            RestTime = amt;
            Dirty();
        }

        public void RemoveCreature()
        {
            if (SlotState != (int)State.Fill) return;
            SlotState = (int)State.Open;
            PetRefId = string.Empty;
            PetRank = 0;
            PetLevel = 0;
            RestTime = NO_REST;
            Dirty();
        }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            Pet.Man.GenerateSlotFromUser(this);

            Dirty();
        }
    }
}
