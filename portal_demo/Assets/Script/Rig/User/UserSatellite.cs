namespace Binaree.Game.User
{
    using Component;
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;
    using Utility;

    public class UserSatellite : UserData
    {
        public const string TAG = "sat";

        [XmlAttribute("id")]
        public string RefId { get; private set; }

        [XmlAttribute("lvl")]
        public int Level { get; private set; }

        [XmlIgnore]
        public string CrystalRefId { get; private set; }

        [XmlIgnore]
        public int CrystalRank { get; private set; }

        [XmlAttribute("cry")]
        public string QuantityStr
        {
            get { return CrystalRefId + ":" + CrystalRank; }
            private set { string[] split = value.Split(':');  CrystalRefId = split[0]; CrystalRank = Convert.ToInt32(split[1]); }
        }

        [XmlAttribute("card")]
        public int Card { get; private set; }

        public UserSatellite() : base()
        {
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }
        
        public void SetLevel(int lvl)
        {
            Level = lvl;
            Dirty();
        }

        public void SetCrystal(CrystalBlob cry, int rank)
        {
            CrystalRefId = cry.Data.RefIdFromId(Crystal.FULL_HEADER);
            CrystalRank = rank;
            Dirty();
        }

        public void SetCard(int amt)
        {
            Card = amt;
            Dirty();
        }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            Satellite.Man.GenerateFromUser(this);

            Dirty();
        }
    }
}
