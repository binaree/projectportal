namespace Binaree.Game.User
{
    using System.Xml;
    using System.Xml.Serialization;
    using Component;
    using System.Collections.Generic;
    
    [XmlRoot(GraphRemote.TAG)]
    public class GraphRemote : UserGraph
    {
        public const string TAG = "remote";

        [XmlElement(UserRemoteSlot.TAG)]
        public List<UserRemoteSlot> Slot { get; private set; }

        public GraphRemote() : base()
        {
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            for (int i = 0; i < Slot.Count; ++i)
                Slot[i].Load(this);

            Dirty();
        }
    }
}
