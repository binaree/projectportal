namespace Binaree.Game.User
{
    using Component;
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;
    using Utility;

    public class UserSupply : UserData
    {
        public const string TAG = "s";

        [XmlAttribute("t")]
        public int SupplyType { get; private set; }

        [XmlAttribute("ref")]
        public string RefId { get; private set; }

        [XmlAttribute("qty")]
        public int Quantity { get; private set; }

        public UserSupply() : base()
        {
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }
        
        public void SetQuantity(int amt)
        {
            Quantity = amt;
            Dirty();
        }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            if (SupplyType == (int)SupplyRig.Attractant)
                Attractant.Man.GenerateFromUser(this);
            else if (SupplyType == (int)SupplyRig.Line)
                Line.Man.GenerateFromUser(this);
            else if (SupplyType == (int)SupplyRig.Rocket)
                Rocket.Man.GenerateFromUser(this);

            Dirty();
        }
    }
}
