namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiCrystalTile : UiTileObj
    {
        public const string PREFAB = "tile_crystal";
        public const string CRYSTAL_DIR = "crystalui/";

        public Image Selected;
        public Image Icon;
        public UiStarRank Rank;
        public int SetRank;
        public Text Quantity;

        private CrystalBlob crystal = null;
        protected UiProbePanel targetPanel = null;

        override public string RefId() { return SetRank.ToString(); }

        override public void AssignBlob(GameBlob b)
        {
            crystal = b as CrystalBlob;
            Spritey.ReplaceIcon(Icon, CRYSTAL_DIR, crystal.TheCreature.Id);
            Rank.AssignRank(SetRank, Table.Man.Creature.MaxRank);
            Quantity.text = "Qty: " + crystal.RankQty(SetRank);
            DeselectTile();
        }

        virtual public void AssignTarget(UiProbePanel target)
        {
            targetPanel = target;
        }

        override public void SelectTile()
        {
            if (crystal.RankQty(SetRank) <= 0) return;
            if (targetPanel != null)
                targetPanel.SelectRank(SetRank);
            Selected.gameObject.SetActive(true);
        }

        override public void DeselectTile()
        {
            Selected.gameObject.SetActive(false);

        }
    }
}
