namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;

    public class UiProbeTile : UiRigTile
    {
        public const string PREFAB = "tile_probe";

        public UiStarRank Rank;

        private CrystalBlob crystal = null;

        override public string RefId() { return crystal.TheCreature.Id; }
        override public string IconDir() { return "crystalui/"; }
        override public RigBlob GetBlob() { return crystal; }

        override public void AssignBlob(GameBlob b)
        {
            crystal = b as CrystalBlob;
            LoadRig(RefId());
            Refresh();
        }

        override public void SelectTile()
        {
            AssignSelect(true);
        }

        override public void DeselectTile()
        {
            AssignSelect(false);
        }

        override public void Refresh()
        {
            if (Rig.Man.TheProbe.Active == crystal)
            {
                Rank.AssignRank(Rig.Man.TheProbe.CrystalRank, Table.Man.Creature.MaxRank);
                Rank.gameObject.SetActive(true);
            }
            else
                Rank.gameObject.SetActive(false);
        }
    }
}
