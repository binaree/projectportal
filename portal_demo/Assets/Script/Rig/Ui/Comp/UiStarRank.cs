namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiStarRank : UiObj
    {
        public GameObject StarRankPanel;
        public List<GameObject> StarRank;
        public GameObject TrophyRank;

        private void Awake()
        {
            CloseAll();
        }

        public void AssignRank(int rank, int max)
        {
            CloseAll();
            if(rank == max)
            {
                TrophyRank.SetActive(true);
                StarRankPanel.SetActive(false);
            }
            else
            {
                TrophyRank.SetActive(false);
                StarRankPanel.SetActive(true);
                for(int i = 0; i < StarRank.Count; ++i)
                {
                    StarRank[i].SetActive(i <= rank);
                }
            }
        }

        private void CloseAll()
        {
            for (int i = 0; i < StarRank.Count; ++i)
                StarRank[i].SetActive(false);
            TrophyRank.SetActive(false);
        }
    }
}
