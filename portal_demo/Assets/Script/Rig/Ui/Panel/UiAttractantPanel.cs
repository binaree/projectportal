namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiAttractantPanel : UiRigPanel
    {
        public Text Quantity;

        public Text SellLabel;

        private AttractantBlob supply = null;

        override public string IconDir() { return "supplyui/"; }
        override public string PrefabName() { return UiSupplyTile.PREFAB; }
        override public bool IsEquipped() { return supply == Rig.Man.TheAttractant; }

        private void OnEnable()
        {
            Deselect();
            Tile.Initialize();
            List<AttractantBlob> list = Attractant.Man.AllOwned();
            AttractantBlob activeBlob = Rig.Man.TheAttractant;
            for (int i = 0; i < list.Count; ++i)
            {
                UiRigTile t = AddTile(list[i]);
                if (activeBlob == list[i])
                    t.SelectTile();
            }
        }

        override public void ShowBlob(RigBlob b)
        {
            supply = b as AttractantBlob;
            base.ShowBlob(b);
            Quantity.text = (supply.Owns()) ? "Qty: " + supply.Quantity.ToString() : "";
        }

        public override void Deselect()
        {
            base.Deselect();
            supply = null;
        }

        override public void SelectItem()
        {
            if (IsEquipped()) return;
            Rig.Man.AssignAttractant(supply);
            base.SelectItem();
        }

        public void SelectSell()
        {
            Say.Log("Sell has been selected");
        }
    }
}
