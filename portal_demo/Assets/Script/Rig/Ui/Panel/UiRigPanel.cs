namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiRigPanel : UiObj
    {
        public GameObject InfoPanel;
        public Text Name;
        public Image Icon;

        public Text SelectLabel;

        public UiTileSetObj Tile;

        virtual public string IconDir() { return string.Empty; }
        virtual public string PrefabName() { return string.Empty; }
        virtual public bool IsEquipped() { return false; }

        virtual public void SelectItem()
        {
            SelectLabel.text = "In Use";
        }

        virtual public void ShowBlob(RigBlob b)
        {
            Tile.DeselectAll();
            Name.text = Script.Man.Name(b.GetRigData().Id);
            Spritey.ReplaceIcon(Icon, IconDir(), b.GetRigData().Id);
            InfoPanel.SetActive(true);
            SelectLabel.text = (IsEquipped()) ? "In Use" : "Equip";
        }

        virtual public void Deselect()
        {
            InfoPanel.SetActive(false);
        }

        virtual public UiRigTile AddTile(RigBlob b)
        {
            UiRigTile t = Tile.AddNewItem(PrefabName(), b) as UiRigTile;
            if(t != null) t.AssignTarget(this);
            return t;
        }
    }
}
