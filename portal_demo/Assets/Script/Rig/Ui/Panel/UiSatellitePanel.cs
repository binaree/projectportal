namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiSatellitePanel : UiRigPanel
    {
        public Text Level;

        public Text UpgradeLabel;

        private SatelliteBlob satellite = null;

        override public string IconDir() { return "satelliteui/"; }
        override public string PrefabName() { return UiSatelliteTile.PREFAB; }
        override public bool IsEquipped() { return satellite == Rig.Man.TheSatellite; }

        private void OnEnable()
        {
            Deselect();
            Tile.Initialize();
            List<SatelliteBlob> list = Satellite.Man.AllOwned();
            SatelliteBlob activeBlob = Rig.Man.TheSatellite;
            for (int i = 0; i < list.Count; ++i)
            {
                UiRigTile t = AddTile(list[i]);
                if (activeBlob == list[i])
                    t.SelectTile();
            }
        }

        override public void ShowBlob(RigBlob b)
        {
            satellite = b as SatelliteBlob;
            if (satellite == null) { Deselect(); return; }
            base.ShowBlob(b);
            Level.text = "Lvl: " + satellite.Level;
        }

        public override void Deselect()
        {
            base.Deselect();
            satellite = null;
        }

        override public void SelectItem()
        {
            if (IsEquipped()) return;
            Rig.Man.AssignSatellite(satellite);
            base.SelectItem();
        }

        public void SelectUpgrade()
        {
            Say.Log("Sell has been selected");
        }
    }
}
