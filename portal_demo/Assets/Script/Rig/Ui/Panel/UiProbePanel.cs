namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiProbePanel : UiRigPanel
    {
        public UiStarRank Rank;

        public List<UiCrystalTile> CrystalSelect;

        public Text SellLabel;

        private CrystalBlob crystal = null;
        private int assignRank = -1;

        override public string IconDir() { return "crystalui/"; }
        override public string PrefabName() { return UiProbeTile.PREFAB; }
        override public bool IsEquipped() { return crystal == Rig.Man.TheProbe.Active && assignRank == Rig.Man.TheProbe.CrystalRank; }

        private void OnEnable()
        {
            Deselect();
            Tile.Initialize();
            List<CrystalBlob> list = Crystal.Man.AllOwned();
            CrystalBlob activeBlob = Rig.Man.TheProbe.Active;
            for (int i = 0; i < list.Count; ++i)
            {
                UiRigTile t = AddTile(list[i]);
                if (activeBlob == list[i] && t != null)
                    t.SelectTile();
            }
        }

        override public void ShowBlob(RigBlob b)
        {
            crystal = b as CrystalBlob;
            Tile.DeselectAll();
            Name.text = Script.Man.Name(crystal.TheCreature.Id);
            Spritey.ReplaceIcon(Icon, IconDir(), crystal.TheCreature.Id);
            InfoPanel.SetActive(true);
            SelectLabel.text = (IsEquipped()) ? "In Use" : "Equip";

            int best = -1;
            for(int i = 0; i < CrystalSelect.Count; ++i)
            {
                CrystalSelect[i].AssignBlob(crystal);
                CrystalSelect[i].AssignTarget(this);
                if (crystal.RankQty(i) > 0) best = i;
            }

            if (crystal == Rig.Man.TheProbe.Active && crystal.RankQty(Rig.Man.TheProbe.CrystalRank) > 0)
                CrystalSelect[Rig.Man.TheProbe.CrystalRank].SelectTile();
            else if (best > -1)
                CrystalSelect[best].SelectTile();
        }

        public override void Deselect()
        {
            base.Deselect();
            crystal = null;
        }

        override public void SelectItem()
        {
            if (IsEquipped()) return;
            Rig.Man.TheProbe.AssignCrystal(crystal, assignRank);
            Tile.Refresh();
            base.SelectItem();
        }

        public void SelectSell()
        {
            Say.Log("Sell has been selected");
        }

        public void SelectRank(int rank)
        {
            DeselectAllCrystal();
            assignRank = rank;
            Rank.AssignRank(rank, Table.Man.Creature.MaxRank);
        }

        private void DeselectAllCrystal()
        {
            for (int i = 0; i < CrystalSelect.Count; ++i)
                CrystalSelect[i].DeselectTile();
        }
    }
}
