namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Probe : DataManager
    {
        public const string DATA_HEADER = "probe_";

        private Dictionary<string, ProbeBlob> bait = new Dictionary<string, ProbeBlob>();
        
        private static Probe _instance = new Probe();

        public static Probe Man { get { return _instance; } }

        public ProbeData Data(string id)
        {
            return GetData(id) as ProbeData;
        }

        public ProbeData DataP(string id)
        {
            return GetData(DATA_HEADER + id) as ProbeData;
        }

        public ProbeBlob Blob(int refNum)
        {
            return GetBlob(refNum) as ProbeBlob;
        }

        public ProbeBlob Blob(string id)
        {
            if( !bait.ContainsKey(id) )
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return bait[id];
        }

        public ProbeBlob BlobP(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "probe/p");
        }

        protected override GameData NewData(XmlNode node)
        {
            ProbeData d = new ProbeData(node);
            ProbeBlob b = GenerateRefBlob(d, d.RefNum) as ProbeBlob;
            bait.Add(d.Id, b);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new ProbeBlob(d as ProbeData);
        }
    }
}
