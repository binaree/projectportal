namespace Binaree.Game.Core
{
    using System.Xml;

    /// <summary>
    /// Handles loading and accessing of static data for game components
    /// NOTE: id must be unique for the type of data being tracked
    /// </summary>
    public class GameData
    {
        public string Id { get; private set; }

        public GameData() { Id = "no_id"; }

        public GameData(XmlNode xData)
        {
            Id = xData.Attributes.GetNamedItem("id").Value;

            if (Id == "")
                Say.Warn("data is missing id!");
        }

        public string RefIdFromId(string dataHeader)
        {
            return Id.Replace(dataHeader, "");
        }
    }
}