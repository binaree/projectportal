namespace Binaree.Game.User
{  
    
    /// <summary>
    /// Functions as the root point for user data being stored within this data branch
    /// </summary>
    public class UserGraph : UserData
    {
        protected bool isDirty = false;

        override public void Dirty()
        {
            if (parentComp != null) parentComp.Dirty();
            isDirty = true;
        }

        override public bool IsDirty() { return isDirty; }
        override public void Clean() { isDirty = false; }
    }
}
