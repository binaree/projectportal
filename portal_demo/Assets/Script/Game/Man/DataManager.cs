namespace Binaree.Game.Core
{
    using System.Xml;
    using System.Collections.Generic;
    using User;

    /// <summary>
    /// Manages all game data & game blobs for a component
    /// </summary>
    public class DataManager
    {
        protected Dictionary<string, GameData> data = new Dictionary<string, GameData>();
        protected Dictionary<int, GameBlob> blob = new Dictionary<int, GameBlob>();

        virtual protected GameData NewData(XmlNode node) { return null; }
        virtual protected GameBlob NewBlob(GameData d) { return null; }

        public List<GameData> AllGameData()
        {
            List<GameData> all = new List<GameData>();
            foreach (KeyValuePair<string, GameData> d in data)
                all.Add(d.Value);
            return all;
        }

        protected GameData GetData(string id)
        {
            if(data.ContainsKey(id))
                    return data[id];
            Say.Warn("Could not find Data of id = " + id);
            return null;
        }

        protected GameBlob GetBlob(int refNum)
        {
            if (blob.ContainsKey(refNum))
                return blob[refNum];
            Say.Warn("Could not find Blob of ref = " + refNum);
            return null;
        }

        protected void LoadData(XmlDocument xml, string xpath)
        {
            XmlNodeList nodes = xml.SelectNodes(xpath);
            foreach (XmlNode node in nodes)
            {
                GameData d = NewData(node);
                data.Add(d.Id, d);
            }
        }

        protected GameBlob GenerateBlob(GameData gameData)
        {
            GameBlob b = NewBlob(gameData);
            b.GenerateNew(gameData);
            blob.Add(b.RefNum, b);
            return b;
        }

        protected GameBlob GenerateRefBlob(GameData gameData, int refNum)
        {
            GameBlob b = NewBlob(gameData);
            b.GenerateNew(gameData);
            b.SetDirectRef(refNum);
            blob.Add(b.RefNum, b);
            return b;
        }

        protected GameBlob LoadBlob(UserData userData)
        {
            GameData d = GetData(GameBlob.IdFromCompoundId(userData.UserId()));
            if (d == null) return null;
            GameBlob b = NewBlob(d);
            b.LoadFromUser(userData);
            blob.Add(b.RefNum, b);
            return b;
        }
    }
}
