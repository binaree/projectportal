namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.Object;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;

    public class SurfaceBlob : GameBlob
    {
        public SurfaceData Data { get; private set; }

        public SurfaceBlob(SurfaceData d)
        {
            Data = d;
        }

        public float Score(SurfaceCamObj surface)
        {
            // This function will eventually pass in an image taken from the camera
            // This image will then be checked against the properties specified in the data to determine
            //   the score for this surface with 0 = no chance & 1 = perfect fit.
            //Color core = surface.SampleRegionByPercent(0.5f, 0.5f, Data.CoreSize, Data.CoreSize);
            /*Texture2D tex = new Texture2D(1, 1);
            Vuforia.CameraDevice cam = Vuforia.CameraDevice.Instance;
            cam.SetFrameFormat(Vuforia.Image.PIXEL_FORMAT.RGB888, true);
            cam.GetCameraImage(Vuforia.Image.PIXEL_FORMAT.RGB888).CopyToTexture(tex);
            Color core = SampleRegionByPercent(tex, 0.5f, 0.5f, Data.CoreSize, Data.CoreSize);*/
            Color core = Color.white;
            float match = ColorMatch(core, Data.CoreColor);
            if (match >= Data.CoreColorThreshold) return 0.0f;
            float score = 1.0f - match; 
            //Say.Log("Color: " + Data.CoreColor.ToString() + ", Match = " + match + ", Score = " + score);
            return score;
        }

        private Color SampleRegionByPercent(Texture2D tex, float x, float y, float sizeWidth, float sizeHeight)
        {
            //Say.Log("Surface width = " + camTexture.width + ", Surface height = " + camTexture.height);
            int startX = (int)((tex.width * x) - (sizeWidth / 2.0f));
            int startY = (int)((tex.height * y) - (sizeHeight / 2.0f));
            int width = (int)(sizeWidth);
            int height = (int)(sizeHeight);

            Color[] pixels = tex.GetPixels(startX, startY, width, height);
            if (pixels.Length == 0) return Color.black;

            float[] col = { 0.0f, 0.0f, 0.0f };
            for (int i = 0; i < pixels.Length; ++i)
            {
                col[0] += pixels[i].r;
                col[1] += pixels[i].g;
                col[2] += pixels[i].b;
            }

            Color final = new Color(col[0] / pixels.Length, col[1] / pixels.Length, col[2] / pixels.Length);
            //Say.Log("Color average is " + final.ToString());
            return final;
        }

        private float ColorMatch(Color col, Vector3 matchVec)
        {
            Vector3 vec = new Vector3(col.r, col.g, col.b);
            float val = Mathf.Abs(vec.x - matchVec.x);
            val += Mathf.Abs(vec.y - matchVec.y);
            val += Mathf.Abs(vec.z - matchVec.z);
            return val / 3.0f;
        }
    }
}