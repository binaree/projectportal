namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.Object;
    using Binaree.Game.Ui;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class PortalBlob : GameBlob
    {
        public PortalData Data { get; private set; }
        public PortalObj Obj { get; private set; }
        
        public GateBlob PortalGate { get; private set; }
        public List<RegionBlob> Region { get; private set; }
        public SurfaceBlob PortalSurface { get; private set; }

        public float SessionTime { get; private set; }
        public float CurrentTime { get; private set; }
        public float RareBonus { get; private set; }

        public PortalBlob(PortalData d)
        {
            Data = d;
            Region = null;
            Obj = null;
            PortalSurface = Surface.Man.CreateSurfaceForPortal(d);
            PortalGate = Gate.Man.CreateGate(this);
        }

        public void Open()
        {
            Obj = PortalFactory.Inst.TakePortal(Data.Id);
            Obj.Use();

            PopulateRegions();

            SessionTime = Table.Man.Portal.DetermineSession(this);
            CurrentTime = 0.0f;
            RareBonus = Table.Man.Portal.DetermineRareBonus(this);

            Rig.Man.TheRemote.OpenPortal();
            Table.Man.Guide.PortalOpened(this);
            UiMain.Man.Header.EnablePortal(this, true);
        }

        public void Close()
        {
            if (Obj != null)
            {
                PortalFactory.Inst.Release(Obj);
                Obj = null;
            }

            Region = null;

            CurrentTime = SessionTime = 0.0f;

            UiMain.Man.Header.EnablePortal(null, true);
        }

        public bool UpdateSession()
        {
            CurrentTime += TimeLord.Inst.ArenaDelta;
            if( CurrentTime >= SessionTime )
            {
                Portal.Man.CloseActivePortal();
                return true;
            }
            return false;
        }

        private void PopulateRegions()
        {
            Region = new List<RegionBlob>();
            for(int i = 0; i < Data.Region.Count; ++i)
            {
                RegionBlob r = new RegionBlob(Data.Region[i], this);
                Region.Add(r);
            }
        }
    }
}