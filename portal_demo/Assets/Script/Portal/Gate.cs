namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Gate : DataManager
    {
        public const string DATA_HEADER = "gate_";
        public const string LEAGUE_HEADER = DATA_HEADER + League.DATA_HEADER;
        public const string PET_HEADER = DATA_HEADER + Pet.DATA_HEADER;
        
        private static Gate _instance = new Gate();

        public static Gate Man { get { return _instance; } }

        public GateData Data(string id)
        {
            return GetData(id) as GateData;
        }

        public GateData DataG(string id)
        {
            return GetData(DATA_HEADER + id) as GateData;
        }

        public GateData DataLeague(string refId)
        {
            return GetData(LEAGUE_HEADER + refId) as GateData;
        }

        public GateData DataPet(string refId)
        {
            return GetData(PET_HEADER + refId) as GateData;
        }

        public GateBlob CreateGate(PortalBlob port)
        {
            return new GateBlob(port);
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "gate/g");
        }

        protected override GameData NewData(XmlNode node)
        {
            GateData d = new GateData(node);
            return d;
        }
    }
}
