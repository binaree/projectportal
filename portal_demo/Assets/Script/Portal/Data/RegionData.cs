namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class RegionCreature
    {
        public string RefId;
        public float Occurence;
        public float AttractRate;
    }

    public class RegionData : GameData
    {
        private const char LIST_SEP = ',';
        private const char VAL_SEP = '|';
        private const int VAL_ID = 0;
        private const int VAL_OCC = 1;
        private const int VAL_ATT = 2;

        public SplitF Depth { get; private set; }
        public float PollTime { get; private set; }
        public List<RegionCreature> Creature { get; private set; }
        public float BaseOccurence { get; private set; }
        public float BaseAttract { get; private set; }

        public RegionData(XmlNode xData) : base(xData)
        {
            Depth = new SplitF(xData.Attributes.GetNamedItem("depth").Value);
            PollTime = Convert.ToSingle(xData.Attributes.GetNamedItem("poll").Value);
            BaseOccurence = Convert.ToSingle(xData.Attributes.GetNamedItem("occ").Value);
            BaseAttract = Convert.ToSingle(xData.Attributes.GetNamedItem("att").Value);

            Creature = new List<RegionCreature>();
            string[] split = xData.Attributes.GetNamedItem("cr").Value.Split(LIST_SEP);
            for (int i = 0; i < split.Length; ++i)
                AddCreature(split[i]);
        }

        private void AddCreature(string val)
        {
            string[] split = val.Split(VAL_SEP);
            RegionCreature f = new RegionCreature();
            if (split.Length > VAL_ID) f.RefId = split[VAL_ID]; else f.RefId = "";
            if (split.Length > VAL_OCC) f.Occurence = Convert.ToSingle(split[VAL_OCC]); else f.Occurence = 1.0f;
            if (split.Length > VAL_ATT) f.AttractRate = Convert.ToSingle(split[VAL_ATT]); else f.AttractRate = 1.0f;
            Creature.Add(f);
        }
    }
}