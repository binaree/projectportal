namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Xml;
    using UnityEngine;
    using Utility;

    public enum SurfaceType
    {
        FillColor = 0,
    }

    public class SurfaceData : GameData
    {
        private static readonly string[] SURFACE_TYPE = { "fill" };

        public SurfaceType Type { get; private set; }
        public float CoreSize { get; private set; }
        public float CoreWeight { get; private set; }
        public Vector3 CoreColor { get; private set; }
        public float CoreColorThreshold { get; private set; }

        public SurfaceData(XmlNode xData) : base(xData)
        {
            Type = StrToSurfaceType(xData.Attributes.GetNamedItem("type").Value);
            CoreSize = Convert.ToSingle(xData.Attributes.GetNamedItem("coreS").Value);
            CoreWeight = Convert.ToSingle(xData.Attributes.GetNamedItem("coreW").Value);
            CoreColorThreshold = Convert.ToSingle(xData.Attributes.GetNamedItem("coreT").Value);
            CoreColor = Stringy.ParseVector3(xData.Attributes.GetNamedItem("coreC").Value);
        }

        public static SurfaceType StrToSurfaceType(string val)
        {
            for (int i = 0; i < SURFACE_TYPE.Length; ++i)
                if (val == SURFACE_TYPE[i]) return (SurfaceType)i;
            return SurfaceType.FillColor;
        }

        public static string SurfaceTypeToStr(SurfaceType val)
        {
            return SURFACE_TYPE[(int)val];
        }
    }
}