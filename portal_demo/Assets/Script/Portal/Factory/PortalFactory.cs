namespace Binaree.Game.Factory
{
    using Component;
    using Core;
    using Base;
    using System.Collections.Generic;
    using Object;
    
    public class PortalFactory : GameFactory
    {
        public string PortalDirectory = "portal";
        public int PreloadAmount = 1;

        private static PortalFactory _instance = null;

        public static PortalFactory Inst { get { return _instance; } }

        override public void Generate()
        {
            _instance = this;
            List<GameData> allPortal = Portal.Man.AllGameData();
            for(int i = 0; i < allPortal.Count; ++i)
            {
                if (store.ContainsKey(allPortal[i].Id)) continue;
                TrackPrefab(allPortal[i].Id, PreloadAmount);
            }
            hasGenerated = true;
        }

        override protected string FormatTypeToPrefab(string objType) { return objType; }
        override protected string PrefabDirectory() { return PortalDirectory + "/"; }

        public PortalObj TakePortal(string objType) { return Take(objType) as PortalObj; }
    }
}
