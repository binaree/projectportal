namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class PortalKeyComp
    {
        public int RefNum;
        public string RefId;
        public SplitF Power;
        public SplitF RareBonus;
        public float AddLevel = 0;

        public PortalKeyComp(string val)
        {
            RefNum = Unique.Ref.Value();

            string[] split = val.Split('|');
            if (split.Length > 0) RefId = split[0];
            if (split.Length > 1) Power = new SplitF(split[1]); else Power = new SplitF("0");
            if (split.Length > 2) RareBonus = new SplitF(split[2]); else RareBonus = new SplitF("0");
        }
    }
}