namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    
    public class PortalTable : TableData
    {
        public string DefaultPortalRefId { get; private set; }
        public float ScoreMin { get; private set; }

        public float SessionScale { get; private set; }
        public SplitF LeagueTime { get; private set; }
        public SplitF PetTime { get; private set; }

        public SplitF RareBonus { get; private set; }

        public PortalTable(XmlNode xData) : base(xData)
        {
            DefaultPortalRefId = xData.Attributes.GetNamedItem("default").Value;
            ScoreMin = Convert.ToSingle(xData.Attributes.GetNamedItem("scoreMin").Value);

            SessionScale = Convert.ToSingle(xData.Attributes.GetNamedItem("ses").Value);
            LeagueTime = new SplitF(xData.Attributes.GetNamedItem("lea").Value);
            PetTime = new SplitF(xData.Attributes.GetNamedItem("pet").Value);

            RareBonus = new SplitF(xData.Attributes.GetNamedItem("rare").Value);
        }

        public PortalBlob PickPortal(List<PortalBlob> port)
        {
            PortalBlob best = null;
            float score = -1.0f;

            for(int i = 0; i < port.Count; ++i)
            {
                if (!port[i].PortalGate.CanOpen()) continue;
                
                float val = port[i].PortalSurface.Score(Step.Man.Layer.Surface);
                if (val > score)
                {
                    score = val;
                    best = port[i];
                }
            }

            if (score > ScoreMin)
                return best;

            return Portal.Man.BlobP(DefaultPortalRefId);
        }

        public float DetermineSession(PortalBlob port)
        {
            float session = LeagueTime.Value(port.PortalGate.LeaguePower());
            if (port.PortalGate.PetPowerNeg()) session -= PetTime.Value(port.PortalGate.PetPower());
            else session += PetTime.Value(port.PortalGate.PetPower());
            session *= port.Data.SessionTime;
            session *= SessionScale;
            return session;
        }

        public float DetermineRareBonus(PortalBlob port)
        {
            float bonus = port.PortalGate.LeagueRareBonus();
            bonus += port.PortalGate.PetRareBonus();
            if (bonus < RareBonus.Min()) bonus = RareBonus.Min();
            if (bonus > RareBonus.Max()) bonus = RareBonus.Max();
            return bonus;
        }
    }
}