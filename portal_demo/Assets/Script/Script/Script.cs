namespace Binaree.Game.Utility
{
    using System;
    using System.Xml;
    using System.Collections.Generic;
    using Core;

    /// <summary>
    /// Handles retrieving and distributing all user facing text in the game
    /// </summary>
    public class Script
    {
        public const string NAME_HEADER = "name_";
        public const string TEXT_HEADER = "txt_";

        private static Script _instance = new Script();

        private Dictionary<string, string> scripts = new Dictionary<string, string>();

        public static Script Man { get { return _instance; } }

        public string Direct(string id)
        {
            if (scripts.ContainsKey(id))
                return scripts[id];
            Say.Warn("Could not find Script of id = " + id);
            return "***NONE***";
        }

        public string Name(string id)
        {
            return Direct(NAME_HEADER + id);
        }

        public string Text(string id)
        {
            return Direct(TEXT_HEADER + id);
        }

        public void Load(XmlDocument xml)
        {
            XmlNodeList nodes = xml.SelectNodes("script/s");
            LoadAddendum(nodes);
        }

        public void LoadAddendum(XmlNodeList nodes)
        {
            foreach (XmlNode node in nodes)
            {
                scripts.Add(node.Attributes.GetNamedItem("id").Value, node.Attributes.GetNamedItem("d").Value);
            }
        }
    }
}
