namespace Binaree.Game.Component
{
    using Core;
    using System;
    using Ui;

    public class StepInit : StepBase
    {
        public const string STEP_ID = "init";

        private bool isGenerating = false;

        override public void Begin()
        {
            isGenerating = false;
            UiMain.Man.SelectPage(UiHomePage.PAGE_ID);
            UiMain.Man.Header.EnableCurrency(true);
            UiMain.Man.Header.EnableLeague(League.Man.Standard);
            Say.Log("Have begun the init step");
        }

        override public void End()
        {

        }

        public bool StartGenerate()
        {
            if (!Rig.Man.TheRemote.CanOpen())
            {
                Say.Log("Remote pet cannot open the portal, try selecting a new pet to open the portal.");
                // TODO: Show select remote pet popup here
                return false;
            }

            isGenerating = true;
            UiMain.Man.Header.SetVisibility(false);
            UiMain.Man.SelectPage(UiGeneratePage.PAGE_ID);
            return true;
        }

        public void CancelGenerate()
        {
            Say.Log("Cancel generate...");
            isGenerating = false;
            UiMain.Man.Header.SetVisibility(true);
            UiMain.Man.SelectPage(UiHomePage.PAGE_ID);
        }

        public void CaptureSurface()
        {
            Say.Log("Capturing the Portal (would go here)...");
            isGenerating = false;
            Step.Man.Layer.ArControl.FindPlane();
            Portal.Man.OpenPortal();
            Step.Man.Begin(StepSelect.STEP_ID);
        }

        override public void HandleRemoteSelect()
        {
            if (isGenerating)
                CaptureSurface();
            else
                UiMain.Man.SelectOverlay(UiRemoteOverlay.PAGE_ID);
        }
    }
}