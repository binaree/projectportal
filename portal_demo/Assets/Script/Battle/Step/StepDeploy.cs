namespace Binaree.Game.Component
{
    using Binaree.Game.Core;
    using System;
    using Table;
    using Ui;

    public class StepDeploy : StepBase
    {
        public const string STEP_ID = "deploy";

        override public void Begin()
        {
            UiMain.Man.Header.SetVisibility(false);
            UiMain.Man.SelectPage(UiDeployPage.PAGE_ID);
        }

        override public void End()
        {
            UiMain.Man.Header.SetVisibility(true);
        }

        public void GoDeploy(float deepPercent)
        {
            //Say.Log("GO! Depth% is = " + depthPercent);
            Rig.Man.TheSatellite.CreateSatellite();
            Table.Man.Depth.SetDepth(deepPercent);
            Step.Man.Begin(StepWait.STEP_ID);
        }
    }
}