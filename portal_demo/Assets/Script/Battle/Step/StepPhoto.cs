namespace Binaree.Game.Component
{
    using Binaree.Game.Core;
    using System;
    using Table;
    using Ui;

    public class StepPhoto : StepBase
    {
        public const string STEP_ID = "photo";

        override public void Begin()
        {
            UiMain.Man.SelectPage(UiCatchPage.PAGE_ID);
        }

        override public void End()
        {
        }
    }
}