namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    using UnityEngine;

    public class BattleTable : TableData
    {
        public float CurrentPower { get; private set; }
        public float PowerLevel { get { return CurrentPower / inputMax; } }

        public float PowerScale { get; private set; }
        public float DecayScale { get; private set; }
        public float AppScale { get; private set; }

        public SplitF ActionTime { get; private set; }
        public SplitF ControlTime { get; private set; }
        public SplitF StandardPollTime { get; private set; }
        public SplitF LimitPollTime { get; private set; }
        
        private CreatureBlob cr = null;
        private TensionTable tension = null;
        private float inputMax = 0.0f;
        private float inputPower = 0.0f;
        private float inputDecay = 0.0f;
        private float useAppScale = 1.0f;

        public BattleTable(XmlNode xData) : base(xData)
        {
            PowerScale = Convert.ToSingle(xData.Attributes.GetNamedItem("pow").Value);
            DecayScale = Convert.ToSingle(xData.Attributes.GetNamedItem("decay").Value);
            AppScale = Convert.ToSingle(xData.Attributes.GetNamedItem("app").Value);

            ActionTime = new SplitF(xData.Attributes.GetNamedItem("at").Value);
            ControlTime = new SplitF(xData.Attributes.GetNamedItem("ct").Value);
            StandardPollTime = new SplitF(xData.Attributes.GetNamedItem("spt").Value);
            LimitPollTime = new SplitF(xData.Attributes.GetNamedItem("lpt").Value);

            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
                useAppScale = AppScale;
        }

        public void StartBattle()
        {
            CurrentPower = 0.0f;
            cr = Creature.Man.Active;
            tension = Table.Man.Tension;
            inputMax = Rig.Man.TheRemote.Data.LvlInputMax(Rig.Man.TheRemote.Level);
            inputPower = Rig.Man.TheRemote.Data.LvlInputPower(Rig.Man.TheRemote.Level);
            inputDecay = Rig.Man.TheRemote.Data.LvlInputDecay(Rig.Man.TheRemote.Level);
        }

        public void ApplyInput()
        {
            CurrentPower += inputPower * PowerScale * useAppScale;
            if (CurrentPower > inputMax)
                CurrentPower = inputMax;
            //Say.Log("Current power is not at " + CurrentPower);
        }

        public void DecayInput()
        {
            if (Creature.Man.Active == null) return;

            CurrentPower -= inputDecay * DecayScale * TimeLord.Inst.ArenaDelta;
            if (CurrentPower < 0.0f)
                CurrentPower = 0.0f;
            else if (CurrentPower > 0.0f)
                ApplyPower();

            cr.MoveAi.UpdateMove();
            tension.UpdateTension();
        }

        public float ActionAdjust(float curTime)
        {
            float rodAction = Rig.Man.TheSatellite.Data.LvlAction(Rig.Man.TheSatellite.Level);
            return curTime * ActionTime.Value(rodAction);
        }

        public float ControlAdjust(float curTime)
        {
            float rodCon = Rig.Man.TheSatellite.Data.LvlControl(Rig.Man.TheSatellite.Level);
            return curTime * ControlTime.Value(rodCon);
        }

        public float StandardPollAdjust(float curTime)
        {
            float rodLimit = Rig.Man.TheSatellite.Data.LvlLimit(Rig.Man.TheSatellite.Level);
            return curTime * StandardPollTime.Value(rodLimit);
        }

        public float LimitPollAdjust(float curTime)
        {
            float rodLimit = Rig.Man.TheSatellite.Data.LvlLimit(Rig.Man.TheSatellite.Level);
            return curTime * LimitPollTime.Value(rodLimit);
        }

        private void ApplyPower()
        {
            //Say.Log("Applying power of " + CurrentPower );
            tension.ApplyRemotePower(CurrentPower, PowerLevel);
        }

    }
}