namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    
    public class DepthTable : TableData
    {
        public float CurrentDepth { get; private set; }

        public float DepthScale { get; private set; }
        public float WarningDepth { get; private set; }

        public float WaitDrawScale { get; private set; }
        public float DrawScale { get; private set; }
        public float DragScale { get; private set; }
        public float SpeedScale { get; private set; }

        private float reelDraw = 0.0f;
        private float reelDrag = 0.0f;
        private float maxDistance = 0.0f;

        public DepthTable(XmlNode xData) : base(xData)
        {
            DepthScale = Convert.ToSingle(xData.Attributes.GetNamedItem("scale").Value);
            WarningDepth = Convert.ToSingle(xData.Attributes.GetNamedItem("warn").Value);

            WaitDrawScale = Convert.ToSingle(xData.Attributes.GetNamedItem("wait").Value);
            DrawScale = Convert.ToSingle(xData.Attributes.GetNamedItem("draw").Value);
            DragScale = Convert.ToSingle(xData.Attributes.GetNamedItem("drag").Value);
            SpeedScale = Convert.ToSingle(xData.Attributes.GetNamedItem("speed").Value);
        }

        public float MaxDistance()
        {
            float reelDepth = Rig.Man.TheRemote.Data.LvlMaxDepth(Rig.Man.TheRemote.Level);
            float lineSize = Rig.Man.TheLine.Data.Size;
            return  reelDepth * lineSize * DepthScale;
        }

        public float SetDepth(float sinkPercent)
        {
            reelDraw = Rig.Man.TheRemote.Data.LvlDraw(Rig.Man.TheRemote.Level);
            reelDrag = Rig.Man.TheRemote.Data.LvlDrag(Rig.Man.TheRemote.Level);
            maxDistance = MaxDistance();
            CurrentDepth = Table.Man.Deploy.DeployDistance(sinkPercent);
            return CurrentDepth;
        }

        public void ApplyWaitPower(float powLvl)
        {
            float draw = reelDraw * powLvl * WaitDrawScale * TimeLord.Inst.ArenaDelta;
            DrawLine(draw);
        }

        public void ApplyDraw(float powLvl)
        {
            float draw = reelDraw * powLvl * DrawScale * TimeLord.Inst.ArenaDelta;
            DrawLine(draw);
        }

        public void ApplyDrag(float powLvl)
        {
            float drag = reelDrag * powLvl * DragScale * TimeLord.Inst.ArenaDelta;
            DragLine(drag);
        }

        private void DrawLine(float draw)
        {
            //Say.Log("Drawing " + draw);
            CurrentDepth -= draw;
            if (CurrentDepth <= 0.0f)
            {
                if (Creature.Man.Active != null && Creature.Man.Active.HitAi.Status == HitStatus.Attached)
                {
                    Say.Log("Congratulations! You caught the creature!");
                    Step.Man.Battle.Catch();
                }
                else
                {
                    Step.Man.Wait.DrawIn();
                }
            }
        }

        private void DragLine(float drag)
        {
            drag *= Creature.Man.Active.Stats.Speed * SpeedScale;
            CurrentDepth += drag;
            if (CurrentDepth > maxDistance)
                Step.Man.Battle.LostCreature(StepBattle.Lost.Distance);
        }
    }
}