namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    using UnityEngine;

    public class WaitTable : TableData
    {
        public float CurrentPower { get; private set; }
        public float PowerLevel { get { return CurrentPower / inputMax; } }

        public float PowerScale { get; private set; }
        public float DecayScale { get; private set; }
        public float AppScale { get; private set; }

        private AttractTable attract = null;
        private SetTable hit = null;
        private float inputMax = 0.0f;
        private float inputPower = 0.0f;
        private float inputDecay = 0.0f;
        private float useAppScale = 1.0f;

        public WaitTable(XmlNode xData) : base(xData)
        {
            PowerScale = Convert.ToSingle(xData.Attributes.GetNamedItem("pow").Value);
            DecayScale = Convert.ToSingle(xData.Attributes.GetNamedItem("decay").Value);
            AppScale = Convert.ToSingle(xData.Attributes.GetNamedItem("app").Value);

            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
                useAppScale = AppScale;
        }

        public void StartWait()
        {
            CurrentPower = 0.0f;
            attract = Table.Man.Attract;
            hit = Table.Man.Set;
            inputMax = Rig.Man.TheRemote.Data.LvlInputMax(Rig.Man.TheRemote.Level);
            inputPower = Rig.Man.TheRemote.Data.LvlInputPower(Rig.Man.TheRemote.Level);
            inputDecay = Rig.Man.TheRemote.Data.LvlInputDecay(Rig.Man.TheRemote.Level);
        }

        public void ApplyInput()
        {
            CurrentPower += inputPower * PowerScale * useAppScale;
            if (CurrentPower > inputMax)
                CurrentPower = inputMax;
            //Say.Log("Current power is not at " + CurrentPower);
        }

        public void DecayInput()
        {
            CurrentPower -= inputDecay * DecayScale * TimeLord.Inst.ArenaDelta;
            if (CurrentPower < 0.0f)
            {
                CurrentPower = 0.0f;
                attract.EstablishDepth(); // NOTE: a small bug here if current power is exactly 0.0f...
            }
            else if (CurrentPower > 0.0f)
                ApplyPower();

            attract.UpdateAttract();
            hit.UpdateHit(CurrentPower > 0.0f);
        }

        private void ApplyPower()
        {
            //Say.Log("Applying power of " + CurrentPower );
            Table.Man.Depth.ApplyWaitPower(PowerLevel);
            if (Creature.Man.Active == null)
                attract.MoveDepth();
        }
    }
}