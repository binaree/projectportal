namespace Binaree.Game.Component
{
    using Binaree.Game.Core;
    using Binaree.Game.Object;
    using System.Collections.Generic;

    public class Step
    {
        public LayerControl Layer { get; private set; }
        public StepBase Current { get; private set; }

        public StepInit Init { get { return GetStep(StepInit.STEP_ID) as StepInit; } }
        public StepSelect Select { get { return GetStep(StepSelect.STEP_ID) as StepSelect; } }
        public StepDeploy Deploy { get { return GetStep(StepDeploy.STEP_ID) as StepDeploy; } }
        public StepWait Wait { get { return GetStep(StepWait.STEP_ID) as StepWait; } }
        public StepBattle Battle { get { return GetStep(StepBattle.STEP_ID) as StepBattle; } }
        public StepCatch Catch { get { return GetStep(StepCatch.STEP_ID) as StepCatch; } }
        public StepPhoto Photo { get { return GetStep(StepPhoto.STEP_ID) as StepPhoto; } }

        private Dictionary<string, StepBase> step = new Dictionary<string, StepBase>();

        private static Step _instance = new Step();

        public static Step Man { get { return _instance; } }

        public Step()
        {
            Current = null;
            step.Add(StepInit.STEP_ID, new StepInit());
            step.Add(StepSelect.STEP_ID, new StepSelect());
            step.Add(StepDeploy.STEP_ID, new StepDeploy());
            step.Add(StepWait.STEP_ID, new StepWait());
            step.Add(StepBattle.STEP_ID, new StepBattle());
            step.Add(StepCatch.STEP_ID, new StepCatch());
            step.Add(StepPhoto.STEP_ID, new StepPhoto());
        }

        public StepBase GetStep(string stepId)
        {
            if (!step.ContainsKey(stepId))
            {
                Say.Log("Could not find step = " + stepId);
                return null;
            }
            return step[stepId];
        }

        public void Begin(string stepId)
        {
            End();
            Current = GetStep(stepId);
            Current.Begin();
        }

        public void End()
        {
            if (Current == null) return;
            Current.End();
            Current = null;
        }

        public void SetLayer(LayerControl lay)
        {
            Layer = lay;
        }
    }
}
