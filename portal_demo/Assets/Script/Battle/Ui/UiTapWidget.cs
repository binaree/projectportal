namespace Binaree.Game.Ui
{
    using Component;
    using Core;
    using Object;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;
    
    public class UiTapWidget : UiObj
    {
        public enum Style
        {
            Wait = 0,
            Battle = 1,
        }

        public Style BattleMode = Style.Battle;

        private WaitTable wait = null;
        private BattleTable battle = null;

        private void Awake()
        {
            wait = Table.Man.Wait;
            battle = Table.Man.Battle;
        }

        private void Update()
        {
            if (BattleMode == Style.Wait)
                wait.DecayInput();
            else if (BattleMode == Style.Battle)
                battle.DecayInput();
        }

        public void ScreenTap()
        {
            if (BattleMode == Style.Wait)
                wait.ApplyInput();
            else if (BattleMode == Style.Battle)
                battle.ApplyInput();
        }
    }
}
