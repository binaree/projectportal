namespace Binaree.Game.Ui
{
    using Component;
    using Core;
    using Object;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;
    
    public class UiDepthWidget : UiObj
    {
        public Slider Meter;
        public GameObject Warning;
        public Text DepthLabel;
        public Text MaxDepthLabel;

        private DepthTable depth;
        private float maxDepth = 0.0f;
        private float curDepth = 0.0f;

        private void Awake()
        {
            depth = Table.Man.Depth;
            
        }

        private void OnEnable()
        {
            Warning.SetActive(false);
            maxDepth = depth.MaxDistance();
            curDepth = depth.CurrentDepth;
            DepthLabel.text = ((int)curDepth) + "m";
            MaxDepthLabel.text = ((int)maxDepth) + "m";

            Meter.value = curDepth / maxDepth;
        }

        private void Update()
        {
            curDepth = depth.CurrentDepth;
            DepthLabel.text = ((int)curDepth) + "m";
            DepthLabel.text = ((int)depth.CurrentDepth) + "m";

            Meter.value = curDepth / maxDepth;
            Warning.SetActive(Meter.value >= depth.WarningDepth);
        }
    }
}
