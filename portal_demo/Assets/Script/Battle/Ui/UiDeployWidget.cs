namespace Binaree.Game.Ui
{
    using Component;
    using Core;
    using Object;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;
    
    public class UiDeployWidget : UiObj
    {
        public Image Back;
        public GameObject Marker;
        public Text MinDepthLabel;
        public Text MaxDepthLabel;

        public float MinY = -200.0f;
        public float MaxY = 200.0f;

        private float distance = 0.0f;
        private float pos = 0.0f;

        private void Awake()
        {
            
        }

        private void OnEnable()
        {
            Marker.transform.localPosition = new Vector3(Marker.transform.localPosition.x, MinY, Marker.transform.localPosition.z);
            distance = MaxY - MinY;
            pos = 0;
            MinDepthLabel.text = ((int)Table.Man.Deploy.MinDeployDistance()) + "m";
            MaxDepthLabel.text = ((int)Table.Man.Deploy.MaxDeployDistance()) + "m";
        }

        private void Update()
        {
            pos += Table.Man.Deploy.MarkerMoveSpeed();
            if (pos >= 1.0f)
                pos = 0.0f;

            Marker.transform.localPosition = new Vector3(Marker.transform.localPosition.x, MinY + (distance * pos), Marker.transform.localPosition.z);
        }

        public float CurrentPosition() { return pos; }
    }
}
