namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.User;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class GuideBlob : GameBlob
    {

        public GuideBlob(GuideData d)
        {
        }

        virtual public GuideData GetGuideData() { return null; }
    }
}