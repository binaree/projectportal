namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.User;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;

    public class GuidePortalBlob : GuideBlob
    {
        public enum RewardState
        {
            Incomplete = 0,
            Ready = 1,
            Complete = 2,
        }

        public GuidePortalData Data { get; private set; }
        public UserGuidePortal User { get; private set; }

        public RewardBlob OpenReward { get; private set; }
        public bool NewPortal { get; private set; }

        public GuidePortalBlob(GuideData d) : base(d)
        {
            Data = d as GuidePortalData;
            OpenReward = null;
            NewPortal = false;
        }

        override public GuideData GetGuideData() { return Data; }

        public override void LoadFromUser(UserData userData)
        {
            User = userData as UserGuidePortal;
        }

        public bool IsNewPortal() { return TimesOpened() == 0; }
        public int TimesOpened() { return User.Count; }
        public bool IsLongestSession(PortalBlob port) { return port.SessionTime > SessionRecord(); }
        public float SessionRecord() { return User.MaxSession; }

        public void UpdateGuide(PortalBlob port)
        {
            if (IsNewPortal())
            {
                User.SetGuideReward((int)RewardState.Ready);
            }

            User.IncTimesOpened();
            if (IsLongestSession(port))
                User.SetSession(port.SessionTime);
        }

        public void SetReward(RewardBlob rew, bool newPortal)
        {
            OpenReward = rew;
            NewPortal = newPortal;
        }

        public bool CanRewardGuide() { return User.GuideReward != (int)RewardState.Complete; }
        public bool HasGuideRewarded() { return User.GuideReward == (int)RewardState.Complete; }
        public bool GuideRewardAvailable() { return User.GuideReward == (int)RewardState.Ready; }
        public bool GuideRewardIncomplete() { return User.GuideReward == (int)RewardState.Incomplete; }

        public RewardBlob CollectGuideReward()
        {
            if (!GuideRewardAvailable()) return null;
            OpenReward = Reward.Man.GenerateReward(Data.GuideCompleteReward);
            User.SetGuideReward((int)RewardState.Complete);
            return OpenReward;
        }
    }
}