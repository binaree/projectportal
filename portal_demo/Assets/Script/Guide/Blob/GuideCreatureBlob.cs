namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.User;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class GuideCreatureBlob : GuideBlob
    {
        public enum RewardState
        {
            Incomplete = 0,
            Ready = 1,
            Complete = 2,
        }

        public GuideCreatureData Data { get; private set; }
        public UserGuideCreature User { get; private set; }

        public RewardBlob CatchReward { get; private set; }
        public bool NewCatch { get; private set; }

        public GuideCreatureBlob(GuideData d) : base(d)
        {
            Data = d as GuideCreatureData;
            CatchReward = null;
            NewCatch = false;
        }

        override public GuideData GetGuideData() { return Data; }

        public override void LoadFromUser(UserData userData)
        {
            User = userData as UserGuideCreature;
        }

        public bool IsNewCreature()
        {
            int maxRank = Table.Man.Creature.MaxRank;
            for (int i = 0; i <= maxRank; ++i)
                if (User.NumberCaught[i] > 0)
                    return false;
            return true;
        }

        public bool IsNewRank(CreatureBlob cr) { return NumberCaught(cr) == 0; }
        public int NumberCaught(CreatureBlob cr) { return User.NumberCaught[(int)cr.Rank]; }
        public bool IsSizeRecord(CreatureBlob cr) { return cr.SizeLevel > SizeRecord(cr); }
        public float SizeRecord(CreatureBlob cr) { return User.SizeRecord[(int)cr.Rank]; }

        public void UpdateGuide(CreatureBlob cr)
        {
            if (IsNewRank(cr))
            {
                for(int i = 0; i <= (int)cr.Rank; ++i)
                {
                    if (!RankRewardIncomplete(i)) continue;
                    User.SetRankReward(i, (int)RewardState.Ready);
                }
            }

            User.IncNumCaught((int)cr.Rank, 1);
            if (IsSizeRecord(cr))
                User.SetSizeRecord((int)cr.Rank, cr.SizeLevel);
        }

        public void SetReward(RewardBlob rew, bool newCreature)
        {
            CatchReward = rew;
            NewCatch = newCreature;
        }

        public bool CanRewardRank(int rankSlot) { return User.RankReward[rankSlot] != (int)RewardState.Complete; }
        public bool HasRankRewarded(int rankSlot) { return User.RankReward[rankSlot] == (int)RewardState.Complete; }
        public bool RankRewardAvailable(int rankSlot) { return User.RankReward[rankSlot] == (int)RewardState.Ready; }
        public bool RankRewardIncomplete(int rankSlot) { return User.RankReward[rankSlot] == (int)RewardState.Incomplete; }

        public RewardBlob CollectRankReward(int rankSlot)
        {
            if (!RankRewardAvailable(rankSlot)) return null;
            CatchReward = Reward.Man.GenerateReward(Data.RankCollectReward[rankSlot]);
            User.SetRankReward(rankSlot, (int)RewardState.Complete);

            int maxRank = Table.Man.Creature.MaxRank;
            for (int i = maxRank; i >= 0; --maxRank)
                if (CanRewardRank(i))
                    return CatchReward;
            User.SetGuideReward((int)RewardState.Ready);
            return CatchReward;
        }

        public bool CanRewardGuide() { return User.GuideReward != (int)RewardState.Complete; }
        public bool HasGuideRewarded() { return User.GuideReward == (int)RewardState.Complete; }
        public bool GuideRewardAvailable() { return User.GuideReward == (int)RewardState.Ready; }
        public bool GuideRewardIncomplete() { return User.GuideReward == (int)RewardState.Incomplete; }

        public RewardBlob CollectGuideReward()
        {
            if (!GuideRewardAvailable()) return null;
            CatchReward = Reward.Man.GenerateReward(Data.GuideCompleteReward);
            User.SetGuideReward((int)RewardState.Complete);
            return CatchReward;
        }
    }
}