namespace Binaree.Game.User
{
    using Component;
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;
    using Utility;
    
    public class UserGuidePortal : UserData
    {
        public const string TAG = "p";

        [XmlAttribute("id")]
        public string RefId { get; private set; }

        [XmlAttribute("num")]
        public int Count { get; private set; }

        [XmlAttribute("max")]
        public float MaxSession { get; private set; }

        [XmlAttribute("rew")]
        public int GuideReward { get; private set; }

        public UserGuidePortal() : base()
        {
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }

        public void IncTimesOpened()
        {
            Count++;
            Dirty();
        }

        public void SetSession(float sesTime)
        {
            MaxSession = sesTime;
            Dirty();
        }

        public void SetGuideReward(int val)
        {
            GuideReward = val;
            Dirty();
        }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            Guide.Man.GeneratePortalFromUser(this);

            Dirty();
        }
    }
}
