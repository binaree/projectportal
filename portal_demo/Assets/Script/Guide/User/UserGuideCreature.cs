namespace Binaree.Game.User
{
    using Component;
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;
    using Utility;
    
    public class UserGuideCreature : UserData
    {
        public const string TAG = "g";

        [XmlAttribute("id")]
        public string RefId { get; private set; }

        [XmlIgnore]
        public List<int> NumberCaught { get; private set; }

        [XmlAttribute("num")]
        public string NumberCaughtStr
        {
            get { return Stringy.FormatList<int>(NumberCaught); }
            private set { Stringy.ParseList<int>(value, NumberCaught); }
        }

        [XmlIgnore]
        public List<float> SizeRecord { get; private set; }

        [XmlAttribute("size")]
        public string SizeRecordStr
        {
            get { return Stringy.FormatList<float>(SizeRecord); }
            private set { Stringy.ParseList<float>(value, SizeRecord); }
        }

        [XmlIgnore]
        public List<int> RankReward { get; private set; }

        [XmlAttribute("rank")]
        public string RankRewardStr
        {
            get { return Stringy.FormatList<int>(RankReward); }
            private set { Stringy.ParseList<int>(value, RankReward); }
        }

        [XmlAttribute("rew")]
        public int GuideReward { get; private set; }

        public UserGuideCreature() : base()
        {
            NumberCaught = new List<int>();
            SizeRecord = new List<float>();
            RankReward = new List<int>();
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }

        public void IncNumCaught(int slotNum, int amt)
        {
            NumberCaught[slotNum] += amt;
            Dirty();
        }

        public void SetSizeRecord(int slotNum, float size)
        {
            SizeRecord[slotNum] = size;
            Dirty();
        }

        public void SetRankReward(int slotNum, int val)
        {
            RankReward[slotNum] = val;
            Dirty();
        }

        public void SetGuideReward(int val)
        {
            GuideReward = val;
            Dirty();
        }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);
            
            Guide.Man.GenerateCreatureFromUser(this);

            Dirty();
        }
    }
}
