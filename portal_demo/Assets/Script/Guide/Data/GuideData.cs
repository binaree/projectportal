namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public enum GuideType
    {
        Creature = 0,
        Portal = 1,
    }

    public class GuideData : GameData
    {
        public static readonly string[] TYPE = { "cr", "port" };

        public GuideType Type { get; protected set; }
        public float RewardScale { get; private set; }

        public GuideData(XmlNode xData) : base(xData)
        {
            RewardScale = Convert.ToSingle(xData.Attributes.GetNamedItem("rs").Value);
        }

        public static GuideType StrToType(string val)
        {
            for (int i = 0; i < TYPE.Length; ++i)
                if (val == TYPE[i]) return (GuideType)i;
            return GuideType.Creature;
        }

        public static string TypeToStr(GuideType val)
        {
            return TYPE[(int)val];
        }
    }
}