namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class GuidePortalData : GuideData
    {
        public string GuideCompleteReward { get; private set; }

        public GuidePortalData(XmlNode xData) : base(xData)
        {
            Type = GuideType.Portal;
            GuideCompleteReward = xData.Attributes.GetNamedItem("rew").Value;
        }
    }
}