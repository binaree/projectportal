namespace Binaree.Game.Ui
{
    using Component;
    using Core;
    using Object;
    using UnityEngine;
    using Utility;
    
    public class UiTouchPad : UiObj
    {
        public enum State
        {
            Inactive = 0,
            Pressed = 1,
            Act = 2,
        }

        public GameObject TouchImage;
        
        public float MinActDistance = 50.0f;
        public float MaxPowerDistance = 500.0f;

        private State state = State.Inactive;
        private Vector3 pressPos = Vector3.zero;
        private float power = 0.0f;

        private void Awake()
        {
            Use();
            TurnOff();
        }

        private void Update()
        {
            if (state == State.Inactive) return;
            float useDist = Inputy.DistToPoint(pressPos);
            //Say.Log("distace is " + useDist);
            if (useDist < MinActDistance)
            {
                state = State.Pressed;
            }
            else
            {
                power = (useDist - MinActDistance) / (MaxPowerDistance - MinActDistance);
                if (power > 1.0f) power = 1.0f;
                state = State.Act;
            }
        }

        public void TurnOn()
        {
            state = State.Inactive;
            gameObject.SetActive(true);
        }

        public void TouchPress()
        {
            if (state != State.Inactive) return;
            state = State.Pressed;
            pressPos = Inputy.InputPosition();
            TouchImage.gameObject.transform.position = pressPos;
            TouchImage.gameObject.SetActive(true);
        }

        public void TurnOff()
        {
            state = State.Inactive;
            HideStuff();
            gameObject.SetActive(false);
        }

        public void TouchRelease()
        {
            if (state == State.Inactive) return;
            if(state == State.Act)
            {
                //#Battle.Man.TransitionTo(BattleState.Act);
            }
            else
            {
                state = State.Inactive;
                HideStuff();
            }
        }

        public float ActPower() { return power; }
        public Vector3 TouchDir() { return (Inputy.InputPosition() - pressPos); }

        private void HideStuff()
        {
            TouchImage.gameObject.SetActive(false);
        }
        
    }
}
