namespace Binaree.Game.Factory
{
    using Component;
    using Core;
    using Base;
    using System.Collections.Generic;
    using Object;
    
    public class SatelliteFactory : GameFactory
    {
        public string SatelliteDirectory = "satellite";
        public int PreloadAmount = 1;

        private static SatelliteFactory _instance = null;

        public static SatelliteFactory Inst { get { return _instance; } }

        override public void Generate()
        {
            _instance = this;
            List<GameData> allSat = Satellite.Man.AllGameData();
            for(int i = 0; i < allSat.Count; ++i)
            {
                if (store.ContainsKey(allSat[i].Id)) continue;
                TrackPrefab(allSat[i].Id, PreloadAmount);
            }
            hasGenerated = true;
        }

        override protected string FormatTypeToPrefab(string objType) { return objType; }
        override protected string PrefabDirectory() { return SatelliteDirectory + "/"; }

        public SatelliteObj TakeSatellite(string objType) { return Take(objType) as SatelliteObj; }
    }
}
