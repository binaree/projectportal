namespace Binaree.Game.Object
{
    using Component;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Manages the layers available for spawning in the stage
    /// Responsible for passing Unity Engine functionality to the modes
    /// </summary>
    public class LayerControl : GameObj
    {
        public enum World
        {
            Backdrop = 0,
            Bottom = 1,
            Ground = 2,
            Between = 3,
            Sky = 4,
            Top = 5,
        }

        public enum Ui
        {
            Sub = 0,
            Mid = 1,
            Top = 2,
        }

        public List<GameObject> WorldLayer;
        public List<GameObject> UiLayer;
        public GameObject GuideLayer;
        public SurfaceCamObj Surface;
        public GameObject ArSurface;
        public ArPlaneControl ArControl;

        private void Awake()
        {
            GuideLayer.SetActive(false);
            Step.Man.SetLayer(this);
        }

        private void Start()
        {
            Step.Man.Begin(StepInit.STEP_ID);
            gameObject.SetActive(false);
        }

        private void Update()
        {
        }

        public GameObject AssignObjToWorld(GameObj assignObj, World toLayer)
        {
            assignObj.gameObject.transform.SetParent(WorldLayer[(int)toLayer].transform);

            // Layer dictates the y position of all objects for layering purposes
            assignObj.gameObject.transform.localPosition = new Vector3(assignObj.gameObject.transform.localPosition.x, 0.0f, assignObj.gameObject.transform.localPosition.z);
            return WorldLayer[(int)toLayer];
        }

        public void AssignObjToUi(GameObj assignObj, Ui toLayer)
        {
            // TODO: This
        }

        public void AttachToAr()
        {
            gameObject.transform.SetParent(ArSurface.transform);
            gameObject.SetActive(true);
        }
    }
}
