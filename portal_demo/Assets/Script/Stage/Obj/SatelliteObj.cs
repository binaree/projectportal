namespace Binaree.Game.Object
{
    using Base;
    using Component;
    using Core;
    using Factory;
    using Object;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class SatelliteObj : PropObj
    {
        public enum Status
        {
            Wait = 0,
            Nibble = 1,
            Bite = 2,
            Hit = 3,
        }

        public GameObject TheSatellite;
        public GameObject BiteRippleObj;
        public GameObject NibRippleObj;

        public float SplashTime = 2.0f;
        public float NibbleArea = 500.0f;

        public float BiteOffset = -0.05f;

        private Status state = Status.Wait;
        private float curTime = 0.0f;

        override public GameObject GetSpawn(int spawnIndex) { return gameObject; }

        public void AssignToPortal(PortalBlob port)
        {
            Use();
            gameObject.transform.position = port.Obj.GetSpawn(0).transform.position;
            SetWait();
        }

        override public void Use()
        {
            base.Use();
            Step.Man.Layer.AssignObjToWorld(this, LayerControl.World.Ground);
        }

        public void SetWait()
        {
            //TheSatellite.color = new Color(TheSatellite.color.r, TheSatellite.color.b, TheSatellite.color.b, 1.0f);
            TheSatellite.transform.localPosition = Vector3.zero;
            TheSatellite.gameObject.SetActive(true);
            BiteRippleObj.gameObject.SetActive(false);
            NibRippleObj.gameObject.SetActive(false);
        }

        public void SetNibble( float pow )
        {
            TheSatellite.gameObject.SetActive(true);
            //TheSatellite.color = new Color(TheSatellite.color.r, TheSatellite.color.b, TheSatellite.color.b, NibbleMaxAlpha);
            TheSatellite.transform.localPosition = new Vector3(0.0f, BiteOffset * pow, 0.0f);
            BiteRippleObj.gameObject.SetActive(false);
            NibRippleObj.gameObject.SetActive(true);
        }

        public void SetBite()
        {
            TheSatellite.gameObject.SetActive(true);
            //TheSatellite.color = new Color(TheSatellite.color.r, TheSatellite.color.b, TheSatellite.color.b, BiteMinAlpha);
            TheSatellite.transform.localPosition = new Vector3(0.0f, BiteOffset, 0.0f);
            BiteRippleObj.gameObject.SetActive(true);
            NibRippleObj.gameObject.SetActive(false);
        }

        public void SetHit()
        {
            TheSatellite.gameObject.SetActive(false);
            BiteRippleObj.gameObject.SetActive(false);
            NibRippleObj.gameObject.SetActive(true);
        }
    }
}
