namespace Binaree.Game.Core
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Controls access to game time by wrapping unity time systems
    /// This allows more game control over time allowing various game systems to be paused independently
    /// </summary>
    public class TimeLord : MonoBehaviour
    {
        public float UnityDelta { get; private set; }
        public float GameDelta { get; private set; }
        public float ArenaDelta { get; private set; }

        public bool UnityPause { get; private set; }
        public bool GamePause { get; private set; }
        public bool ArenaPause { get; private set; }

        public float UnityScale { get; private set; }
        public float GameScale { get; private set; }
        public float ArenaScale { get; private set; }

        private static TimeLord _instance = null;

        public static TimeLord Inst { get { return _instance; } }

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            _instance = this;
            UnityPause = GamePause = ArenaPause = false;
            UnityScale = GameScale = ArenaScale = 1.0f;
        }

        private void Update()
        {
            UnityDelta = (UnityPause) ? 0.0f : (UnityEngine.Time.deltaTime * UnityScale);
            GameDelta = (GamePause) ? 0.0f : (UnityDelta * GameScale);
            ArenaDelta = (ArenaPause) ? 0.0f : (GameDelta * ArenaScale);
        }

        public void PauseUnity(bool pauseStatus) { UnityPause = pauseStatus; }
        public void PauseGame(bool pauseStatus) { GamePause = pauseStatus; }
        public void PauseArena(bool pauseStatus) { ArenaPause = pauseStatus; }

        public void ScaleUnity(float scaleAmt) { UnityScale = scaleAmt; }
        public void ScaleGame(float scaleAmt) { GameScale = scaleAmt; }
        public void ScaleArena(float scaleAmt) { ArenaScale = scaleAmt; }
    }
}
