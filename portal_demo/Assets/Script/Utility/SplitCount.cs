﻿namespace Binaree.Game.Utility
{
    using System;
    
    public class SplitCount
    {
        public string Label { get; private set; }
        public float Count { get; private set; }

        public SplitCount(string val)
        {
            string[] split = val.Split(':');
            if (split.Length == 1)
                Label = split[0];
            else
            {
                Label = split[0];
                Count = Convert.ToInt32(split[1]);
            }
        }
    }
}
