namespace Binaree.Game.Utility
{
    using Component;
    using Core;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Collection of helper functions to help with common stage calculations
    /// </summary>
    public class StageHelper
    {
        private const float BEHIND_ANGLE = 90.0f;
        private const float BIG_MAX = 9999999.0f;

        // Find the arena object that is closest to the target position
        /*public static T ClosestTo<T>(List<T> objects, Vector3 targetPosition) where T : PropBlob
        {
            T close = objects[0];
            float closeDist = BIG_MAX;
            for(int i = 0; i < objects.Count; ++i)
            {
                float objDist = DistanceBetween(targetPosition, objects[i].GetPropObject().transform.position);
                if(objDist < closeDist)
                {
                    closeDist = objDist;
                    close = objects[i];
                }
            }
            return close;
        }*/

        // Find the distance between two arena objects
        public static float DistanceBetween(Vector3 position, Vector3 target)
        {
            // first, we need to remove any y component from calculations as the game is "2D"
            position.y = target.y = 0.0f;

            return Vector3.Distance(position, target);
        }

        // Find the angle between two positions relative to a root object
        public static float AngleBetween(Vector3 rootPos, Vector3 obj1, Vector3 obj2)
        {
            // first, let's remove an y component to ensure a "2D" calculation
            rootPos.y = obj1.y = obj2.y = 0.0f;

            return Vector3.Angle( (obj1 - rootPos), (obj2 - rootPos) );
        }

        // Find the distance from the node to the given line
        public static float DistanceToLine(Vector3 rootPos, Vector3 linePos, Vector3 objPos, bool ignoreBehind)
        {
            // first, let's remove an y component to ensure a "2D" calculation
            rootPos.y = linePos.y = objPos.y = 0.0f;

            Vector3 lineDir = (linePos - rootPos).normalized;
            Vector3 linePt = ProjectPointOnLine(rootPos, lineDir, objPos);
            if (ignoreBehind && Vector3.Angle(lineDir, (objPos - rootPos).normalized) > BEHIND_ANGLE) return BIG_MAX;
            return Vector3.Distance(linePt, objPos);
        }

        // Rotates the object to face the current input position
        public static void FaceScreenPosition(Vector3 inPos, Vector3 worldPos, GameObject rotObj)
        {
            Vector3 dir = inPos - Camera.main.WorldToScreenPoint(worldPos);
            dir = new Vector3(dir.x, dir.y, 0.0f);
            float rot = Vector3.Angle(dir.normalized, Vector3.up);
            //Debug.Log("rot = " + rot + "; dirX = " + dir.x + "; dirY = " + dir.y + "; dirZ = " + dir.z);
            if (dir.x < 0.0f)
                rot *= -1.0f;
            Quaternion quat = Quaternion.AngleAxis(rot, Vector3.up);
            rotObj.transform.rotation = quat;
        }

        public static void SnapToObj(GameObject moveObj, GameObject posObj)
        {
            moveObj.transform.position = new Vector3(posObj.transform.position.x, 0.0f, posObj.transform.position.z);
        }

        // ****** BORROWED FROM A Math3d Open Source Library ********** //
        //This function returns a point which is a projection from a point to a line.
        //The line is regarded infinite. If the line is finite, use ProjectPointOnLineSegment() instead.
        public static Vector3 ProjectPointOnLine(Vector3 linePoint, Vector3 lineVec, Vector3 point)
        {
            //get vector from point on line to point in space
            Vector3 linePointToPoint = point - linePoint;

            float t = Vector3.Dot(linePointToPoint, lineVec);

            return linePoint + lineVec * t;
        }
        // ****** BORROWED FROM A Math3d Open Source Library ********** //
    }
}