﻿namespace Binaree.Game.Utility
{
    using System;

    public class SplitInt
    {
        private int[] iSplit = new int[2];

        /// <summary>
        /// Takes a string in the format of "min:max" and splits it into two integers
        /// Useful for random values in a range or values that progress such as a level
        /// </summary>
        public SplitInt(string val)
        {
            string[] split = val.Split(':');
            if (split.Length == 0)
                iSplit[0] = iSplit[1] = 0;
            else if (split.Length == 1)
                iSplit[0] = iSplit[1] = Convert.ToInt32(split[0]);
            else
            {
                iSplit[0] = Convert.ToInt32(split[0]);
                iSplit[1] = Convert.ToInt32(split[1]);
            }
        }

        public int Value(float percent)
        {
            return (int)(iSplit[0] + (percent * (iSplit[1] - iSplit[0])));
        }

        public int Value(int ind, int low, int high)
        {
            float fInd = (float)(ind - low) / (float)(high - low);
            if (fInd < 0.0f) fInd = 0.0f;
            if (fInd > 1.0f) fInd = 1.0f;
            return (int)(iSplit[0] + (fInd * (float)(iSplit[1] - iSplit[0])));
        }

        public int Rand()
        {
            return (int)(iSplit[0] + (Randomize.Value() * (float)(iSplit[1] - iSplit[0])));
        }

        public int Min() { return iSplit[0]; }
        public int Max() { return iSplit[1]; }
    }
}
