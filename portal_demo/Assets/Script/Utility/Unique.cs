namespace Binaree.Game.Core
{
    using System.Collections.Generic;

    /// <summary>
    /// Handles the giving and recording of unique reference numbers to ensure that components, such as blobs
    ///   are guaranteed to have a unique value.
    /// NOTE: Be sure to load all blobs (along with calling Purge) BEFORE generating new blobs
    /// </summary>
    public class Unique
    {
        private const int HOLD_VAL = 5000;
        private const int USED_CAP = 1000;

        private int[] open = new int[HOLD_VAL];
        private List<int> used = new List<int>(USED_CAP);
        private int lockValue = -1;
        private int openIterator = HOLD_VAL;
        private int numberIterator = 0;

        private static Unique _instance = new Unique();

        public static Unique Ref { get { return _instance; } }

        public int Value()
        {
            if (lockValue >= 0) return lockValue;
            if (openIterator == HOLD_VAL)
                PopulateOpen();
            return open[openIterator++];
        }

        public void Purge(int val)
        {
            if (used.Contains(val))
            {
                Say.Warn("Unique Ref Value of " + val + " has already been purged.");
                return;
            }
            used.Add(val);
        }

        public void Lock(int val) { lockValue = val; }
        public void EndLock() { lockValue = -1; }

        private void PopulateOpen()
        {
            Say.Log("Unique is now populating open Ref numbers");
            int added = 0;
            while (added < HOLD_VAL)
            {
                if (!used.Contains(numberIterator))
                    open[added++] = numberIterator;
                numberIterator++;
            }
            openIterator = 0;
        }
    }
}
