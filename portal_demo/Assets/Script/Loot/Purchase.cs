namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;
    using Binaree.Game.Utility;

    public class Purchase : DataManager
    {
        public const string DATA_HEADER = "pur_";

        private List<PurchaseData> purchase = new List<PurchaseData>();
        
        private static Purchase _instance = new Purchase();

        public static Purchase Man { get { return _instance; } }

        public PurchaseData Data(string id)
        {
            return GetData(id) as PurchaseData;
        }

        public PurchaseData DataP(string id)
        {
            return GetData(DATA_HEADER + id) as PurchaseData;
        }

        public List<PurchaseData> AllPurchases()
        {
            return purchase;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "purchase/p");
        }

        protected override GameData NewData(XmlNode node)
        {
            PurchaseData d = new PurchaseData(node);
            purchase.Add(d);
            return d;
        }
    }
}
