namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Loot : DataManager
    {
        public const string DATA_HEADER = "loot_";
        public const string TABLE_HEADER = DATA_HEADER + "tab_";

        private List<LootBlob> premium = new List<LootBlob>();

        private static Loot _instance = new Loot();

        public static Loot Man { get { return _instance; } }

        public LootData Data(string id)
        {
            return GetData(id) as LootData;
        }

        public LootData DataL(string id)
        {
            return GetData(DATA_HEADER + id) as LootData;
        }

        public LootTableData DataTab(string tabRefId)
        {
            return GetData(TABLE_HEADER + tabRefId) as LootTableData;
        }

        public List<LootBlob> PremiumLoot()
        {
            if (premium.Count == 0)
            {
                List<GameData> all = AllGameData();
                for (int i = 0; i < all.Count; ++i)
                    if ((all[i] as LootData).Type == LootType.Premium)
                        premium.Add(new LootBlob(all[i] as LootBoxData));
            }
            return premium;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "loot/l");
        }

        protected override GameData NewData(XmlNode node)
        {
            LootType type = LootData.StrToType(node.Attributes.GetNamedItem("type").Value);
            if (type == LootType.Table) return new LootTableData(node);
            return new LootBoxData(node);
        }
    }
}
