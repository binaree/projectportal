namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiPurchaseTile : UiTileObj
    {
        public const string PREFAB = "tile_purchase";

        public UiCurrencyInfo Obtain;
        public Text PurchaseInfo;
        public Text PurchaseAmount;

        private PurchaseData purchase = null;

        public void AssignPurchase(PurchaseData pur)
        {
            purchase = pur;

            Obtain.AssignCurrency(purchase.Give.Type, (int)purchase.Give.Amount.Max());
            PurchaseInfo.text = Script.Man.Text(purchase.Id);
            PurchaseAmount.text = Stringy.FormatCost(Convert.ToSingle(purchase.Cost));
        }

        public override void SelectTile()
        {
            UiConfirmOverlay over = UiMain.Man.SelectOverlay(UiConfirmOverlay.PAGE_ID) as UiConfirmOverlay;
            over.SetData("confirm_purchase", "confirm_purchase", "cancel", "buy", CompletePurchase);
        }

        private void CompletePurchase()
        {
            Currency.Man.Add(purchase.Give.Type, (int)purchase.Give.Amount.Max());
            UiMain.Man.Header.RefreshCurrency();
        }
    }
}
