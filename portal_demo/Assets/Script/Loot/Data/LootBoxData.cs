namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class LootBoxData : LootData
    {
        public List<LootBoxComp> Draw { get; private set; }
        public List<CurrencyComp> Cost { get; private set; }
        public string Prefab { get; private set; }

        public LootBoxData(XmlNode xData) : base(xData)
        {
            Prefab = xData.Attributes.GetNamedItem("pre").Value;

            Draw = new List<LootBoxComp>();
            string[] split = xData.Attributes.GetNamedItem("box").Value.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < split.Length; ++i)
                Draw.Add(new LootBoxComp(split[i]));

            if (Type == LootType.Premium)
            {
                Cost = new List<CurrencyComp>();
                split = xData.Attributes.GetNamedItem("cost").Value.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < split.Length; ++i)
                    Cost.Add(new CurrencyComp(split[i]));
            }
            else
                Cost = null;
        }       
        
        public bool HasCost() { return Cost != null && Cost.Count > 0; }
        public bool IsPremium() { return Type == LootType.Premium; }
        
        public int CostAmount(Currency.Type curType)
        {
            if (!HasCost()) return 0;
            for (int i = 0; i < Cost.Count; ++i)
                if (Cost[i].Type == curType)
                    return (int)Cost[i].Amount.Rand();
            return 0;
        }
    }
}