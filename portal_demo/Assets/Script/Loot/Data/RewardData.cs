namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public enum RewardType
    {
        None = 0,
        Currency = 1,
        Table = 2,
        Supply = 3,
        Crystal = 4,
        LootBox = 5,
        Card = 6,
    }

    public class RewardData : GameData
    {
        public static readonly string[] TYPE = { "", "cur", "tab", "sup", "cry", "box", "card" };

        public RewardType Type { get; private set; }
        public string Value { get; private set; }

        public RewardData(XmlNode xData) : base(xData)
        {
            Type = StrToType(xData.Attributes.GetNamedItem("type").Value);
            Value = xData.Attributes.GetNamedItem("val").Value;
        }

        public static RewardType StrToType(string val)
        {
            for (int i = 0; i < TYPE.Length; ++i)
                if (val == TYPE[i]) return (RewardType)i;
            return RewardType.None;
        }

        public static string TypeToStr(RewardType val)
        {
            return TYPE[(int)val];
        }
    }
}