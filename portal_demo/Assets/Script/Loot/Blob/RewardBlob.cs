namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.Ui;
    using Binaree.Game.User;
    using Core;
    using Factory;
    using System;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;

    public enum FinalReward
    {
        None = 0,
        Currency = 1,
        Attractant = 2,
        Line = 3,
        Rocket = 4,
        Crystal = 5,
        LootBox = 6,
        Card = 7,
    }

    public class RewardBlob : GameBlob
    {
        public RewardData Data { get; private set; }
        public FinalReward RewardWon { get; private set; }
        public GameData RewardData { get; private set; }
        public int RewardSubNum { get; private set; }
        public int Amount { get; private set; }
        public bool Bonus { get; private set; }
        public float BonusAmount { get; private set; }
        public float RewardLevel { get; private set; }

        public RewardBlob(RewardData d, bool isBonus, float bonusAmount, float level = -1.0f)
        {
            Data = d;
            Bonus = isBonus;
            BonusAmount = bonusAmount;
            RewardLevel = level;
            RewardWon = FinalReward.None;
            DetermineReward();
        }

        public void Give()
        {
            if (RewardWon == FinalReward.Currency) GiveCurrency();
            else if (RewardWon == FinalReward.Crystal) GiveCrystal();
            else if (RewardWon == FinalReward.Card) GiveCard();
            else GiveSupply();
        }

        public string IconDir()
        {
            if (RewardWon == FinalReward.Currency) return Currency.DIR;
            else if (RewardWon == FinalReward.Crystal) return UiCrystalTile.CRYSTAL_DIR;
            else if (RewardWon == FinalReward.Card) return "NOT SUPPORTED";
            else if (RewardWon == FinalReward.Line || RewardWon == FinalReward.Rocket || RewardWon == FinalReward.Attractant) return UiSupplyTile.SUPPLY_DIR;
            return "NONE";
        }

        public string IconPrefab()
        {
            if (RewardWon == FinalReward.Currency) return Currency.PREFAB[RewardSubNum];
            else if (RewardWon == FinalReward.Crystal && RewardData != null) return (RewardData as CrystalData).CreatureId;
            if (RewardData != null)
                return RewardData.Id;
            return "NONE";
        }

        public string RewardName()
        {
            string use = string.Empty;
            if (RewardWon == FinalReward.Currency) use = Currency.NAME[RewardSubNum];
            else if (RewardWon == FinalReward.Crystal && RewardData != null) use = (RewardData as CrystalData).CreatureId;
            else if (RewardData != null)
                use = RewardData.Id;

            if (use == string.Empty) return "";
            return Script.Man.Name(use);
        }

        private void DetermineReward()
        {
            if (Data.Type == RewardType.Currency) DetermineCurrency();
            else if (Data.Type == RewardType.Supply) DetermineSupply();
            else if (Data.Type == RewardType.Crystal) DetermineCrystal();
            else if (Data.Type == RewardType.Table) DetermineLootTable();
            else if (Data.Type == RewardType.LootBox) DetermineLootBox();
            else if (Data.Type == RewardType.Card) DetermineCard();
        }

        private void DetermineCurrency()
        {
            CurrencyComp comp = new CurrencyComp(Data.Value);
            RewardWon = FinalReward.Currency;
            RewardData = null;
            RewardSubNum = (int)comp.Type;
            Amount = (int)((RewardLevel >= 0 ? comp.Amount.Value(RewardLevel) : comp.Amount.Rand()) * BonusAmount);
        }

        private void DetermineSupply()
        {
            SupplyComp comp = new SupplyComp(Data.Value);
            if(comp.Style == LootTableStyle.Attractant)
            {
                RewardWon = FinalReward.Attractant;
                RewardData = Attractant.Man.DataA(comp.RefId);
            }
            else if(comp.Style == LootTableStyle.Line)
            {
                RewardWon = FinalReward.Line;
                RewardData = Line.Man.DataL(comp.RefId);
            }
            else if(comp.Style == LootTableStyle.Rocket)
            {
                RewardWon = FinalReward.Rocket;
                RewardData = Rocket.Man.DataR(comp.RefId);
            }
            RewardSubNum = 0;
            Amount = (int)((RewardLevel >= 0 ? comp.Amount.Value(RewardLevel) : comp.Amount.Rand()) * BonusAmount);
        }

        private void DetermineCrystal()
        {
            CrystalComp comp = new CrystalComp(Data.Value);
            RewardWon = FinalReward.Crystal;
            RewardData = Crystal.Man.DataRef(comp.RefId);
            RewardSubNum = comp.Rank;
            Amount = (int)((RewardLevel >= 0 ? comp.Amount.Value(RewardLevel) : comp.Amount.Rand()) * BonusAmount);
        }

        private void DetermineLootTable()
        {
            LootTableData rootTable = FindLootTable(Data.Value);
            if(rootTable == null) { Say.Log("Did not find loot table for " + Data.Value); return; }
            RewardData rew = rootTable.FindReward();
            if(rew == null) { Say.Log("Did not find loot reward for " + Data.Value); return; }
            RewardBlob b = new RewardBlob(rew, Bonus, BonusAmount);
            RewardWon = b.RewardWon;
            RewardData = b.RewardData;
            RewardSubNum = b.RewardSubNum;
            Amount = b.Amount;
        }

        private void DetermineLootBox()
        {
            // TODO: Figure out how this will work, maybe some kind of key system?
        }

        private void DetermineCard()
        {
            CardComp comp = new CardComp(Data.Value);
            RewardWon = FinalReward.Card;
            RewardData = Satellite.Man.DataS(comp.RefId);
            RewardSubNum = 0;
            Amount = (int)((RewardLevel >= 0 ? comp.Amount.Value(RewardLevel) : comp.Amount.Rand()) * BonusAmount);
        }

        private void GiveCurrency()
        {
            Currency.Man.Add((Currency.Type)RewardSubNum, Amount);
        }

        private void GiveSupply()
        {
            SupplyBlob b = null;
            if (RewardWon == FinalReward.Line)
                b = Line.Man.Blob(RewardData.Id);
            else if (RewardWon == FinalReward.Rocket)
                b = Rocket.Man.Blob(RewardData.Id);
            else if (RewardWon == FinalReward.Attractant)
                b = Attractant.Man.Blob(RewardData.Id);

            if (b != null)
                b.AdjustQuantity(Amount);
        }

        private void GiveCrystal()
        {
            CrystalBlob b = Crystal.Man.Blob(RewardData.Id);
            b.AdjustQty(Amount, RewardSubNum);
        }

        private void GiveCard()
        {
            // TODO: Figure this out
        }

        private LootTableData FindLootTable(string val)
        {
            string[] split = val.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            for(int i = split.Length -1; i >= 0; --i)
            {
                SplitTrig loot = new SplitTrig(split[i]);
                if(loot.CheckLow())
                {
                    LootTableData tab = Loot.Man.DataTab(loot.Value);
                    return tab;
                }
            }
            return null;
        }
    }
}