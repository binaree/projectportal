namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.Ui;
    using Binaree.Game.User;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class LootBlob : GameBlob
    {
        public LootBoxData Data { get; private set; }

        public List<RewardBlob> Rewards { get; private set; }

        public LootBlob(LootBoxData d)
        {
            Data = d;

            Rewards = Reward.Man.FormFromBox(Data.Draw);
        }

        public bool CanAcquire()
        {
            int hcCost = HardCost();
            if (hcCost == 0) return true;
            if (hcCost < 0) return false;
            return hcCost < Currency.Man.Value(Currency.Type.Hard);
        }

        public int HardCost()
        {
            return Data.CostAmount(Currency.Type.Hard);
        }

        public bool Acquire()
        {
            if (!CanAcquire()) return false;
            Currency.Man.Take(Currency.Type.Hard, HardCost());
            UiMain.Man.Header.RefreshCurrency();

            for (int i = 0; i < Rewards.Count; ++i)
                Rewards[i].Give();

            UiRewardOverlay over = UiMain.Man.SelectOverlay(UiRewardOverlay.PAGE_ID) as UiRewardOverlay;
            over.AssignLoot(this, AcquireComplete);
            return true;
        }

        private void AcquireComplete()
        {
            UiMain.Man.Header.RefreshCurrency();
            Rewards = Reward.Man.FormFromBox(Data.Draw);
        }
    }
}