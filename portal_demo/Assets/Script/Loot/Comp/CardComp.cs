namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class CardComp
    {
        public string RefId;
        public SplitF Amount;

        public CardComp(string val)
        {
            string[] split = val.Split('|');
            if (split.Length > 0) RefId = split[0];
            if (split.Length > 1) Amount = new SplitF(split[2]);
        }
    }
}