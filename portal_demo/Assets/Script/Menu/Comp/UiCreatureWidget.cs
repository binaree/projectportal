namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiCreatureWidget : UiObj
    {
        private const string CREATURE_DIR = "creatureui/";

        public Image CreatureImage;
        public Image GlowFx;
        public GameObject ReadyPanel;
        public Text ReadyLabel;
        public GameObject RestPanel;
        public Text RestLabel;
        public GameObject SelectPanel;
        public Text SelectLabel;

        private PetBlob pet = null;

        private void Awake()
        {
            CloseAll();
        }

        public void SelectCreature()
        {
            Step.Man.Current.HandleRemoteSelect();
        }

        public void Activate()
        {
            gameObject.SetActive(true);
            AssignCreature(Rig.Man.TheRemote.Pet);
        }

        public void AssignCreature(PetBlob p)
        {
            if (!gameObject.activeSelf) return;
            pet = p;
            Refresh();
        }

        public void Refresh()
        {
            CloseAll();
            if (pet == null)
            {
                CreatureImage.gameObject.SetActive(false);
                SelectPanel.SetActive(true);
                return;
            }

            Spritey.ReplaceIcon(CreatureImage, CREATURE_DIR, pet.DataC.Id);
            if (pet.CanUse())
                ReadyPanel.SetActive(true);
            else
            {
                RestPanel.SetActive(true);
                UpdateRest();
            }
        }

        private void CloseAll()
        {
            ReadyPanel.SetActive(false);
            RestPanel.SetActive(false);
            SelectPanel.SetActive(false);
        }

        private void UpdateRest()
        {
            TimeSpan ts = TimeSpan.FromSeconds(pet.TimeToUse());
            ReadyLabel.text = string.Format("{0}:{1}", ts.Minutes, ts.Seconds);
        }
    }
}
