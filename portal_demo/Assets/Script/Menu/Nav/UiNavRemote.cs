namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using System;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiNavRemote : UiNavPanel
    {
        public const string NAV_ID = "remote";

        public Text CancelLabel;

        private UiRemoteOverlay overlay = null;

        override public string RefId() { return NAV_ID; }

        override public void Activate()
        {
            UiMain.Man.Footer.SelectWidget(UiFooter.Widget.None);
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public void SelectCancel()
        {
            if (overlay != null)
                overlay.CloseOverlay();
        }

        public void AssignOverlay(UiRemoteOverlay over)
        {
            overlay = over;
        }
    }
}
