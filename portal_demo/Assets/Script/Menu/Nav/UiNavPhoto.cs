namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using System;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiNavPhoto : UiNavPanel
    {
        public const string NAV_ID = "photo";

        public Text CancelLabel;
        public Text SnapLabel;
        public Text ShareLabel;

        override public string RefId() { return NAV_ID; }

        override public void Activate()
        {
            UiMain.Man.Footer.SelectWidget(UiFooter.Widget.None);
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public void SelectCancel()
        {
            if (!(UiMain.Man.SelectedPage is UiCatchPage)) return;
            (UiMain.Man.SelectedPage as UiCatchPage).CancelPhoto();
        }

        public void SelectSnap()
        {
            UiOkOverlay ok = UiMain.Man.SelectOverlay(UiOkOverlay.PAGE_ID) as UiOkOverlay;
            ok.SetData("na", "na_snap", "ok");
        }

        public void SelectShare()
        {
            UiMain.Man.SelectOverlay(UiShareOverlay.PAGE_ID);
        }
    }
}
