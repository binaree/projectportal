namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using System;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiNavMain : UiNavPanel
    {
        public const string NAV_ID = "main";

        public Text OpenLabel;
        public Text TankLabel;
        public Text RigLabel;
        public Text BoxLabel;
        public Text GuideLabel;

        override public string RefId() { return NAV_ID; }

        override public void Activate()
        {
            UiMain.Man.Footer.SelectWidget(UiFooter.Widget.Pet);
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public void SelectOpen()
        {
            Step.Man.Init.StartGenerate();
        }

        public void SelectTank()
        {
            UiMain.Man.SelectOverlay(UiTankOverlay.PAGE_ID);
        }

        public void SelectRig()
        {
            UiMain.Man.SelectOverlay(UiRigOverlay.PAGE_ID);
        }

        public void SelectBox()
        {
            UiMain.Man.SelectOverlay(UiBoxOverlay.PAGE_ID);
        }

        public void SelectGuide()
        {
            UiMain.Man.SelectOverlay(UiGuideOverlay.PAGE_ID);
        }
    }
}
