namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Core;
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class UiHomePage : UiPage
    {
        public const string PAGE_ID = "home";
        
        public GameObject HomePanel;        

        override public string RefId() { return PAGE_ID; }

        override public void Activate()
        {
            UiMain.Man.Footer.SelectNav(UiNavMain.NAV_ID);
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }
    }
}
