namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Core;
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class UiDeployPage : UiPage
    {
        public const string PAGE_ID = "deploy";

        public UiDeployWidget DeployWidget;

        override public string RefId() { return PAGE_ID; }

        override public void Activate()
        {
            UiMain.Man.Footer.SelectNav(UiNavDeploy.NAV_ID);
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public float DeployPosition()
        {
            return DeployWidget.CurrentPosition();
        }
    }
}
