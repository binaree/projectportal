namespace Binaree.Game.Ui
{
    using System;
    using System.Collections.Generic;
    using Core;
    using UnityEngine;

    /// <summary>
    /// The root for all ui which acts as an access point for communication with ui systems
    /// </summary>
    public class UiMain : MonoBehaviour
    {
        public float ProjectWidth = 640;
        public float ProjectHeight = 960;

        public UiHeader Header;
        public UiFooter Footer;
        public List<UiPage> GamePage;
        public List<UiOverlay> GameOverlay;

        public UiPage SelectedPage { get; private set; }

        private Dictionary<string, UiPage> page = new Dictionary<string, UiPage>();
        private Dictionary<string, UiOverlay> overlay = new Dictionary<string, UiOverlay>();

        private static UiMain _instance = null;

        public static UiMain Man { get { return _instance; } }

        private void Awake()
        {
            _instance = this;
            foreach (UiPage pg in GamePage)
                page.Add(pg.RefId(), pg);
            foreach (UiOverlay ov in GameOverlay)
                overlay.Add(ov.RefId(), ov);
            CloseAllOverlays();
        }

        public UiPage SelectPage(string pg)
        {
            CloseAllPages();
            SelectedPage = page[pg];
            if( SelectedPage == null)
            {
                Say.Warn("Ui could not find a page with a reference name of " + pg);
                return null;
            }
            SelectedPage.gameObject.SetActive(true);
            SelectedPage.Activate();
            return SelectedPage;
        }

        public UiOverlay SelectOverlay(string over)
        {
            //Say.Log("Selecting the overlay " + overlayRefNum);
            //CloseAllOverlays();
            if (over == string.Empty) return null;

            UiOverlay select = overlay[over];
            if (select == null)
            {
                Say.Warn("Ui could not find an overlay with a reference name of " + over);
                return null;
            }
            select.ActivateOverlay();
            return select;
        }

        public void RefreshAll()
        {
            if (Header != null)
                Header.RefreshAll();
            if (SelectedPage != null)
                SelectedPage.Refresh();
        }

        public float XDistScale()
        {
            return UnityEngine.Screen.width / ProjectWidth;
        }

        public float YDistScale()
        {
            return UnityEngine.Screen.height / ProjectHeight;
        }

        /*public void GoBackPage()
        {
            if (SelectedPage == null || SelectedPage.RefId() == StartPage)
            {
                UiConfirmOverlay confirm = SelectOverlay(UiConfirmOverlay.PAGE_ID) as UiConfirmOverlay;
                confirm.SetData("confirm_exit", "confirm_exit", "confirm_cancel", "confirm_exit_ok", ExitGame);
            }
            else
            {
                SelectPage(StartPage);
            }
        }*/

        private void CloseAllOverlays()
        {
            for (int i = 0; i < GameOverlay.Count; ++i)
                GameOverlay[i].gameObject.SetActive(false);
        }

        private void CloseAllPages()
        {
            for (int i = 0; i < GamePage.Count; ++i)
                GamePage[i].gameObject.SetActive(false);
            if (SelectedPage != null)
                SelectedPage.Close();
            SelectedPage = null;
        }

        private void ExitGame()
        {
            Say.Log("This would be where you exit the game... Coming soon!");
        }

        public static GameObject GeneratePrefab(string prefabDir, string prefabName)
        {
            GameObject resObj = Resources.Load(prefabDir + prefabName) as GameObject;
            if (resObj == null) { Say.Warn("Unable to find resource " + prefabDir + prefabName); return null; }
            GameObject bannerObj = UnityEngine.Object.Instantiate(resObj) as GameObject;
            if (bannerObj == null) { Say.Warn("Unable to instantiate loaded resource " + resObj.name); return null; }
            return bannerObj;
        }
    }
}
