namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public class UiLeagueOverlay : UiOverlay
    {

        public const string PAGE_ID = "league";

        public UiLeaguePanel LeaguePanel;

        override public string RefId() { return PAGE_ID; }

        override public void ActivateOverlay()
        {
            base.ActivateOverlay();
            LeaguePanel.AssignLeague(League.Man.Standard);
        }

        override public void CloseOverlay()
        {
            base.CloseOverlay();
        }

        public void SelectClose()
        {
            CloseOverlay();
        }
    }
}
