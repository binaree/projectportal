namespace Binaree.Game.Ui
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public class UiPortalOverlay : UiOverlay
    {

        public const string PAGE_ID = "portal";

        public Text CloseText;

        override public string RefId() { return PAGE_ID; }

        override public void ActivateOverlay()
        {
            base.ActivateOverlay();
        }

        override public void CloseOverlay()
        {
            base.CloseOverlay();
        }

        public void SelectClose()
        {
            CloseOverlay();
        }
    }
}
