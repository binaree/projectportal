namespace Binaree.Game.Ui
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public class UiOkOverlay : UiOverlay
    {

        public const string PAGE_ID = "ok";

        public Text Header;
        public Text Body;

        public Button CloseButton;
        public Text CloseText;

        override public string RefId() { return PAGE_ID; }

        override public void ActivateOverlay()
        {
            base.ActivateOverlay();
        }

        override public void CloseOverlay()
        {
            base.CloseOverlay();
        }

        public void SelectClose()
        {
            CloseOverlay();
        }

        public void SetData(string headerTxtId, string bodyTxtId, string closeTxtId)
        {
            Header.text = Script.Man.Name(headerTxtId);
            Body.text = Script.Man.Text(bodyTxtId);
            CloseText.text = Script.Man.Text(closeTxtId);
        }
    }
}
