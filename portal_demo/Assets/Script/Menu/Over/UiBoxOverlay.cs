namespace Binaree.Game.Ui
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public class UiBoxOverlay : UiOverlay
    {
        public const string PAGE_ID = "box";

        public UiLootPanel LootPanel;

        private UiNavPanel lastNav = null;

        override public string RefId() { return PAGE_ID; }

        override public void ActivateOverlay()
        {
            base.ActivateOverlay();
            lastNav = UiMain.Man.Footer.SelectedNav;
            UiNavBox nav = UiMain.Man.Footer.SelectNav(UiNavBox.NAV_ID) as UiNavBox;
            nav.AssignOverlay(this);
            LootPanel.Activate();
        }

        override public void CloseOverlay()
        {
            base.CloseOverlay();
            UiMain.Man.Footer.SelectNav(lastNav.RefId());
        }
    }
}
