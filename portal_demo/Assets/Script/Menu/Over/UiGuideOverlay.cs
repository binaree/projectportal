namespace Binaree.Game.Ui
{
    using Binaree.Game.Core;
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public class UiGuideOverlay : UiOverlay
    {
        public const string PAGE_ID = "guide";

        private UiNavPanel lastNav = null;

        override public string RefId() { return PAGE_ID; }

        override public void ActivateOverlay()
        {
            base.ActivateOverlay();
            lastNav = UiMain.Man.Footer.SelectedNav;
            UiNavGuide nav = UiMain.Man.Footer.SelectNav(UiNavGuide.NAV_ID) as UiNavGuide;
            nav.AssignOverlay(this);
        }

        override public void CloseOverlay()
        {
            base.CloseOverlay();
            UiMain.Man.Footer.SelectNav(lastNav.RefId());
            lastNav = null;
        }

        public void ShowNavOption(UiNavGuide.Option option)
        {
            Say.Log("Show nav option = " + option.ToString());
        }
    }
}
