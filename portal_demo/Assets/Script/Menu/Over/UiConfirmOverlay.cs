namespace Binaree.Game.Ui
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public class UiConfirmOverlay : UiOverlay
    {

        public const string PAGE_ID = "confirm";

        public Text Header;
        public Text Body;

        public Button CloseButton;
        public Text CloseText;

        public Button ConfirmButton;
        public Text ConfirmText;

        private Action confirmAction = null;

        override public string RefId() { return PAGE_ID; }

        override public void ActivateOverlay()
        {
            base.ActivateOverlay();
        }

        override public void CloseOverlay()
        {
            base.CloseOverlay();
        }

        public void SelectClose()
        {
            CloseOverlay();
        }

        public void SelectConfirm()
        {
            if (confirmAction != null)
                confirmAction();
            CloseOverlay();
        }

        public void SetData(string headerTxtId, string bodyTxtId, string closeTxtId, string confirmTextId, Action confirmAct)
        {
            Header.text = Script.Man.Name(headerTxtId);
            Body.text = Script.Man.Text(bodyTxtId);
            CloseText.text = Script.Man.Text(closeTxtId);
            ConfirmText.text = Script.Man.Text(confirmTextId);
            confirmAction = confirmAct;
        }
    }
}
