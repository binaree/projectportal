namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public class UiRewardOverlay : UiOverlay
    {
        public const string PAGE_ID = "reward";

        public GameObject RewardContainer;
        public UiOpenLoot OpenLoot;

        private LootBlob loot = null;
        private List<UiRewardWidget> reward = null;
        private Action completeAction = null;
        private int currentReward = 0;

        override public string RefId() { return PAGE_ID; }

        override public void ActivateOverlay()
        {
            base.ActivateOverlay();
        }

        public void AssignLoot(LootBlob theLoot, Action complete)
        {
            loot = theLoot;
            completeAction = complete;
            OpenLoot.AssignLoot(loot);
        }

        public void AssignReward(RewardBlob b, Action complete)
        {
            List<RewardBlob> list = new List<RewardBlob>();
            list.Add(b);
            AssignRewards(list, complete);
        }

        public void AssignRewards(List<RewardBlob> list, Action complete)
        {
            completeAction = complete;
            reward = new List<UiRewardWidget>();
            for (int i = 0; i < list.Count; ++i)
                CreateNewReward(list[i]);

            currentReward = -1;
            SelectConfirm();
        }

        override public void CloseOverlay()
        {
            base.CloseOverlay();
            if (completeAction != null)
                completeAction();
        }

        public void SelectConfirm()
        {
            if(loot != null)
            {
                OpenLoot.Open(LootBoxOpened);
                return;
            }

            if (!ShowCurrent())
                CloseOverlay();
        }

        private UiRewardWidget CreateNewReward(RewardBlob rew)
        {
            GameObject go = UiMain.GeneratePrefab(UiRewardWidget.DIR, UiRewardWidget.PREFAB);
            if (go == null) return null;
            UiRewardWidget wid = go.GetComponent<UiRewardWidget>();
            if (wid == null) return null;

            go.transform.SetParent(RewardContainer.transform, false);
            wid.AssignReward(rew);
            reward.Add(wid);
            return wid;
        }

        private bool ShowCurrent()
        {
            if (currentReward >= 0)
                reward[currentReward].Deactivate();
            if (currentReward >= (reward.Count - 1)) return false;
            reward[++currentReward].Activate();
            return true;
        }

        private void LootBoxOpened()
        {
            List<RewardBlob> rew = loot.Rewards;
            loot = null;
            AssignRewards(rew, completeAction);
        }
    }
}
