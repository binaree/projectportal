namespace Binaree.Game.Ui
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public class UiRemoteOverlay : UiOverlay
    {
        public const string PAGE_ID = "remote";

        public UiPetPanel PetPanel; 

        private UiNavPanel lastNav = null;

        override public string RefId() { return PAGE_ID; }

        override public void ActivateOverlay()
        {
            base.ActivateOverlay();
            lastNav = UiMain.Man.Footer.SelectedNav;
            UiNavRemote nav = UiMain.Man.Footer.SelectNav(UiNavRemote.NAV_ID) as UiNavRemote;
            nav.AssignOverlay(this);
        }

        override public void CloseOverlay()
        {
            base.CloseOverlay();
            UiMain.Man.Footer.SelectNav(lastNav.RefId());
            lastNav = null;
        }
    }
}
