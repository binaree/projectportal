namespace Binaree.Game.Ui
{
    using Binaree.Game.Core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public class UiRigOverlay : UiOverlay
    {
        public const string PAGE_ID = "rig";

        public List<UiRigPanel> RigPanel;

        private UiNavPanel lastNav = null;

        override public string RefId() { return PAGE_ID; }

        override public void ActivateOverlay()
        {
            CloseAllPanels();
            base.ActivateOverlay();
            lastNav = UiMain.Man.Footer.SelectedNav;
            UiNavRig nav = UiMain.Man.Footer.SelectNav(UiNavRig.NAV_ID) as UiNavRig;
            nav.AssignOverlay(this);
        }

        override public void CloseOverlay()
        {
            base.CloseOverlay();
            UiMain.Man.Footer.SelectNav(lastNav.RefId());
            lastNav = null;
        }

        public void ShowNavOption(UiNavRig.Option option)
        {
            CloseAllPanels();
            RigPanel[(int)option].gameObject.SetActive(true);
        }

        private void CloseAllPanels()
        {
            for (int i = 0; i < RigPanel.Count; ++i)
                RigPanel[i].gameObject.SetActive(false);
        }
    }
}
