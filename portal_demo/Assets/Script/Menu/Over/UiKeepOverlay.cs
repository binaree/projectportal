namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;

    public class UiKeepOverlay : UiOverlay
    {

        public const string PAGE_ID = "keep";

        public Text ConfirmText;
        public Text CloseText;

        override public string RefId() { return PAGE_ID; }

        override public void ActivateOverlay()
        {
            base.ActivateOverlay();
        }

        override public void CloseOverlay()
        {
            base.CloseOverlay();
        }

        public void SelectConfirm()
        {
            CloseOverlay();
            Step.Man.Catch.KeepCatch();
        }

        public void SelectClose()
        {
            CloseOverlay();
        }
    }
}
