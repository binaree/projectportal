namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using System;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    /// The header responsible for displaying all ui elements located on the top of the screen
    /// </summary>
    public class UiHeader : UiObj
    {
        private const string PORTAL_DIR = "portalui/";

        public GameObject CurrencyPanel;
        public Text HcAmount;
        public Text ScAmount;

        public GameObject LeaguePanel;
        public UiLeagueBadge LeagueBadge;
        public Text LeagueName;
        public GameObject LeagueInfoPanel;
        public Text PromoteText;

        public GameObject PortalPanel;
        public Text PortalName;
        public GameObject PortalInfoPanel;
        public Image PortalIcon;
        public GameObject PortalTimePanel;
        public Text PortalTime;
        public GameObject TimeWarning;
        public Text ClosePortalText;

        private void Start()
        {
            HideAll();
        }

        private void Update()
        {
            if (PortalPanel.activeSelf)
                SetPortalTime(Portal.Man.Active);
        }

        public void SetVisibility(bool turnOn)
        {
            gameObject.SetActive(turnOn);
            if (turnOn)
                RefreshAll();
        }

        public void SelectLeague()
        {
            UiMain.Man.SelectOverlay(UiLeagueOverlay.PAGE_ID);
        }

        public void SelectCurrency()
        {
            UiMain.Man.SelectOverlay(UiCurrencyOverlay.PAGE_ID);
        }

        public void SelectPortal()
        {
            UiMain.Man.SelectOverlay(UiPortalOverlay.PAGE_ID);
        }

        public void ClosePortal()
        {
            Portal.Man.KillPortal();
        }

        public void RefreshAll()
        {
            RefreshLeague();
            RefreshCurrency();
            RefreshPortal();
        }

        public void RefreshLeague()
        {
            if (LeaguePanel.activeSelf == false) return;
            LeagueBadge.AssignLeague(League.Man.Standard.Data, League.Man.Standard.IsMax());
            LeagueName.text = Script.Man.Name(League.DATA_HEADER + League.Man.Standard.Data.Group);
        }

        public void RefreshCurrency()
        {
            if (CurrencyPanel.activeSelf == false) return;
            ScAmount.text = Currency.Man.Value(Currency.Type.Soft).ToString();
            HcAmount.text = Currency.Man.Value(Currency.Type.Hard).ToString();
        }

        public void RefreshPortal()
        {
            if (PortalPanel.activeSelf == false) return;
            PortalBlob p = Portal.Man.Active;
            Spritey.ReplaceIcon(PortalIcon, PORTAL_DIR, p.Data.Id);
            PortalName.text = Script.Man.Name(p.Data.Id);
            SetPortalTime(p);
        }

        public void EnableCurrency(bool turnOn)
        {
            CurrencyPanel.SetActive(turnOn);
            RefreshCurrency();
        }

        public void EnableLeague(LeagueBlob league)
        {
            PortalPanel.SetActive(false);
            if(league == null)
            {
                LeaguePanel.SetActive(false);
                return;
            }

            LeaguePanel.SetActive(true);
            RefreshLeague();
        }

        public void EnablePortal(PortalBlob port, bool showInfo)
        {
            LeaguePanel.SetActive(false);
            if (port == null)
            {
                PortalPanel.SetActive(false);
                return;
            }
            
            PortalPanel.SetActive(true);
            PortalInfoPanel.SetActive(showInfo);
            RefreshPortal();
        }

        private void HideAll()
        {
            LeaguePanel.SetActive(false);
            PortalPanel.SetActive(false);
            CurrencyPanel.SetActive(false);
        }

        private void SetPortalTime(PortalBlob port)
        {
            TimeSpan ts = TimeSpan.FromSeconds(port.CurrentTime);
            PortalTime.text = string.Format("{0}:{1}", ts.Minutes, ts.Seconds);
            TimeWarning.SetActive(port.CurrentTime <= Table.Man.League.WarningTime);
        }
    }
}
