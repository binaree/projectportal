namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;

    public class LeagueTable : TableData
    {
        public float WarningTime { get; private set; }

        private Dictionary<string, int> standardMax = new Dictionary<string, int>();

        public LeagueTable(XmlNode xData) : base(xData)
        {
            Stringy.ParseDict<int>(xData.Attributes.GetNamedItem("std").Value, standardMax);
            WarningTime = Convert.ToSingle(xData.Attributes.GetNamedItem("wt").Value);
        }

        public int StandardMax(string groupId)
        {
            if(!standardMax.ContainsKey(groupId))
            {
                Say.Warn("No max exists for group: " + groupId);
                return 1;
            }
            return standardMax[groupId];
        }
    }
}