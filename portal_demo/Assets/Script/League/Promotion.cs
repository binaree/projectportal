namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Promotion : DataManager
    {
        public const string DATA_HEADER = "promo_";
        
        private static Promotion _instance = new Promotion();

        public static Promotion Man { get { return _instance; } }

        public PromotionData Data(string id)
        {
            return GetData(id) as PromotionData;
        }

        public PromotionData DataP(string id)
        {
            return GetData(DATA_HEADER + id) as PromotionData;
        }

        public PromotionBlob GenerateLeaguePromotion(LeagueBlob league)
        {
            PromotionData d = DataP(league.Data.Id);
            PromotionBlob b = new PromotionBlob(d);
            return b;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "promotion/p");
        }

        protected override GameData NewData(XmlNode node)
        {
            PromotionData d = new PromotionData(node);
            return d;
        }
    }
}
