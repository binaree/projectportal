namespace Binaree.Game.User
{
    using System.Xml;
    using System.Xml.Serialization;
    using Component;
    using System.Collections.Generic;
    
    [XmlRoot(GraphLeague.TAG)]
    public class GraphLeague : UserGraph
    {
        public const string TAG = "league";

        [XmlElement(UserLeague.TAG)]
        public List<UserLeague> LeagueType { get; private set; }

        public GraphLeague() : base()
        {
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            for (int i = 0; i < LeagueType.Count; ++i)
                LeagueType[i].Load(this);

            Dirty();
        }
    }
}
