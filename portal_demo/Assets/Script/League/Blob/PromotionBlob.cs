namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class PromotionBlob : GameBlob
    {
        public PromotionData Data { get; private set; }
        public List<CollectionBlob> Collect { get; private set; }

        public PromotionBlob(PromotionData d)
        {
            Data = d;

            Collect = new List<CollectionBlob>();
            for (int i = 0; i < Data.Collection.Count; ++i)
                Collect.Add(Collection.Man.GenerateCollection(Data.Collection[i], i));
        }

        public bool CanPromote()
        {
            if (!HasPromotion()) return false;
            for (int i = 0; i < Collect.Count; ++i)
                if (!Collect[i].Complete)
                    return false;
            return true;
        }

        public bool HasPromotion()
        {
            return !Data.IsLastPromo();
        }

        public bool HasCost()
        {
            for (int i = 0; i < Data.Cost.Count; ++i)
                if (Currency.Man.Value((Currency.Type)i) < Data.Cost[i])
                    return false;
            return true;
        }

        public int PromotionCost(Currency.Type curType)
        {
            return Data.Cost[(int)curType];
        }

        public void Execute()
        {
            for (int i = 0; i < Data.Cost.Count; ++i)
                if (Data.Cost[i] > 0)
                    Currency.Man.Take((Currency.Type)i, Data.Cost[i]);
        }
    }
}