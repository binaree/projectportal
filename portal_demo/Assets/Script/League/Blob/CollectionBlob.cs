namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;

    public class CollectionBlob : GameBlob
    {
        public CollectionData Data { get; private set; }
        public bool Complete { get; private set; }
        public int Slot { get; private set; }

        private RewardBlob reward = null;

        public CollectionBlob(CollectionData d)
        {
            Data = d;
            Complete = false;
        }

        public void SetSlot(int slotNum)
        {
            Slot = slotNum;
        }

        public bool CanComplete()
        {
            if (Complete) return false;
            for (int i = 0; i < Data.Crystal.Count; ++i)
                if (!Data.Crystal[i].CanComplete())
                    return false;
            return true;
        }

        public bool CompleteCollection()
        {
            if (!CanComplete()) return false;

            for(int i = 0; i < Data.Crystal.Count; ++i)
            {
                CrystalBlob b = Crystal.Man.BlobRef(Data.Crystal[i].CrystalRefId);
                b.AdjustQty(-Data.Crystal[i].Quantity, Data.Crystal[i].CrystalRank);
            }

            RewardBlob rew = GetReward();
            if (rew != null)
                rew.Give();

            Complete = true;
            return true;
        }

        public bool CompleteLeagueCollection(LeagueBlob league, int slot = 0, bool saveCollect = true)
        {
            if (!CompleteCollection()) return false;
            if(saveCollect && league != null)
            {
                league.User.SetComplete(slot, true);
            }
            return true;
        }

        public RewardBlob GetReward()
        {
            if (reward != null) return reward;
            if (Data.RewardRefId == string.Empty) return null;
            reward = Reward.Man.GenerateReward(Data.RewardRefId);
            return reward;
        }
    }
}