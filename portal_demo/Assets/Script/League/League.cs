namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;
    using Binaree.Game.Utility;

    public class League : DataManager
    {
        public const string DATA_HEADER = "lea_";

        public LeagueBlob Standard { get { return league[(int)LeagueType.Standard]; } }
        public LeagueBlob Event { get { return league[(int)LeagueType.Event]; } }
        public LeagueBlob Tournament { get { return league[(int)LeagueType.Tournament]; } }

        private List<LeagueBlob> league = new List<LeagueBlob>();
        private List<string> standardProgression = new List<string>();
        
        private static League _instance = new League();

        public static League Man { get { return _instance; } }

        public LeagueData Data(string id)
        {
            return GetData(id) as LeagueData;
        }

        public LeagueData DataL(string id)
        {
            return GetData(DATA_HEADER + id) as LeagueData;
        }

        public LeagueBlob GenerateFromUser(UserLeague user)
        {
            if(league[user.Type] != null)
            {
                Say.Warn("You can only have 1 league of the same type! Current duplicates of league type #" + user.Type);
            }

            LeagueData d = DataL(user.RefId);
            LeagueBlob b = new LeagueBlob(d);
            AssignLeague(b, user, false);
            return b;
        }

        public LeagueBlob Promote(LeagueBlob current)
        {
            if (!current.Promote.HasPromotion()) return current;
            LeagueData d = current.NextLeague();
            if (d == null) return current;
            LeagueBlob next = new LeagueBlob(d);
            AssignLeague(next, current.User, true);
            return league[(int)next.Data.Type];
        }

        private void AssignLeague(LeagueBlob lea, UserLeague user, bool resetUser = true)
        {
            int slot = (int)lea.Data.Type;
            if (league[slot] != null)
                league[slot].Deactivate();
            lea.AssignUser(user, resetUser);
            league[(int)lea.Data.Type] = lea;
        }

        public void Load(XmlDocument xml)
        {
            XmlNode node = xml.SelectSingleNode("league");
            Stringy.ParseList(node.Attributes.GetNamedItem("std").Value, standardProgression);

            LoadData(xml, "league/l");

            for (int i = 0; i < LeagueData.TYPE.Length; ++i)
                league.Add(null);
        }

        protected override GameData NewData(XmlNode node)
        {
            LeagueData d = new LeagueData(node);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new LeagueBlob(d as LeagueData);
        }
    }
}
