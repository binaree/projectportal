namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;

    public class UiLeaguePanel : UiObj
    {
        public GameObject InfoPanel;
        public Text Name;
        public Text MarkQuantity;
        public UiLeagueBadge LeagueInfo;

        public GameObject CollectionPanel;
        public UiTileSetObj Collections;

        public GameObject PromotePanel;
        public UiLeagueBadge NextLeague;
        public GameObject RewardPanel;
        public UiRewardInfo RewardInfo;
        public Text PromoteCost;
        public GameObject DisablePromote;

        public GameObject MaxPanel;

        private LeagueBlob league = null;

        public void AssignLeague(LeagueBlob lea)
        {
            league = lea;
            Refresh();
        }

        public void Refresh()
        {
            CloseAll();
            if (league == null) return;
            Name.text = league.Name();
            MarkQuantity.text = Currency.Man.Value(Currency.Type.Mark).ToString();
            LeagueInfo.AssignLeague(league.Data, league.IsMax());

            if (!league.Promote.HasPromotion())
                SetMaxPromotion();
            else if (league.Promote.CanPromote())
                SetPromotion();
            else
                SetCollection();
        }

        public void SelectPromote()
        {
            if (league == null || !league.Promote.HasCost()) return;
            league.DoPromote(PromotionComplete);
        }

        public void CollectionComplete()
        {
            if (league.Promote.CanPromote())
            {
                CloseAll();
                SetPromotion();
            }
            else
                Collections.Refresh();
            UiMain.Man.Header.RefreshAll();
        }

        public void PromotionComplete()
        {
            AssignLeague(League.Man.Standard);
            UiMain.Man.Header.RefreshAll();
        }

        private void SetCollection()
        {
            CollectionPanel.SetActive(true);
            for (int i = 0; i < league.Promote.Collect.Count; ++i)
            {
                UiLeagueCollection col = Collections.AddNewItem(UiLeagueCollection.LEAGUE_PREFAB, null) as UiLeagueCollection;
                col.AssignCollection(league.Promote.Collect[i]);
                col.AssignTarget(this);
            }
        }

        private void SetPromotion()
        {
            PromotePanel.SetActive(true);
            LeagueData next = league.NextLeague();
            int max = Table.Man.League.StandardMax(next.Group);
            NextLeague.AssignLeague(next, next.Rank == max);
            
            PromoteCost.text = league.Promote.PromotionCost(Currency.Type.Mark).ToString();
            DisablePromote.SetActive(!league.Promote.HasCost());

            RewardBlob rew = league.GetReward();
            if(rew != null)
            {
                RewardPanel.SetActive(true);
                RewardInfo.AssignReward(rew);
            }
        }

        private void SetMaxPromotion()
        {
            MaxPanel.SetActive(true);
        }

        private void CloseAll()
        {
            CollectionPanel.SetActive(false);
            PromotePanel.SetActive(false);
            RewardPanel.SetActive(false);
            DisablePromote.SetActive(false);
            MaxPanel.SetActive(false);
            Collections.Initialize();
        }
    }
}
