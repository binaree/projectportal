namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Hook : DataManager
    {
        public const string DATA_HEADER = "hook_";

        private Dictionary<string, HookBlob> hook = new Dictionary<string, HookBlob>();

        private static Hook _instance = new Hook();

        public static Hook Man { get { return _instance; } }

        public HookData Data(string id)
        {
            return GetData(id) as HookData;
        }

        public HookData DataH(string id)
        {
            return GetData(DATA_HEADER + id) as HookData;
        }

        public HookBlob Blob(int refNum)
        {
            return GetBlob(refNum) as HookBlob;
        }

        public HookBlob Blob(string id)
        {
            if (!hook.ContainsKey(id))
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return hook[id];
        }

        public HookBlob BlobH(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "hook/h");
        }

        protected override GameData NewData(XmlNode node)
        {
            HookData d = new HookData(node);
            HookBlob b = GenerateRefBlob(d, d.RefNum) as HookBlob;
            hook.Add(d.Id, b);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new HookBlob(d as HookData);
        }
    }
}
