namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Bobber : DataManager
    {
        public const string DATA_HEADER = "bob_";

        private Dictionary<string, BobberBlob> bob = new Dictionary<string, BobberBlob>();

        private static Bobber _instance = new Bobber();

        public static Bobber Man { get { return _instance; } }

        public BobberData Data(string id)
        {
            return GetData(id) as BobberData;
        }

        public BobberData DataB(string id)
        {
            return GetData(DATA_HEADER + id) as BobberData;
        }

        public BobberBlob Blob(int refNum)
        {
            return GetBlob(refNum) as BobberBlob;
        }

        public BobberBlob Blob(string id)
        {
            if (!bob.ContainsKey(id))
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return bob[id];
        }

        public BobberBlob BlobB(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "bobber/b");
        }

        protected override GameData NewData(XmlNode node)
        {
            BobberData d = new BobberData(node);
            BobberBlob b = GenerateRefBlob(d, d.RefNum) as BobberBlob;
            bob.Add(d.Id, b);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new BobberBlob(d as BobberData);
        }
    }
}
