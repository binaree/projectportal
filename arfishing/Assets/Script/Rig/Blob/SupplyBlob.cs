namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class SupplyBlob : RigBlob
    {
        public int Quantity { get; private set; }

        public SupplyBlob(SupplyData d)
        {
            Quantity = d.StartQuantity;
        }

        public bool CanUse() { return Quantity != 0; }
        public bool IsInfinite() { return Quantity < 0; }
        public bool Owns() { return Quantity > 0; }
    }
}