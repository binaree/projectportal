namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.Object;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class BobberBlob : GearBlob
    {
        public BobberData Data { get; private set; }
        public BobberObj Obj { get; private set; }

        public BobberBlob(BobberData d)
        {
            Data = d;
            Obj = null;
        }

        override public RigData GetRigData() { return Data; }

        public void CreateBobber()
        {
            ReleaseBobber();
            Obj = BobberFactory.Inst.TakeBobber(Data.Id);
            Obj.AssignToPortal(Portal.Man.Active);
        }

        public void ReleaseBobber()
        {
            if (Obj == null) return;
            BobberFactory.Inst.Release(Obj);
            Obj = null;
        }
    }
}