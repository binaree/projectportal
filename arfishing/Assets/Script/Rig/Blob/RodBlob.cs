namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class RodBlob : GearBlob
    {

        public RodData Data { get; private set; }

        public RodBlob(RodData d)
        {
            Data = d;
        }

        override public RigData GetRigData() { return Data; }
    }
}