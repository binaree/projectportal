namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class HookBlob : SupplyBlob
    {
        public HookData Data { get; private set; }
        public float Set { get; private set; }

        public HookBlob(HookData d) : base(d)
        {
            Data = d;
            Set = 0.0f;
        }

        override public RigData GetRigData() { return Data; }

        public float DetermineSet(FishBlob f)
        {
            Set = Table.Man.Hook.SetChance(f);
            return Set;
        }
    }
}