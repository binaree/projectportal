namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class ReelBlob : GearBlob
    {
        public ReelData Data { get; private set; }

        public ReelBlob(ReelData d)
        {
            Data = d;
        }

        override public RigData GetRigData() { return Data; }
    }
}