namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class AttractantBlob : SupplyBlob
    {
        public AttractantData Data { get; private set; }

        public AttractantBlob(AttractantData d) : base(d)
        {
            Data = d;
        }

        override public RigData GetRigData() { return Data; }
    }
}