namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class BaitBlob : SupplyBlob
    {
        public BaitData Data { get; private set; }

        public BaitBlob(BaitData d) : base(d)
        {
            Data = d;
        }

        override public RigData GetRigData() { return Data; }
    }
}