namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class SinkerBlob : SupplyBlob
    {
        public SinkerData Data { get; private set; }

        public SinkerBlob(SinkerData d) : base(d)
        {
            Data = d;
        }

        override public RigData GetRigData() { return Data; }
    }
}