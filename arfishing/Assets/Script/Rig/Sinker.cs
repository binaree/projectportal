namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Sinker : DataManager
    {
        public const string DATA_HEADER = "sink_";

        private Dictionary<string, SinkerBlob> sink = new Dictionary<string, SinkerBlob>();

        private static Sinker _instance = new Sinker();

        public static Sinker Man { get { return _instance; } }

        public SinkerData Data(string id)
        {
            return GetData(id) as SinkerData;
        }

        public SinkerData DataF(string id)
        {
            return GetData(DATA_HEADER + id) as SinkerData;
        }

        public SinkerBlob Blob(int refNum)
        {
            return GetBlob(refNum) as SinkerBlob;
        }

        public SinkerBlob Blob(string id)
        {
            if (!sink.ContainsKey(id))
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return sink[id];
        }

        public SinkerBlob BlobS(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "sinker/s");
        }

        protected override GameData NewData(XmlNode node)
        {
            SinkerData d = new SinkerData(node);
            SinkerBlob b = GenerateRefBlob(d, d.RefNum) as SinkerBlob;
            sink.Add(d.Id, b);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new SinkerBlob(d as SinkerData);
        }
    }
}
