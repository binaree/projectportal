namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public enum GearRig
    {
        Rod = 0,
        Reel = 1,
        Bobber = 2,
    }

    public enum SupplyRig
    {
        Bait = 0,
        Hook = 1,
        Line = 2,
        Sinker = 3,
        Attractant = 4,
    }

    public class Rig
    {
        public const int GEAR_COUNT = 3;
        public const int SUPPLY_COUNT = 5;

        public RodBlob TheRod { get { return gear[(int)GearRig.Rod] as RodBlob; } }
        public ReelBlob TheReel { get { return gear[(int)GearRig.Reel] as ReelBlob; } }
        public BobberBlob TheBobber { get { return gear[(int)GearRig.Bobber] as BobberBlob; } }

        public BaitBlob TheBait { get { return supply[(int)SupplyRig.Bait] as BaitBlob; } }
        public HookBlob TheHook { get { return supply[(int)SupplyRig.Hook] as HookBlob; } }
        public LineBlob TheLine { get { return supply[(int)SupplyRig.Line] as LineBlob; } }
        public SinkerBlob TheSinker { get { return supply[(int)SupplyRig.Sinker] as SinkerBlob; } }
        public AttractantBlob TheAttractant { get { return supply[(int)SupplyRig.Attractant] as AttractantBlob; } }

        private List<GearBlob> gear = new List<GearBlob>();
        private List<SupplyBlob> supply = new List<SupplyBlob>();

        private static Rig _instance = new Rig();

        public static Rig Man { get { return _instance; } }

        public Rig()
        {
            for (int i = 0; i < GEAR_COUNT; ++i)
                gear.Add(null);
            for (int i = 0; i < SUPPLY_COUNT; ++i)
                supply.Add(null);
        }

        public void AssignUserRig(string val)
        {
            string[] split = val.Split(',');
            for(int i = 0; i < split.Length; ++i)
            {
                if (split[i] == string.Empty) continue;
                if (i < GEAR_COUNT)
                {
                    SetGear((GearRig)i, split[i]);
                }
                else if ((i - GEAR_COUNT) < SUPPLY_COUNT)
                {
                    SetSupply((SupplyRig)(i - GEAR_COUNT), split[i]);
                }
            }
        }

        public void UpdateUserRig()
        {
            string val = "";
            for (int i = 0; i < GEAR_COUNT; ++i)
                if (gear[i] != null)
                    val += gear[i].GetRigData().Id + ",";
            for (int i = 0; i < SUPPLY_COUNT; ++i)
                if (supply[i] != null)
                    val += supply[i].GetRigData().Id + ",";
            //Say.Log("Current rig setup is... " + val);
            //Player.Man.SetRig(val);
        }

        public void SetGear(GearRig g, string refId)
        {
            if (g == GearRig.Rod)
                gear[(int)GearRig.Rod] = Rod.Man.BlobR(refId);
            else if (g == GearRig.Reel)
                gear[(int)GearRig.Reel] = Reel.Man.BlobR(refId);
            else if (g == GearRig.Bobber)
                gear[(int)GearRig.Bobber] = Bobber.Man.BlobB(refId);
        }

        public void SetSupply(SupplyRig s, string refId)
        {
            if (s == SupplyRig.Bait)
                supply[(int)SupplyRig.Bait] = Bait.Man.BlobB(refId);
            else if (s == SupplyRig.Hook)
                supply[(int)SupplyRig.Hook] = Hook.Man.BlobH(refId);
            else if (s == SupplyRig.Line)
                supply[(int)SupplyRig.Line] = Line.Man.BlobL(refId);
            else if (s == SupplyRig.Sinker)
                supply[(int)SupplyRig.Sinker] = Sinker.Man.BlobS(refId);
            else if (s == SupplyRig.Attractant)
                supply[(int)SupplyRig.Attractant] = Attractant.Man.BlobA(refId);
        }
    }
}
