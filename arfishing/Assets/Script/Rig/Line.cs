namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Line : DataManager
    {
        public const string DATA_HEADER = "line_";

        private Dictionary<string, LineBlob> line = new Dictionary<string, LineBlob>();

        private static Line _instance = new Line();

        public static Line Man { get { return _instance; } }

        public LineData Data(string id)
        {
            return GetData(id) as LineData;
        }

        public LineData DataL(string id)
        {
            return GetData(DATA_HEADER + id) as LineData;
        }

        public LineBlob Blob(int refNum)
        {
            return GetBlob(refNum) as LineBlob;
        }

        public LineBlob Blob(string id)
        {
            if (!line.ContainsKey(id))
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return line[id];
        }

        public LineBlob BlobL(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "line/l");
        }

        protected override GameData NewData(XmlNode node)
        {
            LineData d = new LineData(node);
            LineBlob b = GenerateRefBlob(d, d.RefNum) as LineBlob;
            line.Add(d.Id, b);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new LineBlob(d as LineData);
        }
    }
}
