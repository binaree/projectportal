namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Reel : DataManager
    {
        public const string DATA_HEADER = "reel_";

        private Dictionary<string, ReelBlob> reel = new Dictionary<string, ReelBlob>();

        private static Reel _instance = new Reel();

        public static Reel Man { get { return _instance; } }

        public ReelData Data(string id)
        {
            return GetData(id) as ReelData;
        }

        public ReelData DataR(string id)
        {
            return GetData(DATA_HEADER + id) as ReelData;
        }

        public ReelBlob Blob(int refNum)
        {
            return GetBlob(refNum) as ReelBlob;
        }

        public ReelBlob Blob(string id)
        {
            if (!reel.ContainsKey(id))
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return reel[id];
        }

        public ReelBlob BlobR(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "reel/r");
        }

        protected override GameData NewData(XmlNode node)
        {
            ReelData d = new ReelData(node);
            ReelBlob b = GenerateRefBlob(d, d.RefNum) as ReelBlob;
            reel.Add(d.Id, b);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new ReelBlob(d as ReelData);
        }
    }
}
