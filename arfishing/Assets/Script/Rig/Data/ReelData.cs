namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Xml;
    using Utility;

    public enum ReelType
    {
        Tap = 0,
        Crank = 1,
        Swipe = 2,
        Automatic = 3,
        Hold = 4,
    }

    public class ReelData : GearData
    {
        private static readonly string[] REEL_TYPE = { "t", "c", "s", "a", "h" };

        public ReelType Type { get; private set; }
        public string UiPrefab { get; private set; }
        public SplitF InputMax { get; private set; }
        public SplitF InputPower { get; private set; }
        public SplitF InputDecay { get; private set; }
        public SplitF Draw { get; private set; }
        public SplitF Drag { get; private set; }
        public SplitF DragTension { get; private set; }
        public SplitF MaxDepth { get; private set; }
        public string ProPower { get; private set; }

        public ReelData(XmlNode xData) : base(xData)
        {
            Type = StrToReelType(xData.Attributes.GetNamedItem("type").Value);
            UiPrefab = xData.Attributes.GetNamedItem("ui").Value;
            InputMax = new SplitF(xData.Attributes.GetNamedItem("inM").Value);
            InputPower = new SplitF(xData.Attributes.GetNamedItem("inP").Value);
            InputDecay = new SplitF(xData.Attributes.GetNamedItem("inD").Value);
            Draw = new SplitF(xData.Attributes.GetNamedItem("draw").Value);
            Drag = new SplitF(xData.Attributes.GetNamedItem("drag").Value);
            DragTension = new SplitF(xData.Attributes.GetNamedItem("dT").Value);
            MaxDepth = new SplitF(xData.Attributes.GetNamedItem("depth").Value);
            ProPower = xData.Attributes.GetNamedItem("pro").Value;
        }

        public float LvlInputMax(int lvl) { return InputMax.Value(lvl / MaxLevel); }
        public float LvlInputPower(int lvl) { return InputPower.Value(lvl / MaxLevel); }
        public float LvlInputDecay(int lvl) { return InputDecay.Value(lvl / MaxLevel); }
        public float LvlDraw(int lvl) { return Draw.Value(lvl / MaxLevel); }
        public float LvlDrag(int lvl) { return Drag.Value(lvl / MaxLevel); }
        public float LvlDragTension(int lvl) { return DragTension.Value(lvl / MaxLevel); }
        public float LvlMaxDepth(int lvl) { return MaxDepth.Value(lvl / MaxLevel); }

        public static ReelType StrToReelType(string val)
        {
            for (int i = 0; i < REEL_TYPE.Length; ++i)
                if (val == REEL_TYPE[i]) return (ReelType)i;
            return ReelType.Tap;
        }

        public static string ReelTypeToStr(ReelType val)
        {
            return REEL_TYPE[(int)val];
        }
    }
}