namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Xml;
    using Utility;

    public class SupplyData : RigData
    {
        public int StartQuantity { get; private set; }

        public SupplyData(XmlNode xData) : base(xData)
        {
            StartQuantity = Convert.ToInt32(xData.Attributes.GetNamedItem("qty").Value);
        }
        
    }
}