namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class BaitFish
    {
        public string RefId;
        public float Attraction;
        public float RankMod;
        public float SizeMod;
    }

    public class BaitData : SupplyData
    {
        private const char LIST_SEP = ',';
        private const char VAL_SEP = '|';
        private const int VAL_ID = 0;
        private const int VAL_ATT = 1;
        private const int VAL_RANK = 2;
        private const int VAL_SIZE = 3;

        public List<BaitFish> Fish { get; private set; }

        public BaitData(XmlNode xData) : base(xData)
        {
            Fish = new List<BaitFish>();
            string[] split = xData.Attributes.GetNamedItem("fish").Value.Split(LIST_SEP);
            for (int i = 0; i < split.Length; ++i)
                AddFish(split[i]);
        }
        
        private void AddFish(string val)
        {
            string[] split = val.Split(VAL_SEP);
            BaitFish f = new BaitFish();
            if (split.Length > VAL_ID) f.RefId = split[VAL_ID]; else f.RefId = "";
            if (split.Length > VAL_ATT) f.Attraction = Convert.ToSingle(split[VAL_ATT]); else f.Attraction = 0.0f;
            if (split.Length > VAL_RANK) f.RankMod = Convert.ToSingle(split[VAL_RANK]); else f.RankMod = 1.0f;
            if (split.Length > VAL_SIZE) f.SizeMod = Convert.ToSingle(split[VAL_SIZE]); else f.SizeMod = 1.0f;
            Fish.Add(f);
        }
    }
}