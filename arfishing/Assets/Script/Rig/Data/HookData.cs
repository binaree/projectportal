namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public enum HookSize
    {
        Tiny = 0,
        ExtraSmall = 1,
        Small = 2,
        Medium = 3,
        Large = 4,
        ExtraLarge = 5,
        Giant = 6,
        Colossus = 7,
    }

    public class HookData : SupplyData
    {
        private static readonly string[] SIZE = { "t", "xS", "s", "m", "l", "xl", "xxl", "xxxl" };

        public HookSize Size { get; private set; }
        public float SizeDecay { get; private set; }
        public List<float> Set { get; private set; }

        public HookData(XmlNode xData) : base(xData)
        {
            Size = StrToHookSize(xData.Attributes.GetNamedItem("size").Value);
            SizeDecay = Convert.ToSingle(xData.Attributes.GetNamedItem("sizeD").Value);
            Set = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("set").Value, Set);
        }

        public float SetRarity(FishRarity r) { return Set[(int)r]; }

        public static HookSize StrToHookSize(string val)
        {
            for (int i = 0; i < SIZE.Length; ++i)
                if (val == SIZE[i]) return (HookSize)i;
            return HookSize.Medium;
        }

        public static string HookSizeToStr(HookSize val)
        {
            return SIZE[(int)val];
        }
    }
}