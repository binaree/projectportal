namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Xml;
    using Utility;

    public class GearData : RigData
    {
        public int MaxLevel { get; private set; }

        public GearData(XmlNode xData) : base(xData)
        {
            MaxLevel = Convert.ToInt32(xData.Attributes.GetNamedItem("max").Value);
        }
        
    }
}