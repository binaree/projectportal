namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Xml;
    using Utility;

    public class BobberData : GearData
    {
        public SplitF Action { get; private set; }
        public SplitF RankMod { get; private set; }
        public SplitF SizeMod { get; private set; }

        public BobberData(XmlNode xData) : base(xData)
        {
            Action = new SplitF(xData.Attributes.GetNamedItem("act").Value);
            RankMod = new SplitF(xData.Attributes.GetNamedItem("rank").Value);
            SizeMod = new SplitF(xData.Attributes.GetNamedItem("size").Value);
        }

        public float LvlAction(int lvl) { return Action.Value(lvl / MaxLevel); }
        public float LvlRankMod(int lvl) { return RankMod.Value(lvl / MaxLevel); }
        public float LvlSizeMod(int lvl) { return SizeMod.Value(lvl / MaxLevel); }
    }
}