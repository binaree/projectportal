namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Xml;
    using Utility;

    public class SinkerData : SupplyData
    {
        public SplitF Depth { get; private set; }

        public SinkerData(XmlNode xData) : base(xData)
        {
            Depth = new SplitF(xData.Attributes.GetNamedItem("depth").Value);
        }
        
    }
}