namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Xml;
    using Utility;

    public enum RigRarity
    {
        Common = 0,
        Rare = 1,
        Legend = 2,
    }

    public class RigData : GameData
    {
        private static readonly string[] RARITY = { "c", "r", "l" };

        public int RefNum { get; private set; }
        public RigRarity Rarity { get; private set; }

        public RigData(XmlNode xData) : base(xData)
        {
            RefNum = Convert.ToInt32(xData.Attributes.GetNamedItem("num").Value);
            Rarity = StrToRarity(xData.Attributes.GetNamedItem("rare").Value);
        }

        public static RigRarity StrToRarity(string val)
        {
            for (int i = 0; i < RARITY.Length; ++i)
                if (val == RARITY[i]) return (RigRarity)i;
            return RigRarity.Common;
        }

        public static string RarityToStr(FishRarity val)
        {
            return RARITY[(int)val];
        }
    }
}