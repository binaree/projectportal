namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Xml;
    using Utility;

    public class RodData : GearData
    {
        public SplitF Strength { get; private set; }
        public SplitF Action { get; private set; }
        public SplitF Control { get; private set; }
        public SplitF Limit { get; private set; }

        public RodData(XmlNode xData) : base(xData)
        {
            Strength = new SplitF(xData.Attributes.GetNamedItem("str").Value);
            Action = new SplitF(xData.Attributes.GetNamedItem("act").Value);
            Control = new SplitF(xData.Attributes.GetNamedItem("con").Value);
            Limit = new SplitF(xData.Attributes.GetNamedItem("lim").Value);
        }
        
        public float LvlStrength(int lvl)
        {
            return Strength.Value(lvl / MaxLevel);
        }

        public float LvlAction(int lvl)
        {
            return Action.Value(lvl / MaxLevel);
        }

        public float LvlControl(int lvl)
        {
            return Control.Value(lvl / MaxLevel);
        }

        public float LvlLimit(int lvl)
        {
            return Limit.Value(lvl / MaxLevel);
        }
    }
}