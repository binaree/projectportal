namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Bait : DataManager
    {
        public const string DATA_HEADER = "bait_";

        private Dictionary<string, BaitBlob> bait = new Dictionary<string, BaitBlob>();
        
        private static Bait _instance = new Bait();

        public static Bait Man { get { return _instance; } }

        public BaitData Data(string id)
        {
            return GetData(id) as BaitData;
        }

        public BaitData DataB(string id)
        {
            return GetData(DATA_HEADER + id) as BaitData;
        }

        public BaitBlob Blob(int refNum)
        {
            return GetBlob(refNum) as BaitBlob;
        }

        public BaitBlob Blob(string id)
        {
            if( !bait.ContainsKey(id) )
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return bait[id];
        }

        public BaitBlob BlobB(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "bait/b");
        }

        protected override GameData NewData(XmlNode node)
        {
            BaitData d = new BaitData(node);
            BaitBlob b = GenerateRefBlob(d, d.RefNum) as BaitBlob;
            bait.Add(d.Id, b);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new BaitBlob(d as BaitData);
        }
    }
}
