namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Rod : DataManager
    {
        public const string DATA_HEADER = "rod_";

        private Dictionary<string, RodBlob> rod = new Dictionary<string, RodBlob>();

        private static Rod _instance = new Rod();

        public static Rod Man { get { return _instance; } }

        public RodData Data(string id)
        {
            return GetData(id) as RodData;
        }

        public RodData DataR(string id)
        {
            return GetData(DATA_HEADER + id) as RodData;
        }

        public RodBlob Blob(int refNum)
        {
            return GetBlob(refNum) as RodBlob;
        }

        public RodBlob Blob(string id)
        {
            if (!rod.ContainsKey(id))
            {
                Say.Log("Cannot find the requested blob of id = " + id);
                return null;
            }
            return rod[id];
        }

        public RodBlob BlobR(string refId)
        {
            return Blob(DATA_HEADER + refId);
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "rod/r");
        }

        protected override GameData NewData(XmlNode node)
        {
            RodData d = new RodData(node);
            RodBlob b = GenerateRefBlob(d, d.RefNum) as RodBlob;
            rod.Add(d.Id, b);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new RodBlob(d as RodData);
        }
    }
}
