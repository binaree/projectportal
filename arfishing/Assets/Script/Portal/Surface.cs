namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Surface : DataManager
    {
        public const string DATA_HEADER = "sur_";

        private static Surface _instance = new Surface();

        public static Surface Man { get { return _instance; } }

        public SurfaceData Data(string id)
        {
            return GetData(id) as SurfaceData;
        }

        public SurfaceData DataS(string id)
        {
            return GetData(DATA_HEADER + id) as SurfaceData;
        }

        public SurfaceBlob Blob(int refNum)
        {
            return GetBlob(refNum) as SurfaceBlob;
        }

        public SurfaceBlob CreateSurfaceForPortal(PortalData pd)
        {
            SurfaceData d = DataS(pd.Id);
            SurfaceBlob b = GenerateRefBlob(d, pd.RefNum) as SurfaceBlob;
            return b;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "surface/s");
        }

        protected override GameData NewData(XmlNode node)
        {
            SurfaceData d = new SurfaceData(node);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new SurfaceBlob(d as SurfaceData);
        }
    }
}
