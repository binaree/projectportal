namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class PortalData : GameData
    {
        public int RefNum { get; private set; }
        public float SessionTime { get; private set; }
        public List<RegionData> Region { get; private set; }

        public PortalData(XmlNode xData) : base(xData)
        {
            RefNum = Convert.ToInt32(xData.Attributes.GetNamedItem("num").Value);
            SessionTime = Convert.ToSingle(xData.Attributes.GetNamedItem("time").Value);

            Region = new List<RegionData>();
            XmlNodeList nodes = xData.SelectNodes("r");
            foreach (XmlNode node in nodes)
            {
                RegionData d = new RegionData(node);
                Region.Add(d);
            }
        }
    }
}