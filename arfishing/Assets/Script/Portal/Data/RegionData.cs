namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public class RegionFish
    {
        public string RefId;
        public float Occurence;
        public float AttractRate;
    }

    public class RegionData : GameData
    {
        private const char LIST_SEP = ',';
        private const char VAL_SEP = '|';
        private const int VAL_ID = 0;
        private const int VAL_OCC = 1;
        private const int VAL_ATT = 2;

        public SplitF Depth { get; private set; }
        public float PollTime { get; private set; }
        public List<RegionFish> Fish { get; private set; }
        public float BaseOccurence { get; private set; }
        public float BaseAttract { get; private set; }

        public RegionData(XmlNode xData) : base(xData)
        {
            Depth = new SplitF(xData.Attributes.GetNamedItem("depth").Value);
            PollTime = Convert.ToSingle(xData.Attributes.GetNamedItem("poll").Value);
            BaseOccurence = Convert.ToSingle(xData.Attributes.GetNamedItem("occ").Value);
            BaseAttract = Convert.ToSingle(xData.Attributes.GetNamedItem("att").Value);

            Fish = new List<RegionFish>();
            string[] split = xData.Attributes.GetNamedItem("fish").Value.Split(LIST_SEP);
            for (int i = 0; i < split.Length; ++i)
                AddFish(split[i]);
        }

        private void AddFish(string val)
        {
            string[] split = val.Split(VAL_SEP);
            RegionFish f = new RegionFish();
            if (split.Length > VAL_ID) f.RefId = split[VAL_ID]; else f.RefId = "";
            if (split.Length > VAL_OCC) f.Occurence = Convert.ToSingle(split[VAL_OCC]); else f.Occurence = 1.0f;
            if (split.Length > VAL_ATT) f.AttractRate = Convert.ToSingle(split[VAL_ATT]); else f.AttractRate = 1.0f;
            Fish.Add(f);
        }
    }
}