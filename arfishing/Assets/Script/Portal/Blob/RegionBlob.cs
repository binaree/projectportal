namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;

    public class RegionBlob : GameBlob
    {
        public RegionData Data { get; private set; }
        public PortalBlob Portal { get; private set; }

        public float PollTime { get; private set; }

        public RegionBlob(RegionData d, PortalBlob port)
        {
            Data = d;
            Portal = port;
            PollTime = 0.0f;
        }

        public bool IsValid(float curDepth)
        {
            if (curDepth < Data.Depth.Min()) return false;
            if (Data.Depth.Max() > 0 && curDepth > Data.Depth.Max()) return false;
            return true;
        }

        public void Activate(bool init)
        {
            if (init) PollTime = Data.PollTime;
        }

        public void Close()
        {

        }

        public bool UpdateRegion()
        {
            PollTime -= TimeLord.Inst.ArenaDelta;
            if (PollTime <= 0.0f)
            {
                PollTime = Data.PollTime;
                return true;
            }
            return false;
        }
    }
}