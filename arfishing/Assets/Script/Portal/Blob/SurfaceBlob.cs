namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.Object;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;

    public class SurfaceBlob : GameBlob
    {
        public SurfaceData Data { get; private set; }

        public SurfaceBlob(SurfaceData d)
        {
            Data = d;
        }

        public float Score(SurfaceCamObj surface)
        {
            // This function will eventually pass in an image taken from the camera
            // This image will then be checked against the properties specified in the data to determine
            //   the score for this surface with 0 = no chance & 1 = perfect fit.
            Color core = surface.SampleRegionByPercent(0.5f, 0.5f, Data.CoreSize, Data.CoreSize);
            float match = ColorMatch(core, Data.CoreColor);
            if (match >= Data.CoreColorThreshold) return 0.0f;
            float score = 1.0f - match;
            //Say.Log("Color: " + Data.CoreColor.ToString() + ", Match = " + match + ", Score = " + score);
            return score;
        }

        private float ColorMatch(Color col, Vector3 matchVec)
        {
            Vector3 vec = new Vector3(col.r, col.g, col.b);
            float val = Mathf.Abs(vec.x - matchVec.x);
            val += Mathf.Abs(vec.y - matchVec.y);
            val += Mathf.Abs(vec.z - matchVec.z);
            return val / 3.0f;
        }
    }
}