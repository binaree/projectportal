namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    
    public class PortalTable : TableData
    {
        public int DefaultPortalRefNum { get; private set; }
        public float ScoreMin { get; private set; }

        public PortalTable(XmlNode xData) : base(xData)
        {
            DefaultPortalRefNum = Convert.ToInt32(xData.Attributes.GetNamedItem("default").Value);
            ScoreMin = Convert.ToSingle(xData.Attributes.GetNamedItem("scoreMin").Value);
        }

        public PortalBlob PickPortal(List<PortalBlob> port)
        {
            PortalBlob best = null;
            float score = -1.0f;

            for(int i = 0; i < port.Count; ++i)
            {
                float val = port[i].PortalSurface.Score(Step.Man.Layer.Surface);
                if (val > score)
                {
                    score = val;
                    best = port[i];
                }
            }

            if (score > ScoreMin)
                return best;

            return Portal.Man.Blob(DefaultPortalRefNum);
        }
    }
}