namespace Binaree.Game.Object
{
    using Base;
    using Component;
    using Core;
    using Factory;
    using Object;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class PortalObj : PropObj
    {
        public SpriteRenderer PortalImage;
        public GameObject BobberPos;

        override public GameObject GetSpawn(int spawnIndex) { return BobberPos; }

        override public void Use()
        {
            base.Use();
            Step.Man.Layer.AssignObjToWorld(this, LayerControl.World.Ground);
        }
    }
}
