namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;

    /// <summary>
    /// Handles loading and access for all game tables which are used to perform design specific procedures to a blob
    /// </summary>
    public class Table
    {
        private const string TABLE_HEADER = "tab_";
        private static Table _instance = new Table();

        public AttractTable Attract { get; private set; }
        public CastTable Cast { get; private set; }
        public CurrencyTable Currency { get; private set; }
        public DepthTable Depth { get; private set; }
        public FishTable Fish { get; private set; }
        public HitTable Hit { get; private set; }
        public HookTable Hook { get; private set; }
        public PlayerTable Player { get; private set; }
        public PortalTable Portal { get; private set; }
        public ReelTable Reel { get; private set; }
        public RewardTable Reward { get; private set; }
        public TensionTable Tension { get; private set; }
        public WaitTable Wait { get; private set; }

        public static Table Man { get { return _instance; } }

        public void Load(XmlDocument xml)
        {
            XmlNodeList nodes = xml.SelectNodes("tables/t");
            foreach (XmlNode node in nodes)
            {
                NewTableFromStr(node.Attributes.GetNamedItem("type").Value, node);
            }
        }

        private void NewTableFromStr(string str, XmlNode node)
        {
            if (str == "attract") Attract = new AttractTable(node);
            else if (str == "cast") Cast = new CastTable(node);
            else if (str == "currency") Currency = new CurrencyTable(node);
            else if (str == "depth") Depth = new DepthTable(node);
            else if (str == "fish") Fish = new FishTable(node);
            else if (str == "hit") Hit = new HitTable(node);
            else if (str == "hook") Hook = new HookTable(node);
            else if (str == "player") Player = new PlayerTable(node);
            else if (str == "portal") Portal = new PortalTable(node);
            else if (str == "reel") Reel = new ReelTable(node);
            else if (str == "reward") Reward = new RewardTable(node);
            else if (str == "ten") Tension = new TensionTable(node);
            else if (str == "wait") Wait = new WaitTable(node);
            else Say.Warn("Table man could not find a table of type " + str);
        }
    }
}
