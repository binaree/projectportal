namespace Binaree.Game.Fx
{
    using Base;
    using Component;
    using Core;
    using Factory;
    using Object;
    using UnityEngine;

    /// <summary>
    /// Fx object that is used to float text from a starting position
    /// </summary>
    public class FxFloatText : FxObj
    {
        private const float MOVE_SCALE = 100.0f;
        private const string DAMAGE_FORMAT = "-";
        private const string RESTORE_FORMAT = "+";

        public enum Style
        {
            Damage = 0,
            Restore = 1,
            Direct = 2,
        }

        public Style TextStyle = Style.Damage;
        public LayerControl.World WorldLayer = LayerControl.World.Top;
        public string UnitySortingLayer = "Top";
        public TextMesh TheText;

        public float FloatSpeed = 100.0f;
        public float LifeTime = 1.0f;

        private float curTime = 0.0f;

        override public void Use()
        {
            //#GameObject lay = Battle.Man.Layer.AssignObjToWorld(this, WorldLayer);
            //#gameObject.transform.Translate(0.0f, -lay.transform.position.y, 0.0f);
            base.Use();
        }

        public void Activate(string baseStr, Vector3 startPos)
        {
            if (TextStyle == Style.Damage) TheText.text = DAMAGE_FORMAT + baseStr;
            else if (TextStyle == Style.Restore) TheText.text = RESTORE_FORMAT + baseStr;
            else TheText.text = baseStr;

            Renderer renderer = TheText.gameObject.GetComponent<Renderer>();
            if (renderer != null)
            {
                renderer.sortingOrder = 100;
                renderer.sortingLayerName = UnitySortingLayer;
            }

            curTime = 0.0f;
            gameObject.transform.position = startPos;
            Use();
        }

        private void Update()
        {
            curTime += TimeLord.Inst.ArenaDelta;
            if(curTime >= LifeTime)
            {
                FxFactory.Inst.Release(this);
                return;
            }
            gameObject.transform.Translate(Vector3.forward * TimeLord.Inst.ArenaDelta * FloatSpeed * MOVE_SCALE);
        }
    }
}
