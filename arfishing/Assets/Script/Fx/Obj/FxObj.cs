namespace Binaree.Game.Base
{
    using Core;
    using UnityEngine;

    /// <summary>
    /// Objects created for cosmetic flavor with no impact upon the gameplay
    /// </summary>
    public class FxObj : GameObj
    {
        
    }
}
