namespace Binaree.Game.Ui
{
    using Binaree.Game.Utility;
    using Component;
    using Core;
    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    /// The header responsible for displaying all ui elements located on the top of the screen
    /// </summary>
    public class UiHeader : UiObj
    {
        public GameObject BatteryPanel;
        public Text BatteryAmount;

        public GameObject CurrencyPanel;
        public Text HcAmount;

        public GameObject PortalPanel;
        public Text PortalName;

        public GameObject PortalTimePanel;
        public Text PortalTime;
        public GameObject TimeWarning;
        public GameObject CloseButton;

        private void Start()
        {
            SetPortal(null);
        }

        private void Update()
        {
            if (PortalTimePanel.activeSelf)
                RefreshTime();
        }

        public void SetVisibility(bool turnOn)
        {
            gameObject.SetActive(turnOn);
            if (turnOn)
                RefreshAll();
        }

        public void SelectBattery()
        {
            Say.Log("Select battery");
        }

        public void SelectCurrency()
        {
            Say.Log("Select currency");
        }

        public void SelectPortal()
        {
            Say.Log("Select portal");
        }

        public void ClosePortal()
        {
            Portal.Man.KillPortal();
        }

        public void RefreshAll()
        {
            RefreshBattery();
            RefreshCurrency();
            RefreshTime();
        }

        public void RefreshBattery()
        {
            BatteryAmount.text = "? / ?";
        }

        public void RefreshCurrency()
        {
            HcAmount.text = "???";
        }

        public void RefreshTime()
        {
            PortalTime.text = "??:??";
            TimeWarning.SetActive(false);
            CloseButton.SetActive(Step.Man.Current == Step.Man.Select);
        }

        public void SetPortal(PortalBlob port)
        {
            RefreshAll();
            if (port == null)
            {
                PortalTimePanel.SetActive(false);
                PortalName.text = Script.Man.Name("no_portal");
                return;
            }
            
            PortalTimePanel.SetActive(true);
            PortalName.text = Script.Man.Name(port.Data.Id);
        }
    }
}
