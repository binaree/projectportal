namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Core;
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class UiCastPage : UiPage
    {
        public const string PAGE_ID = "cast";

        public UiCastWidget CastWidget;

        override public string RefId() { return PAGE_ID; }

        override public void Activate()
        {
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public void GoCast()
        {
            Step.Man.Cast.GoCast(CastWidget.CurrentPosition());
        }

        public void CancelCast()
        {
            Step.Man.Begin(StepSelect.STEP_ID);
        }
    }
}
