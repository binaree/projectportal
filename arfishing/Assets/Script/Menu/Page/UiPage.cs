namespace Binaree.Game.Ui
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    /// Handles a page within the menu for the game
    /// Only one page may be visisble at a time
    /// </summary>
    public class UiPage : UiObj
    {

        virtual public string RefId() { return "NONE"; }

        virtual public void Activate()
        {

        }

        virtual public void Refresh()
        {

        }

        virtual public void Close()
        {

        }
    }
}
