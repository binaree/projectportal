namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Core;
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class UiSelectPage : UiPage
    {
        public const string PAGE_ID = "select";
        

        override public string RefId() { return PAGE_ID; }

        override public void Activate()
        {
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public void SelectCast()
        {
            Step.Man.Select.StartCast();
        }

        public void SelectRig()
        {
            Say.Log("Rig has been selected");
        }

        public void SelectGuide()
        {
            Say.Log("Guide has been selected");
        }
    }
}
