namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Core;
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class UiHomePage : UiPage
    {
        public const string PAGE_ID = "home";
        
        public GameObject HomePanel;
        public GameObject GeneratePanel;
        

        override public string RefId() { return PAGE_ID; }

        override public void Activate()
        {
            SetupHome();
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        public void SelectPortal()
        {
            Step.Man.Init.StartGenerate();
            UiMain.Man.Header.SetVisibility(false);
            SetupGenerate();
        }

        public void SelectRig()
        {
            Say.Log("Rig has been selected");
        }

        public void SelectGuide()
        {
            Say.Log("Guide has been selected");
        }

        public void SelectGenerate()
        {
            Step.Man.Init.CaptureSurface();
        }

        public void CancelGenerate()
        {
            Step.Man.Init.CancelGenerate();
            UiMain.Man.Header.SetVisibility(true);
            SetupHome();
        }

        private void SetupHome()
        {
            HomePanel.SetActive(true);
            GeneratePanel.SetActive(false);
        }

        private void SetupGenerate()
        {
            HomePanel.SetActive(false);
            GeneratePanel.SetActive(true);
        }
    }
}
