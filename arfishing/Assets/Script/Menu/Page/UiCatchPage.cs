namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Binaree.Game.Utility;
    using Core;
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class UiCatchPage : UiPage
    {
        public enum State
        {
            Catch = 0,
            Info = 1,
            Photo = 2,
        }

        public const string PAGE_ID = "catch";
        private const string FISH_DIR = "fishui/";

        public GameObject CatchUi;
        public GameObject PhotoUi;

        public Image FishImage;
        public float ShowTime = 2.0f;
        public GameObject FishFooterPanel;

        public GameObject FishPanel;
        public Text FishName;
        public GameObject TrophyIcon;
        public GameObject StarRankPanel;
        public Text StarRankValue;
        public Text SizeValue;
        public Text SCValue;
        public Text MarkValue;

        private State state = State.Catch;
        private float curTime = 0.0f;

        override public string RefId() { return PAGE_ID; }

        override public void Activate()
        {
            CatchUi.SetActive(false);
            PhotoUi.SetActive(false);
            SetFishImage();
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }

        private void Update()
        {
            if (state == State.Catch)
            {
                curTime -= TimeLord.Inst.ArenaDelta;
                if (curTime <= 0.0f)
                    SetInfo();
            }
        }

        public void SelectFish()
        {
            Step.Man.Catch.ResumeFishing();
        }

        public void SelectPhoto()
        {
            SetPhoto();
        }

        public void SelectGuide()
        {
            Say.Log("Guide selected");
        }

        public void SnapPhoto()
        {
            Say.Log("Take photo now");
        }

        public void CancelPhoto()
        {
            SetInfo();
        }

        private void SetFishImage()
        {
            FishBlob f = Fish.Man.Active;
            FishName.text = Script.Man.Name(f.Data.Id);
            Spritey.ReplaceIcon(FishImage, FISH_DIR, f.Data.Id);
            FishImage.gameObject.SetActive(true);
            SizeValue.text = f.Size.ToString("0.##") + "in";
            SCValue.text = f.ScReward.ToString();
            MarkValue.text = f.MarkReward.ToString();
            if( f.IsTrophy())
            {
                TrophyIcon.gameObject.SetActive(true);
                StarRankPanel.SetActive(false);
            }
            else
            {
                TrophyIcon.gameObject.SetActive(false);
                StarRankPanel.SetActive(true);
                StarRankValue.text = ((int)f.Rank).ToString();
            }

            curTime = ShowTime;
            state = State.Catch;
        }

        private void SetInfo()
        {
            CatchUi.SetActive(true);
            PhotoUi.SetActive(false);
            UiMain.Man.Header.SetVisibility(true);
            state = State.Info;
        }

        private void SetPhoto()
        {
            CatchUi.SetActive(false);
            PhotoUi.SetActive(true);
            UiMain.Man.Header.SetVisibility(false);
            state = State.Photo;
        }
    }
}
