namespace Binaree.Game.Ui
{
    using Binaree.Game.Component;
    using Core;
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class UiReelPage : UiPage
    {
        public const string PAGE_ID = "reel";

        public UiDepthWidget DepthWidget;
        public UiTensionWidget TensionWidget;
        public UiTapWidget TapWidget;
        public Text InstructionLabel;

        override public string RefId() { return PAGE_ID; }

        override public void Activate()
        {
        }

        override public void Refresh()
        {

        }

        override public void Close()
        {

        }
    }
}
