namespace Binaree.Game.Ui
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    /// Handles displaying and updating load screen information
    /// </summary>
    public class LoadingScreen : MonoBehaviour
    {
        public enum Style
        {
            None = 0,
            Progress = 1,
            Animate = 2,
        }

        public GameObject AnimateContainer;
        public GameObject AnimateObj;
        public GameObject ProgressContainer;
        public Text ProgressText;
        public float RotateAmount = -1.0f;

        private static LoadingScreen _instance = null;

        private Style loadStyle = Style.None;

        public static LoadingScreen Inst { get { return _instance; } }

        void Awake()
        {
            _instance = this;

            // Keep this around
            DontDestroyOnLoad(this);

            ShowLoad(Style.None);
        }

        public void ShowLoad(Style useStyle)
        {
            loadStyle = useStyle;
            ProgressText.text = "0%";
            ProgressContainer.SetActive(loadStyle == Style.Progress);
            AnimateObj.transform.localRotation = Quaternion.identity;
            AnimateContainer.SetActive(loadStyle == Style.Animate);
            gameObject.SetActive(true);
        }

        public void HideLoad()
        {
            gameObject.SetActive(false);
        }

        public void Progress(float amt)
        {
            ProgressText.text = ((int)(amt * 100)) + "%";
            AnimateObj.transform.Rotate(0.0f, 0.0f, RotateAmount * UnityEngine.Time.deltaTime);
        }
    }
}
