namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Stat : DataManager
    {
        public const string DATA_HEADER = "stat_";
        
        private static Stat _instance = new Stat();

        public static Stat Man { get { return _instance; } }

        public StatData Data(string id)
        {
            return GetData(id) as StatData;
        }

        public StatData DataS(string id)
        {
            return GetData(DATA_HEADER + id) as StatData;
        }

        public StatBlob Blob(int refNum)
        {
            return GetBlob(refNum) as StatBlob;
        }

        public StatBlob CreateForFish(FishBlob fish)
        {
            StatData d = DataS(fish.Data.Id + "_" + fish.Rank);
            StatBlob b = GenerateBlob(d) as StatBlob;
            b.AssignFish(fish);
            return b;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "stat/s");
        }

        protected override GameData NewData(XmlNode node)
        {
            StatData d = new StatData(node);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new StatBlob(d as StatData);
        }
    }
}
