namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Hit : DataManager
    {
        public const string DATA_HEADER = "hit_";
        
        private static Hit _instance = new Hit();

        public static Hit Man { get { return _instance; } }

        public HitData Data(string id)
        {
            return GetData(id) as HitData;
        }

        public HitData DataH(string id)
        {
            return GetData(DATA_HEADER + id) as HitData;
        }

        public HitBlob Blob(int refNum)
        {
            return GetBlob(refNum) as HitBlob;
        }

        public HitBlob CreateForFish(FishBlob fish)
        {
            HitData d = DataH(fish.Data.Id);
            HitBlob b = GenerateBlob(d) as HitBlob;
            b.AssignFish(fish);
            return b;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "hit/h");
        }

        protected override GameData NewData(XmlNode node)
        {
            HitData d = new HitData(node);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new HitBlob(d as HitData);
        }
    }
}
