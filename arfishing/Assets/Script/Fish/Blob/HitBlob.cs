namespace Binaree.Game.Component
{
    using Base;
    using Binaree.Game.Object;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;

    public enum HitStatus
    {
        Wait = 0,
        Nibble = 1,
        Bite = 2,
        Hooked = 3,
    }
    
    public class HitBlob : GameBlob
    {
        public HitData Data { get; private set; }
        public HitStatus Status { get; private set; }

        private FishBlob fish = null;
        private BobberObj bobber = null;
        private float actTime = 0.0f;
        private int nibsBeforeBite = 0;

        public HitBlob(HitData d)
        {
            Data = d;
        }

        public void AssignFish(FishBlob theFish)
        {
            fish = theFish;
            bobber = Rig.Man.TheBobber.Obj;
            nibsBeforeBite = Data.NibCount.Rand();

            if (Data.Hit == FishHit.Striker)
                CheckAction(Table.Man.Wait.CurrentPower > 0.0f);
            else
                SetActTime();
        }

        public void UpdateHit(bool isMoving)
        {
            actTime -= TimeLord.Inst.ArenaDelta;
            if( actTime < 0.0f )
            {
                if (Status == HitStatus.Wait)
                    CheckAction(isMoving);
                else if (Status == HitStatus.Nibble)
                    EndNibble();
                else if (Status == HitStatus.Bite)
                {
                    EndBite();
                    Step.Man.Wait.LostFish(StepWait.Lost.TookBait);
                }
            }
        }

        public float BaseHitChance()
        {
            if (Status == HitStatus.Wait) return 0.0f;
            else if (Status == HitStatus.Bite) return 1.0f;
            return Data.NibHitChance.Rand();
        }

        public bool MissHit(float delay)
        {
            if (Status == HitStatus.Bite)
            {
                EndBite();
                return true;
            }
            else if (Status == HitStatus.Nibble)
            {
                EndNibble();
                if( Randomize.Low(Data.NibLoseChance.Rand()) )
                {
                    return true;
                }
            }
            actTime += delay;
            return false;
        }

        public void SetHook()
        {
            Say.Log("Hooked!");
            Status = HitStatus.Hooked;
            actTime = 9999999.0f;
            bobber.SetHit();
        }

        private void CheckAction(bool isMoving)
        {
            if (Randomize.Low((isMoving) ? Data.MoveAttract : Data.StillAttract))
            {
                if (nibsBeforeBite <= 0)
                    SetBite();
                else
                    SetNibble();
            }
            else
                SetActTime();
        }

        private void SetActTime()
        {
            actTime = Data.ActionTime.Rand();
            Status = HitStatus.Wait;
            bobber.SetWait();
        }

        private void SetNibble()
        {
            Say.Log("Nibble");
            actTime = Data.NibTime.Rand();
            Status = HitStatus.Nibble;
            bobber.SetNibble();
            --nibsBeforeBite;
        }

        private void EndNibble()
        {
            SetActTime();
        }

        private void SetBite()
        {
            Say.Log("Bite");
            actTime = Data.BiteTime.Rand();
            Status = HitStatus.Bite;
            bobber.SetBite();
        }

        private void EndBite()
        {
            SetActTime();
            actTime = 9999999.0f;
        }
    }
}