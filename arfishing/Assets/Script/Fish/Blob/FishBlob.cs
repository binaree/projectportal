namespace Binaree.Game.Component
{
    using Base;
    using Core;
    using Factory;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using Utility;
    
    public class FishBlob : GameBlob
    {
        public FishData Data { get; private set; }
        public StatBlob Stats { get; private set; }
        public HitBlob HitAi { get; private set; }
        public MoveBlob MoveAi { get; private set; }

        public int Rank { get; private set; }
        public int IndexRank { get { return Rank - 1; } }
        public float Size { get; private set; }
        public HookSize SizeType { get; private set; }
        public float SizeLevel { get; private set; }

        public int ScReward { get; private set; }
        public int MarkReward { get; private set; }

        public FishBlob(FishData d)
        {
            Data = d;
        }

        public void Activate(int rank, float size)
        {
            Rank = rank;
            Size = size;
            SizeType = Table.Man.Hook.HookSizeByFish(this);
            SizeLevel = (Size - Data.SizeChart[rank - 1]) / (Data.SizeChart[rank] - Data.SizeChart[rank - 1]);

            Stats = Stat.Man.CreateForFish(this);
            HitAi = Hit.Man.CreateForFish(this);
            MoveAi = Move.Man.CreateForFish(this);

            ScReward = Table.Man.Reward.DetermineScReward(this);
            MarkReward = Table.Man.Reward.DetermineMarkReward(this);
        }

        public void Clear()
        {

        }

        public bool IsTrophy() { return Rank == Table.Man.Fish.MaxRank; }
    }
}