namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Xml;
    using Utility;

    public class StatData : GameData
    {
        public SplitF Power { get; private set; }
        public SplitF Speed { get; private set; }
        public SplitF Recover { get; private set; }
        public SplitF Consume { get; private set; }
        public SplitF Stamina { get; private set; }

        public StatData(XmlNode xData) : base(xData)
        {
            Power = new SplitF(xData.Attributes.GetNamedItem("pow").Value);
            Speed = new SplitF(xData.Attributes.GetNamedItem("sp").Value);
            Recover = new SplitF(xData.Attributes.GetNamedItem("rec").Value);
            Consume = new SplitF(xData.Attributes.GetNamedItem("con").Value);
            Stamina = new SplitF(xData.Attributes.GetNamedItem("st").Value);
        }
    }
}