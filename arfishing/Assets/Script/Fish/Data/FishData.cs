namespace Binaree.Game.Component
{
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using Utility;

    public enum FishRarity
    {
        Common = 0,
        Rare = 1,
        Legend = 2,
    }

    public class FishData : GameData
    {
        private static readonly string[] RARITY = { "c", "r", "l" };

        public int RefNum { get; private set; }
        public FishRarity Rarity { get; private set; }
        public List<float> RankRate { get; private set; }
        public List<float> SizeChart { get; private set; }
        public float ScScalar { get; private set; }
        public float MarkScalar { get; private set; }

        public FishData(XmlNode xData) : base(xData)
        {
            RefNum = Convert.ToInt32(xData.Attributes.GetNamedItem("num").Value);
            Rarity = StrToRarity(xData.Attributes.GetNamedItem("rare").Value);

            RankRate = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("rank").Value, RankRate);

            SizeChart = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("size").Value, SizeChart);

            ScScalar = Convert.ToSingle(xData.Attributes.GetNamedItem("sc").Value);
            MarkScalar = Convert.ToSingle(xData.Attributes.GetNamedItem("mark").Value);
        }

        public static FishRarity StrToRarity(string val)
        {
            for (int i = 0; i < RARITY.Length; ++i)
                if (val == RARITY[i]) return (FishRarity)i;
            return FishRarity.Common;
        }

        public static string RarityToStr(FishRarity val)
        {
            return RARITY[(int)val];
        }
    }
}