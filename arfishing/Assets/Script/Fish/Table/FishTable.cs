namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    
    public class FishTable : TableData
    {
        public int MaxRank { get; private set; }
        public List<string> LaunchAction { get; private set; }
        public List<string> ReactionAction { get; private set; }

        public FishTable(XmlNode xData) : base(xData)
        {
            MaxRank = Convert.ToInt32(xData.Attributes.GetNamedItem("maxRank").Value);

            LaunchAction = new List<string>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("launch").Value, LaunchAction);

            ReactionAction = new List<string>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("react").Value, ReactionAction);
        }
    }
}