namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    
    public class RewardTable : TableData
    {
        public List<SplitF> ScReward { get; private set; }
        public List<SplitF> MarkReward { get; private set; }
        public List<float> ScRarityScale { get; private set; }
        public List<float> MarkRarityScale { get; private set; }

        public RewardTable(XmlNode xData) : base(xData)
        {
            ScReward = new List<SplitF>();
            string[] split = xData.Attributes.GetNamedItem("sc").Value.Split(',');
            for (int i = 0; i < split.Length; ++i)
                ScReward.Add(new SplitF(split[i]));

            MarkReward = new List<SplitF>();
            split = xData.Attributes.GetNamedItem("mark").Value.Split(',');
            for (int i = 0; i < split.Length; ++i)
                MarkReward.Add(new SplitF(split[i]));

            ScRarityScale = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("scR").Value, ScRarityScale);

            MarkRarityScale = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("markR").Value, MarkRarityScale);
        }

        public int DetermineScReward(FishBlob f)
        {
            SplitF use = ScReward[f.IndexRank];
            return (int)(use.Value(f.SizeLevel) * ScRarityScale[(int)f.Data.Rarity] * f.Data.ScScalar);
        }

        public int DetermineMarkReward(FishBlob f)
        {
            SplitF use = MarkReward[f.IndexRank];
            return (int)(use.Value(f.SizeLevel) * MarkRarityScale[(int)f.Data.Rarity] * f.Data.MarkScalar);
        }
    }

}