namespace Binaree.Game.Component
{
    using System.Xml;
    using System.Collections.Generic;
    using Core;
    using Base;
    using Factory;
    using Table;
    using User;
    using Ui;
    using Object;

    public class Fish : DataManager
    {
        public const string DATA_HEADER = "fish_";

        public FishBlob Active { get; private set; }
        
        private static Fish _instance = new Fish();

        public static Fish Man { get { return _instance; } }

        public FishData Data(string id)
        {
            return GetData(id) as FishData;
        }

        public FishData DataF(string id)
        {
            return GetData(DATA_HEADER + id) as FishData;
        }

        public FishBlob Blob(int refNum)
        {
            return GetBlob(refNum) as FishBlob;
        }

        public FishBlob GenerateFish(FishData d)
        {
            ClearActive();
            Active = GenerateBlob(d) as FishBlob;
            return Active;
        }

        public void ClearActive()
        {
            if (Active == null) return;
            Active.Clear();
            Active = null;
        }

        public void Load(XmlDocument xml)
        {
            LoadData(xml, "fish/f");
        }

        protected override GameData NewData(XmlNode node)
        {
            FishData d = new FishData(node);
            return d;
        }
        
        override protected GameBlob NewBlob(GameData d)
        {
            return new FishBlob(d as FishData);
        }
    }
}
