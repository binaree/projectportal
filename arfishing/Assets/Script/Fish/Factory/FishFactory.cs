namespace Binaree.Game.Factory
{
    using Component;
    using Core;
    using Base;
    using System.Collections.Generic;
    using Object;
    
    public class FishFactory : GameFactory
    {
        public const string FISH_HEADER = "fish_";

        public string FishDirectory = "fish";
        public int PreloadAmount = 1;

        private static FishFactory _instance = null;

        public static FishFactory Inst { get { return _instance; } }

        override public void Generate()
        {
            _instance = this;
            List<GameData> allHero = Fish.Man.AllGameData();
            for(int i = 0; i < allHero.Count; ++i)
            {
                if (store.ContainsKey(allHero[i].Id)) continue;
                TrackPrefab(allHero[i].Id, PreloadAmount);
            }
            hasGenerated = true;
        }

        override protected string FormatTypeToPrefab(string objType) { return /*HERO_HEADER +*/ objType; }
        override protected string PrefabDirectory() { return FishDirectory + "/"; }

        public FishObj TakeHero(string objType) { return Take(objType) as FishObj; }
    }
}
