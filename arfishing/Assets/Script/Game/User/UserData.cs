namespace Binaree.Game.User
{

    /// <summary>
    /// Handles the loading and storing of all dynamic properties that need preserved between sessions
    /// </summary>
    public class UserData
    {
        protected UserData parentComp = null; 

        public UserData()
        {

        }

        virtual public string UserId() { return "NONE"; }
        virtual public string Tag() { return "NONE"; }
        virtual public UserData Parent() { return parentComp; }
        virtual public void Dirty() { if (parentComp != null) parentComp.Dirty(); }
        virtual public bool IsDirty() { return false; }
        virtual public void Clean() { }

        virtual public void Load(UserData useParent)
        {
            parentComp = useParent;
        }
    }
}
