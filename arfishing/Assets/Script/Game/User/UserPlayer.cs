namespace Binaree.Game.User
{
    using Component;
    using Core;
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;
    using Utility;

    public class UserPlayer : UserData
    {
        public const string TAG = "player";

        [XmlAttribute("name")]
        public string Name { get; private set; }

        [XmlAttribute("lvl")]
        public float Level { get; private set; }

        [XmlAttribute("rig")]
        public string Rig { get; private set; }

        public UserPlayer() : base()
        {
        }

        override public string UserId() { return TAG; }
        override public string Tag() { return TAG; }
        
        public void SetLevel(float lvl) { Level = lvl; Dirty(); }
        public void SetRig(string rig) { Rig = rig; Dirty(); }

        override public void Load(UserData useParent)
        {
            base.Load(useParent);

            Player.Man.Sync(this);

            Dirty();
        }
    }
}
