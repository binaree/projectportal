namespace Binaree.Game.Core
{
    using System.Collections;
    using System.Xml;
    using Component;
    using Table;
    using Ui;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using User;
    using Utility;
    using System.Collections.Generic;

    /// <summary>
    /// Functions as the project root
    /// Responsible for loading all game, factory, and user data
    /// Handles the movement between game scenes
    /// </summary>
    public class TheCreator : MonoBehaviour
    {
        public const int MAIN_SCENE_NUM = 1;

        public string UserId = "User"; // the id to use when referencing the user
        public string UserDir = ""; // the directory to output user data to; if empty, defaults to the ApplicationPath/UserId
        public string UserStartDir = ""; // the directory to input user data from; if empty, defaults to the UserDir

        public List<GameFactory> Factory; // Stores a list of factories used to create game objects

        // Xml text assets which store all the data used for the game
        public TextAsset AttractantXml;
        public TextAsset BaitXml;
        public TextAsset BobberXml;
        public TextAsset FishXml;
        public TextAsset HitXml;
        public TextAsset HookXml;
        public TextAsset LineXml;
        public TextAsset MoveXml;
        public TextAsset PortalXml;
        public TextAsset ReelXml;
        public TextAsset RodXml;
        public TextAsset ScriptXml;
        public TextAsset SinkerXml;
        public TextAsset StatXml;
        public TextAsset SurfaceXml;
        public TextAsset TableXml;

        private AsyncOperation asyncLoading = null;

        private static TheCreator _instance = null;

        public static TheCreator Man { get { return _instance; } }

        void Awake()
        {
            _instance = this;
            DontDestroyOnLoad(this);
            Say.Log("The Creator is Awake");

            // Required for loading sub-data
            Fish.Man.Load(AssetToXml(FishXml));
            Surface.Man.Load(AssetToXml(SurfaceXml));
            Table.Man.Load(AssetToXml(TableXml));

            // Standard loading
            Attractant.Man.Load(AssetToXml(AttractantXml));
            Bait.Man.Load(AssetToXml(BaitXml));
            Bobber.Man.Load(AssetToXml(BobberXml));
            Hit.Man.Load(AssetToXml(HitXml));
            Hook.Man.Load(AssetToXml(HookXml));
            Line.Man.Load(AssetToXml(LineXml));
            Move.Man.Load(AssetToXml(MoveXml));
            Portal.Man.Load(AssetToXml(PortalXml));
            Reel.Man.Load(AssetToXml(ReelXml));
            Rod.Man.Load(AssetToXml(RodXml));
            Script.Man.Load(AssetToXml(ScriptXml));
            Sinker.Man.Load(AssetToXml(SinkerXml));
            Stat.Man.Load(AssetToXml(StatXml));
        }

        void Start()
        {
            // Hopefully all required data is loaded before this executes?
            User.Graph.Generate(UserId, UserDir, UserStartDir);
            UserLoaded();
        }

        void Update()
        {
            //if (asyncLoading != null)
            //    LoadingScreen.Inst.Progress(asyncLoading.progress);
        }

        private void UserLoaded()
        {
            Say.Log("User has been loaded, proceeding to generate all Factories");
            for (int i = 0; i < Factory.Count; ++i)
                Factory[i].Generate();

            Say.Log("All loading should now be complete. Launching into game...");
            LaunchMainMenu();
        }

        IEnumerator UpdatedLoad(int lvlNum)
        {
            //LoadingScreen.Inst.ShowLoad(LoadingScreen.Style.Progress);
            asyncLoading = SceneManager.LoadSceneAsync(lvlNum);

            while (!asyncLoading.isDone)
            {
                //LoadingScreen.Inst.Progress(asyncLoading.progress);
                yield return null;
            }

            asyncLoading = null;
        }

        public void LaunchMainMenu()
        {
            Say.Log("Launching into Main Menu. Standby.");
            StartCoroutine(UpdatedLoad(MAIN_SCENE_NUM));
        }

        public static XmlDocument AssetToXml(TextAsset ta)
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(ta.ToString());
            return xml;
        }
    }
}
