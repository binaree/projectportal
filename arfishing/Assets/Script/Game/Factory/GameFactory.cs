namespace Binaree.Game.Core
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Handles and tracks the instantiation of game objects
    /// </summary>
    public class GameFactory : MonoBehaviour
    {
        public bool StoreObjects = false;

        protected Dictionary<string, List<GameObj>> store = new Dictionary<string, List<GameObj>>();
        protected Dictionary<string, List<GameObj>> used = new Dictionary<string, List<GameObj>>();

        protected bool hasGenerated = false;
        
        public GameObj Take(string objType)
        {
            if(!hasGenerated) { Say.Warn(name + " has NOT been generated. Be sure to connect this factory to TheCreator object"); return null; }
            return CreateNew(objType);
        }

        public void Release(GameObj usedObj)
        {
            if(!StoreObjects)
            {
                usedObj.Release(gameObject);
                Destroy(usedObj.gameObject);
                return;
            }
        }

        protected GameObj CreateNew(string objType)
        {
            string prefabName = FormatTypeToPrefab(objType);
            GameObject resObj = Resources.Load(PrefabDirectory() + prefabName, typeof(GameObject)) as GameObject;
            if (resObj == null) { Say.Warn(name + " unable to find resource " + PrefabDirectory() +  prefabName); return null; }
            GameObject newObj = UnityEngine.Object.Instantiate(resObj) as GameObject;
            if (newObj == null) { Say.Warn(name + " unable to instantiate loaded resource " + resObj.name); return null; }            
            GameObj obj = newObj.GetComponent<GameObj>();
            obj.Release(gameObject);
            return obj;
        }

        protected void TrackPrefab(string objType, int preloadCount)
        {
            if (!StoreObjects) return;
            Say.Warn(name + " cannot track: TRACKING IS NOT CURRENTLY SUPPORTED!");
            store.Add(objType, new List<GameObj>());
            used.Add(objType, new List<GameObj>());
        }

        virtual public void Generate() { hasGenerated = true; }
        virtual protected string FormatTypeToPrefab(string objType) { return objType; }
        virtual protected string PrefabDirectory() { return ""; }
    }
}
