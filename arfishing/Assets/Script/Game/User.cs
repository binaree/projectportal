namespace Binaree.Game.User
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Serialization;
    using Core;
    using System.Text;
    using System.Xml;
    using UnityEngine;

    /// <summary>
    /// Tracks all user graphs and provide functionality to save/load game data
    /// </summary>
    public class User : UserGraph
    {
        public const string FILE_FORMAT = ".xml";

        public string Id { get; private set; }
        public string UserDir { get; private set; }
        public string UserStartDir { get; private set; }

        private Dictionary<string, UserGraph> graphs = new Dictionary<string, UserGraph>();

        private static User _instance = new User();

        public static User Graph { get { return _instance; } }

        public User() : base()
        {

        }

        public UserAccount Account() { return graphs[UserAccount.TAG] as UserAccount; }

        public void Generate(string id, string dir, string startDir)
        {
            Id = id;
            if (Application.platform != RuntimePlatform.WindowsEditor || dir == string.Empty)
            {
                Say.Log("Forcing data to persistent data path");
                UserDir = Application.persistentDataPath + "/" + Id + "/";
            }
            else
                UserDir = dir + "/";
            
            UserStartDir = (startDir != String.Empty) ? startDir + "/" : UserDir;

            if (!Directory.Exists(UserDir))
                Directory.CreateDirectory(UserDir);

            graphs.Add(UserAccount.TAG, AddGraph<UserAccount>(UserAccount.TAG));

            SaveDirty();
        }

        public void SaveDirty()
        {
            if (!IsDirty()) return;

            foreach (KeyValuePair<string, UserGraph> u in graphs)
                if (u.Value.IsDirty())
                    SaveGraph(u.Value);

            Clean();
        }

        protected UserGraph AddGraph<T>(string tag) where T : UserGraph
        {
            T g = null;
            if (UserDir == UserStartDir) g = Deserialize<T>(UserStartDir + tag + User.FILE_FORMAT);
            else g = DeserializeTextAsset<T>(UserStartDir + tag);
            g.Load(this);
            return g;
        }

        protected void SaveGraph(UserData d)
        {
            Serialize(d, UserDir + d.Tag() + User.FILE_FORMAT);
            d.Clean();
        }

        public static void Serialize(object item, string path)
        {
            Say.Log("Serializing item to path = " + path);
            XmlSerializer serializer = new XmlSerializer(item.GetType());
            XmlWriterSettings setting = new XmlWriterSettings() { Indent = true, OmitXmlDeclaration = true, Encoding = Encoding.UTF8 };
            XmlWriter writer = XmlWriter.Create(path, setting);
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            serializer.Serialize(writer, item, ns);
            writer.Close();
        }

        public static T Deserialize<T>(string path)
        {
            Say.Log("Deserializing item located at path = " + path);
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StreamReader reader = new StreamReader(path);
            T deserialized = (T)serializer.Deserialize(reader.BaseStream);
            reader.Close();
            return deserialized;
        }

        public static T DeserializeTextAsset<T>(string path)
        {
            Say.Log("Deserializing item located in resource directory at path = " + path);
            TextAsset ta = Resources.Load<TextAsset>(path);
            StringReader reader = new StringReader(ta.text);
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            T deserialized = (T)serializer.Deserialize(reader);
            return deserialized;
        }
    }
}
