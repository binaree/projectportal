namespace Binaree.Game.Core
{
    using UnityEngine;

    /// <summary>
    /// Handles functionality of a game component as it relates to a physical representation in Unity
    /// </summary>
    public class GameObj : MonoBehaviour
    {
        public enum ObjState
        {
            Unused = 0,
            InUse = 1,
        }

        protected ObjState objState = ObjState.Unused;

        virtual public void Use()
        {
            objState = ObjState.InUse;
            gameObject.SetActive(true);
        }

        public void Release(GameObject attachTo)
        {
            objState = ObjState.Unused;
            gameObject.transform.SetParent(attachTo.transform, false);
            gameObject.SetActive(false);
        }        
    }
}
