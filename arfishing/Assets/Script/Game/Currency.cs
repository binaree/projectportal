namespace Binaree.Game.Component
{
    using System;
    using System.Collections.Generic;
    using Core;
    using User;
    using Ui;

    /// <summary>
    /// Tracks all currencies used in the game
    /// </summary>
    public class Currency
    {
        public const int CURRENCY_COUNT = 3;
        public enum Type
        {
            Soft = 0,
            Hard = 1,
            Mark = 2,
        }

        private UserCurrency userData = null;
        private List<int> currency = new List<int>();
        private List<int> battlePool = null;

        private static Currency _instance = new Currency();

        public static Currency Man { get { return _instance; } }

        public Currency()
        {
            for (int i = 0; i < CURRENCY_COUNT; ++i)
                currency.Add(0);
        }

        public int Value(Type currencyType) { return currency[(int)currencyType]; }

        public int Take(Type currencyType, int amount)
        {
            return Mod((int)currencyType, -amount);
        }

        public int Add(Type currencyType, int amount)
        {
            return Mod((int)currencyType, amount);
        }

        public void Sync(UserCurrency theUserData)
        {
            for(int i = 0; i < theUserData.CurrencyValue.Count; ++i)
            {
                currency[i] = theUserData.CurrencyValue[i];
            }
            theUserData.SetCurrencies(currency);
            userData = theUserData;
        }

        private int Mod(int currencyType, int amount)
        {
            currency[currencyType] += amount;
            if(currency[currencyType] < 0)
            {
                Say.Warn("Currency of type " + (Type)currencyType + " was reduced to less than 0");
                currency[currencyType] = 0;
            }
            userData.SetCurrencies(currency);
            //#UiMain.Man.TheHeader.RefreshCurrency();
            //#UiMain.Man.PageSelect.RefreshOnlyActive();
            return currency[currencyType];
        }
    }
}
