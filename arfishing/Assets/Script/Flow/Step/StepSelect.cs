namespace Binaree.Game.Component
{
    using Binaree.Game.Core;
    using System;
    using Ui;

    public class StepSelect : StepBase
    {
        public const string STEP_ID = "select";

        override public void Begin()
        {
            UiMain.Man.Header.SetVisibility(true);
            UiMain.Man.SelectPage(UiSelectPage.PAGE_ID);
        }

        override public void End()
        {

        }

        public void StartCast()
        {
            Step.Man.Begin(StepCast.STEP_ID);
        }
    }
}