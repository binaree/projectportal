namespace Binaree.Game.Component
{
    using Binaree.Game.Core;
    using System;
    using Table;
    using Ui;

    public class StepCatch : StepBase
    {
        public const string STEP_ID = "catch";

        override public void Begin()
        {
            UiMain.Man.SelectPage(UiCatchPage.PAGE_ID);
        }

        override public void End()
        {

        }

        public void ResumeFishing()
        {
            Fish.Man.ClearActive();
            Step.Man.Begin(StepSelect.STEP_ID);
        }
    }
}