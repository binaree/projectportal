namespace Binaree.Game.Component
{
    using Binaree.Game.Core;
    using System;
    using Table;
    using Ui;

    public class StepCast : StepBase
    {
        public const string STEP_ID = "cast";

        override public void Begin()
        {
            UiMain.Man.SelectPage(UiCastPage.PAGE_ID);
        }

        override public void End()
        {

        }

        public void GoCast(float sinkPercent)
        {
            //Say.Log("GO! Depth% is = " + depthPercent);
            Rig.Man.TheBobber.CreateBobber();
            Table.Man.Depth.SetDepth(sinkPercent);
            Step.Man.Begin(StepWait.STEP_ID);
        }
    }
}