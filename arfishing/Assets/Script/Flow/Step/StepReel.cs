namespace Binaree.Game.Component
{
    using Binaree.Game.Core;
    using System;
    using Table;
    using Ui;

    public class StepReel : StepBase
    {
        public enum Lost
        {
            Distance = 0,
            LinePower = 1,
            Tension = 2,
        }

        public const string STEP_ID = "reel";

        override public void Begin()
        {
            Table.Man.Reel.StartReel();
            UiMain.Man.SelectPage(UiReelPage.PAGE_ID);
            StartReel();
        }

        override public void End()
        {

        }

        public void StartReel()
        {
            Table.Man.Tension.StartTension();
        }

        public void Catch()
        {
            Rig.Man.TheBobber.ReleaseBobber();
            Step.Man.Begin(StepCatch.STEP_ID);
        }

        public void LostFish(Lost method)
        {
            UiOkOverlay over = UiMain.Man.SelectOverlay(UiOkOverlay.PAGE_ID) as UiOkOverlay;
            if (method == Lost.Distance)
                over.SetData("lost_fish", "lost_depth", "ok");
            else if (method == Lost.LinePower)
                over.SetData("lost_fish", "lost_power", "ok");
            else if (method == Lost.Tension)
                over.SetData("lost_fish", "lost_tension", "ok");

            Fish.Man.ClearActive();
            Rig.Man.TheBobber.ReleaseBobber();
            Step.Man.Begin(StepSelect.STEP_ID);
        }
    }
}