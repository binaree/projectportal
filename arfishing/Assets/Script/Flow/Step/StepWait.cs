namespace Binaree.Game.Component
{
    using Binaree.Game.Core;
    using System;
    using Table;
    using Ui;

    public class StepWait : StepBase
    {
        public enum Lost
        {
            BadBait = 0,
            BadDepth = 1,
            TookBait = 2,
            FailHit = 3,
        }
        public const string STEP_ID = "wait";

        override public void Begin()
        {
            Table.Man.Wait.StartWait();
            UiMain.Man.SelectPage(UiWaitPage.PAGE_ID);
            RestartWait();
        }

        override public void End()
        {

        }

        public void RestartWait()
        {
            Table.Man.Attract.EstablishDepth(true);
        }

        public void AttemptHit()
        {
            if (Table.Man.Hit.AttemptHit())
                Step.Man.Begin(StepReel.STEP_ID);
        }

        public void LostFish(Lost method)
        {
            UiOkOverlay over = UiMain.Man.SelectOverlay(UiOkOverlay.PAGE_ID) as UiOkOverlay;
            if (method == Lost.BadBait)
                over.SetData("no_fish", "bad_bait", "ok");
            else if (method == Lost.BadDepth)
                over.SetData("no_fish", "bad_depth", "ok");
            else if (method == Lost.TookBait)
                over.SetData("lost_bait", "took_bait", "ok");
            else if (method == Lost.FailHit)
                over.SetData("lost_bait", "fail_hit", "ok");

            ReelIn();
        }

        public void ReelIn()
        {
            Fish.Man.ClearActive();
            Rig.Man.TheBobber.ReleaseBobber();
            Step.Man.Begin(StepSelect.STEP_ID);
        }
    }
}