namespace Binaree.Game.Component
{
    using System;

    public class StepBase
    {
        virtual public void Begin()
        {

        }

        virtual public void End()
        {

        }
    }
}