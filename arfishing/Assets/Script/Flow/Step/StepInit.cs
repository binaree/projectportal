namespace Binaree.Game.Component
{
    using Core;
    using System;
    using Ui;

    public class StepInit : StepBase
    {
        public const string STEP_ID = "init";

        override public void Begin()
        {
            UiMain.Man.SelectPage(UiHomePage.PAGE_ID);
            Say.Log("Have begun the init step");
        }

        override public void End()
        {

        }

        public void StartGenerate()
        {

        }

        public void CancelGenerate()
        {

        }

        public void CaptureSurface()
        {
            Say.Log("Capturing the Portal (would go here)...");
            Portal.Man.OpenPortal();
            Step.Man.Begin(StepSelect.STEP_ID);
        }
    }
}