namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    
    public class CastTable : TableData
    {
        public float MarkerSpeed { get; private set; }

        public CastTable(XmlNode xData) : base(xData)
        {
            MarkerSpeed = Convert.ToSingle(xData.Attributes.GetNamedItem("speed").Value);
        }
        
        public float MarkerMoveSpeed()
        {
            return MarkerSpeed * TimeLord.Inst.ArenaDelta;
        }

        public float MinCastDistance()
        {
            float maxReel = Table.Man.Depth.MaxReelDistance();
            float sinkerScale = Rig.Man.TheSinker.Data.Depth.Min();
            return maxReel * sinkerScale;
        }

        public float MaxCastDistance()
        {
            float maxReel = Table.Man.Depth.MaxReelDistance();
            float sinkerScale = Rig.Man.TheSinker.Data.Depth.Max();
            return maxReel * sinkerScale;
        }

        public float CastDistance(float sinkPercent)
        {
            float maxReel = Table.Man.Depth.MaxReelDistance();
            float sinkerScale = Rig.Man.TheSinker.Data.Depth.Value(sinkPercent);
            return maxReel * sinkerScale;
        }
    }
}