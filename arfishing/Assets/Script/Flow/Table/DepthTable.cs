namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    
    public class DepthTable : TableData
    {
        public float CurrentDepth { get; private set; }

        public float DepthScale { get; private set; }
        public float WarningDepth { get; private set; }

        public float WaitDrawScale { get; private set; }
        public float ReelDrawScale { get; private set; }
        public float ReelDragScale { get; private set; }
        public float FishSpeedScale { get; private set; }

        private float reelDraw = 0.0f;
        private float reelDrag = 0.0f;
        private float maxDistance = 0.0f;

        public DepthTable(XmlNode xData) : base(xData)
        {
            DepthScale = Convert.ToSingle(xData.Attributes.GetNamedItem("scale").Value);
            WarningDepth = Convert.ToSingle(xData.Attributes.GetNamedItem("warn").Value);

            WaitDrawScale = Convert.ToSingle(xData.Attributes.GetNamedItem("wait").Value);
            ReelDrawScale = Convert.ToSingle(xData.Attributes.GetNamedItem("draw").Value);
            ReelDragScale = Convert.ToSingle(xData.Attributes.GetNamedItem("drag").Value);
            FishSpeedScale = Convert.ToSingle(xData.Attributes.GetNamedItem("speed").Value);
        }

        public float MaxReelDistance()
        {
            float reelDepth = Rig.Man.TheReel.Data.LvlMaxDepth(Rig.Man.TheReel.Level);
            float lineSize = Rig.Man.TheLine.Data.Size;
            return  reelDepth * lineSize * DepthScale;
        }

        public float SetDepth(float sinkPercent)
        {
            reelDraw = Rig.Man.TheReel.Data.LvlDraw(Rig.Man.TheReel.Level);
            reelDrag = Rig.Man.TheReel.Data.LvlDrag(Rig.Man.TheReel.Level);
            maxDistance = MaxReelDistance();
            CurrentDepth = Table.Man.Cast.CastDistance(sinkPercent);
            return CurrentDepth;
        }

        public void ApplyWaitPower(float powLvl)
        {
            float draw = reelDraw * powLvl * WaitDrawScale * TimeLord.Inst.ArenaDelta;
            DrawLine(draw);
        }

        public void ApplyReelDraw(float powLvl)
        {
            float draw = reelDraw * powLvl * ReelDrawScale * TimeLord.Inst.ArenaDelta;
            DrawLine(draw);
        }

        public void ApplyReelDrag(float powLvl)
        {
            float drag = reelDrag * powLvl * ReelDragScale * TimeLord.Inst.ArenaDelta;
            DragLine(drag);
        }

        private void DrawLine(float draw)
        {
            //Say.Log("Drawing " + draw);
            CurrentDepth -= draw;
            if (CurrentDepth <= 0.0f)
            {
                if (Fish.Man.Active != null && Fish.Man.Active.HitAi.Status == HitStatus.Hooked)
                {
                    Say.Log("Congratulations! You caught the fish!");
                    Step.Man.Reel.Catch();
                }
                else
                {
                    Step.Man.Wait.ReelIn();
                }
            }
        }

        private void DragLine(float drag)
        {
            drag *= Fish.Man.Active.Stats.Speed * FishSpeedScale;
            CurrentDepth += drag;
            if (CurrentDepth > maxDistance)
                Step.Man.Reel.LostFish(StepReel.Lost.Distance);
        }
    }
}