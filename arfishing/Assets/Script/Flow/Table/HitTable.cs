namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;

    public class HitTable : TableData
    {
        public SplitF MissDelay { get; private set; }
        public List<float> BaseHit { get; private set; }
        public List<float> SetHit { get; private set; }
        public List<float> ControlHit { get; private set; }

        private DepthTable depth = null;
        private FishBlob fish = null;
        private RegionBlob region = null;

        public HitTable(XmlNode xData) : base(xData)
        {
            MissDelay = new SplitF(xData.Attributes.GetNamedItem("delay").Value);

            BaseHit = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("base").Value, BaseHit);

            SetHit = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("set").Value, SetHit);

            ControlHit = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("con").Value, ControlHit);
        }

        public void AssignAttractedFish(FishBlob attract, RegionBlob fishRegion)
        {
            depth = Table.Man.Depth;
            fish = attract;
            region = fishRegion;
        }

        public bool UpdateHit(bool isMoving)
        {
            if (fish == null) return false;
            if (depth.CurrentDepth < region.Data.Depth.Min())
            {
                ReleaseFish();
                return false;
            }

            fish.HitAi.UpdateHit(isMoving);
            return false;
        }

        public bool AttemptHit()
        {
            if (fish == null) return false;
            float baseHit = fish.HitAi.BaseHitChance() * BaseHit[(int)fish.Data.Rarity];
            float set = Rig.Man.TheHook.DetermineSet(fish) * SetHit[(int)fish.Data.Rarity];
            float control = Rig.Man.TheRod.Data.LvlControl(Rig.Man.TheRod.Level) * ControlHit[fish.IndexRank];
            float hitChance = baseHit * set * control;
            Say.Log("Hit chance = " + hitChance);
            
            if( Randomize.Low(hitChance))
            {
                fish.HitAi.SetHook();
                return true;
            }
            else
            {
                if( fish.HitAi.MissHit(MissDelay.Rand()) )
                {
                    Step.Man.Wait.LostFish(StepWait.Lost.FailHit);
                }
            }
            return false;
        }

        private void ReleaseFish()
        {
            fish = null;
            region = null;
            Table.Man.Attract.EstablishDepth();
        }
    }
}