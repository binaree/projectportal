namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;
    using Binaree.Game.Ui;

    public class TensionTable : TableData
    {
        public float Tension { get; private set; }
        public float MaxTension { get; private set; }
        public float TensionLevel { get { return Tension / MaxTension; } }
        public bool IsLimit { get; private set; }
        public int LimitCount { get; private set; }

        public float LimitTension { get; private set; }
        public SplitF RodStrength { get; private set; }
        public float ReelPower { get; private set; }
        public float FishPower { get; private set; }
        public float LineMax { get; private set; }
        public float ReelDrag { get; private set; }
        public float ReelTension { get; private set; }
        public float FishTension { get; private set; }
        public float StaminaRestore { get; private set; }
        public float StaminaConsume { get; private set; }

        public List<float> StandardLoss { get; private set; }
        public List<float> LimitLoss { get; private set; }
        public float LimitScale { get; private set; }

        private DepthTable depth = null;
        private FishBlob fish = null;
        private float reelPow = 0.0f;
        private float fishPow = 0.0f;
        private bool fishMoving = false;
        private float fishMaxPower = 0.0f;
        private float dragTension = 0.0f;

        public TensionTable(XmlNode xData) : base(xData)
        {
            LimitTension = Convert.ToSingle(xData.Attributes.GetNamedItem("limit").Value);
            RodStrength = new SplitF(xData.Attributes.GetNamedItem("str").Value);

            ReelPower = Convert.ToSingle(xData.Attributes.GetNamedItem("reel").Value);
            FishPower = Convert.ToSingle(xData.Attributes.GetNamedItem("fish").Value);
            LineMax = Convert.ToSingle(xData.Attributes.GetNamedItem("lMax").Value);
            ReelDrag = Convert.ToSingle(xData.Attributes.GetNamedItem("rDrag").Value);
            ReelTension = Convert.ToSingle(xData.Attributes.GetNamedItem("rt").Value);
            FishTension = Convert.ToSingle(xData.Attributes.GetNamedItem("ft").Value);
            StaminaConsume = Convert.ToSingle(xData.Attributes.GetNamedItem("con").Value);
            StaminaRestore = Convert.ToSingle(xData.Attributes.GetNamedItem("rest").Value);

            StandardLoss = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("sLoss").Value, StandardLoss);

            LimitLoss = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("lLoss").Value, LimitLoss);
            LimitScale = Convert.ToSingle(xData.Attributes.GetNamedItem("lSc").Value);
        }

        public void StartTension()
        {
            depth = Table.Man.Depth;
            fish = Fish.Man.Active;
            Tension = reelPow = fishPow = 0.0f;
            MaxTension = RodStrength.Value(Rig.Man.TheRod.Data.LvlStrength(Rig.Man.TheRod.Level));
            IsLimit = fishMoving = false;
            LimitCount = 0;
            fishMaxPower = Rig.Man.TheLine.Data.MaxPower * LineMax;
            dragTension = Rig.Man.TheReel.Data.LvlDragTension(Rig.Man.TheReel.Level) * ReelDrag;
        }

        public void ApplyReelPower(float pow, float powLvl)
        {
            reelPow = pow * ReelPower;
        }

        public void ApplyFishMove(FishBlob fish, float pow, bool isMoving)
        {
            fishMoving = isMoving;
            fishPow = pow * fish.Stats.Power * FishPower;
            if (fishPow >= fishMaxPower)
            {                
                Step.Man.Reel.LostFish(StepReel.Lost.LinePower);
            }
        }

        public void UpdateTension()
        {
            if (Fish.Man.Active == null) return;

            float pow = (reelPow - fishPow) * dragTension;
            Tension = (reelPow * ReelTension) + (fishPow * FishTension);
            if(pow > 0.0f)
            {
                depth.ApplyReelDraw(pow);
            }
            else
            {
                //Say.Log("Style is " + fish.MoveAi.Style);
                if (fish.MoveAi.Style != MoveType.Thrashing)
                    depth.ApplyReelDrag(-pow);
                //else
                //    Say.Log("Should not be moving because of thrash");
            }
            DetermineTensionLimit();

            fish.Stats.RestoreStamina(StaminaRestore);
            if (fish.Stats.TakeStamina(Math.Abs(pow) * StaminaConsume))
                fish.MoveAi.ExhaustFish();
            //Say.Log("Fish stamina is " + fish.Stats.Stamina);
        }

        public bool CheckTension(bool isLimitCheck)
        {
            //if (isLimitCheck && !IsLimit) return false;

            float baseLose = fish.MoveAi.BaseLoss;
            baseLose *= (isLimitCheck) ? LimitLoss[(int)fish.IndexRank] : StandardLoss[(int)fish.IndexRank];
            if (isLimitCheck)
                baseLose += LimitScale * LimitCount++;
            float hook = Table.Man.Hook.TensionChance(fish, isLimitCheck);
            //float control = Rig.Man.TheRod.Data.LvlControl(Rig.Man.TheRod.Level) * ControlHit[fish.IndexRank];
            float loseChance = baseLose * hook;
            //Say.Log("Lose chance = " + loseChance + " with limit check: " + isLimitCheck + ", " + baseLose + ", " + hook);

            if (Randomize.Low(loseChance))
            {
                Step.Man.Reel.LostFish(StepReel.Lost.Tension);
                return true;
            }
            return false;
        }

        private bool DetermineTensionLimit()
        {
            if (TensionLevel >= LimitTension)
            {
                if(IsLimit == false)
                {
                    IsLimit = true;
                    LimitCount = 0;
                }
            }
            else
            {
                if(IsLimit == true)
                {
                    IsLimit = false;
                }
            }
            return IsLimit;
        }
    }
}