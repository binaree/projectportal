namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;

    public class AttractTable : TableData
    {
        public List<float> BaseOccurence { get; private set; }
        public List<float> BaseAttract { get; private set; }
        public List<float> BaseRankRate { get; private set; }
        public List<float> BaseSizeRate { get; private set; }

        private List<RegionBlob> region = null;

        public AttractTable(XmlNode xData) : base(xData)
        {
            BaseOccurence = new List<float>();
            Stringy.ParseList<float>(xData.Attributes.GetNamedItem("occ").Value, BaseOccurence);

            BaseAttract = new List<float>();
            Stringy.ParseList<float>(xData.Attributes.GetNamedItem("att").Value, BaseAttract);

            BaseRankRate = new List<float>();
            Stringy.ParseList<float>(xData.Attributes.GetNamedItem("rank").Value, BaseRankRate);

            BaseSizeRate = new List<float>();
            Stringy.ParseList<float>(xData.Attributes.GetNamedItem("size").Value, BaseSizeRate);
        }

        public void EstablishDepth(bool init = false)
        {
            float depth = Table.Man.Depth.CurrentDepth;
            region = new List<RegionBlob>();
            List<RegionBlob> all = Portal.Man.Active.Region;
            for(int i = 0; i < all.Count; ++i)
            {
                if(all[i].IsValid(depth))
                {
                    all[i].Activate(init);
                    region.Add(all[i]);
                }
            }
        }

        public void MoveDepth()
        {
            float depth = Table.Man.Depth.CurrentDepth;
            for(int i = region.Count - 1; i >= 0; --i)
            {
                if(!region[i].IsValid(depth))
                {
                    region[i].Close();
                    region.RemoveAt(i);
                }
            }
        }

        public bool UpdateAttract()
        {
            if (Fish.Man.Active != null) return false;

            for (int i = region.Count - 1; i >= 0; --i)
            {
                if (region[i].UpdateRegion())
                {
                    Say.Log("Checking for fish in region = " + region[i].Data.Id);
                    FishBlob fish = AttractFish(region[i]);
                    if (fish != null)
                    {
                        Table.Man.Hit.AssignAttractedFish(fish, region[i]);
                        region = null;
                        return true;
                    }
                }
            }
            return false;
        }

        private FishBlob AttractFish(RegionBlob reg)
        {
            List<RegionFish> present = FindOccuringFish(reg);
            if (present.Count < 1) return null;
            //Say.Log("Number of occuring fish is " + present.Count);

            BaitFish attract = AttractedFish(reg, present);
            if (attract == null) return null;
            //Say.Log("Bait has attrated fish " + attract.RefId);

            return CreateAttractedFish(attract);
        }

        private List<RegionFish> FindOccuringFish(RegionBlob reg)
        {
            List<RegionFish> occ = new List<RegionFish>();
            List<RegionFish> all = reg.Data.Fish;
            for(int i = 0; i < all.Count; ++i)
            {
                FishData d = Fish.Man.DataF(all[i].RefId);
                float check = all[i].Occurence * reg.Data.BaseOccurence * BaseOccurence[(int)d.Rarity];
                if (Randomize.Low(check))
                    occ.Add(all[i]);
            }
            return occ;
        }

        private BaitFish AttractedFish(RegionBlob reg, List<RegionFish> occ)
        {
            List<BaitFish> bf = Rig.Man.TheBait.Data.Fish;
            float lineDetect = Rig.Man.TheLine.Data.Detection;
            for( int i = occ.Count - 1; i >= 0; --i)
            {
                for( int j = bf.Count - 1; j >= 0; --j)
                {
                    if (occ[i].RefId != bf[j].RefId) continue;
                    FishData d = Fish.Man.DataF(occ[i].RefId);
                    float att = bf[j].Attraction * lineDetect * reg.Data.BaseAttract * BaseAttract[(int)d.Rarity];
                    if (Randomize.Low(att))
                    {
                        return bf[j];
                    }
                }
            }
            return null;
        }

        private FishBlob CreateAttractedFish(BaitFish bf)
        {
            FishData d = Fish.Man.DataF(bf.RefId);
            FishBlob b = Fish.Man.GenerateFish(d);
            int rank = PickRank(d, bf);
            float size = PickSize(d, bf, rank);
            b.Activate(rank, size);
            Say.Log("You attracted a " + d.Id + " of rank = " + rank + " & size = " + size);
            return b;
        }

        private int PickRank(FishData d, BaitFish bf)
        {
            float checkMod = bf.RankMod * BaseRankRate[(int)d.Rarity];
            checkMod *= Rig.Man.TheBobber.Data.LvlRankMod(Rig.Man.TheBobber.Level);

            for (int i = d.RankRate.Count -1; i >= 0; --i)
            {
                if (Randomize.Low(d.RankRate[i] * checkMod))
                    return i + 1;
            }

            return 1;
        }

        private float PickSize(FishData d, BaitFish bf, int rank)
        {
            float sizeMod = bf.SizeMod * BaseSizeRate[(int)d.Rarity];
            sizeMod *= Rig.Man.TheBobber.Data.LvlSizeMod(Rig.Man.TheBobber.Level);

            float sizeMin = d.SizeChart[rank - 1];
            float sizeMax = d.SizeChart[rank];
            float size = Randomize.Range(sizeMin, sizeMax) * sizeMod;
            if (size < sizeMin) size = sizeMin;
            if (size > sizeMax) size = sizeMax;
            return size;
        }
    }
}