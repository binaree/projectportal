namespace Binaree.Game.Table
{
    using System.Xml;
    using Core;
    using Utility;
    using System;
    using System.Collections.Generic;
    using Component;

    public class HookTable : TableData
    {
        public List<float> SizeChart { get; private set; }
        public float DecayBase { get; private set; }
        public float TensionBase { get; private set; }
        public float LimitBase { get; private set; }

        public HookTable(XmlNode xData) : base(xData)
        {
            SizeChart = new List<float>();
            Stringy.ParseList(xData.Attributes.GetNamedItem("size").Value, SizeChart);
            DecayBase = Convert.ToSingle(xData.Attributes.GetNamedItem("decay").Value);
            TensionBase = Convert.ToSingle(xData.Attributes.GetNamedItem("ten").Value);
            LimitBase = Convert.ToSingle(xData.Attributes.GetNamedItem("lim").Value);
        }

        public HookSize HookSizeByFish(FishBlob f)
        {
            for(int i = 0; i < SizeChart.Count; ++i)
            {
                if (f.Size < SizeChart[i])
                    return (HookSize)i;
            }
            return HookSize.Colossus;
        }

        public float SetChance(FishBlob f)
        {
            HookData hook = Rig.Man.TheHook.Data;
            float set = hook.SetRarity(f.Data.Rarity);
            if (f.SizeType == hook.Size)
                return set;
            int dist = Math.Abs((int)f.SizeType - (int)hook.Size);
            set -= hook.SizeDecay * DecayBase * dist;
            if (set < 0.0f)
                set = 0.0f;
            return set;
        }

        public float TensionChance(FishBlob f, bool isLimit)
        {
            HookData hook = Rig.Man.TheHook.Data;
            float set = hook.SetRarity(f.Data.Rarity) * ((isLimit) ? LimitBase : TensionBase);
            if (f.SizeType == hook.Size)
                return set;
            int dist = Math.Abs((int)f.SizeType - (int)hook.Size);
            set -= hook.SizeDecay * DecayBase * dist;
            if (set < 0.0f)
                set = 0.0f;
            return set;
        }
    }
}