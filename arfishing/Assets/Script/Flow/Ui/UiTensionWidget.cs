namespace Binaree.Game.Ui
{
    using Component;
    using Core;
    using Object;
    using System.Collections.Generic;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;
    
    public class UiTensionWidget : UiObj
    {
        public Slider Meter;
        public GameObject Warning;
        public List<GameObject> FishMove;
        public GameObject MoveWarning;
        public Text MaxLabel;

        private FishBlob fish = null;
        private TensionTable tension = null;
        private MoveType curMove = MoveType.Warning;

        private void Awake()
        {
            tension = Table.Man.Tension;            
        }

        private void OnEnable()
        {
            fish = Fish.Man.Active;
            Warning.SetActive(false);
            Meter.value = 0.0f;

            SetFishMove();
        }

        private void Update()
        {
            //Say.Log("Tension le")
            //Meter.value += (tension.TensionLevel - Meter.value) * TimeLord.Inst.ArenaDelta * 5.0f;
            Meter.value = tension.TensionLevel;
            //Say.Log("Current Tension = " + tension.Tension + ", Max Tension = " + tension.MaxTension + ", Tension Level = " + tension.TensionLevel);
            Warning.SetActive(tension.IsLimit);
            SetFishMove();
        }

        private void SetFishMove()
        {
            if (curMove == fish.MoveAi.Style) return;
            for (int i = 0; i < FishMove.Count; ++i)
                FishMove[i].SetActive(false);
            curMove = fish.MoveAi.Style;
            FishMove[(int)curMove].SetActive(true);
            MoveWarning.SetActive((curMove == MoveType.Warning || curMove == MoveType.Running || curMove == MoveType.Thrashing));
        }
    }
}
