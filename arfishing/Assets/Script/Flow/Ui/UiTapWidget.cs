namespace Binaree.Game.Ui
{
    using Component;
    using Core;
    using Object;
    using Table;
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;
    
    public class UiTapWidget : UiObj
    {
        public enum Style
        {
            Wait = 0,
            Reel = 1,
        }

        public Style ReelMode = Style.Reel;

        private WaitTable wait = null;
        private ReelTable reel = null;

        private void Awake()
        {
            wait = Table.Man.Wait;
            reel = Table.Man.Reel;
        }

        private void Update()
        {
            if (ReelMode == Style.Wait)
                wait.DecayInput();
            else if (ReelMode == Style.Reel)
                reel.DecayInput();
        }

        public void ScreenTap()
        {
            if (ReelMode == Style.Wait)
                wait.ApplyInput();
            else if (ReelMode == Style.Reel)
                reel.ApplyInput();
        }
    }
}
