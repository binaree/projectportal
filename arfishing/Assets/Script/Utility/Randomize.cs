﻿namespace Binaree.Game.Utility
{

    /// <summary>
    /// Wraps Unity random to allow easy modification in the future
    /// </summary>
    public class Randomize
    {
        public static float Value()
        {
            return UnityEngine.Random.value;
        }

        public static int Index(int count)
        {
            return (int)(Value() * count);
        }

        public static bool High(float val)
        {
            return Randomize.Value() >= val;
        }

        public static bool Low(float val)
        {
            return Randomize.Value() <= val;
        }

        public static float Range(float min, float max)
        {
            return min + ((max - min) * Value());
        }
    }
}
