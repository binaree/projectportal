﻿namespace Binaree.Game.Utility
{
    using System.Collections.Generic;
    using System;
    using UnityEngine;

    /// <summary>
    /// Handles basic string formatting functionality
    /// Used for commonly used string formats such as comma separated lists
    /// </summary>
    public class Stringy
    {
        public static string FormatList<T>(List<T> list, string separator = ",")
        {
            string s = "";
            foreach (T val in list)
                s += val.ToString() + separator;
            s = s.Remove(s.Length - 1);
            return s;
        }

        public static void ParseList<T>(string val, List<T> list, char separator = ',')
        {
            string[] split = val.Split(separator);
            foreach (string s in split)
                list.Add( (T)Convert.ChangeType(s, typeof(T)) );
        }

        public static void ParseEmpty<T>(string val, List<T> list, char separator = ',')
        {
            string[] split = val.Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in split)
                list.Add((T)Convert.ChangeType(s, typeof(T)));
        }

        public static void ParseSplitFList(string val, List<SplitF> list, char separator = ',')
        {
            string[] split = val.Split(separator);
            foreach (string s in split)
                list.Add(new SplitF(s));
        }

        public static Vector3 ParseVector3(string val, char separator = ',')
        {
            string[] split = val.Split(separator);
            Vector3 v = Vector3.zero;
            if (split.Length > 0) v.x = Convert.ToSingle(split[0]);
            if (split.Length > 1) v.y = Convert.ToSingle(split[1]);
            if (split.Length > 2) v.z = Convert.ToSingle(split[2]);
            return v;
        }
    }
}
