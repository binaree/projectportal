namespace Binaree.Game.Core
{
    using UnityEngine;

    /// <summary>
    /// Wrapper around Unity Debug logging to give us more control of messaging if needed
    /// </summary>
    public class Say
    {
        public static void Log(string msg)
        {
            Debug.Log(msg);
        }

        public static void Warn(string msg)
        {
            Debug.LogWarning(msg);
        }

        public static void Error(string msg)
        {
            Debug.LogError(msg);
        }
    }
}