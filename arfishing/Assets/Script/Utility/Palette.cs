﻿namespace Binaree.Game.Utility
{
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;

    public class Palette : MonoBehaviour
    {
        //public List<Color> CharacterOverlay;

        // ---------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Stuff to handle making the manager singleton-esque
        /// </summary>

        // static instance of the manager class for referencing
        private static Palette _instance = null;

        // Constructor to instantiate the instance for code reference
        public Palette()
        {
            _instance = this;
        }

        // Manager access within the script
        public static Palette Man
        {
            get
            {
                if (_instance == null)
                    Debug.LogError("The Palette has not yet been instantiated, please do not try to access the manager during before it is Awake");
                return _instance;
            }
        }

        private void OnDestroy()
        {
            _instance = null;
        }
        // ---------------------------------------------------------------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------------------------------------------------------------
    }
}
