namespace Binaree.Game.Factory
{
    using Component;
    using Core;
    using Base;
    using System.Collections.Generic;
    using Object;
    
    public class BobberFactory : GameFactory
    {
        public string BobberDirectory = "bobber";
        public int PreloadAmount = 1;

        private static BobberFactory _instance = null;

        public static BobberFactory Inst { get { return _instance; } }

        override public void Generate()
        {
            _instance = this;
            List<GameData> allBobber = Bobber.Man.AllGameData();
            for(int i = 0; i < allBobber.Count; ++i)
            {
                if (store.ContainsKey(allBobber[i].Id)) continue;
                TrackPrefab(allBobber[i].Id, PreloadAmount);
            }
            hasGenerated = true;
        }

        override protected string FormatTypeToPrefab(string objType) { return objType; }
        override protected string PrefabDirectory() { return BobberDirectory + "/"; }

        public BobberObj TakeBobber(string objType) { return Take(objType) as BobberObj; }
    }
}
