﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CreatureStatus
{
    Attack = 0,
    Defend = 1,
    Charge = 2,
    Tired = 3,
    Warn = 4,
}

public enum CreatureAbility
{
    Restore = 0,
    AttackUp = 1,
    Surge = 2,
    Energize = 3,
}

public class Mode1Creature : MonoBehaviour
{
    private static readonly string[] ABILITY_NAME = { "Restore", "Attack Up!", "Surge!", "Energized" };

    public float MaxEnergy = 100.0f;
    public float MaxStamina = 100.0f;

    public float AttackPower = 1.0f;
    public float MaxAttack = 10.0f;
    public float MaxAttackDecay = 0.5f;
    public float AttackStaminaCost = 1.0f;

    public float DefensePower = 1.0f;
    public float DefenseStaminaDrain = 0.5f;
    
    public float StandardEnergyRecover = 0.0f;
    public float StandardStaminaRecover = 1.0f;
    public float StandardAbilityTime = 1.0f;
    public float ChargeEnergyRecover = 0.2f;
    public float ChargeStaminaRecover = 3.0f;
    public float ChargeAbilityTime = 2.0f;
    public float TiredEnergyRecover = 0.0f;
    public float TiredStaminaRecover = 1.0f;
    public float TiredAbilityTime = 0.5f;
    public float TiredRecoverery = 1.0f;
    public float SurpriseStamina = 0.0f;

    public CreatureAbility Ability = CreatureAbility.Surge;
    public float AbilityChargeTime = 60.0f;
    public float AbilityValue = 0.0f;
    public float AbilityStaminaCost = 0.0f;
    public float AbilityDuration = 0.0f;

    public CreatureStatus Status { get; protected set; }
    public Mode1Creature Opponent { get; protected set; }
    
    public float Energy { get; protected set; }
    public float EnergyLevel { get { return Energy / MaxEnergy; } }
    public float Stamina { get; private set; }
    public float StaminaLevel { get { return Stamina / MaxStamina; } }

    public float Attack { get; protected set; }
    public float AttackLevel { get { return Attack / MaxAttack; } }

    public string AbilityName { get { return ABILITY_NAME[(int)Ability]; } }
    public float AbilityTime { get; protected set; }
    public float PerformTime { get; protected set; }
    public float AbilityTimeLevel { get { return AbilityTime / AbilityChargeTime; } }
    public float PerformTimeLevel { get { return PerformTime / AbilityDuration; } }
    public bool CanUseAbility { get { return AbilityTime >= AbilityChargeTime; } }
    public bool IsPerforming { get { return PerformTime > 0.0f; } }

    protected bool inUse = false;
    protected float attackMod = 1.0f;
    protected float staminaMod = 1.0f;

    private void Awake()
    {
        Initialize();
    }

    private void Update()
    {
        if (Mode1Game.Inst.GameOver) return;
        Upkeep();
    }

    virtual protected float AppScale() { return 1.0f; }

    virtual protected void Initialize()
    {
        Energy = MaxEnergy;
        Stamina = Attack = AbilityTime = PerformTime = 0.0f;
        attackMod = staminaMod = 1.0f;
    }

    virtual protected bool Upkeep()
    {
        if (!Mode1Game.Inst.BattleStarted) return false;
        if (!inUse) return false;
        UpdateEnergy();
        UpdateStamina();
        UpdateAttack();
        UpdateAbility();
        return true;
    }

    virtual public void StopUse()
    {
        inUse = false;
        Initialize();
    }

    virtual public void StartBattle(bool surprised, Mode1Creature against)
    {
        Status = CreatureStatus.Charge;
        Opponent = against;
        Energy = MaxEnergy;
        Attack = AbilityTime = Stamina = PerformTime = 0.0f;
        attackMod = staminaMod = 1.0f;
        AdjustStamina((surprised) ? (MaxStamina * SurpriseStamina) : MaxStamina);
    }

    virtual protected void UpdateEnergy()
    {
        if (Energy >= MaxEnergy) return;
        float upAmt = StandardEnergyRecover;
        if (Status == CreatureStatus.Charge) upAmt = ChargeEnergyRecover;
        else if (Status == CreatureStatus.Tired) upAmt = TiredEnergyRecover;
        AdjustEnergy( UnityEngine.Time.deltaTime * upAmt );
    }

    virtual protected void UpdateStamina()
    {
        float upAmt = StandardStaminaRecover;
        if (Status == CreatureStatus.Charge) upAmt = ChargeStaminaRecover;
        else if (Status == CreatureStatus.Tired) upAmt = TiredStaminaRecover;
        else if (Status == CreatureStatus.Defend) upAmt = -(DefenseStaminaDrain * staminaMod);
        AdjustStamina( UnityEngine.Time.deltaTime * upAmt );        
    }

    virtual protected void UpdateAttack()
    {
        if (Attack <= 0.0f) return;
        AdjustAttack(-UnityEngine.Time.deltaTime * MaxAttackDecay);
    }

    virtual protected void UpdateAbility()
    {
        if (PerformTime > 0.0f)
        {
            PerformTime -= UnityEngine.Time.deltaTime;
            if (PerformTime <= 0.0f)
                AbilityPerformComplete();
            return;
        }

        if (AbilityTime >= AbilityChargeTime) return;
        float upAmt = StandardAbilityTime;
        if (Status == CreatureStatus.Charge) upAmt = ChargeAbilityTime;
        else if (Status == CreatureStatus.Tired) upAmt = TiredAbilityTime;
        AbilityTime += UnityEngine.Time.deltaTime * upAmt;
        if (AbilityTime >= AbilityChargeTime)
            AbilityReady();
    }

    virtual protected void AdjustEnergy(float amount)
    {
        Energy += amount;
        if (Energy > MaxEnergy) Energy = MaxEnergy;
        else if (Energy <= 0.0f)
        {
            Mode1Game.Inst.FinishBattle(this is Mode1Pet);
            Energy = 0.0f;
        }
    }

    virtual protected void AdjustStamina(float amount)
    {
        Stamina += amount;
        if (Stamina > MaxStamina) Stamina = MaxStamina;
        CheckForTired();
    }

    virtual protected float AdjustAttack(float amount)
    {
        Attack += amount * AppScale();
        if (Attack < 0.0f) Attack = 0.0f;
        if (Attack > (MaxAttack * attackMod))
        {
            float dif = Attack - (MaxAttack * attackMod);
            Attack = (MaxAttack * attackMod);
            return amount - dif;
        }
        return amount;
    }

    virtual protected void CheckForTired()
    {
        if(Status == CreatureStatus.Tired)
        {
            if (Stamina >= (TiredRecoverery * MaxStamina))
                RecoverFromTired();
        }

        if (Stamina > 0.0f) return;
        Stamina = 0.0f;
        Status = CreatureStatus.Tired;
    }

    virtual protected void RecoverFromTired()
    {
        Status = CreatureStatus.Charge;
    }

    virtual protected void PerformAttack(float usePower, float staminaCost)
    {
        if (Status == CreatureStatus.Tired) return;
        Status = CreatureStatus.Attack;

        staminaCost *= AppScale();
        staminaCost *= staminaMod;
        if(Stamina < staminaCost)
        {
            AdjustStamina(-staminaCost);
            return;
        }

        AdjustStamina(-staminaCost);
        usePower *= attackMod;
        usePower = AdjustAttack(usePower);
        if(usePower > 0.0f)
            Opponent.DoDamage(usePower, AppScale());
    }

    virtual public void DoDamage(float attackPower, float scaleFinal)
    {
        if(Status == CreatureStatus.Defend)
        {
            attackPower -= DefensePower;
            if (attackPower <= 0.0f) return;
        }
        attackPower *= scaleFinal;
        AdjustEnergy(-attackPower);
    }

    virtual protected void StartCharge()
    {
        Status = CreatureStatus.Charge;
    }

    virtual protected void StartDefense()
    {
        Status = CreatureStatus.Defend;
    }

    virtual protected void AbilityReady()
    {

    }

    virtual protected void PerformAbility()
    {
        if (Stamina < AbilityStaminaCost)
        {
            AdjustStamina(-AbilityStaminaCost);
            return;
        }

        if(Ability == CreatureAbility.Restore)
        {
            AdjustEnergy(AbilityValue);
            AbilityPerformComplete();
        }
        else if (Ability == CreatureAbility.AttackUp)
        {
            PerformTime = AbilityDuration;
            attackMod = AbilityValue;
        }
        else if (Ability == CreatureAbility.Energize)
        {
            PerformTime = AbilityDuration;
            staminaMod = AbilityValue;
        }
        else if (Ability == CreatureAbility.Surge)
        {
            PerformAttack(Stamina * AbilityValue, 0);
            AbilityPerformComplete();
        }

        AdjustStamina(-AbilityStaminaCost);
    }

    virtual protected void AbilityPerformComplete()
    {
        PerformTime = 0.0f;
        AbilityTime = 0.0f;
    }
}
