﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mode1Energy : MonoBehaviour
{
    public Slider Meter;
    public GameObject Warn;
    public float WarnAmount = 0.2f;

    public PortalWidget portal;

    private Mode1Creature creature = null;

	// Use this for initialization
	void Awake ()
    {
        Meter.value = 0.0f;
        Warn.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (creature == null) return;
        Warn.SetActive(creature.EnergyLevel < WarnAmount);
        Meter.value = creature.EnergyLevel;
	}

    public void AssignCreature(Mode1Creature cr)
    {
        gameObject.SetActive(true);
        creature = cr;
        Warn.SetActive(creature.EnergyLevel < WarnAmount);
        Meter.value = creature.EnergyLevel;
    }
}
