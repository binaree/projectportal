﻿using Binaree.Game.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mode1CCharge : Mode1CPattern
{
    public string ChargeTime = "3:5";
    public string StaminaCutoff = "1.0";
    public string EnergyCutoff = "1.0";
    public string EnergyLimit = "0.25";

    private SplitF chargeTime;
    private SplitF staminaCutoff;
    private SplitF energyCutoff;
    private SplitF energyLimit;

    private float patternTime = 0.0f;
    private float stCutoff = 0.0f;
    private float enCutoff = 0.0f;
    private float enLimit = 0.0f;

    private void Awake()
    {
        chargeTime = new SplitF(ChargeTime);
        staminaCutoff = new SplitF(StaminaCutoff);
        energyCutoff = new SplitF(EnergyCutoff);
        energyLimit = new SplitF(EnergyLimit);
    }

    override public bool StartPattern(Mode1Catch cat)
    {
        base.StartPattern(cat);
        patternTime = chargeTime.Rand();
        stCutoff = staminaCutoff.Rand() * creature.MaxStamina;
        enCutoff = energyCutoff.Rand() * creature.MaxEnergy;
        enLimit = energyLimit.Rand() * creature.MaxEnergy;
        //Debug.Log("Start charge for " + patternTime + " st: " + stCutoff + ", en: " + enCutoff + ", lim: " + enLimit);
        creature.SetStatus(CreatureStatus.Charge);
        return true;
    }

    override public bool UpdatePattern()
    {
        if (creature == null || creature.Status == CreatureStatus.Tired) return false;
        patternTime -= UnityEngine.Time.deltaTime;
        if (patternTime <= 0.0f) return true;
        if (creature.Stamina >= stCutoff) return true;
        if (creature.Energy >= enCutoff) return true;
        if (creature.Energy <= enLimit) return true;
        return false;
    }

    override public CreatureStatus PatternStatus() { return CreatureStatus.Charge; }
}
