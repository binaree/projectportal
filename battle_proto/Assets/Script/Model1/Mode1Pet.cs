﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Mode1Pet : Mode1Creature
{
    public float PingAmp = 1.0f;
    public float PingDuration = 1.0f;
    public float MissRecover = 0.5f;

    public float Perception = 1.0f;

    public float TapStaminaRecover = 0.01f;
    public float AttackCooldown = 0.5f;

    private int petIndex = 0;

    private bool isCooldown = false;
    private float cooldownTime = 0.0f;

    override protected float AppScale() { return Mode1Game.Inst.AppScale; }
    public int PetIndex() { return petIndex; }

    override protected bool Upkeep()
    {
        if ( !base.Upkeep() ) return false;
        
        if(isCooldown)
        {
            cooldownTime += UnityEngine.Time.deltaTime;
            if(cooldownTime >= AttackCooldown)
            {
                isCooldown = false;
                cooldownTime = 0.0f;
                ProcessNoAction();
            }
        }
        return true;
    }

    public void Selected(int index)
    {
        petIndex = index;
        inUse = true;
    }

    public void Deploy(PortalWidget portal)
    {
        Mode1Game.Inst.StartDeploy();
    }

    override public void StartBattle(bool surprised, Mode1Creature against)
    {
        base.StartBattle(surprised, against);
    }

    override protected void AbilityReady()
    {
        base.AbilityReady();
    }

    public void ProcessAttack()
    {
        if (Status == CreatureStatus.Tired)
        {
            AdjustStamina(TapStaminaRecover * AppScale());
            return;
        }
        isCooldown = true;
        cooldownTime = 0.0f;
        PerformAttack(AttackPower, AttackStaminaCost);
    }

    public void ProcessDefend()
    {
        if (Status == CreatureStatus.Defend) return;
        if (Status == CreatureStatus.Tired) return;
        StartDefense();
        isCooldown = false;
    }

    public void ProcessNoAction()
    {
        if (Status == CreatureStatus.Tired) return;
        StartCharge();
        isCooldown = false;
    }

    public bool ProcessAbility()
    {
        if (Status == CreatureStatus.Tired) return false;
        if (!CanUseAbility) return false;
        if (Stamina < AbilityStaminaCost) return false;
        PerformAbility();
        return true;
    }

    override protected void PerformAbility()
    {
        base.PerformAbility();
        Mode1Game.Inst.PetWidget.AbilityUsed(AbilityName);
    }
}
