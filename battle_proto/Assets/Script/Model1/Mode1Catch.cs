﻿using Binaree.Game.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mode1Catch : Mode1Creature
{
    public enum State
    {
        None = 0,
        Wait = 1,
        Hit = 2,
        Attack = 3,
        Engage = 4,
    }

    public string NumHits = "1:3";
    public string HitChance = "0.25:0.75";
    public string HitTime = "0.5:1.5";
    public string WaitTime = "0.25:0.75";
    public string AttackTime = "0.5:1.5";

    public List<Mode1CPattern> Pattern;
    public string WarnTime = "0.25:0.5";
    public float AbilityPerformStamina = 0.6f;

    public bool Unknown { get; private set; }
    public Mode1CPattern Active { get { return Pattern[patternIndex]; } }

    private int catchIndex = 0;

    private SplitF numHits;
    private SplitF hitChance;
    private SplitF hitTime;
    private SplitF waitTime;
    private SplitF attackTime;
    private SplitF warnTime;

    private Mode1Pet pet;
    private State state = State.None;
    private int hits = 0;
    private float chance = 0.0f;
    private float time = 0.0f;

    private int patternIndex = 0;
    private float warn = 0.0f;

    override protected void Initialize()
    {
        base.Initialize();
        numHits = new SplitF(NumHits);
        hitChance = new SplitF(HitChance);
        hitTime = new SplitF(HitTime);
        waitTime = new SplitF(WaitTime);
        attackTime = new SplitF(AttackTime);
        warnTime = new SplitF(WarnTime);
        Unknown = true;
	}

    override protected bool Upkeep()
    {
        if (!inUse) return false;

		if (time > 0.0f)
        {
            time -= UnityEngine.Time.deltaTime;
            if(time <= 0.0f)
            {
                if (state == State.Wait) DetermineHitAction();
                else if (state == State.Hit) SetHitWait();
                else if (state == State.Attack) Mode1Game.Inst.StartBattle(false, true);
            }
        }

        if (!base.Upkeep()) return false;
        if (ProcessPattern())
        {
            if (Active.UpdatePattern())
                AdvancePattern();
        }
        else if (warn > 0.0f)
        {
            warn -= UnityEngine.Time.deltaTime;
            if(warn <= 0.0f)
            {
                warn = 0.0f;
                PerformAbility();
            }
        }
        return true;
	}

    public bool IsEngaged() { return state == State.Engage; }
    public void Caught() { Unknown = false; }

    public void Selected(int index)
    {
        catchIndex = index;
        pet = Mode1Game.Inst.Pet;
        hits = (int)numHits.Rand();
        if (hits > 0)
            SetHitWait();
        else
            DetermineHitAction();
        inUse = true;
    }

    public void ProcessHit()
    {
        bool success = false;
        if (state == State.Attack) success = true;
        else if (state == State.Hit) success = UnityEngine.Random.value <= chance;

        if(success)
        {
            Mode1Game.Inst.StartBattle(state == State.Hit, false);
        }
        else
        {
            if(UnityEngine.Random.value > pet.MissRecover)
                Mode1Game.Inst.StartBattle(false, true);
        }
    }

    override public void StartBattle(bool surprised, Mode1Creature against)
    {
        state = State.Engage;
        base.StartBattle(surprised, against);
        patternIndex = -1;
        if(!surprised)
            AdvancePattern();
    }

    override protected void RecoverFromTired()
    {
        if (patternIndex == -1)
            AdvancePattern();
        else
            Status = Active.PatternStatus();
    }

    private void SetHitWait()
    {
        time = waitTime.Rand();
        chance = 0.0f;
        state = State.Wait;
        Mode1Game.Inst.PetWidget.Ping.ShowPing(chance);
    }

    private void DetermineHitAction()
    {
        if(hits-- <= 0)
        {
            time = attackTime.Rand() * pet.PingDuration;
            chance = 1.0f;
            state = State.Attack;
        }
        else
        {
            time = hitTime.Rand() * pet.PingDuration;
            chance = hitChance.Rand() * pet.PingAmp;
            state = State.Hit;
        }
        Mode1Game.Inst.PetWidget.Ping.ShowPing(chance);
    }

    private void AdvancePattern()
    {
        if (CanUseAbility && !IsPerforming && StaminaLevel >= AbilityPerformStamina)
        {
            warn = warnTime.Rand() * (Opponent as Mode1Pet).Perception;
            SetStatus(CreatureStatus.Warn);
            return;
        }

        if (++patternIndex >= Pattern.Count)
            patternIndex = 0;
        
        if (!Active.StartPattern(this))
            AdvancePattern();
    }

    public void SetStatus(CreatureStatus s)
    {
        Status = s;
    }

    public void PerformAttack()
    {
        SetStatus(CreatureStatus.Attack);
        PerformAttack(AttackPower, AttackStaminaCost);
    }

    public bool ProcessPattern()
    {
        if (Status == CreatureStatus.Tired || Status == CreatureStatus.Warn) return false;
        return true;
    }

    override protected void PerformAbility()
    {
        base.PerformAbility();
        Mode1Game.Inst.CatchWidget.AbilityUsed(AbilityName);
        if (Status != CreatureStatus.Tired)
            AdvancePattern();
    }
}
