﻿using Binaree.Game.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mode1CAttack : Mode1CPattern
{
    public string AttackTime = "0.1:0.2";
    public string MaxAttack = "10:15";
    public string StaminaLimit = "0.25";

    private SplitF attackTime;
    private SplitF maxAttack;
    private SplitF staminaLimit;

    private float patternTime = 0.0f;
    private float attackCount = 0;
    private float limit = 0.0f;

    private void Awake()
    {
        attackTime = new SplitF(AttackTime);
        maxAttack = new SplitF(MaxAttack);
        staminaLimit = new SplitF(StaminaLimit);
    }

    override public bool StartPattern(Mode1Catch cat)
    {
        base.StartPattern(cat);
        attackCount = (int)maxAttack.Rand();
        patternTime = attackTime.Rand();
        limit = staminaLimit.Rand() * creature.MaxStamina;
        creature.SetStatus(CreatureStatus.Attack);
        return true;
    }

    override public bool UpdatePattern()
    {
        if (creature == null || creature.Status == CreatureStatus.Tired) return false;
        patternTime -= UnityEngine.Time.deltaTime;
        if (patternTime <= 0.0f) return !DoAttack();
        if (creature.Stamina <= limit) return true;
        return false;
    }

    private bool DoAttack()
    {
        creature.PerformAttack();
        if (--attackCount <= 0)
        {
            return false;
        }

        patternTime = attackTime.Rand();
        return true;
    }

    override public CreatureStatus PatternStatus() { return CreatureStatus.Attack; }
}
