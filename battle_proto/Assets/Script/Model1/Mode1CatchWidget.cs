﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mode1CatchWidget : MonoBehaviour
{
    public List<GameObject> Catch;
    public List<GameObject> Unknown;
    public Text AbilityName;
    public Mode1Energy Energy;
    public Mode1Stamina Stamina;
    public Mode1StatusInfo Status;
    public float AbilityShowTime = 2.0f;

    private Mode1Catch creature = null;
    private float showTime = 0.0f;

	public void Restart()
    {
        HideAll();
        gameObject.SetActive(false);
	}    
	
	// Update is called once per frame
	void Update ()
    {
        if (!creature.IsPerforming)
        {
            if (showTime > 0.0f)
            {
                showTime -= UnityEngine.Time.deltaTime;
                if (showTime <= 0.0f)
                    AbilityName.text = "";
            }
        }
	}

    public void SelectCatch(int index, bool isUnknown)
    {
        HideAll();
        if (isUnknown)
            Unknown[index].SetActive(true);
        else
            Catch[index].SetActive(true);
    }

    public void BeginBattle()
    {
        creature = Mode1Game.Inst.Catch;
        gameObject.SetActive(true);
        Energy.AssignCreature(creature);
        Stamina.AssignCreature(creature);
        Status.AssignCreature(creature);
    }

    public void AbilityUsed(string abilityName)
    {
        AbilityName.text = abilityName;
        showTime = creature.IsPerforming ? 0.01f : AbilityShowTime;
    }

    private void HideAll()
    {
        for (int i = 0; i < Catch.Count; ++i)
            Catch[i].SetActive(false);
        for (int i = 0; i < Unknown.Count; ++i)
            Unknown[i].SetActive(false);
        AbilityName.text = "";
    }
}
