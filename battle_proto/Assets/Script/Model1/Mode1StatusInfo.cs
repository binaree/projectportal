﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mode1StatusInfo : MonoBehaviour
{
    public List<GameObject> Status;
    public Slider AttackMeter;

    private int statusIndex = -1;
    private Mode1Creature creature = null;

    // Use this for initialization
    void Awake ()
    {
        HideAll();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (creature == null) return;
        if (statusIndex != (int)creature.Status)
            ShowStatus(creature.Status);

        if(AttackMeter != null)
            AttackMeter.value = creature.AttackLevel;
    }

    public void AssignCreature(Mode1Creature cr)
    {
        gameObject.SetActive(true);
        creature = cr;
        ShowStatus(creature.Status);
        if (AttackMeter != null)
            AttackMeter.value = creature.AttackLevel;
    }

    public void ShowStatus(CreatureStatus st)
    {
        HideAll();
        statusIndex = (int)st;
        Status[statusIndex].SetActive(true);
    }

    private void HideAll()
    {
        for (int i = 0; i < Status.Count; ++i)
            Status[i].SetActive(false);
        statusIndex = -1;
    }
}
