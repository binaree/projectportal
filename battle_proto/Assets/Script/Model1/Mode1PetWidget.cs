﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mode1PetWidget : MonoBehaviour
{
    public List<GameObject> Pet;
    public Mode1Ping Ping;
    public Mode1Energy Energy;
    public Mode1Stamina Stamina;
    public Mode1StatusInfo Status;
    public PortalWidget Portal;

    public GameObject NotReady;
    public GameObject SelectPanel;
    public GameObject AbilityActive;
    public GameObject AbilityPanel;
    public Text AbilityName;
    public Slider AbilityCharge;
    public GameObject DeployPanel;
    public float AbilityShowTime = 2.0f;

    private bool hasDeployed = false;
    private Mode1Pet creature = null;
    private float showTime = 0.0f;

    public void Restart()
    {
        HideAll(false);
        NotReady.SetActive(true);
        hasDeployed = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(AbilityPanel.activeSelf)
        {
            if (creature.IsPerforming)
            {
                AbilityCharge.value = creature.PerformTimeLevel;
                AbilityActive.SetActive(false);
            }
            else
            {
                AbilityCharge.value = creature.AbilityTimeLevel;
                AbilityActive.SetActive(creature.CanUseAbility);

                if (showTime > 0.0f)
                {
                    showTime -= UnityEngine.Time.deltaTime;
                    if (showTime <= 0.0f)
                        AbilityName.text = "";
                }
            }            
        }
    }

    public void SelectMain()
    {
        if (!hasDeployed)
        {
            DeployPanel.SetActive(false);
            SelectPanel.SetActive(true);
        }
        else if (AbilityActive.activeSelf)
        {
            if (creature.ProcessAbility())
            {
                AbilityActive.SetActive(false);
            }
        }
    }

    public void SelectPet(int index)
    {
        HideAll(false);
        Pet[index].SetActive(true);
        Mode1Game.Inst.SelectPet(index);
        DeployPanel.SetActive(true);
    }

    public void SelectDeploy()
    {
        HideAll(true);
        Ping.gameObject.SetActive(true);
        Ping.ShowPing(0.0f);
        Mode1Game.Inst.Pet.Deploy(Portal);
        hasDeployed = true;
    }

    public void BeginBattle()
    {
        HideAll(true);
        creature = Mode1Game.Inst.Pet;
        Energy.AssignCreature(creature);
        Stamina.AssignCreature(creature);
        Status.AssignCreature(creature);
        AbilityPanel.SetActive(true);
        AbilityCharge.value = 0.0f;
    }

    public void AbilityUsed(string abilityName)
    {
        AbilityName.text = abilityName;
        showTime = creature.IsPerforming ? 0.01f : AbilityShowTime;
    }

    private void HideAll(bool keepPet)
    {
        if (!keepPet)
        {
            for (int i = 0; i < Pet.Count; ++i)
                Pet[i].SetActive(false);
        }
        Ping.gameObject.SetActive(false);
        Energy.gameObject.SetActive(false);
        Stamina.gameObject.SetActive(false);
        Status.gameObject.SetActive(false);
        NotReady.gameObject.SetActive(false);
        SelectPanel.gameObject.SetActive(false);
        AbilityActive.gameObject.SetActive(false);
        DeployPanel.gameObject.SetActive(false);
        AbilityPanel.SetActive(false);
        AbilityName.text = "";
    }
}
