﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mode1CPattern : MonoBehaviour
{
    protected Mode1Catch creature = null;

    virtual public bool StartPattern(Mode1Catch cat)
    {
        creature = cat;
        return false;
    }

    virtual public bool UpdatePattern()
    {
        return true;
    }

    virtual public CreatureStatus PatternStatus() { return CreatureStatus.Warn; }
}
