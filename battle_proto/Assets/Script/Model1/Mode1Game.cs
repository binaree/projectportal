﻿using Binaree.Game.Ui;
using Binaree.Game.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Mode1Game : MonoBehaviour
{
    public PortalWidget Portal;
    public UiDeployWidget Deploy;
    public Mode1PetWidget PetWidget;
    public Mode1CatchWidget CatchWidget;

    public List<Mode1Pet> AllPet;
    public List<Mode1Catch> AllCatch;

    public GameObject VictoryPanel;
    public GameObject DefeatPanel;

    public string AttractTime = "0.5:1";
    public List<float> AttractCatch;
    public List<float> AttractDepth;
    public float DefendHoldTime = 0.1f;
    public float devicePowerScale = 0.05f;

    public Mode1Pet Pet { get; private set; }
    public Mode1Catch Catch { get; private set; }

    public bool GameOver { get { return isOver; } }
    public bool BattleStarted { get; private set; }
    public float AppScale { get; private set; }

    private bool isOver = false;
    private SplitF attractTime;
    private float curAttractTime = 0.0f;

    private bool isDeploy = false;
    private bool inputDown = false;
    private float inputTime = 0.0f;

    private static Mode1Game _instance = null;

    public static Mode1Game Inst { get { return _instance; } }

	// Use this for initialization
	void Awake ()
    {
        _instance = this;
        attractTime = new SplitF(AttractTime);
        Restart();

        AppScale = 1.0f;
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            AppScale = devicePowerScale;

    }

    public void Restart()
    {
        Catch = null;
        Portal.Restart();
        PetWidget.Restart();
        CatchWidget.Restart();
        VictoryPanel.SetActive(false);
        DefeatPanel.SetActive(false);
        if (Deploy != null) Deploy.gameObject.SetActive(false);
        curAttractTime = inputTime = 0.0f;
        isOver = BattleStarted = inputDown = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (isOver) return;

        if (curAttractTime > 0)
        {
            curAttractTime -= UnityEngine.Time.deltaTime;
            if (curAttractTime <= 0.0f)
            {
                if( Deploy != null)
                {
                    for(int i = AttractDepth.Count - 1; i >= 0; --i)
                    {
                        if (Deploy.CurrentPosition() < AttractDepth[i]) continue;
                        SelectCatch(i);
                        break;
                    }
                }
                else
                {
                    for (int i = AttractCatch.Count - 1; i >= 0; --i)
                    {
                        if (UnityEngine.Random.value > AttractCatch[i]) continue;
                        SelectCatch(i);
                        break;
                    }
                }
            }
        }

        if( inputDown )
        {
            inputTime += UnityEngine.Time.deltaTime;
            if (inputTime >= DefendHoldTime)
                Pet.ProcessDefend();
        }
	}

    public void SelectPet(int index)
    {
        if (Pet != null)
            Pet.StopUse();
        Pet = AllPet[index];
        Pet.Selected(index);
    }

    public void SelectCatch(int index)
    {
        Catch = AllCatch[index];
        Catch.Selected(index);
        CatchWidget.SelectCatch(index, Catch.Unknown);
    }

    public void SelectClose()
    {
        SceneManager.LoadScene(0);
    }

    public void SelectContinue()
    {
        Restart();
    }

    public void ScreenTap()
    {
        if (isOver) return;
        if (isDeploy) { StartAttract(); return; }
        if (Catch == null) return;
        if (!Catch.IsEngaged()) Catch.ProcessHit();
        else
        {
            inputDown = true;
            inputTime = 0.0f;            
        }
    }

    public void ScreenRelease()
    {
        if (isOver) return;
        if (!inputDown) return;
        if (inputTime < DefendHoldTime)
            Pet.ProcessAttack();
        else
            Pet.ProcessNoAction();
        inputTime = 0.0f;
        inputDown = false;
    }

    public void StartDeploy()
    {
        if (Deploy != null)
        {
            Deploy.gameObject.SetActive(true);
            isDeploy = true;
        }
        else
            StartAttract();
    }

    public void StartAttract()
    {
        if (Deploy != null) Deploy.gameObject.SetActive(false);
        Portal.ShowPet(Pet.PetIndex());
        curAttractTime = attractTime.Rand();
        isDeploy = false;
    }

    public void StartBattle(bool isSurprise, bool isMissed)
    {
        Pet.StartBattle(isMissed, Catch);
        Catch.StartBattle(isSurprise, Pet);
        PetWidget.BeginBattle();
        CatchWidget.BeginBattle();
        BattleStarted = true;
    }

    public void FinishBattle(bool isDefeat)
    {
        isOver = true;
        BattleStarted = false;
        VictoryPanel.SetActive(!isDefeat);
        DefeatPanel.SetActive(isDefeat);
        if (!isDefeat)
            Catch.Caught();
        Catch.StopUse();
        Catch = null;
    }
}
