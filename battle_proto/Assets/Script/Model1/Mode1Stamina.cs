﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mode1Stamina : MonoBehaviour
{
    public Slider Meter;
    public GameObject Warn;
    public float WarnAmount = 0.1f;

    private Mode1Creature creature = null;

    // Use this for initialization
    void Awake()
    {
        Meter.value = 0.0f;
        Warn.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (creature == null) return;
        Warn.SetActive(creature.StaminaLevel < WarnAmount);
        Meter.value = creature.StaminaLevel;
    }

    public void AssignCreature(Mode1Creature cr)
    {
        gameObject.SetActive(true);
        creature = cr;
        Warn.SetActive(creature.StaminaLevel < WarnAmount);
        Meter.value = creature.StaminaLevel;
    }
}
