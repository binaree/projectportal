﻿using Binaree.Game.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mode2CWarn : Mode2CPattern
{
    public string WarnTime = "3:5";

    private SplitF warnTime;

    private float patternTime = 0.0f;

    private void Awake()
    {
        warnTime = new SplitF(WarnTime);
    }

    override public bool StartPattern(Mode2Catch cat)
    {
        base.StartPattern(cat);
        patternTime = warnTime.Rand();
        creature.SetStatus(PatternStatus());
        return true;
    }

    override public bool UpdatePattern()
    {
        if (creature == null || !creature.CanAct) return false;
        patternTime -= UnityEngine.Time.deltaTime;
        if (patternTime <= 0.0f) return true;
        return false;
    }

    override public Mode2Creature.DefenseMode PatternStatus() { return Mode2Creature.DefenseMode.Warn; }
}
