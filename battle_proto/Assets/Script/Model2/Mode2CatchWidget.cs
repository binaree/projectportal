﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mode2CatchWidget : MonoBehaviour
{
    public List<GameObject> Catch;
    public Text ActionName;
    public Mode2Energy Energy;
    public Mode2Stamina Stamina;
    public Mode2Attack Attack;
    public Mode2StatusInfo Status;
    public float AbilityShowTime = 2.0f;

    private Mode2Catch creature = null;
    private float showTime = 0.0f;

	public void Restart()
    {
        HideAll();
        gameObject.SetActive(false);
	}    
	
	// Update is called once per frame
	void Update ()
    {
        if (!creature.IsPerforming)
        {
            if (showTime > 0.0f)
            {
                showTime -= UnityEngine.Time.deltaTime;
                if (showTime <= 0.0f)
                    ActionName.text = "";
            }
        }
	}

    public void SelectCatch(int index, bool isUnknown)
    {
        HideAll();
        Catch[index].SetActive(true);
    }

    public void BeginBattle()
    {
        creature = Mode2Game.Inst.Catch;
        if(creature.Defense != Mode2Creature.DefenseMode.Run) gameObject.SetActive(true);
        Energy.AssignCreature(creature);
        Stamina.AssignCreature(creature);
        Attack.AssignCreature(creature);
        Status.AssignCreature(creature);
    }

    public void AbilityUsed(string abilityName)
    {
        ActionName.text = abilityName;
        showTime = creature.IsPerforming ? 0.01f : AbilityShowTime;
    }

    private void HideAll()
    {
        for (int i = 0; i < Catch.Count; ++i)
            Catch[i].SetActive(false);
        ActionName.text = "";
    }
}
