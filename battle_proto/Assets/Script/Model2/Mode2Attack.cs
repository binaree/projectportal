﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mode2Attack : MonoBehaviour
{
    public Slider Meter;
    public GameObject Ready;

    private Mode2Creature creature = null;

	// Use this for initialization
	void Awake ()
    {
        Meter.value = 0.0f;
        Ready.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
        RefreshValues();
	}

    public void AssignCreature(Mode2Creature cr)
    {
        gameObject.SetActive(true);
        creature = cr;
        RefreshValues();
    }

    private void RefreshValues()
    {
        if (creature == null) return;
        Ready.SetActive(creature.CanUseAttack);
        Meter.value = creature.AttackTimeLevel;
    }
}
