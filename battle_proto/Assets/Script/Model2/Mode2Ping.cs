﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mode2Ping : MonoBehaviour
{
    public Slider Meter;
    public GameObject Warn;
    public float WarnAmount = 0.95f;

    public PortalWidget Portal;

	// Use this for initialization
	void Awake ()
    {
        Meter.value = 0.0f;
        Warn.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void ShowPing(float amt)
    {
        Meter.value = amt;
        Warn.gameObject.SetActive(amt >= WarnAmount);
        Portal.ShowHit(amt);
    }
}
