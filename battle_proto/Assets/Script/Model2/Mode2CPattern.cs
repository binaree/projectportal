﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mode2CPattern : MonoBehaviour
{
    protected Mode2Catch creature = null;

    virtual public bool StartPattern(Mode2Catch cat)
    {
        creature = cat;
        return false;
    }

    virtual public bool UpdatePattern()
    {
        return true;
    }

    virtual public Mode2Creature.DefenseMode PatternStatus() { return Mode2Creature.DefenseMode.Warn; }
}
