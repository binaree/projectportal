﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mode2RunWidget : MonoBehaviour
{
    public GameObject DivePanel;

    public GameObject GoPanel;
    public Slider Depth;
    public Text DepthLabel;
    public List<GameObject> Warning;

    public PortalWidget Portal;
    public EngageWidget Engage;

    private bool isGo = false;
    private Mode2Pet pet;
    private Mode2Catch creature;

	public void Restart()
    {
        HideAll();
        gameObject.SetActive(false);
        isGo = false;
        pet = null;
        creature = null;
	}    
	
	// Update is called once per frame
	void Update ()
    {
        if (!isGo) return;
        Depth.value = creature.DepthLevel;
        DepthLabel.text = ((1.0f - creature.DepthLevel) * creature.EscapeDepth).ToString("0.##") + "m";
        HideWarnings();
        int depth = pet.DepthType(creature.DepthLevel);
        if (depth >= Warning.Count)
            Warning[Warning.Count - 1].SetActive(true);
        else
            Warning[depth].SetActive(true);
	}

    public void ScreenTap()
    {
        if(DivePanel.activeSelf)
            StartCatch();
        else
            creature.AdjustRunDepth(-(pet.RunSpeed * pet.AppScale()));
    }

    public void BeginRun(bool isInitial)
    {
        HideAll();
        gameObject.SetActive(true);
        DivePanel.SetActive(true);
        pet = Mode2Game.Inst.Pet;
        creature = Mode2Game.Inst.Catch;
        Portal.ShowCatch(creature.CatchIndex(), false);
        Engage.HideCatch();
        if (isInitial)
            StartCatch();
        Mode2Game.Inst.ShowBattlePanels(false);
    }

    private void StartCatch()
    {
        HideAll();
        GoPanel.SetActive(true);
        Portal.Zoom(true);
        Portal.ShowPet(pet.PetIndex());
        Engage.HidePet();
        isGo = true;
    }

    public void RunComplete()
    {
        Portal.Zoom(false);
        Portal.HideAll();
        Engage.ShowPet(Mode2Game.Inst.Pet.PetIndex());
        Engage.ShowCatch(Mode2Game.Inst.Catch.CatchIndex());
        Mode2Game.Inst.ShowBattlePanels(true);
        Restart();
    }

    private void HideAll()
    {
        DivePanel.SetActive(false);
        GoPanel.SetActive(false);
        Depth.value = 0.0f;
        HideWarnings();
    }

    public void HideWarnings()
    {
        for (int i = 0; i < Warning.Count; ++i)
            Warning[i].SetActive(false);
    }
}
