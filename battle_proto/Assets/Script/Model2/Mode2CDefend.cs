﻿using Binaree.Game.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mode2CDefend : Mode2CPattern
{
    public string DefendTime = "2:4";
    public string StaminaLimit = "0.25";

    private SplitF defendTime;
    private SplitF staminaLimit;

    private float patternTime = 0.0f;
    private float limit = 0.0f;

    private void Awake()
    {
        defendTime = new SplitF(DefendTime);
        staminaLimit = new SplitF(StaminaLimit);
    }

    override public bool StartPattern(Mode2Catch cat)
    {
        base.StartPattern(cat);
        patternTime = defendTime.Rand();
        limit = staminaLimit.Rand() * creature.MaxStamina;
        creature.SetStatus(PatternStatus());
        return true;
    }

    override public bool UpdatePattern()
    {
        if (creature == null || !creature.CanAct) return false;
        patternTime -= UnityEngine.Time.deltaTime;
        if (patternTime <= 0.0f) return true;
        if (creature.Stamina <= limit) return true;
        return false;
    }

    override public Mode2Creature.DefenseMode PatternStatus() { return Mode2Creature.DefenseMode.Defend; }
}
