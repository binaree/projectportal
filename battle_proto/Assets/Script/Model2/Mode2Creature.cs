﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mode2Creature : MonoBehaviour
{
    private static readonly string[] ABILITY_NAME = { "Restore", "Attack Up!", "Surge!", "Energized" };

    public enum DefenseMode
    {
        Charge = 0,
        Defend = 1,
        Tired = 2,
        Run = 3,
        Warn = 4,
    }

    public float MaxEnergy = 100.0f;
    public float MaxStamina = 100.0f;

    public float AttackPower = 1.0f;
    public float AttackCooldown = 5.0f;
    public float AttackStaminaCost = 1.0f;

    public float DefensePower = 1.0f;
    public float DefenseStaminaDrain = 0.5f;
    
    public float StandardEnergyRecover = 0.0f;
    public float StandardStaminaRecover = 1.0f;
    public float StandardAbilityTime = 1.0f;
    public float ChargeEnergyRecover = 0.2f;
    public float ChargeStaminaRecover = 3.0f;
    public float ChargeAbilityTime = 2.0f;
    public float TiredEnergyRecover = 0.0f;
    public float TiredStaminaRecover = 1.0f;
    public float TiredAbilityTime = 0.5f;
    public float TiredRecoverery = 1.0f;
    public float SurpriseStamina = 0.0f;
    public float RunSurpriseStamina = 0.0f;
    public float RunSurpriseEnergy = 0.2f;

    public CreatureAbility Ability = CreatureAbility.Surge;
    public float AbilityChargeTime = 60.0f;
    public float AbilityValue = 0.0f;
    public float AbilityStaminaCost = 0.0f;
    public float AbilityDuration = 0.0f;

    public float RunSpeed = 1.0f;

    public DefenseMode Defense { get; protected set; }
    public Mode2Creature Opponent { get; protected set; }
    
    public float Energy { get; protected set; }
    public float EnergyLevel { get { return Energy / MaxEnergy; } }
    public float Stamina { get; private set; }
    public float StaminaLevel { get { return Stamina / MaxStamina; } }
    
    public float AttackTime { get; protected set; }
    public float AttackTimeLevel { get { return AttackTime / AttackCooldown; } }
    public bool CanUseAttack { get { return AttackTime <= 0.0f; } }
    public bool CanAct { get { return Defense != DefenseMode.Tired && Defense != DefenseMode.Run; } }

    public string AbilityName { get { return ABILITY_NAME[(int)Ability]; } }
    public float AbilityTime { get; protected set; }
    public float PerformTime { get; protected set; }
    public float AbilityTimeLevel { get { return 1.0f - (AbilityTime / AbilityChargeTime); } }
    public float PerformTimeLevel { get { return 1.0f - (PerformTime / AbilityDuration); } }
    public bool CanUseAbility { get { return AbilityTime >= AbilityChargeTime; } }
    public bool IsPerforming { get { return PerformTime > 0.0f; } }

    protected bool inUse = false;
    protected float attackMod = 1.0f;
    protected float staminaMod = 1.0f;
    protected float runDepth = 0.0f;

    private void Awake()
    {
        Initialize();
    }

    private void Update()
    {
        if (Mode2Game.Inst.GameOver) return;
        Upkeep();
    }

    virtual public float AppScale() { return 1.0f; }

    virtual protected void Initialize()
    {
        Energy = MaxEnergy;
        Stamina = AttackTime = AbilityTime = PerformTime = 0.0f;
        attackMod = staminaMod = 1.0f;
    }

    virtual protected bool Upkeep()
    {
        if (!Mode2Game.Inst.BattleStarted) return false;
        if (!inUse) return false;
        UpdateEnergy();
        UpdateStamina();
        UpdateAttack();
        UpdateAbility();
        return true;
    }

    virtual public void StopUse()
    {
        inUse = false;
        Initialize();
    }

    virtual public void StartBattle(bool surprised, Mode2Creature against)
    {
        Defense = DefenseMode.Charge;
        Opponent = against;
        Energy = MaxEnergy;
        AttackTime = AbilityTime = Stamina = PerformTime = 0.0f;
        attackMod = staminaMod = 1.0f;
        AdjustStamina((surprised) ? (MaxStamina * SurpriseStamina) : MaxStamina);
    }

    virtual protected void UpdateEnergy()
    {
        if (Energy >= MaxEnergy) return;
        float upAmt = StandardEnergyRecover;
        if (Defense == DefenseMode.Charge) upAmt = ChargeEnergyRecover;
        else if (Defense == DefenseMode.Tired) upAmt = TiredEnergyRecover;
        AdjustEnergy( UnityEngine.Time.deltaTime * upAmt );
    }

    virtual protected void UpdateStamina()
    {
        float upAmt = StandardStaminaRecover;
        if (Defense == DefenseMode.Charge) upAmt = ChargeStaminaRecover;
        else if (Defense == DefenseMode.Tired) upAmt = TiredStaminaRecover;
        else if (Defense == DefenseMode.Defend) upAmt = -(DefenseStaminaDrain * staminaMod);
        AdjustStamina( UnityEngine.Time.deltaTime * upAmt );        
    }

    virtual protected void UpdateAttack()
    {
        if (AttackTime <= 0.0f) return;
        float upAmt = StandardAbilityTime;
        if (Defense == DefenseMode.Charge) upAmt = ChargeAbilityTime;
        else if (Defense == DefenseMode.Tired) upAmt = TiredAbilityTime;
        AttackTime -= UnityEngine.Time.deltaTime * upAmt;
        if (AttackTime <= 0.0f)
            AttackReady();
    }

    virtual protected void UpdateAbility()
    {
        if (PerformTime > 0.0f)
        {
            PerformTime -= UnityEngine.Time.deltaTime;
            if (PerformTime <= 0.0f)
                AbilityPerformComplete();
            return;
        }

        if (AbilityTime >= AbilityChargeTime) return;
        float upAmt = StandardAbilityTime;
        if (Defense == DefenseMode.Charge) upAmt = ChargeAbilityTime;
        else if (Defense == DefenseMode.Tired) upAmt = TiredAbilityTime;
        AbilityTime += UnityEngine.Time.deltaTime * upAmt;
        if (AbilityTime >= AbilityChargeTime)
            AbilityReady();
    }

    virtual protected void AdjustEnergy(float amount)
    {
        Energy += amount;
        if (Energy > MaxEnergy) Energy = MaxEnergy;
        else if (Energy <= 0.0f)
        {
            Mode2Game.Inst.FinishBattle(this is Mode2Pet);
            Energy = 0.0f;
        }
    }

    virtual protected void AdjustStamina(float amount)
    {
        Stamina += amount;
        if (Stamina > MaxStamina) Stamina = MaxStamina;
        CheckForTired();
    }

    virtual protected void CheckForTired()
    {
        if(Defense == DefenseMode.Tired)
        {
            if (Stamina >= (TiredRecoverery * MaxStamina))
                RecoverFromTired();
        }

        if (Stamina > 0.0f) return;
        Stamina = 0.0f;
        Defense = DefenseMode.Tired;
    }

    virtual protected void RecoverFromTired()
    {
        Defense = DefenseMode.Charge;
    }

    virtual protected void PerformAttack(float usePower, float staminaCost)
    {
        if (!CanAct) return;

        AttackTime = AttackCooldown;
        
        staminaCost *= staminaMod;
        if(Stamina < staminaCost)
        {
            AdjustStamina(-staminaCost);
            return;
        }

        AdjustStamina(-staminaCost);
        usePower *= attackMod;
        if(usePower > 0.0f)
            Opponent.DoDamage(usePower, AppScale());
    }

    virtual public void DoDamage(float attackPower, float scaleFinal)
    {
        if(Defense == DefenseMode.Defend)
        {
            attackPower -= DefensePower;
            if (attackPower <= 0.0f) return;
        }
        attackPower *= scaleFinal;
        AdjustEnergy(-attackPower);
    }

    virtual protected void StartCharge()
    {
        Defense = DefenseMode.Charge;
    }

    virtual protected void StartDefense()
    {
        Defense = DefenseMode.Defend;
    }

    virtual protected void AttackReady()
    {

    }

    virtual protected void AbilityReady()
    {

    }

    virtual protected void PerformAbility()
    {
        if (!CanAct) return;

        if (Stamina < AbilityStaminaCost)
        {
            AdjustStamina(-AbilityStaminaCost);
            return;
        }

        if(Ability == CreatureAbility.Restore)
        {
            AdjustEnergy(AbilityValue);
            AbilityPerformComplete();
        }
        else if (Ability == CreatureAbility.AttackUp)
        {
            PerformTime = AbilityDuration;
            attackMod = AbilityValue;
        }
        else if (Ability == CreatureAbility.Energize)
        {
            PerformTime = AbilityDuration;
            staminaMod = AbilityValue;
        }
        else if (Ability == CreatureAbility.Surge)
        {
            PerformAttack(Stamina * AbilityValue, 0);
            AbilityPerformComplete();
        }

        AdjustStamina(-AbilityStaminaCost);
    }

    virtual protected void AbilityPerformComplete()
    {
        PerformTime = 0.0f;
        AbilityTime = 0.0f;
    }

    virtual public void ProcessRun()
    {
        runDepth = 0.0f;
        Defense = DefenseMode.Run;
    }

    virtual public void EndRun(bool surprised)
    {
        Defense = DefenseMode.Charge;
        if (surprised)
        {
            AdjustStamina(-(RunSurpriseStamina * MaxStamina));
            AdjustEnergy(-(RunSurpriseEnergy * MaxEnergy));
        }
    }
}
