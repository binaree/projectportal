﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mode2StatusInfo : MonoBehaviour
{
    public List<GameObject> Status;

    private int statusIndex = -1;
    private Mode2Creature creature = null;

    // Use this for initialization
    void Awake ()
    {
        HideAll();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (creature == null) return;
        if (statusIndex != (int)creature.Defense)
            ShowStatus(creature.Defense);
    }

    public void AssignCreature(Mode2Creature cr)
    {
        gameObject.SetActive(true);
        creature = cr;
        ShowStatus(creature.Defense);
    }

    public void ShowStatus(Mode2Creature.DefenseMode st)
    {
        HideAll();
        statusIndex = (int)st;
        Status[statusIndex].SetActive(true);
    }

    private void HideAll()
    {
        for (int i = 0; i < Status.Count; ++i)
            Status[i].SetActive(false);
        statusIndex = -1;
    }
}
