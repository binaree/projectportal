﻿using Binaree.Game.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Mode2Pet : Mode2Creature
{
    public float PingAmp = 1.0f;
    public float PingDuration = 1.0f;
    public float MissRecover = 0.5f;

    public float Perception = 1.0f;

    public float TapStaminaRecover = 0.01f;
    public List<float> RunLevel;

    private int petIndex = 0;

    override public float AppScale() { return Mode2Game.Inst.AppScale; }
    public int PetIndex() { return petIndex; }

    override protected bool Upkeep()
    {
        if ( !base.Upkeep() ) return false;                
        
        return true;
    }

    public void Selected(int index)
    {
        petIndex = index;
        inUse = true;
    }

    public void Deploy(PortalWidget portal)
    {
        Mode2Game.Inst.StartDeploy();
    }

    override public void StartBattle(bool surprised, Mode2Creature against)
    {
        base.StartBattle(surprised, against);
    }

    override protected void AttackReady()
    {
        base.AttackReady();
    }

    override protected void AbilityReady()
    {
        base.AbilityReady();
    }

    public void ProcessTap()
    {
        if (Defense == DefenseMode.Tired)
        {
            AdjustStamina(TapStaminaRecover * AppScale());
            return;
        }
    }

    public bool ProcessAttack()
    {
        if (!CanAct) return false;
        if (!CanUseAttack) return false;
        PerformAttack(AttackPower, AttackStaminaCost);
        Mode2Game.Inst.PetWidget.ActionUsed("Attack!");
        return true;
    }

    public void ProcessDefend()
    {
        if (Defense == DefenseMode.Defend) return;
        if (!CanAct) return;
        StartDefense();
    }

    public void ProcessNoAction()
    {
        if (!CanAct) return;
        StartCharge();
    }

    public bool ProcessAbility()
    {
        if (!CanAct) return false;
        if (!CanUseAbility) return false;
        if (Stamina < AbilityStaminaCost) return false;
        PerformAbility();
        return true;
    }

    override protected void PerformAbility()
    {
        base.PerformAbility();
        Mode2Game.Inst.PetWidget.ActionUsed(AbilityName);
    }

    public int DepthType(float depthLevel)
    {
        for (int i = 0; i < RunLevel.Count; ++i)
            if (depthLevel < RunLevel[i])
                return i;
        return 11;
    }
}
