﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mode2Ability : MonoBehaviour
{
    public Slider Meter;
    public GameObject Ready;
    public GameObject InUse;
    public Text Name;

    private Mode2Creature creature = null;

	// Use this for initialization
	void Awake ()
    {
        Meter.value = 0.0f;
        Ready.SetActive(false);
        InUse.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
        RefreshValues();
	}

    public void AssignCreature(Mode2Creature cr)
    {
        gameObject.SetActive(true);
        creature = cr;
        RefreshValues();
    }

    private void RefreshValues()
    {
        if (creature == null) return;
        if (creature.IsPerforming)
        {
            Ready.SetActive(false);
            InUse.SetActive(true);
            Meter.value = creature.PerformTimeLevel;
        }
        else
        {
            InUse.SetActive(false);
            Ready.SetActive(creature.CanUseAbility);
            Meter.value = creature.AbilityTimeLevel;
        }
    }
}
