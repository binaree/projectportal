﻿using Binaree.Game.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mode2Catch : Mode2Creature
{
    public enum State
    {
        None = 0,
        Wait = 1,
        Hit = 2,
        Strike = 3,
        Engage = 4,
    }

    public string NumHits = "1:3";
    public string HitChance = "0.25:0.75";
    public string HitTime = "0.5:1.5";
    public string WaitTime = "0.25:0.75";
    public string StrikeTime = "0.5:1.5";
    public string HoldAttackTime = "0:1";
    public string RunTime = "2:10";
    public string StartDepth = "5:25";    

    public List<Mode2CPattern> Pattern;
    public string WarnTime = "0.25:0.5";
    public List<float> RunEnergyLevel;
    public float AttackPerformStamina = 0.25f;
    public float AbilityPerformStamina = 0.6f;

    public float EscapeDepth = 50.0f;
    
    public Mode2CPattern Active { get { return Pattern[patternIndex]; } }
    public float DepthLevel { get { return 1.0f - (runDepth / EscapeDepth); } }

    private int catchIndex = 0;

    private SplitF numHits;
    private SplitF hitChance;
    private SplitF hitTime;
    private SplitF waitTime;
    private SplitF strikeTime;
    private SplitF warnTime;
    private SplitF holdAttackTime;
    private SplitF runTimeTotal;
    private SplitF startDepth;

    private Mode2Pet pet;
    private State state = State.None;
    private int hits = 0;
    private float chance = 0.0f;
    private float time = 0.0f;
    private float attTime = 0.0f;

    private int patternIndex = 0;
    private float warn = 0.0f;

    private int runIndex = 0;
    private float runTime = 0.0f;

    override protected void Initialize()
    {
        base.Initialize();
        numHits = new SplitF(NumHits);
        hitChance = new SplitF(HitChance);
        hitTime = new SplitF(HitTime);
        waitTime = new SplitF(WaitTime);
        strikeTime = new SplitF(StrikeTime);
        warnTime = new SplitF(WarnTime);
        holdAttackTime = new SplitF(HoldAttackTime);
        runTimeTotal = new SplitF(RunTime);
        startDepth = new SplitF(StartDepth);
    }

    public int CatchIndex() { return catchIndex; }

    override protected bool Upkeep()
    {
        if (!inUse) return false;

		if (time > 0.0f)
        {
            time -= UnityEngine.Time.deltaTime;
            if(time <= 0.0f)
            {
                if (state == State.Wait) DetermineHitAction();
                else if (state == State.Hit) SetHitWait();
                else if (state == State.Strike) Mode2Game.Inst.StartBattle(false, true);
            }
        }

        if (runTime > 0.0f)
        {
            runTime -= UnityEngine.Time.deltaTime;
            if (runTime <= 0.0f)
            {
                RunComplete(false);
            }
            else
            {
                AdjustRunDepth(UnityEngine.Time.deltaTime * RunSpeed);
            }
            return false;
        }

        if (!base.Upkeep()) return false;
        if (ProcessPattern())
        {
            if (Active.UpdatePattern())
                AdvancePattern();
        }
        else if (warn > 0.0f)
        {
            warn -= UnityEngine.Time.deltaTime;
            if(warn <= 0.0f)
            {
                warn = 0.0f;
                if (!CheckForRun(false))
                    PerformAbility();
            }
        }
        return true;
	}

    public bool IsEngaged() { return state == State.Engage; }

    public void Selected(int index)
    {
        catchIndex = index;
        pet = Mode2Game.Inst.Pet;
        hits = (int)numHits.Rand();
        if (hits > 0)
            SetHitWait();
        else
            DetermineHitAction();
        inUse = true;
        runIndex = 0;
    }

    public void ProcessHit()
    {
        bool success = false;
        if (state == State.Strike) success = true;
        else if (state == State.Hit) success = UnityEngine.Random.value <= chance;

        if(success)
        {
            Mode2Game.Inst.StartBattle(state == State.Hit, false);
        }
        else
        {
            if(UnityEngine.Random.value > pet.MissRecover)
                Mode2Game.Inst.StartBattle(false, true);
        }
    }

    override public void StartBattle(bool surprised, Mode2Creature against)
    {
        state = State.Engage;
        base.StartBattle(surprised, against);
        runIndex = 0;
        if (!CheckForRun(true))
        {
            EndRun(false);
        }
    }

    override protected void RecoverFromTired()
    {
        if (patternIndex == -1)
            AdvancePattern();
        else
            Defense = Active.PatternStatus();
    }

    override protected void AttackReady()
    {
        base.AttackReady();
        attTime = holdAttackTime.Rand();
    }

    private void SetHitWait()
    {
        time = waitTime.Rand();
        chance = 0.0f;
        state = State.Wait;
        Mode2Game.Inst.PetWidget.Ping.ShowPing(chance);
    }

    private void DetermineHitAction()
    {
        if(hits-- <= 0)
        {
            time = strikeTime.Rand() * pet.PingDuration;
            chance = 1.0f;
            state = State.Strike;
        }
        else
        {
            time = hitTime.Rand() * pet.PingDuration;
            chance = hitChance.Rand() * pet.PingAmp;
            state = State.Hit;
        }
        Mode2Game.Inst.PetWidget.Ping.ShowPing(chance);
    }

    private void AdvancePattern()
    {
        if (CanRun())
        {
            warn = warnTime.Rand() * (Opponent as Mode2Pet).Perception;
            SetStatus(Mode2Creature.DefenseMode.Warn);
            return;
        }

        if (CanUseAbility && !IsPerforming && StaminaLevel >= AbilityPerformStamina)
        {
            warn = warnTime.Rand() * (Opponent as Mode2Pet).Perception;
            SetStatus(Mode2Creature.DefenseMode.Warn);
            return;
        }

        if (++patternIndex >= Pattern.Count)
            patternIndex = 0;
        
        if (!Active.StartPattern(this))
            AdvancePattern();
    }

    public void SetStatus(Mode2Creature.DefenseMode d)
    {
        Defense = d;
    }

    public void PerformAttack()
    {
        PerformAttack(AttackPower, AttackStaminaCost);
        Mode2Game.Inst.CatchWidget.AbilityUsed("Attack!");
    }

    public bool ProcessPattern()
    {
        if (!CanAct || Defense == Mode2Creature.DefenseMode.Warn) return false;
        if (Defense == DefenseMode.Charge && attTime > 0.0f)
        {
            attTime -= UnityEngine.Time.deltaTime;
            if (attTime <= 0.0f)
            {
                PerformAttack();
                attTime = 0.0f;
            }
        }
        return true;
    }

    override protected void PerformAbility()
    {
        base.PerformAbility();
        Mode2Game.Inst.CatchWidget.AbilityUsed(AbilityName);
        if (CanAct)
            AdvancePattern();
    }

    private bool CanRun()
    {
        if (runIndex >= RunEnergyLevel.Count) return false;
        if (RunEnergyLevel[runIndex] < EnergyLevel) return false;
        return true;
    }

    private bool CheckForRun(bool isBattleStart)
    {
        if (!CanRun()) return false;

        runIndex++;
        ProcessRun();
        Opponent.ProcessRun();
        Mode2Game.Inst.RunWidget.BeginRun(isBattleStart);
        return true;
    }

    override public void ProcessRun()
    {
        runDepth = startDepth.Value(StaminaLevel);
        runTime = runTimeTotal.Value(StaminaLevel);
        SetStatus(DefenseMode.Run);
    }

    public bool AdjustRunDepth(float amt)
    {
        runDepth += amt;
        if (runDepth >= EscapeDepth)
        {
            RunComplete(true);
            return false;
        }
        if (runDepth < 0.0f)
            RunComplete(false);
        return true;
    }

    private void RunComplete(bool isFail)
    {
        runTime = 0.0f;
        if (isFail) { Mode2Game.Inst.EscapeBattle(); return; }
        int runType = (Opponent as Mode2Pet).DepthType(DepthLevel);
        if (runType == 0) { Mode2Game.Inst.EscapeBattle(); return; }
        EndRun(runType > 2);
        Opponent.EndRun(runType < 2);
    }

    override public void EndRun(bool surprised)
    {
        base.EndRun(surprised);
        patternIndex = -1;
        CheckForTired();
        if (Defense != DefenseMode.Tired)
            AdvancePattern();
        attTime = holdAttackTime.Rand();
        Mode2Game.Inst.RunWidget.RunComplete();
    }
}
