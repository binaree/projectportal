﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mode2PetWidget : MonoBehaviour
{
    public List<GameObject> Pet;
    public Mode2Ping Ping;

    public GameObject BattlePanel;
    public Mode2Energy Energy;
    public Mode2Stamina Stamina;
    public Mode2Attack Attack;
    public Mode2Ability Ability;
    public Mode2StatusInfo Status;
    public PortalWidget Portal;

    public GameObject NotReady;
    public GameObject SelectPanel;
    public Text ActionName;
    public GameObject DeployPanel;
    public float AbilityShowTime = 2.0f;

    private bool hasDeployed = false;
    private Mode2Pet creature = null;
    private float showTime = 0.0f;

    public void Restart()
    {
        gameObject.SetActive(true);
        HideAll(false);
        NotReady.SetActive(true);
        hasDeployed = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(BattlePanel.activeSelf)
        {
            if (showTime > 0.0f)
            {
                showTime -= UnityEngine.Time.deltaTime;
                if (showTime <= 0.0f)
                    ActionName.text = "";
            }         
        }
    }

    public void SelectMain()
    {
        if (!hasDeployed)
        {
            DeployPanel.SetActive(false);
            SelectPanel.SetActive(true);
        }
        else if (Ping.gameObject.activeSelf)
            Mode2Game.Inst.ScreenTap();
    }

    public void SelectPet(int index)
    {
        HideAll(false);
        Pet[index].SetActive(true);
        Mode2Game.Inst.SelectPet(index);
        DeployPanel.SetActive(true);
    }

    public void SelectDeploy()
    {
        HideAll(true);
        Ping.gameObject.SetActive(true);
        Ping.ShowPing(0.0f);
        Mode2Game.Inst.Pet.Deploy(Portal);
        Portal.Zoom(true);
        hasDeployed = true;
    }

    public void SelectAction()
    {
        creature.ProcessAttack();
    }

    public void SelectAbility()
    {
        creature.ProcessAbility();
    }

    public void BeginBattle()
    {
        HideAll(true);
        creature = Mode2Game.Inst.Pet;
        Energy.AssignCreature(creature);
        Stamina.AssignCreature(creature);
        Attack.AssignCreature(creature);
        Ability.AssignCreature(creature);
        Status.AssignCreature(creature);
        BattlePanel.SetActive(true);
    }

    public void ActionUsed(string actionName)
    {
        ActionName.text = actionName;
        showTime = creature.IsPerforming ? 0.01f : AbilityShowTime;
    }

    private void HideAll(bool keepPet)
    {
        if (!keepPet)
        {
            for (int i = 0; i < Pet.Count; ++i)
                Pet[i].SetActive(false);
        }
        Ping.gameObject.SetActive(false);
        BattlePanel.SetActive(false);
        NotReady.gameObject.SetActive(false);
        SelectPanel.gameObject.SetActive(false);
        DeployPanel.gameObject.SetActive(false);
        ActionName.text = "";
    }
}
