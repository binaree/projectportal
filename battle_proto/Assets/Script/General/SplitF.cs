﻿namespace Binaree.Game.Utility
{
    using System;

    /// <summary>
    /// Takes a string in the format of "min:max" and splits it into two floats
    /// Useful for random values in a range or values that progress such as a level
    /// </summary>
    public class SplitF
    {
        private float[] fSplit = new float[2];

        public SplitF(string val)
        {
            string[] split = val.Split(':');
            if (split.Length == 0)
                fSplit[0] = fSplit[1] = 0;
            else if (split.Length == 1)
                fSplit[0] = fSplit[1] = Convert.ToSingle(split[0]);
            else
            {
                fSplit[0] = Convert.ToSingle(split[0]);
                fSplit[1] = Convert.ToSingle(split[1]);
            }
        }

        public float Value(float percent)
        {
            return fSplit[0] + (percent * (fSplit[1] - fSplit[0]));
        }

        public float Value(int ind, int low, int high)
        {
            float fInd = (ind - low) / (high - low);
            if (fInd < 0.0f) fInd = 0.0f;
            if (fInd > 1.0f) fInd = 1.0f;
            return fSplit[0] + (fInd * (fSplit[1] - fSplit[0]));
        }

        public float Rand()
        {
            return fSplit[0] + (UnityEngine.Random.value * (fSplit[1] - fSplit[0]));
        }

        public float Min() { return fSplit[0]; }
        public float Max() { return fSplit[1]; }

        public void Center()
        {
            float amt = (fSplit[1] - fSplit[0]) / 2.0f;
            fSplit[0] -= amt;
            fSplit[1] -= amt;
        }
    }
}
