﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortalCreature : MonoBehaviour
{
    public enum Style
    {
        Pet = 0,
        Catch = 1,
        CatchUnknown = 2,
    }

    public Style TheStyle = Style.Pet;
    public Image Icon;

    public float MoveBound = 50.0f;
    public float LimitBound = 80.0f;


	// Use this for initialization
	void OnEnable ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
