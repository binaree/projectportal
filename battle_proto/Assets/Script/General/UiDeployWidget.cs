namespace Binaree.Game.Ui
{
    using UnityEngine;
    using UnityEngine.UI;
    using Utility;
    
    public class UiDeployWidget : MonoBehaviour
    {
        public Image Back;
        public GameObject Marker;
        public Text MinDepthLabel;
        public Text MaxDepthLabel;

        public float MinY = -200.0f;
        public float MaxY = 200.0f;
        public float MoveSpeed = 2.0f;

        private float distance = 0.0f;
        private float pos = 0.0f;

        private void Awake()
        {
            
        }

        private void OnEnable()
        {
            Marker.transform.localPosition = new Vector3(Marker.transform.localPosition.x, MinY, Marker.transform.localPosition.z);
            distance = MaxY - MinY;
            pos = 0;
            MinDepthLabel.text = "10m";
            MaxDepthLabel.text = "50m";
        }

        private void Update()
        {
            pos += UnityEngine.Time.deltaTime * MoveSpeed;
            if (pos >= 1.0f)
                pos = 0.0f;

            Marker.transform.localPosition = new Vector3(Marker.transform.localPosition.x, MinY + (distance * pos), Marker.transform.localPosition.z);
        }

        public float CurrentPosition() { return pos; }
    }
}
