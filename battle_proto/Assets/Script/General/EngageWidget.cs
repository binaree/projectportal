﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngageWidget : MonoBehaviour
{
    public List<PortalCreature> Pet;
    public List<PortalCreature> Catch;
    public PortalWidget Portal;

    public PortalCreature ActivePet { get; private set; }
    public PortalCreature ActiveCatch { get; private set; }

    public void Restart()
    {
        HideAll();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowPet(int index)
    {
        if (Pet.Count == 0) return;
        if (ActivePet != null)
            ActivePet.gameObject.SetActive(false);
        ActivePet = Pet[(index < Pet.Count) ? index : 0];
        ActivePet.gameObject.SetActive(true);
    }

    public void HidePet()
    {
        if (ActivePet == null) return;
        ActivePet.gameObject.SetActive(false);
        ActivePet = null;
    }

    public void ShowCatch(int index)
    {
        List<PortalCreature> list = Catch;
        if (list.Count == 0) return;
        if (ActiveCatch != null)
            ActiveCatch.gameObject.SetActive(false);
        ActiveCatch = list[(index < list.Count) ? index : 0];
        ActiveCatch.gameObject.SetActive(true);
    }

    public void HideCatch()
    {
        if (ActiveCatch == null) return;
        ActiveCatch.gameObject.SetActive(false);
        ActiveCatch = null;
    }

    void HideAll()
    {
        for (int i = 0; i < Pet.Count; ++i)
            Pet[i].gameObject.SetActive(false);
        for (int i = 0; i < Catch.Count; ++i)
            Catch[i].gameObject.SetActive(false);
        ActivePet = ActiveCatch = null;
    }
}
