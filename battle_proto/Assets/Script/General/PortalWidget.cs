﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalWidget : MonoBehaviour
{
    public List<PortalCreature> Pet;
    public List<PortalCreature> Catch;
    public List<PortalCreature> Unknown;
    public GameObject Ripple;
    public float ZoomScale = 2.0f;

    public PortalCreature ActivePet { get; private set; }
    public PortalCreature ActiveCatch { get; private set; }

    private Vector3 defaultPosition;

    private void Awake()
    {
        defaultPosition = gameObject.transform.localPosition;
    }

    public void Restart()
    {
        if(defaultPosition == Vector3.zero)
            defaultPosition = gameObject.transform.localPosition;
        HideAll();
        ResetPosition();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowPet(int index)
    {
        if (Pet.Count == 0) return;
        if (ActivePet != null)
            ActivePet.gameObject.SetActive(false);
        ActivePet = Pet[(index < Pet.Count) ? index : 0];
        ActivePet.gameObject.SetActive(true);
    }

    public void ShowCatch(int index, bool isUnknown)
    {
        List<PortalCreature> list = (isUnknown) ? Unknown : Catch;
        if (list.Count == 0) return;
        if (ActiveCatch != null)
            ActiveCatch.gameObject.SetActive(false);
        ActiveCatch = list[(index < list.Count) ? index : 0];
        ActiveCatch.gameObject.SetActive(true);
    }

    public void ShowHit(float amt)
    {
        Ripple.SetActive(amt > 0.1f);
    }

    public void Zoom(bool zoomIn)
    {
        if (zoomIn)
        {
            gameObject.transform.localPosition = Vector3.zero;
            gameObject.transform.localScale = new Vector3(ZoomScale, ZoomScale, ZoomScale);
        }
        else
            ResetPosition();
    }

    public void HideAll()
    {
        for (int i = 0; i < Pet.Count; ++i)
            Pet[i].gameObject.SetActive(false);
        for (int i = 0; i < Catch.Count; ++i)
            Catch[i].gameObject.SetActive(false);
        for (int i = 0; i < Unknown.Count; ++i)
            Unknown[i].gameObject.SetActive(false);
        Ripple.SetActive(false);
        ActivePet = ActiveCatch = null;
    }

    private void ResetPosition()
    {
        gameObject.transform.localPosition = defaultPosition;
        gameObject.transform.localScale = Vector3.one;
    }
}
